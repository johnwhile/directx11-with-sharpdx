﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using Common;
using Common.Fonts;
using Engine;

using SharpDX.Direct3D;
using SharpDX.Direct3D11;

namespace testdevkce
{
    public class DxDevice : DisposableDx
    {
        Device d3device;

        public DxDevice() : base()
        {
            DeviceCreationFlags flag = DeviceCreationFlags.Debug | DeviceCreationFlags.SingleThreaded | DeviceCreationFlags.BgraSupport;
            using (var device = new Device(DriverType.Hardware, flag, FeatureLevel.Level_11_0))
            {
                device.DebugName = "SharpDX.Direct3D11.Device";
                d3device = device.QueryInterface<Device1>();
                d3device.DebugName = "SharpDX.Direct3D11.Device1";
            }
        }

        protected override void Dispose(bool disposemanaged)
        {
            base.Dispose(disposemanaged);
            RemoveAndDispose(ref d3device);
        }
    }



    internal static class Program
    {
        static D3Device device;
        static BlendStateCollector collector;

        static void dosomething()
        {
            var blend1 = collector.CreateOrGet(BlendStateDescription.Default());
        }

        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var form = new Form())
            {
                form.Show();

                 device = new D3Device(GraphicFactory.DefaultAdapter);

                 collector = new BlendStateCollector(device);

                var descr = BlendStateDescription.Default();
                descr.AlphaToCoverageEnable = !descr.AlphaToCoverageEnable;
                var blend2 = collector.CreateOrGet(descr);

                dosomething();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                device.Dispose();

                Application.Run(form);

            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            Console.ReadKey();
        }
    }
}
