﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Engine;
using Engine.Effect;
using Common;
using Common.Maths;
using Common.IO.Wavefront;

namespace GameTest
{
    public class MeshScene : Disposable
    {
        StaticMeshBatchRenderer meshRenderer;
        StaticSplineBatchRenderer splineRenderer;

        EffectShading meshshading;
        EffectSpline splinecolor;

        public Mesh mesh1;
        public Mesh mesh2;
        public Mesh spline1;


        public MeshScene(D3Device device, EffectManager effect_manager)
        {
            var obj = WavefrontObj.Load(AppConfig.ContentPath + "\\Models\\teapot.obj");
            mesh1 = Mesh.ConvertFrom(obj);
            mesh1.Diffuse = Color4b.Blue;
            mesh1.Normals = null;
            mesh1.TexCoords = null;
            mesh1.GenerateNormals();
            
            /*
            mesh1 = MeshPrimitive.Icosahedron();
            mesh1.SubMeshes[0].TessellateAll(3);
            mesh1.Normals = new StructBuffer<Vector3f>(mesh1.VerticesCount);
            mesh1.Diffuse = Color4b.Blue;
            for (int i=0;i< mesh1.VerticesCount;i++)
            {
                mesh1.Normals[i] = mesh1.Vertices[i].Normal;
                mesh1.Vertices[i] = mesh1.Normals[i];
            }
            */

            Debugg.Info($"mesh v:{mesh1.VerticesCount} t:{mesh1.IndicesCount / 3}");


            spline1 = MeshPrimitive.AxisLine();
            spline1.Transform = Matrix4x4f.Scaling(10, 10, 10);

            //mesh2 = mesh1.Copy();
            //mesh2.Diffuse = Color4b.Red;

            meshRenderer = ToDispose(new StaticMeshBatchRenderer(device));
            meshRenderer.Meshes.Add(mesh1);
            meshRenderer.CreateDeviceDependentResources();

            splineRenderer = ToDispose(new StaticSplineBatchRenderer(device));
            splineRenderer.Meshes.Add(spline1);
            splineRenderer.CreateDeviceDependentResources();

            meshshading = ToDispose(new SmoothShading(effect_manager));

            splinecolor = ToDispose(new EffectSpline(effect_manager));
        }

        public void Render(D3Context context, CameraStruct camera)
        {
            EffectAmbient ambient = meshshading.Ambients;
            ambient.View = camera.m_view;
            ambient.Projection = camera.m_proj;
            ambient.ViewProjection = camera.ProjView;
            ambient.CameraPosition = camera.eye;
            meshshading.Ambients = ambient;

            EffectLight light = meshshading.Lights;
            light.LightColor = Color4b.White;
            light.LightPosition = Vector3f.Zero;
            light.LightDirection = -Vector3f.Normalize(Vector3f.UnitY + Vector3f.UnitX);
            meshshading.Lights = light;

            meshshading.SetToContext(context, 0);
            meshRenderer.Draw(context, meshshading);

            splinecolor.SetToContext(context, 0);
            splineRenderer.Draw(context, splinecolor);
        }

        float theta = 0;
        float phi = 0;
        float dir = 1;
        public void Update(GameTime time)
        {
            var pos = mesh1.Transform.Position;

            if (pos.x > 10) dir *= -1;
            if (pos.x < -10) dir *= -1;

            Matrix4x4f move = Matrix4x4f.Translating(dir * (float)time.ElapsedTime * 0.0001f, 0, 0);
            Matrix4x4f rot = Matrix4x4f.RotationY((float)time.ElapsedTime * 0.001f);

            mesh1.Transform = move * mesh1.Transform * rot;
        }
    }
}
