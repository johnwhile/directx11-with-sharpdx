﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Engine;
using Common;
using Common.Maths;
using Engine.Direct2D;
using Common.Gui;
using System.Windows.Forms;
using System.Drawing;
using Common.Inputs;

namespace GameTest
{
    public class Scene2 : Disposable
    {
        D2GuiGraphicRenderer guiRenderer;
        D2GuiSprite cursor;

        GuiManager guiManager;
        GameApplication app;
        AnimatedCursor mainCursor;

        public Scene2(D3Device device, D2Device device2d, GameApplication app)
        {
            this.app = app;

            var atlas = ImageAtlasLayout.Open(@"Graphics\gui.xml");

            var texturemanager = ToDispose(new D2TextureManager(device2d.Context));
            var texture = texturemanager.CreateTexture(atlas.ImageFilename[0], RestoreMode.Immediate);

            //create direct2 renderer for guis
            guiRenderer = ToDispose(new D2GuiGraphicRenderer(device2d.Context));

            //create mouse sprite
            cursor = new D2GuiSprite(guiRenderer);
            cursor.texture = texture;
            atlas.TryGetSource(8, out cursor.source, out _, out _);

            Bitmap bmp = new Bitmap(atlas.ImageFilename[0]);
            var icon = bmp.Clone(cursor.source, bmp.PixelFormat);

            mainCursor = AnimatedCursor.WaitCursor;
            mainCursor.FrameMS = 500;

            //create gui interface and link to base events
            guiManager = new GuiManager();
            
            /*
            app.MouseDown += guiManager.MouseDown;
            app.MouseUp += guiManager.MouseUp;
            app.MouseMove += guiManager.MouseMove;
            app.KeyDown += guiManager.KeyDown;
            */
            //generate first virtual gui control
            guiManager.Root = new D2GuiRoot(guiManager, guiRenderer);
            guiManager.Root.GuiCursor = mainCursor;

  
            D2GuiPanel p1 = new D2GuiPanel(guiManager.Root, guiRenderer);
            p1.Offset = new Vector2i(100, 100);
            p1.Size = new Vector2i(200, 300);
            p1.GuiCursor = mainCursor;

            D2GuiText t1 = new D2GuiText(p1, guiRenderer, "<canc-to-clear>");
            t1.Offset = new Vector2i(20, 20);
            t1.Size = new Vector2i(100, 20);

        }
        public void Update(GameTime time, ViewportClip viewport)
        {
            float ms = (float)time.ElapsedTime;

            mainCursor.UpdateElapsedTime(ms);

            guiManager.Root.Size = viewport.Rectangle.size - new Vector2i(10, 10);
            guiManager.Root.Offset = viewport.Rectangle.position;

        }

        public void Render(D2Context context)
        {
            var mousePosition = guiManager.MousePosition;


            var lastcontrol = guiManager.LastVisitedByMouse;

            if (lastcontrol == null || lastcontrol.GuiCursor == null)
                lastcontrol = guiManager.Root;

            cursor.Draw(mousePosition);

            MouseCursor.SetCursor(lastcontrol.GuiCursor, app.Windows[0].Control);

        }

    }
}
