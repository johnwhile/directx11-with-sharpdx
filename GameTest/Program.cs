﻿using System;

namespace GameTest
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            TestGame.RunGame();
        }
    }
}
