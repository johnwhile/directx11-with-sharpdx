﻿using System;
using System.Windows.Forms;

using Common;
using Common.Maths;
using Engine;

namespace GameTest
{
    public class TestWindow : GameWindow
    {
        public CameraStruct camera;


        public TestWindow(GameApplication application, Control control) : base(application, control)
        {
            camera = new CameraStruct(Vector3f.One * 20, Vector3f.Zero, 1f, 100f, control.Aspect());
            ClientSizeChanged += TestWindow_ClientSizeChanged;

            KeyDown += delegate (KeyEventArgs e)
            {
                if (e.Alt && e.KeyCode == Keys.Enter) SwitchFullscreen();
            };
        }


        private void TestWindow_ClientSizeChanged(Vector2i size)
        {
            camera.aspect = (float)size.width / size.height;
            camera.UpdateProj();
        }
    }
}
