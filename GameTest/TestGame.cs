﻿using Common.Fonts;
using Common.Inputs;
using Common.Maths;
using Common;
using Engine.Direct2D;
using Engine.Effect;
using Engine;
using System;
using System.Windows.Forms;
using System.Drawing;
using SharpDX.DirectWrite;
using SharpDX.DXGI;

namespace GameTest
{
    public class TestGame : GameApplication
    {
        public MultiPanel mainform;

        int draws = 0;
        TestWindow Window1;
        TestWindow Window2;

        EffectManager shaderManager;
        FontSpriteRenderer fontdrawer;
        D2Font d2font;
        MeshScene meshscene;

        public TestGame() : base()
        {
            mainform = new MultiPanel();
            mainform.Size = new Size(800, 600);
            mainform.Show();

            mainform.panel1.BackColor = Color4b.CornflowerBlue;
            mainform.panel2.BackColor = Color4b.Toy;

            Window1 = new TestWindow(this, mainform.panel1);
            Window2 = new TestWindow(this, mainform.panel2);

            Window1.Drawing += Window1Drawing;
            Window2.Drawing += Window1Drawing;

            AddOutputWindow(Window1);
            AddOutputWindow(Window2);

            var form = new Form();
            form.Text = "GameWindow3";
            form.Size = new Size(600, 400);
            form.Show();
            form.BackColor = Color4b.DarkGray;

            var Window3 = new TestWindow(this, form);
            Window3.Drawing += Window1Drawing;
            AddOutputWindow(Window3);
        }


        protected override void Update(GameTime time)
        {
            meshscene.Update(time);
        }


        private void Window1Drawing(SwapChainPresenter presenter)
        {
            var context = Manager.Device3D.Context;
            var stats = context.Stats;
            var viewport = presenter.DefaultViewport;

            meshscene.Render(context, Window1.camera);

            if (Manager.Device2D != null)
            {
                var d2context = Manager.Device2D.Context;

                if (presenter.BeginDraw2D(d2context))
                {
                    WindowMouse.GetPosition(out var mousePosition, Window1.Control.Handle);

                    string vsync = presenter.Description.Vsync ? "on" : "off";

                    d2context.DrawText(d2font, GameTime.PerformanceInfo.ToString() + " vsync: " + vsync, Color4b.Yellow, 5, 10);
                    d2context.DrawText(d2font, gametime.ToString(), Color4b.Black, 5, 10 + 14);
                    d2context.DrawText(d2font, stats.ToString(), Color4b.White, 5, 10 + 14 + 14);


                    /*
                    var screen = Mathelp.ClipSpaceToScreen(clipoint.x, clipoint.y, viewport.Size);

                    d2context.DrawRectangle(new Rectangle4i((int)(screen.x - 1), (int)(screen.y - 1), 3, 3), Color4b.Black);

                    clipoint.z = 0;
                    var point_near = TrackBalCam.camera.ClipToWorldSpace(clipoint);
                    clipoint.z = 1;
                    var point_far = TrackBalCam.camera.ClipToWorldSpace(clipoint);

                    d2context.DrawText(d2font, point_near.ToString(), Color4b.Black, 10, 60);
                    d2context.DrawText(d2font, point_far.ToString(), Color4b.Black, 10, 74);
                    */

                    /*
                    for (int i=0;i< scene1.geometry.Vertices.Count; i++)
                    {
                        var v = Vector3f.TransformCoordinate(scene1.geometry.Vertices[i], scene1.geometry.Transform);

                        var clip = camera.WorldToClipSpace(v);
                        var point = Mathelp.ClipSpaceToScreen(clip.x, clip.y, viewport.Size);

                        d2context.DrawText(d2font, v.ToString(), Color4b.White, (int)point.x, (int)point.y );

                    }
                    */
                }
            }
        }



   

        public static void RunGame()
        {
            using (var game = new TestGame())
            {
                game.TargetElapsedTime = TimeSpan.Zero;
                //game.TargetElapsedTime = TimeSpan.FromMilliseconds(1000.0 / fps);

                game.CreateResources();

                game.RunLoop();
            }
        }


        public void CreateResources()
        {
            var device3d = Manager.Device3D;
            var device2d = Manager.Device2D;

            shaderManager = ToDispose(new EffectManager(device3d, "ShaderManager"));
            meshscene = ToDispose(new MeshScene(device3d, shaderManager));

            fontdrawer = ToDispose(new FontSpriteRenderer(device3d));
            fontdrawer.Font = FontDictionary.GetOrLoad("Arial", AppConfig.ContentPath + "\\Fonts\\Arial.fnt");

            d2font = D2Font.GetFont("Calibri", 14);
        }
    }

}
