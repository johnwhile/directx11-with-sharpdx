﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Common.WinNative;

namespace GameTest
{
    public partial class MultiPanel : Form
    {
        public MultiPanel()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Override windows message loop handling.
        /// </summary>
        protected override void WndProc(ref Message m)
        {
            //Debugg.WriteLine(m.ToString());

            long wparam = m.WParam.ToInt64();
            long lparam = m.LParam.ToInt64();

            switch ((WM)m.Msg)
            {
                case WM.NCHITTEST:
                    {
                        Cursor cur = null;

                        switch ((HT)wparam)
                        {
                            case HT.TOP:
                            case HT.BOTTOM:
                                cur = Cursors.SizeNS; break;
                            case HT.LEFT:
                            case HT.RIGHT:
                                cur = Cursors.SizeWE; break;
                            case HT.BOTTOMLEFT:
                            case HT.TOPRIGHT:
                                cur = Cursors.SizeNESW; break;
                            case HT.BOTTOMRIGHT:
                            case HT.TOPLEFT:
                                cur = Cursors.SizeNWSE;break;
                        }
                        break;
                    }
            }


            base.WndProc(ref m);
        }
    }
}
