﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Engine;
using Common;
using Common.Maths;

namespace GameTest
{
    public class AxisObject : GraphicChild
    {
        Mesh axismesh;
        VertexBuffer vbuffer;
        IndexBuffer ibuffer;

        public readonly InputSlotLayout Layout;


        struct VERTEX
        {
            public Vector3f pos;
            public Vector2f texcoord;
            public Vector4b col;
        };

        public AxisObject(D3Device device, InputSlotLayout layout) : base(device)
        {
            axismesh = MeshPrimitive.AxisLine();
            Layout = layout;

            CreateDeviceDependentResources();
        }

        public void Draw(D3Context context)
        {
            context.SetBuffer(vbuffer);
            context.SetBuffer(ibuffer);
            context.PrimitiveTopology = DxPrimitive.LineList;
            context.DrawIndexed(ibuffer.ElementCount);
        }

        protected  void CreateDeviceDependentResources()
        {
            int vcount = axismesh.VerticesCount;

            var data = new VERTEX[vcount];
            for (int i = 0; i < vcount; i++)
            {
                data[i].pos = axismesh.Vertices[i];
                data[i].texcoord = Vector2f.Zero;
                data[i].col = axismesh.Colors[i];
            }
            RemoveAndDispose(ref vbuffer);
            RemoveAndDispose(ref ibuffer);

            vbuffer = ToDispose(VertexBuffer.Create(device, vcount,  data, 0 , vcount, BufferAccess.Immutable));
            ibuffer = ToDispose(IndexBuffer.Create(Device, axismesh.IndicesCount, axismesh.SubMeshes[0].Indices.ToUint16Array(), 0, -1, BufferAccess.Immutable));
        }

        protected  void CreateSizeDependentResources()
        {

        }
    }
}
