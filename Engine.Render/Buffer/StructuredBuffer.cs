﻿using System;
using System.Runtime.InteropServices;
using Common;
using SharpDX.Direct3D11;

using Buffer = SharpDX.Direct3D11.Buffer;

namespace Engine
{
    /// <summary>
    /// Aim for structures with sizes divisible by 128 bits (sizeof float4)
    /// </summary>
    public class StructuredBuffer : GraphicBuffer
    {
        protected StructuredBuffer(D3Device device, int bytesize, int elementsize, string debugname = null) :
            base(device, bytesize, elementsize, debugname)
        {

        }
        public override void Bind(D3Context context, int offset = 0)
        {
            throw new NotSupportedException();
        }
        /// <summary>
        /// Create a structured buffer.
        /// </summary>
        public static StructuredBuffer Create<T>(D3Device device, T[] data, BufferAccess access = BufferAccess.Immutable, bool isUnorderedAccess = false) where T : unmanaged
        {
            if (access == BufferAccess.Staging) throw new Exception("TO TEST: Resources can't be writable by both CPU and GPU simultaneously!");

            /* from Practical Rendering and Computation with Direct3D 11
            if (!CPUWritable && !GPUWritable)
            {
                desc.BindFlagS = D3D11_BIND_SHADER_RES0URCE;
                desc.Usage = D3D11_USAGE_IMMUTABLE;
                desc.CPUAccessFlags = 0;
            }
            else if (CPUWritable && !GPUWritable)
            {
                desc.BindFlagS = D3D11_BIND_SHADER_RES0URCE;
                desc.Usage = D3D11_USAGE_DYNAMIC;
                desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
            }
            else if (!CPUWritable && GPUWritable)
            {
                desc.BindFlagS = D3D11_BIND_SHADER_RES0URCE |
                D3D11_BIND_UN0RDERED_ACCESS;
                desc.Usage = D3D11_USAGE_DEFAULT;
                desc.CPUAccessFlags = 0;
            }
            else if (CPUWritable && GPUWritable)
            {
                // Handle the error here... 
                // Resources can't be writable by both CPU and GPU simultaneously! 
            }
            */

            int sizeof_t = Marshal.SizeOf<T>();

            if (sizeof_t % 16 != 0)
                Debugg.Warning($"For a better performance of structured buffer aligns the {typeof(T).Name} struct to a 16 bytes");

            if (data == null) throw new ArgumentNullException("data is empty");

            var desc = new BufferDescription()
            {
                SizeInBytes = sizeof_t * data.Length,
                StructureByteStride = sizeof_t,
                BindFlags = BindFlags.ShaderResource,
                CpuAccessFlags = GetCpuAccess(access),
                OptionFlags = ResourceOptionFlags.BufferStructured,
                Usage = GetUsage(access),
            };
            if (isUnorderedAccess && desc.Usage != ResourceUsage.Default) throw new Exception("TO TEST : UnorderedAccessView require ResourceUsage: Default");
            if (isUnorderedAccess) desc.BindFlags |= BindFlags.UnorderedAccess;

            StructuredBuffer structured = new StructuredBuffer(device, desc.SizeInBytes, desc.StructureByteStride, "StructBuffer" + access.ToString());
            structured.Initialize(device, desc, data);
            return structured;
        }

        /// <summary>
        /// linking resource objects to the <b>vertex shaders</b> of the graphics pipeline.
        /// <b>pixel shaders</b> and <b>compute shaders</b> are restricted for <see cref="BindFlags.UnorderedAccess"/>
        /// </summary>
        public void BindToShaderStage(CommonShaderStage shader, int slot)
        {
            if (Stream != null) throw new Exception("Buffer is Locked, you need to UnLock it before blind !");
            //need to check if it's correct
            //if (slot >= CommonShaderStage.register) throw new ArgumentOutOfRangeException($"Directx can use maximum {CommonShaderStage.ConstantBufferHwSlotCount} slots for constant buffer");
            shader.SetShaderResource(slot, SRV);
        }
    }
}
