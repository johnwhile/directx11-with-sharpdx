﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common.Maths;

namespace Engine
{
    /// <summary>
    /// For directx11
    /// </summary>
    public enum DxPrimitive : byte
    {
        Undefined = 0,
        /// <summary><inheritdoc cref="Primitive.Point"/></summary>
        PointList = 1,
        /// <summary><inheritdoc cref="Primitive.LineList"/></summary>
        LineList = 2,
        /// <summary><inheritdoc cref="Primitive.LineStrip"/></summary>
        LineStrip = 3,
        /// <summary><inheritdoc cref="Primitive.TriangleList"/></summary>
        TriangleList = 4,
        /// <summary><inheritdoc cref="Primitive.TriangleStrip"/></summary>
        TriangleStrip = 5,
        #region not-tested
        LineListWithAdjacency = 10,
        LineStripWithAdjacency = 11,
        TriangleListWithAdjacency = 12,
        TriangleStripWithAdjacency = 13,
        PatchListWith1ControlPoints = 33,
        PatchListWith2ControlPoints = 34,
        PatchListWith3ControlPoints = 35,
        PatchListWith4ControlPoints = 36,
        PatchListWith5ControlPoints = 37,
        PatchListWith6ControlPoints = 38,
        PatchListWith7ControlPoints = 39,
        PatchListWith8ControlPoints = 40,
        PatchListWith9ControlPoints = 41,
        PatchListWith10ControlPoints = 42,
        PatchListWith11ControlPoints = 43,
        PatchListWith12ControlPoints = 44,
        PatchListWith13ControlPoints = 45,
        PatchListWith14ControlPoints = 46,
        PatchListWith15ControlPoints = 47,
        PatchListWith16ControlPoints = 48,
        PatchListWith17ControlPoints = 49,
        PatchListWith18ControlPoints = 50,
        PatchListWith19ControlPoints = 51,
        PatchListWith20ControlPoints = 52,
        PatchListWith21ControlPoints = 53,
        PatchListWith22ControlPoints = 54,
        PatchListWith23ControlPoints = 55,
        PatchListWith24ControlPoints = 56,
        PatchListWith25ControlPoints = 57,
        PatchListWith26ControlPoints = 58,
        PatchListWith27ControlPoints = 59,
        PatchListWith28ControlPoints = 60,
        PatchListWith29ControlPoints = 61,
        PatchListWith30ControlPoints = 62,
        PatchListWith31ControlPoints = 63,
        PatchListWith32ControlPoints = 64
        #endregion
    }

    public static class PrimitiveExtension
    {
        static DxPrimitive[] common2dx = new DxPrimitive[] {
            DxPrimitive.Undefined,
            DxPrimitive.PointList,
            DxPrimitive.LineList,
            DxPrimitive.LineStrip,
            DxPrimitive.TriangleList,
            DxPrimitive.TriangleStrip,
            DxPrimitive.Undefined, //fan not supported
            DxPrimitive.Undefined, //7
            DxPrimitive.Undefined, //8
            DxPrimitive.Undefined }; //9

        static Primitive[] dx2common = new Primitive[] {
            Primitive.Undefined,
            Primitive.Point,
            Primitive.LineList,
            Primitive.LineStrip,
            Primitive.TriangleList,
            Primitive.TriangleStrip,
            Primitive.TriangleFan }; //fan not supported


        public static Primitive ToCommon(this DxPrimitive dxprimitive)
        {
            return dx2common[(int)dxprimitive];
        }
        public static DxPrimitive ToSharpDx(this Primitive primitive)
        {
            return common2dx[(int)primitive];
        }
    }
}
