﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Common.Maths;

using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Buffer = SharpDX.Direct3D11.Buffer;


#pragma warning disable 67

namespace Engine
{
    /// <summary>
    /// </summary>
    public class IndexBuffer : GraphicBuffer
    {
        IndexLayout layout;

        private IndexBuffer(D3Device device, int bytesize, IndexLayout layout, string debugname = null) : 
            base(device, bytesize, layout.ByteSize, debugname)
        {
            this.layout = layout;
        }

        /// <summary>
        /// <inheritdoc cref="Create{T}(D3Device, int, T[], int, int, BufferAccess)"/><br/>
        /// Immutable version require a data array
        /// </summary>
        public static IndexBuffer CreateImmutable<T>(D3Device device, T[] data, int offset = 0, int length = -1) where T : unmanaged
        {
            if (length < 0) length = data?.Length ?? 0;
            return Create(device, length, data, offset, length, BufferAccess.Immutable);
        }

        /// <summary>
        /// Create a index buffer
        /// </summary>
        /// <typeparam name="T">can be a coerent format, example a <see cref="ushort"/> when layout is <see cref="Vector3us"/></typeparam>
        /// <param name="count">can not be zero</param>
        /// <param name="data">can be null for dynamic</param>
        /// <param name="offset">where start to pass from data</param>
        /// <param name="length">items to pass from data</param>
        public static IndexBuffer Create<T>(D3Device device, int count, T[] data = null, int offset = 0, int length = -1, BufferAccess access = BufferAccess.Immutable) where T : unmanaged
        {
            if (length < 0) length = data?.Length ?? 0;
            int sizeof_t = Marshal.SizeOf<T>();

            if (!IndexLayout.TryGetLayout<T>(out var layout))
                throw new ArgumentException("The only formats allowed for index buffer is UINT or USHORT and arrays of them");

            //if (data != null && layout.ByteSize * data.Length > layout.ByteSize * count)
            //    throw new ArgumentOutOfRangeException($"The size in bytes of data is greater than buffer size, or the type {typeof(T)} isn't aligned");

            if (!access.HasFlag(BufferAccess._CPU_write) && (data == null || length == 0 || count == 0))
                throw new ArgumentNullException("data must be not empty for immutable buffer");

            var desc = new BufferDescription()
            {
                SizeInBytes = sizeof_t * count,
                StructureByteStride = layout.ByteFormat, //can be only =2 or =4
                BindFlags = BindFlags.IndexBuffer,
                CpuAccessFlags = GetCpuAccess(access),
                OptionFlags = ResourceOptionFlags.None,
                Usage = GetUsage(access),
            };

            IndexBuffer index = new IndexBuffer(device, desc.SizeInBytes, layout, "IndexBuffer" + access.ToString());
            index.Initialize(device, desc, data, offset, length);
            return index;
        }

        /// <summary>
        /// linking resource objects to the shaders of the graphics pipeline.
        /// </summary>
        /// <remarks>index buffer use only one slot, but can use offset</remarks>
        public override void Bind(D3Context context, int offset = 0)
        {
            //if (slot >= InputAssemblerStage.IndexInputResourceSlotCount) throw new ArgumentOutOfRangeException("slot exceeded the limit");
            context.d3contex.InputAssembler.SetIndexBuffer(buffer, layout.format, offset);
        }

        /// <summary>
        /// </summary>
        struct IndexLayout
        {
            /// <summary>
            ///  DXGI_FORMAT_R16_UINT  and DXGI_FORMAT_R32_UINT  are  the  only  formats supported for index buffers
            /// </summary>
            public Format format;

            public int numOfIndis;
            /// <summary>
            /// The size of type defined as numOfIndis * byteFormat, example for <see cref="Vector3ui"/> is 3 * 4bytes
            /// </summary>
            public int ByteSize => numOfIndis * ByteFormat;
            /// <summary>
            /// The size in bytes of struct format, can only be 2 (for 16bit) or 4 (for 32bit)
            /// </summary>
            public int ByteFormat;

            static Dictionary<Type, IndexLayout> indexformats;

            static IndexLayout()
            {
                indexformats = new Dictionary<Type, IndexLayout>();
                indexformats.Add(typeof(ushort), new IndexLayout() { format = Format.R16_UInt, numOfIndis = 1, ByteFormat = sizeof(ushort) });
                indexformats.Add(typeof(int), new IndexLayout() { format = Format.R32_UInt, numOfIndis = 1, ByteFormat = sizeof(int) });
                indexformats.Add(typeof(uint), new IndexLayout() { format = Format.R32_UInt, numOfIndis = 1, ByteFormat = sizeof(uint) });
                indexformats.Add(typeof(Vector3us), new IndexLayout() { format = Format.R16_UInt, numOfIndis = 3, ByteFormat = sizeof(ushort) });
                indexformats.Add(typeof(Vector3ui), new IndexLayout() { format = Format.R32_UInt, numOfIndis = 3, ByteFormat = sizeof(uint) });
            }

            public static bool TryGetLayout<T>(out IndexLayout layout) where T:struct
            {
                return indexformats.TryGetValue(typeof(T), out layout);
            }
        }
    }
}
