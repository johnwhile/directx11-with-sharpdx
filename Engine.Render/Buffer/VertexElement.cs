﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Common;
using Common.Maths;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using DxFormat = SharpDX.DXGI.Format;

namespace Engine
{
    public enum DefaultSemantic
    {
        POSITION,
        POSITIONT,
        TEXCOORD,
        NORMAL,
        BINORMAL,
        TANGENT,
        COLOR
    }


    /// <summary>
    /// Corresponds to one component in the vertex structure. Slot and Offset are intentional omitted and will be auto-calculated.<br/>
    /// You can add more mapped items in <b><see cref="mappedFormat"/></b>
    /// </summary>
    public struct VertexElement
    {
        /// <summary>
        /// Precomputed table of knowledge, you can add new structure here
        /// </summary>
        public static Dictionary<Type, DeclarationFormat> mappedFormat = new Dictionary<Type, DeclarationFormat>();

        static VertexElement()
        {
            if (mappedFormat == null) mappedFormat = new Dictionary<Type, DeclarationFormat>();

            mappedFormat.Add(typeof(int), DeclarationFormat.Int);
            mappedFormat.Add(typeof(Vector2i), DeclarationFormat.Int2);
            mappedFormat.Add(typeof(Vector3i), DeclarationFormat.Int3);
            mappedFormat.Add(typeof(Vector4i), DeclarationFormat.Int4);
            mappedFormat.Add(typeof(Rectangle4i), DeclarationFormat.Int4);

            mappedFormat.Add(typeof(float), DeclarationFormat.Single);
            mappedFormat.Add(typeof(Vector2f), DeclarationFormat.Float2);
            mappedFormat.Add(typeof(Vector3f), DeclarationFormat.Float3);
            mappedFormat.Add(typeof(Vector4f), DeclarationFormat.Float4);
            mappedFormat.Add(typeof(Rectangle4f), DeclarationFormat.Float4);
            mappedFormat.Add(typeof(VectorHalf2), DeclarationFormat.Half2);
            mappedFormat.Add(typeof(VectorHalf4), DeclarationFormat.Half4);

            mappedFormat.Add(typeof(uint), DeclarationFormat.UInt);
            mappedFormat.Add(typeof(Vector2ui), DeclarationFormat.UInt2);
            mappedFormat.Add(typeof(Vector3ui), DeclarationFormat.UInt3);

            mappedFormat.Add(typeof(ushort), DeclarationFormat.UShort);
            mappedFormat.Add(typeof(Vector2us), DeclarationFormat.UShort2);
            mappedFormat.Add(typeof(Vector2s), DeclarationFormat.Short2);

            //D3D11 WARNING: ID3D11Device::CreateInputLayout: The provided input signature expects to read an element with
            //SemanticName/Index: 'COLOR'/0 and component(s) of the type 'int32'.  However, the matching entry in the Input
            //Layout declaration, element[2], specifies mismatched format: 'R8G8B8A8_UINT'.  This is not an error, since
            //behavior is well defined: The element format determines what data conversion algorithm gets applied before it
            //shows up in a shader register. Independently, the shader input signature defines how the shader will interpret
            //the data that has been placed in its input registers, with no change in the bits stored.  It is valid for the
            //application to reinterpret data as a different type once it is in the vertex shader, so this warning is issued
            //just in case reinterpretation was not intended by the author. [ STATE_CREATION WARNING #391: CREATEINPUTLAYOUT_TYPE_MISMATCH]
            
            //mappedFormat.Add(typeof(Color4b), DeclarationFormat.Byte4);
            mappedFormat.Add(typeof(Color4b), DeclarationFormat.UInt);
        }

        /// <summary>	
        /// The HLSL semantic associated with this element in a shader input-signature	
        /// </summary>
        public string SemanticName;
        /// <summary>	
        /// The semantic index for the element. A semantic index modifies a semantic, with an integer index number.
        /// A semantic index is only needed in a case where there is more than one element with the same semantic.
        /// For example, a 4x4 matrix would have four components each with the semantic name <pre><code>matrix</code></pre>,
        /// however each of the four component would have different semantic indices (0, 1, 2, and 3).	
        /// </summary>	
        public byte SemanticIndex;
        /// <summary>	
        /// The data type of the element data. See <strong><see cref="SharpDX.DXGI.Format"/></strong>
        /// </summary>	
        public DeclarationFormat Format;
        /// <summary>
        /// Declare element as per-vertex or per-instance, default = false
        /// </summary>
        public bool Instanced;
        /// <summary>
        /// The number of instances to draw using the same per-instance data before moving to the next element.
        /// 0 for not-instanced, >=1 for instanced.
        /// </summary>
        /// <remarks>
        /// For example, suppose you want to draw 6 instances, but only supply an array of 3 instanced colors: red, green, and blue;
        /// then we set the step rate to 2 and the first 2 instances will be drawn with red, the second two instances will be drawn with green,
        /// and the last 2 instances will be drawn with blue.
        /// If there is a one-to-one correspondence between each instanced data element and each instance, then the step rate would be 1.
        /// For vertex data, specify 0 for the step rate.
        /// </remarks>
        public byte InstancedStep;
        /// <summary>
        /// the size in byte of <see cref="ElementType"/>.
        /// </summary>
        public byte ByteSize
        {
            get
            {
                int size = Marshal.SizeOf(ElementType);
                if (size > 255) throw new NotSupportedException("it seems that the single element has a very large Size");
                return (byte)size;
            }
        }
        /// <summary>
        /// The type is a packed struct
        /// </summary>
        public Type ElementType { get; private set; }

        private static VertexElement Unknow()
        {
            return new VertexElement()
            {
                SemanticName = "UNKNOW",
                SemanticIndex = 0,
                Instanced = false,
                InstancedStep = 0,
                ElementType = null,
                Format = DeclarationFormat.Unknow
            };
        }

        /// <summary>
        /// </summary>
        /// <param name="type">define the struct type</param>
        /// <param name="format">byte size must match with type size and data must be coherent, same value of <b><see cref="SharpDX.DXGI.Format"/></b> </param>
        /// <param name="instancedStepRate"><inheritdoc cref="InstancedStep"/></param>
        public static VertexElement New(Type type, DeclarationFormat format, string semanticname, byte semanticindex = 0, bool instanced = false, byte instancedStepRate = 0)
        {
            if (instanced && instancedStepRate < 1)
            {
                Debugg.Warning("> WARNING : for instanced element, the steprate must be 1 or greater. REF fix number to 1");
                instancedStepRate = 1;
            }
            var element = new VertexElement()
            {
                SemanticName = semanticname,
                SemanticIndex = semanticindex,
                Instanced = instanced,
                InstancedStep = instancedStepRate,
                ElementType = type,
                Format = format
            };

            return element;
        }
        /// <summary>
        /// </summary>
        /// <param name="type">if no format was assigned, it will be use it from <see cref="mappedFormat"/> </param>
        /// <param name="instancedStepRate"><inheritdoc cref="InstancedStep"/></param>
        /// <returns></returns>
        public static VertexElement New(Type type, string semanticname, byte semanticindex = 0, bool instanced = false, byte instancedStepRate = 0)
        {
            if (!mappedFormat.TryGetValue(type, out var format))
            {
                throw new Exception("unknow inputlayout structure, please add to VertexElement.mappedFormat");
                //return Unknow();
            }
#if DEBUG
            if (format== DeclarationFormat.Half4 || format == DeclarationFormat.Half2)
                Debugg.Error("!!! HALF VECTOR NOT TESTED !!!");
#endif
            return New(type, format, semanticname, semanticindex, instanced, instancedStepRate);

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="VertexElement"/> struct.
        /// </summary>
        /// <exception cref="Exception">
        /// Thrown when struct wasn't added in the <see cref="mappedFormat"/>
        /// </exception>
        /// <param name="semantic">can has a semantic index, example "NAME" or "NAME0"</param>
        /// <param name="instancedStepRate"><inheritdoc cref="InstancedStep"/></param>
        public static VertexElement Create<T>(string semantic, bool instanced = false, byte instancedStepRate = 0) where T : struct
        {
            int semanticindex = semantic[semantic.Length - 1] - '0';

            if (semanticindex >=0 && semanticindex <=9)
            {
                semantic = semantic.Substring(0, semantic.Length - 1);
            }
            else
            {
                semanticindex = 0;
            }

            return New(typeof(T), semantic, (byte)semanticindex, instanced, instancedStepRate);
        }
        
        string Semantic => SemanticName + SemanticIndex.ToString();


        /// <summary>
        /// This casting not return the correct element
        /// </summary>
        public static implicit operator InputElement(VertexElement from)
        {
            return new InputElement()
            {
                SemanticName = from.SemanticName,
                SemanticIndex = from.SemanticIndex,
                Format = (DxFormat)from.Format,
                Classification = from.Instanced ? InputClassification.PerInstanceData : InputClassification.PerVertexData,
                InstanceDataStepRate = from.InstancedStep,
                AlignedByteOffset = 0,
                Slot = 0
            };
        }

        public override string ToString()
        {
            return string.Format("{0},{2},{3}", Semantic, Format, Instanced ? "PerInstance" : "PerVertex");
        }


        public override int GetHashCode()
        {
            //DXGI.format is from 1 to 132 (1 byte)
            int hash = (int)Format; //8bit
            hash = (hash << 8) | InstancedStep; //8bit
            hash = (hash << 8) | ByteSize; //8bit
            hash = (hash << 8) | SemanticIndex; //8bit
            //total bit for unique hash=32
            hash = hash ^ SemanticName.GetHashCode();
            if (Instanced) hash = ~hash;
            return hash;
        }

        public override bool Equals(object obj) => obj is VertexElement && EqualsByRef(this , (VertexElement)obj);

        public bool Equals(VertexElement other) => EqualsByRef(in this, in other);
        
        /// <summary>
        /// Passing by reference is faster for structs
        /// </summary>
        public static bool EqualsByRef(in VertexElement a, in VertexElement b)
        {
            return
                a.ElementType == b.ElementType &&
                a.SemanticName == b.SemanticName &&
                a.SemanticIndex == b.SemanticIndex &&
                a.Instanced == b.Instanced &&
                a.InstancedStep == b.InstancedStep &&
                a.Format == b.Format;
        }
        public static bool operator ==(in VertexElement a, in VertexElement b) => EqualsByRef(in a, in b);
        public static bool operator !=(in VertexElement a, in VertexElement b) => !EqualsByRef(in a, in b);
    }
}
