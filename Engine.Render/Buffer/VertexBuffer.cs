﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

using Common.Maths;

using SharpDX.Direct3D11;
using Buffer = SharpDX.Direct3D11.Buffer;

namespace Engine
{
    /// <summary>
    /// </summary>
    public class VertexBuffer : GraphicBuffer
    {
        /// <summary>
        /// apply a null buffer to context to remove it
        /// </summary>
        public static readonly VertexBufferBinding NullBinding = new VertexBufferBinding(null, 0, 0);

        internal VertexBufferBinding binding;

        protected VertexBuffer(D3Device device, int bytesize, int bytestride, string debugname = null) :
            base(device, bytesize, bytestride, debugname)
        {
            binding = new VertexBufferBinding(null, ElementSize, 0);
        }


        /// <summary>
        /// <inheritdoc cref="Create{T}(D3Device, InputSlotLayout, T[], BufferAccess)"/><br/>
        /// Immutable version require a data array
        /// </summary>
        public static VertexBuffer CreateImmutable<T>(D3Device device, T[] data, int offset = 0, int length =-1 , int bytestride = 0) where T : unmanaged  
        {
            if (length  < 0) length  = data?.Length ?? 0;
            return Create(device, length,  data, offset, length , BufferAccess.Immutable, bytestride);
        }
        public static VertexBuffer CreateDynamic<T>(D3Device device, int count, T[] data = null, int offset = 0, int length = -1, int bytestride = 0) where T : unmanaged
        {
            return Create(device, count, data, offset, length, BufferAccess.Dynamic, bytestride);
        }
        /// <summary>
        /// Create a Vertex buffer using a coerent struct <typeparamref name="T"/>
        /// </summary>
        /// <typeparam name="T">can be a coerent format, example a <see cref="float"/> when layout is <see cref="Vector3f"/></typeparam>
        /// <param name="layout">usefull information for correct interpretation of data</param>
        /// <param name="count">how many items can contain, can be different from length only for dynamic buffer</param>
        /// <param name="data">can be null for dynamic</param>
        /// <param name="offset">where start to pass from data</param>
        /// <param name="length">items to pass from data</param>
        /// 
        public static VertexBuffer Create<T>(D3Device device, int count, T[] data = null, int offset = 0, int length = -1, BufferAccess access = BufferAccess.Default, int bytestride = 0) 
            where T : unmanaged
        {
            int sizeof_t = Marshal.SizeOf<T>();
            if (bytestride == 0) bytestride = sizeof_t;

            //if (data != null && sizeof_t != layout.ByteSize)
            //    throw new ArgumentOutOfRangeException($"The size in bytes of \"{typeof(T).Name}\" is greater than buffer stride, or the type \"{typeof(T).Name}\" isn't aligned");

            if (!access.HasFlag(BufferAccess._CPU_write) && (data == null || data.Length == 0 || length == 0)) throw new ArgumentNullException("data must be not empty for immutable buffer");

            var desc = new BufferDescription()
            {
                SizeInBytes = sizeof_t * count,
                //valid only for structured buffer, the directx will set to zero all other cases
                StructureByteStride = bytestride,
                BindFlags = BindFlags.VertexBuffer,
                CpuAccessFlags = GetCpuAccess(access),
                OptionFlags = ResourceOptionFlags.None,
                Usage = GetUsage(access),
            };

            VertexBuffer vertex = new VertexBuffer(device, desc.SizeInBytes, desc.StructureByteStride, "VertexBuffer" + access.ToString());
            vertex.Initialize(device, desc, data, offset, length);
            return vertex;
        }

        /// <summary>
        /// linking resource objects to the <b>vertex shaders</b> of the graphics pipeline.
        /// </summary>
        /// <param name="slot">directx3d report maximum 32 slots</param>
        /// <remarks>remember to assign the correct slot from your inputlayout</remarks>
        public override void Bind(D3Context context, int slot = 0)
        {
            if (slot >= InputAssemblerStage.VertexInputResourceSlotCount) throw new ArgumentOutOfRangeException("slot exceeded the limit");

            if (Stream != null) throw new Exception("Buffer is Locked, you need to UnLock it before blind !");

            binding.Buffer = buffer;
            context.d3contex.InputAssembler.SetVertexBuffers(slot, binding);
        }
    }
}
