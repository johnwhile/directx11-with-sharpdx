﻿using System;
using System.ComponentModel.Design.Serialization;
using System.Runtime.InteropServices;
using Common;
using SharpDX.Direct3D11;
using SharpDX.WIC;
using Buffer = SharpDX.Direct3D11.Buffer;

namespace Engine
{
    public abstract class ConstantBufferBase : GraphicBuffer
    {
        public const int MAX_SLOTS = 16;
        
        public bool CacheChanged { get; protected set; }

        /// <summary>
        /// 1024*64 = maximum bytes for a constant buffer in SM5<br/>
        /// <paramref name="dynamic"/> : for per-frames update, set false to static-constant-buffer
        /// </summary>
        /// <exception cref="ArgumentException">struct must respect the packing rule</exception>
        /// <exception cref="Exception">sdasda</exception>
        protected ConstantBufferBase(D3Device device, int bytesize, bool dynamic, string debugname = null) :
            base(device, bytesize, 16, debugname)
        {
            if (bytesize > sizeof(float) * 16 * 1024) throw new Exception((ushort.MaxValue + 1) + " maximum bytes for a constant buffer in SM5");
            Initialize(device, bytesize, dynamic);
        }

        protected void Initialize(D3Device device, int bytesize, bool dynamic = true)
        {
            //ConstantBuffer flag may NOT be combined with any other bind flags.
            int size = CalcSize(bytesize);
#if DEBUG
            if (size != bytesize) Debugg.Warning($"The size of constant buffer : {bytesize} was fixed to multiple of 16 : {size}");
#endif
            bytesize = size;

            var usage = dynamic ? ResourceUsage.Dynamic : ResourceUsage.Immutable;

            var desc = new BufferDescription()
            {
                SizeInBytes = bytesize,
                StructureByteStride = bytesize, //no need to set stride size, can be zero
                BindFlags = BindFlags.ConstantBuffer,
                CpuAccessFlags = GetCpuAccess(usage),
                OptionFlags = ResourceOptionFlags.None,
                Usage = usage,
            };
            Initialize(device, desc);


        }

        /// <summary>
        /// Write cache data to buffer if changed
        /// </summary>
        public abstract void Update(D3Context context);

        public override void Bind(D3Context context, int offset = 0)
        {
            throw new NotSupportedException();
        }
        public void BindToShaderStage(CommonShaderStage shader, int slot)
        {
            //need to check if it's correct
            int maxslot = CommonShaderStage.ConstantBufferHwSlotCount;
            if (slot >= maxslot) throw new ArgumentOutOfRangeException($"Directx can use maximum {maxslot} slots for constant buffer");
            shader.SetConstantBuffer(slot, buffer);
        }
        /// <summary>
        /// TODO: todo
        /// Check if struct is correctly packed: size must be multiple of 16 byte and packed into a four-component vector.
        /// The purpose is just to remember the correct struct layout, use only not-static struct elements.
        /// </summary>
        public static bool ValidateStructPakingRule<T>() where T : unmanaged
        {
            /*
            int size = Marshal.SizeOf(typeof(T));
            if (size > 16) return size % 16 == 0;
            */

            /*
             * not work with Unions
             * 
            int paking = 0;

            foreach (var info in typeof(T).GetFields())
            {
                Type type = info.FieldType;

                if (!type.IsValueType) return false;
                paking += Marshal.SizeOf(type);

                if (paking % 16 == 0) paking = 0;
                if (paking > 16)
                {
                    Debugg.Print($"the struct {typeof(T).Name} return a not valid packing rule for constant buffer: last field={paking} bytes", DebugInfo.Error);
                    return false;
                }
            }*/

            return true;
        }
        
        /// <summary>
        /// With this formula you get the true size of constant buffer witch is multiple of 16 bytes
        /// </summary>
        protected static int CalcSize(int sizeofstruct) => ((sizeofstruct + 15) / 16) * 16;
    }



    /// <summary>
    /// Use instead the more convenient generic implementation <see cref="ConstantBuffer{T}"/>
    /// </summary>
    public class ConstantBuffer : ConstantBufferBase
    {
        byte[] cache;

        protected ConstantBuffer(D3Device device, int bytesize, bool dynamic = false, string debugname = null) :
            base(device, bytesize, dynamic, debugname)
        {
            cache = new byte[bytesize];
        }

        public unsafe K GetCache<K>() where K : unmanaged
        {
            fixed (byte* ptr = cache) return *(K*)ptr;
        }

        public unsafe void SetCache<K>(K data) where K :unmanaged
        {
            fixed (byte* ptr = cache)
            {
                byte* data_ptr = (byte*)&data;
                System.Buffer.MemoryCopy(data_ptr, ptr, ByteSize, Marshal.SizeOf<K>());
            }
            CacheChanged = true;
        }

        /// <summary>
        /// Use instead the more convenient generic implementation <see cref="ConstantBuffer{T}"/>
        /// </summary>
        public static ConstantBuffer Create(D3Device device, int bytesize, bool dynamic = true, string debugname = null)
        {
            return new ConstantBuffer(device, bytesize, dynamic, debugname);
        }
        /// <summary>
        /// <inheritdoc cref="Create(D3Device, int, bool)"/>
        /// </summary>
        public static ConstantBuffer Create<K>(D3Device device, bool dynamic = true, string debugname = null) where K : unmanaged 
        {
            return Create(device, Marshal.SizeOf<K>(), dynamic, debugname);
        }

        public unsafe override void Update(D3Context context)
        {
            if (CacheChanged)
            {
                fixed (byte* ptr = cache)
                    SetDataInternal(context, new IntPtr(ptr), ByteSize, 0);
                CacheChanged = false;
                context.Stats.AssignCBufferDataCount++;
            }
        }
    }

    /// <summary>
    /// Generic constant buffer with cache implementation, it will update data when you will set the buffer to shader stage with
    /// <b><see cref="D3Context.SetToVertexStage(ConstantBuffer, int)"/></b>
    /// </summary>
    public class ConstantBuffer<T> : ConstantBuffer where T : unmanaged
    {
        T cache;

        public T Data
        {
            get => cache;
            set
            {
                CacheChanged = !cache.Equals(value);
                cache = value;
            }
        }

        protected ConstantBuffer(D3Device device, bool dynamic = true, string debugname = null) : 
            base(device, Marshal.SizeOf<T>(), dynamic, debugname)
        {
        }
        public static ConstantBuffer<T> Create(D3Device device, bool dynamic = true, string debugname = null)
        {
            return new ConstantBuffer<T>(device, dynamic, debugname);
        }

        public override void Update(D3Context context)
        {
            if (CacheChanged)
            {
                SetData(context, ref cache);
                CacheChanged = false;
                context.Stats.AssignCBufferDataCount++;
            }
        }
    }
}
