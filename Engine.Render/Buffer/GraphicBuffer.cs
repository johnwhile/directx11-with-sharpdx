﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Common;
using Common.Maths;

using SharpDX;
using SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;
using D3D = SharpDX.Direct3D;
using Buffer = SharpDX.Direct3D11.Buffer;

namespace Engine
{
    /// <summary>
    /// Base class for vertex, index, constant buffers
    /// </summary>
    public abstract class GraphicBuffer : D3Resource
    {
        //debug now many buffer are created
        public static int BufferGenerationsCount = 0;

        /// <summary>
        /// it's not-null if you map the buffer with <see cref="Lock{T}(bool, int)"/>
        /// </summary>
        protected DataStream Stream = null;
        protected Buffer buffer;
        protected override Resource Resource => buffer;

        public override IntPtr NativePointer => buffer != null ? buffer.NativePointer : IntPtr.Zero;

        protected ShaderResourceView SRV;
        protected UnorderedAccessView UAV;

        public bool CanWrite =>
            buffer != null &&
            !buffer.Description.Usage.HasFlag(ResourceUsage.Immutable) &&
            buffer.Description.CpuAccessFlags.HasFlag(CpuAccessFlags.Write);

        /// <summary>
        /// Gets the number of elements or capacity of buffer<br/>
        /// For <b><see cref="IndexBuffer"/></b> is simply the numbers of indices and not the number of primitives,
        /// example for a triangle <see cref="Vector3i"/> = 3<br/>
        /// </summary>
        public readonly int ElementCount;
        /// <summary>
        /// Gets the size of element T.<br/>
        /// For <b><see cref="VertexBuffer"/></b> is the size of structured defined in  <b><see cref="VertexBuffer.layout"/></b><br/>
        /// For <b><see cref="IndexBuffer"/></b> is 2 or 4 (for 16bit or 32bit format)<br/>
        /// </summary>
        /// <remarks>
        /// dont try to use buffer.Description.StructureByteStride, it will be set to zero for buffers different than StructuredBuffer
        /// </remarks>
        public readonly int ElementSize;
        /// <summary>
        /// Get the size in byte of buffer.
        /// </summary>
        public int ByteSize => buffer != null ? buffer.Description.SizeInBytes : 0;
            

        protected GraphicBuffer(D3Device device, int bytesize, int elementsize, string debugname = null) : base(device, debugname)
        {
            if (bytesize <= 0) throw new ArgumentException("A DynamicVBuffer can't have zero size");
            ElementSize = elementsize;
            ElementCount = bytesize / elementsize;
            BufferGenerationsCount++;
        }

        public abstract void Bind(D3Context context, int offset = 0);


        /// <summary>
        /// By adjusting the ElementOffset and ElementWidth parameters, a single large buffer 
        /// resource can be used to contain a collection of smaller datasets, each with its own SRV, to
        /// provide access to them
        /// </summary>
        protected void InitializeView()
        {
            // Staging resource don't have any views
            if (buffer.Description.Usage == ResourceUsage.Staging) return;

            // For structured buffers, DXGI_FORMAT_UNKNOWN must be used! 
            var srvFormat = PixelFormat.Unknown;
            var uavFormat = PixelFormat.Unknown;

            //raw buffer
            if (buffer.Description.OptionFlags.HasFlag(ResourceOptionFlags.BufferAllowRawViews))
            {
                srvFormat = PixelFormat.R32_Typeless;
                uavFormat = PixelFormat.R32_Typeless;
            }

            if (buffer.Description.BindFlags.HasFlag(BindFlags.ShaderResource))
            {
                var description = new ShaderResourceViewDescription
                {
                    Format = (DXGI.Format)srvFormat,
                    Dimension = D3D.ShaderResourceViewDimension.ExtendedBuffer,
                    BufferEx = 
                    {
                        ElementCount = ElementCount,
                        FirstElement = 0,
                        Flags = ShaderResourceViewExtendedBufferFlags.None
                    }

                };
                //raw buffer
                if (buffer.Description.OptionFlags.HasFlag(ResourceOptionFlags.BufferAllowRawViews))
                {
                    description.BufferEx.Flags |= ShaderResourceViewExtendedBufferFlags.Raw;
                }
                SRV = ToDispose(new ShaderResourceView(Device, buffer, description));
                SRV.DebugName = "SRVofBuffer<" + GetHashCode().ToString("x4") + ">";
                SRV.Tag = this;
            }

            if (buffer.Description.BindFlags.HasFlag(BindFlags.UnorderedAccess))
            {
                var description = new UnorderedAccessViewDescription()
                {
                    Format = (DXGI.Format)uavFormat,
                    Dimension = UnorderedAccessViewDimension.Buffer,
                    Buffer = {
                        ElementCount = ElementCount,
                        FirstElement = 0,
                        Flags = UnorderedAccessViewBufferFlags.None
                    },
                };

                /*
                //raw buffer
                if (buffer.Description.OptionFlags.HasFlag(ResourceOptionFlags.BufferAllowRawViews))
                    description.Buffer.Flags |= UnorderedAccessViewBufferFlags.Raw;

                //StructuredAppendBuffer
                if (((int)buffer.Description.OptionFlags & 128) !=0)
                    description.Buffer.Flags |= UnorderedAccessViewBufferFlags.Append;

                //StructuredCounterBuffer
                if (((int)buffer.Description.OptionFlags & 256) != 0)
                    description.Buffer.Flags |= UnorderedAccessViewBufferFlags.Counter;
                */

                UAV = ToDispose(new UnorderedAccessView(Device, buffer, description));
                UAV.DebugName = "UAVofBuffer<" + GetHashCode().ToString("x4") + ">";
                UAV.Tag = this;
            }

        }


        protected void Initialize(D3Device device, BufferDescription description)
        {
            if (description.Usage != ResourceUsage.Dynamic) Debugg.Error("Creating a empty static buffer ?");
            buffer = ToDispose(new Buffer(device, description));
            InitializeView();
        }

        protected unsafe void Initialize<T>(D3Device device, BufferDescription description, T[] data = null, int offset = 0, int length = -1) where T : unmanaged
        {
            if (length < 0) length = data?.Length ?? 0;
            int sizeof_T = Marshal.SizeOf<T>();
            int size = sizeof_T * (length - offset);

            if (data == null)
            {
                Initialize(device, description);
            }
            else
            {
                //SizeInBytes was already assigned and must be equal of greater than portion of data you pass
                if (description.SizeInBytes == 0) description.SizeInBytes = size;

                fixed (T* ptr = &data[offset])
                    buffer = ToDispose(new Buffer(device, new DataStream(new DataPointer(ptr, size)), description));
                
                InitializeView();
            }
        }

        /// <summary>
        /// Write a coerent data struct or array to buffer. 
        /// </summary>
        /// <param name="source">source buffer</param>
        /// <param name="sourcesize">the size in bytes of source. Remember that <paramref name="source"/> pointer contain the offset</param>
        /// <param name="destOffset">where to start writing to destination buffer, in bytes</param>
        protected unsafe void SetDataInternal(D3Context context, IntPtr source, int sourcesize, int destOffset, bool discard = true)
        {
            if (!CanWrite)
#if DEBUG
                throw new Exception("Buffer can't be write");
#else
                return;
#endif
            if (sourcesize + destOffset > ByteSize)
                throw new ArgumentOutOfRangeException("Size of data to upload + offset is larger than size of buffer");


            // If this buffer is declared as default usage, we can only use UpdateSubresource,
            // which is not optimal but better than nothing.
            if (buffer.Description.Usage == ResourceUsage.Default)
            {
                // Setup the dest region inside the buffer
                if (buffer.Description.BindFlags == BindFlags.ConstantBuffer)
                {
                    context.d3contex.UpdateSubresource(buffer, 0, null, source, 0, 0);
                }
                else
                {
                    context.d3contex.UpdateSubresource(buffer, 0, GetBufferRegion(destOffset, sourcesize), source, 0, 0);
                }
            }
            // If buffer is dynamic, i prefer Map / Unmap(UpdateSubresource it involves an extra copy)
            else if (buffer.Description.Usage == ResourceUsage.Dynamic)
            {
                MapMode mode = discard ? MapMode.WriteDiscard : MapMode.Write;
                //WriteNoOverwrite limited to vertex and index buffer. can't be used with ShaderResourceView
                if (discard && (buffer.Description.BindFlags == BindFlags.VertexBuffer || buffer.Description.BindFlags == BindFlags.IndexBuffer))
                    mode = MapMode.WriteNoOverwrite;

                var box = context.d3contex.MapSubresource(buffer, 0, mode, MapFlags.None);

                System.Buffer.MemoryCopy((void*)source, (void*)IntPtr.Add(box.DataPointer, destOffset), ByteSize, sourcesize);

                context.d3contex.UnmapSubresource(buffer, 0);
            }
            else
            {
                throw new NotSupportedException("Buffer usage not compatible with mapping operation");
            }
        }


        /// <summary>
        /// <inheritdoc cref="SetData{T}(D3Context, T[], int, int, int, bool)"/>
        /// </summary>
        public void SetData<T>(D3Context context, T[] data, bool discard) where T : unmanaged
        {
            SetData(context, data, 0, -1, 0, discard);
        }

        /// <summary>
        /// <inheritdoc cref="SetDataInternal(D3Context, IntPtr, int, int, bool)"/>
        /// </summary>
        /// <param name="startIndex">The starting index to begin setting data from</param>
        /// <param name="elementCount">The number of elements of data to set. Negative for data.lenght</param>
        /// <param name="destOffset">The offset of destination to write to, <b>using size of <typeparamref name="T"/></b></param>
        /// <param name="discard"></param>
        public void SetData<T>(D3Context context, T[] data, int startIndex = 0, int elementCount = -1, int elementOffset = 0, bool discard = true) where T : unmanaged
        {
            if (data==null) throw new ArgumentNullException("data is null");
            if (startIndex < 0 || startIndex >= data.Length) throw new ArgumentOutOfRangeException();
            if (elementCount == 0 || data.Length == 0) return;
            if (elementCount < 0) elementCount = data.Length - startIndex;
            if (startIndex + elementCount > data.Length) throw new ArgumentOutOfRangeException("");

            int sizeofT = Marshal.SizeOf<T>();
            GCHandle datahandle = GCHandle.Alloc(data, GCHandleType.Pinned);

            IntPtr dataptr = IntPtr.Add(datahandle.AddrOfPinnedObject(), startIndex * sizeofT);

            SetDataInternal(context, dataptr, sizeofT * elementCount, sizeofT * elementOffset, discard);
            datahandle.Free();
        }

        /// <summary>
        /// <inheritdoc cref="SetDataInternal(D3Context, IntPtr, int, int, bool)"/>
        /// </summary>
        /// <param name="elementOffset">element to skip before write, <b>using size of <typeparamref name="T"/></b> and not layout element size</param>
        /// <param name="discard"></param>
        public void SetData<T>(D3Context context, ref T data, int elementOffset = 0, bool discard = true) where T : unmanaged
        {
            int sizeofT = Marshal.SizeOf<T>();
            GCHandle datahandle = GCHandle.Alloc(data, GCHandleType.Pinned);
            IntPtr dataptr = datahandle.AddrOfPinnedObject();
            SetDataInternal(context, dataptr, sizeofT, sizeofT * elementOffset, discard);
            datahandle.Free();
        }

        /// <summary>
        /// <inheritdoc cref="SetDataInternal(D3Context, IntPtr, int, int, bool)"/><br/>
        /// Use the Lock and UnLock method: open buffer, write data, then close it
        /// </summary>
        /// <typeparam name="T">a coerent struct</typeparam>
        /// <param name="elementOffset">element to skip before write, <b>using size of <typeparamref name="T"/></b> and not layout element size</param>
        /// <param name="discard">invalidate the entire buffer</param>
        /// <param name="dataOffset">the first element on data array to start writing</param>
        /// <param name="dataLength">number of data element to write</param>
        public virtual void SetData2<T>(D3Context context, T[] data, int elementOffset = 0, bool discard = true, int dataOffset = 0, int dataLength = 0) where T : unmanaged
        {
            if (data == null) throw new ArgumentNullException("data are null");
            if (dataLength == 0) dataLength = data.Length;
            if (dataLength > data.Length + dataOffset) throw new ArgumentOutOfRangeException("too many element to write");

            int sizeT = Marshal.SizeOf<T>();
            int requiresize = sizeT * (dataLength - dataOffset + elementOffset);

            if (requiresize > ByteSize) throw new Exception("data go out of buffer size");

            var stream = Lock<T>(context, discard, elementOffset);
            stream.WriteRange(data, dataOffset, dataLength);
            UnLock(context);
            stream.Dispose();
        }

        /// <summary>
        /// Return the data stream of buffer, it will be disposed on <see cref="UnLock"/> call. <br/>
        /// <b>If It's a ConstantBuffer, the buffer will be always discard and offset set to 0</b>
        /// </summary>
        /// <remarks>
        /// When you map a buffer with D3D11_MAP_WRITE_DISCARD, the runtime always discards the entire buffer.
        /// You can't preserve info in unmapped areas of the buffer by specifying a nonzero offset or limited size field.
        /// </remarks>
        /// <param name="discard">invalidate the entire buffer</param>
        /// <param name="elementOffset">element to skip before write, <b>using size of <typeparamref name="T"/></b> and not layout element size</param>
        public DataStream Lock<T>(D3Context context, bool discard, int elementOffset = 0) where T : struct
        {
            return Lock(context, discard, Marshal.SizeOf<T>() * elementOffset);
        }

        /// <summary>
        /// <inheritdoc cref="Lock{T}(bool, int)"/>
        /// </summary>
        private DataStream Lock(D3Context context, bool discard, int byteOffset = 0)
        {
            if (!CanWrite) throw new Exception("Buffer can't be write");
            if (Stream != null) throw new Exception("Buffer already mapped, need to close it before");

            if (buffer.Description.BindFlags == BindFlags.ConstantBuffer)
            {
                //can not we use WriteNoOverwrite on constant buffer
                context.d3contex.MapSubresource(buffer, 0, MapMode.WriteDiscard, MapFlags.None, out Stream);
            }
            else if (buffer.Description.Usage == ResourceUsage.Dynamic)
            {
                MapMode mode = discard ? MapMode.WriteDiscard : MapMode.Write;
                //WriteNoOverwrite limited to vertex and index buffer. can't be used with ShaderResourceView
                if (discard && (buffer.Description.BindFlags == BindFlags.VertexBuffer || buffer.Description.BindFlags == BindFlags.IndexBuffer))
                    mode = MapMode.WriteNoOverwrite;

                if (byteOffset >= ByteSize) throw new InvalidOperationException("offset position is out of buffer size");

                context.d3contex.MapSubresource(buffer, mode, MapFlags.None, out Stream);
                Stream.Position = byteOffset;
            }
            else
            {
                throw new NotSupportedException("Buffer usage not compatible with mapping operation");
            }

            return Stream;
        }

        public void UnLock(D3Context context)
        {
            if (Stream == null) throw new Exception("Buffer not mapped yet, need to open it before");
            context.d3contex.UnmapSubresource(buffer, 0);
            Stream.Dispose();
            Stream = null;
        }


        /// <summary>
        /// Implicit casting operator to <see cref="Buffer"/>
        /// </summary>
        public static implicit operator Buffer(GraphicBuffer gbuffer)
        {
            return gbuffer != null ? gbuffer.buffer : throw new ArgumentNullException("GraphicBuffer casting with null reference");
        }

        /// <summary>
        /// for simple buffer
        /// </summary>
        private ResourceRegion GetBufferRegion(int byteStart, int byteSize) => new ResourceRegion(byteStart, 0, 0, byteStart + byteSize, 1, 1);
    }
}
