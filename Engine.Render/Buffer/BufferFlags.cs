﻿using System;
using SharpDX.Direct3D11;

namespace Engine
{
    /// <summary>
    /// This representation is useful for remembering access values
    /// </summary>
    [Flags]
    public enum BufferAccess
    {
        _GPU_read = 0, //buffer is always readable by gpu
        _GPU_write = 1,
        _CPU_write = 2,
        _CPU_read = 4,

        Immutable = _GPU_read,
        Default = _GPU_read | _GPU_write,
        Dynamic = _GPU_read | _CPU_write,
        Staging = _GPU_read | _GPU_write | _CPU_read | _CPU_write
    }


    /// <summary>
    /// Flags of a buffer.
    /// </summary>
    [Flags]
    public enum BufferFlags
    {
        /// <summary>
        /// Creates a none buffer. This is equivalent to <see cref="BindFlags.None"/>.
        /// </summary>
        None = 0,
        /// <summary>
        /// Creates a constant buffer. This is equivalent to <see cref="BindFlags.ConstantBuffer"/>.
        /// </summary>
        ConstantBuffer = 1,
        /// <summary>
        /// Creates an index buffer. This is equivalent to <see cref="BindFlags.IndexBuffer"/>.
        /// </summary>
        IndexBuffer = 2,
        /// <summary>
        /// Creates a vertex buffer. This is equivalent to <see cref="BindFlags.VertexBuffer"/>.
        /// </summary>
        VertexBuffer = 4,
        /// <summary>
        /// Creates a render target buffer. This is equivalent to <see cref="BindFlags.RenderTarget"/>.
        /// </summary>
        RenderTarget = 8,
        /// <summary>
        /// Creates a buffer usable as a <see cref="ShaderResourceView"/>. This is equivalent to <see cref="BindFlags.ShaderResource"/>.
        /// </summary>
        ShaderResource = 16,
        /// <summary>
        /// Creates an unordered access buffer. This is equivalent to <see cref="BindFlags.UnorderedAccess"/>.
        /// </summary>
        UnorderedAccess = 32,
        /// <summary>
        /// Creates a structured buffer. This is equivalent to <see cref="ResourceOptionFlags.BufferStructured"/>.
        /// </summary>
        StructuredBuffer = 64,
        /// <summary>
        /// Creates a structured buffer that supports unordered access and append.
        /// This is equivalent to <see cref="ResourceOptionFlags.BufferStructured"/>.
        /// </summary>
        StructuredAppendBuffer = UnorderedAccess | StructuredBuffer | 128,
        /// <summary>
        /// Creates a structured buffer that supports unordered access and counter.
        /// This is equivalent to <see cref="ResourceOptionFlags.BufferStructured"/>.
        /// </summary>
        StructuredCounterBuffer = UnorderedAccess | StructuredBuffer | 256,
        /// <summary>
        /// Creates a raw buffer. This is equivalent to <see cref="ResourceOptionFlags.BufferAllowRawViews"/> 
        /// and <see cref="UnorderedAccessViewBufferFlags.Raw"/>.
        /// </summary>
        RawBuffer = 512,
        /// <summary>
        /// Creates an indirect arguments buffer. This is equivalent to <see cref="ResourceOptionFlags.DrawIndirectArguments"/>.
        /// </summary>
        ArgumentBuffer = 1024,
        /// <summary>	
        /// Creates an output buffer for the stream-output stage.
        /// </summary>	
        StreamOutput = 2048,
    }
}
