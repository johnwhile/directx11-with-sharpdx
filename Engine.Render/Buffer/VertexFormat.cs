﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DxFormat = SharpDX.DXGI.Format;

namespace Engine
{
    /// <summary>
    /// Defines common vertex element formats.
    /// </summary>
    /// <remarks>the values are and abbreviation of <see cref="DxFormat"/></remarks>
    public enum DeclarationFormat
    {
        /// <summary>
        /// define format as error
        /// </summary>
        Unknow = DxFormat.Unknown,

        #region base register component type
        Int = DxFormat.R32_SInt, //43
        UInt = DxFormat.R32_UInt, //42
        Single = DxFormat.R32_Float, //41
        #endregion

        #region Vector base register component type
        Int2 = DxFormat.R32G32_SInt, //18
        Int3 = DxFormat.R32G32B32_SInt, //8
        Int4 = DxFormat.R32G32B32A32_SInt, //4

        UInt2 = DxFormat.R32G32_UInt, //17
        UInt3 = DxFormat.R32G32B32_UInt, //7
        UInt4 = DxFormat.R32G32B32A32_UInt, //3

        Float2 = DxFormat.R32G32_Float, //16
        Float3 = DxFormat.R32G32B32_Float, //6
        Float4 = DxFormat.R32G32B32A32_Float, //2
        #endregion

        Byte = DxFormat.R8_UInt,
        Byte4 = DxFormat.R8G8B8A8_UInt,

        UShort = DxFormat.R16_UInt,
        UShort2 = DxFormat.R16G16_UInt,
        UShort4 = DxFormat.R16G16B16A16_UInt,
        
        Short = DxFormat.R16_SInt,
        Short2 = DxFormat.R16G16_SInt,
        Short4 = DxFormat.R16G16B16A16_SInt,

        //not tested
        Half2 = DxFormat.R16G16_Float,
        Half4 = DxFormat.R16G16B16A16_Float
    }
}
