﻿using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;

namespace Engine
{
    internal class VertexLayout
    {
        /// <summary>
        /// Maximum supported slots in Direct3D
        /// </summary>
        public const int MAX_SLOTS_COUNT = 16;

        InputSlotLayout[] slots;

        public VertexLayout(params InputSlotLayout[] Slots)
        {
            if (Validate(Slots))
            {
                slots = Slots;
            }
        }

        /// <summary>
        /// Create the final array of input elements using difference group of input layout sorted by its slots index
        /// </summary>
        public static InputElement[] GetElements(params InputSlotLayout[] Slots)
        {
            if (!Validate(Slots)) return null;
            int count = 0;

            foreach (var slot in Slots) count += slot.inputs.Count;
            InputElement[] elements = new InputElement[count];
            count = 0;
            for (int i = 0 ; i < Slots.Length; i++)
                for (int j = 0; j < Slots[i].inputs.Count; j++, count++)
                {
                    elements[count] = Slots[i].inputs[j];
                    elements[count].Slot = i;
                }
            return elements;
        }

        static bool Validate(params InputSlotLayout[] Slots)
        {
            if (Slots == null) throw new ArgumentNullException("requires at least one slot");
            if (Slots.Length > MAX_SLOTS_COUNT) throw new ArgumentOutOfRangeException("Directx allows maximum 16 Slots");


            return true;
        }
    }


    /// <summary>
    /// Defines the layout of one slot of vertex buffers that will be bound to the input-assembler stage.<br/>
    /// The reason why i separated the layout by slot is to facilitate the interpretation of the buffers in the instancing technique.
    /// </summary>
    /// <remarks>
    /// <i>the slot value is intentionally omitted because it will be generated during casting to InputElement.</i>
    /// </remarks>
    public class InputSlotLayout : IEquatable<InputSlotLayout>
    {
        byte instancedStep;
        int bytesize;

        internal List<InputElement> inputs;

        /// <summary>
        /// match with Stride in vertex buffer
        /// </summary>
        public int ByteSize => bytesize;

        public bool IsInstanced => instancedStep > 0;

        public int InstancedStep => instancedStep;
        /// <summary>
        /// the slot value is intentionally omitted because it will be generated during casting to InputElement
        /// </summary>
        /// <param name="instanced">the same slot require the same vertex classification (instanced and instancedstep)</param>
        /// <param name="instancedStep">the same slot require the same vertex classification(instanced and instancedstep).
        /// For instanced, must be >=1, for not-instanced must be 0 </param>
        public InputSlotLayout(byte instancedStep = 0)
        {
            this.instancedStep = instancedStep;
            inputs = new List<InputElement>(6);
        }
        public InputSlotLayout(params VertexElement[] elements) : this(0, elements) { }
        public InputSlotLayout(byte instancedStep, params VertexElement[] elements) : this(instancedStep)
        {
            foreach (var element in elements) Add(element);
        }
        
        /// <summary>
        /// please make sure the semantic name is correct, i'll not check it
        /// </summary>
        public void Add(VertexElement element)
        {
            // convert all, except AlignedByteOffset and Slot  
            var input = (InputElement)element;

            input.Classification = IsInstanced ? InputClassification.PerInstanceData : InputClassification.PerVertexData;
            input.InstanceDataStepRate = instancedStep;
            input.AlignedByteOffset = bytesize;

            bytesize += element.ByteSize;
            // to make sure it will be recalculated
            input.Slot = -1;

            inputs.Add(input);

        }
        public override int GetHashCode()
        {
            int hash = inputs.Count;
            foreach (var element in inputs)
                hash ^= inputs.GetHashCode();
            return hash;
        }

        public override bool Equals(object obj)
        {
            return Equals((InputSlotLayout)obj);
        }
        public bool Equals(InputSlotLayout other)
        {
            if (ReferenceEquals(this, other)) return true;
            if (other == null) return false;
            if (inputs.Count != other.inputs.Count) return false;

            for (int i = 0; i < inputs.Count; i++)
                if (!inputs[i].Equals(other.inputs[i])) return false;
            
            return true;
        }

        public override string ToString()
        {
            return string.Format($"elements: {inputs.Count} stride: {bytesize} {0}", IsInstanced ? "PerInstance" : "PerVertex" );
        }
    }
}
