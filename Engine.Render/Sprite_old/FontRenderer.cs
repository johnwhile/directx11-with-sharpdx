﻿using Common.Fonts;
using Common.Maths;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Engine
{
    public class FontRenderer : GraphicChild
    {
        internal FontDictionary font;
        public  List<FontBatch> Batches = new List<FontBatch>();

        FontBatch immediatedraw;

        protected Texture2 texture;
        protected SamplerStateDx trilinear;
        internal InputLayoutDx inputlayout;

        VertexShaderDx vertexshader;
        PixelShaderDx pixelshader;
        GeometryShaderDx geometryshader;
        BlendStateDx blend;
        DepthStateDx nodepth;
        RasterizerStateDx solid;
        ConstantBuffer<FontConstants> fontglobals;
        ConstantBuffer<StringConstants> stringlobals;

        public FontRenderer(D3Device device, FontDictionary font, string name = "SpriteRenderer") : base(device, name)
        {
            this.font = font;
#if DEBUG
            if (device == null) return;
#endif
            trilinear = device.SamplerStates.Trilinear;
            blend = device.BlendStates.Trasparent;
            nodepth = device.DepthStates.NoDepthTest;
            solid = device.RasterizeStates.Solid;
            
            var m_shaderfile = AppConfig.ContentPath + @"\Shaders\sprite_font.hlsl";

            vertexshader = device.VertexShaders.CreateOrGet(m_shaderfile, "VSMain");
            pixelshader = device.PixelShaders.CreateOrGet(m_shaderfile, "PSDistanceField");
            geometryshader = device.GeometryShaders.CreateOrGet( m_shaderfile, "GSMain");

            var vinput = new InputSlotLayout();
            vinput.Add(VertexElement.Create<Vector2s>("POSITION"));
            vinput.Add(VertexElement.Create<Vector2s>("TEXCOORD0"));
            vinput.Add(VertexElement.Create<Vector2s>("TEXCOORD1"));
            
            inputlayout = device.InputStates.CreateOrGet(vertexshader, vinput);

            texture = ToDispose(Texture2.New(device, font.Texture));

            fontglobals = ToDispose(ConstantBuffer<FontConstants>.Create(device));
            fontglobals.Data = new FontConstants()
            {
                viewportsize = new Vector2f(600, 800),
                texturesize = texture.Description.Size
            };

            stringlobals = ToDispose(ConstantBuffer<StringConstants>.Create(device));
            stringlobals.Data = new StringConstants()
            {
                origin = new Vector2f(0, 0),
                scale = 1.0f
            };

            immediatedraw = ToDispose(new FontBatch(this));
        }

        public void Render(D3Context context)
        {
            var currentview = context.CurrentViewport;

            var cache = fontglobals.Data;
            cache.viewportsize = new Vector2f(currentview.Width, currentview.Height);
            fontglobals.Data = cache;
            context.SetToVertexStage(fontglobals, 0);

            context.PrimitiveTopology = DxPrimitive.PointList;
            context.SetRenderState(blend);
            context.SetRenderState(nodepth);
            context.SetRenderState(solid);
            context.SetRenderState(inputlayout);
            context.SetShader(vertexshader);
            context.SetShader(geometryshader);
            context.SetShader(pixelshader);
            context.SetToPixelStage(texture, 0);
            context.SetToPixelStage(trilinear, 0);

            foreach(var batch in Batches)
            {
                context.SetBuffer(batch.DynamicVBuffer);
                
                foreach (var fontstring in batch.fontStrings)
                {
                    var strcache = stringlobals.Data;
                    strcache.origin = fontstring.Position;
                    strcache.scale = fontstring.Scale;
                    strcache.color = fontstring.Color;
                    
                    stringlobals.Data = strcache;
                    context.SetToVertexStage(stringlobals, 1);

                    context.Draw(fontstring.verticesCount, fontstring.startVertex);
                }
            }

        }

       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">maximum <see cref="SpriteChunk"/> characters</param>
        /// <returns></returns>
        public FontString CreateString(string text, int x, int y)
        {
            var str = new FontString(this, text, x, y);
            return str;
        }

        internal FontBatch CreateBatch()
        {
            var batch = ToDispose(new FontBatch(this));
            Batches.Add(batch);
            return batch;
        }

        public void DrawString(string text, int x, int y)
        {

        }

        /// <summary>
        /// Per Frame constant buffer
        /// </summary>
        struct FontConstants
        {
            /// <summary>
            /// viewport rectangle [width, height], in float for convenience
            /// </summary>
            public Vector2f viewportsize;
            /// <summary>
            /// Size of texture, in float for convenience
            /// </summary>
            public Vector2f texturesize;
        }

        /// <summary>
        /// Per Object constant buffer
        /// </summary>
        struct StringConstants
        {
            //origin point of the string, in viewport space coordinates
            public Vector2f origin;
            public float scale;
            public Vector4b color;
        };


    }
    public class FontBatch : DisposableDx
    {
        public const int Capacity = 128;

        static int counter = 0;
        int batchid = 0;
        FontRenderer owner;

        public VertexBuffer DynamicVBuffer;
        public List<FontString> fontStrings;

        internal FontBatch(FontRenderer owner) : base(null)
        {
            batchid = counter++;
            this.owner = owner;
            if (owner.Device != null)
                DynamicVBuffer = ToDispose(VertexBuffer.CreateDynamic<CharVertex>(owner.Device, Capacity));
            fontStrings = new List<FontString>();
        }

        public bool CanContainString(FontString item, out int position)
        {
            position = 0;
            //assuming sprites are sorted by its offset
            foreach(var sprite in fontStrings)
            {
                if (sprite.startVertex - position >= item.verticesCount) return true;
                position = sprite.startVertex + sprite.verticesCount;
            }
            
            return Capacity - position >= item.verticesCount;
        }

        public void AddFontString(FontString fontString, int startvertex)
        {
            //Debug.Print("Add string at " + startvertex);

            fontString.startVertex = startvertex;

            if (startvertex + fontString.verticesCount > Capacity) throw new IndexOutOfRangeException();

            fontStrings.Add(fontString);
            fontStrings.Sort((x, y) => x.startVertex.CompareTo(y.startVertex));

            DynamicVBuffer.SetData(
                owner.Device.context,
                fontString.charsvertices.ToArray(), 
                startvertex * Marshal.SizeOf<CharVertex>(),
                -1, 0, false);
        }

        public override string ToString()
        {
            return string.Format("FontBatch {0}", batchid);
        }
    }
    public class FontString
    {
        FontRenderer renderer;
        Vector2us stringsize;
        public Vector2i Position;
        public Color4b Color;

        public float Scale;
        public bool Draw;
        public Vector2us Size => stringsize;

        //offset index in the vertex buffer
        internal int startVertex = -1;
        //number of sprites
        internal int verticesCount = 0;
        //the batch that contains this instance
        internal FontBatch batch;

        public string text { get; private set; }

        internal List<CharVertex> charsvertices;

        public FontString(FontRenderer renderer, string text, int x, int y)
        {
            this.renderer = renderer;
            Position = new Vector2i(x, y);
            Color = Color4b.Black;
            Scale = 1;
            Update(text);
        }


        /// <summary>
        /// Do for each glyph in text, same function for measure string or build sprites
        /// </summary>
        /// <param name="todo">action for each glyph found</param>
        /// <returns>return the size of string</returns>
        Vector2us ForEachGlyph(string text, Action<Glyph,int,int> todo)
        {
            FontDictionary font = renderer.font;
            int xadvance = 0;
            int yadvance = 0;
            int lineHeight = font.LineHeight - font.PaddingUp - font.PaddingDown;


            foreach (char c in text)
            {
                // Skip carriage returns.
                if (c == '\r') continue;

                // New line, no char
                if (c == '\n')
                {
                    xadvance = 0;
                    yadvance += lineHeight;
                    continue;
                }

                if (font.GetGlyph(c, out var glyph))
                {
                    todo(glyph, xadvance, yadvance);
                    //not sure the padding math is correct
                    xadvance += (short)(glyph.xadvance + font.PaddingRight - font.PaddingLeft);
                }
                else
                {
                    //todo when glyph is undefined
                }
            }
            return new Vector2us(xadvance, yadvance + lineHeight);
        }

        /// <summary>
        /// Rebuild sprites
        /// </summary>
        public void Update(string text)
        {
            this.text = text;
            Remove();

            verticesCount = 0;
            charsvertices = new List<CharVertex>(text.Length);

            //action to generates sprites
            Action<Glyph, int, int> createvertices = (glyph, xadvance, yadvance) =>
            {
                //white space must be not rendered but need to calculate xadvance
                if (!glyph.IsSizeZero)
                {
                    var sprite = new CharVertex()
                    {
                        destPos = new Vector2s(
                            glyph.xoffset + xadvance,
                            glyph.yoffset + yadvance),
                        //destSize will be calcuated from sourceSize
                        sourcePos = new Vector2s(glyph.x, glyph.y),
                        sourceSize = new Vector2s(glyph.width, glyph.height)
                    };
                    charsvertices.Add(sprite);
                }
            };
            stringsize = ForEachGlyph(text, createvertices);
            
            verticesCount = charsvertices.Count;

            if (FontBatch.Capacity < verticesCount) throw new ArgumentOutOfRangeException("chunk value is too small");

            //find a batch to store this item, if not found creates a new one
            startVertex = GetOrCreateBatch(this);
            
            batch.AddFontString(this, startVertex);
        }

        public void Remove()
        {
            startVertex = 0;
            batch?.fontStrings.Remove(this);
            batch = null;
            verticesCount = 0;
        }

        /// <summary>
        /// return the first position where sprite start in the current batch
        /// </summary>
        int GetOrCreateBatch(FontString item)
        {
            foreach (var m_batch in renderer.Batches)
            {
                if (m_batch.CanContainString(item, out int offset))
                {
                    //Debug.Print("batch " + m_batch.ToString() + " can contain string " + this.ToString());
                    batch = m_batch;
                    return offset;
                }
            }

            batch = renderer.CreateBatch();
            return 0;
        }

        public override string ToString()
        {
            if (verticesCount==0) return "removed";
            return string.Format("{0} to {1} batch {2}", startVertex, startVertex + verticesCount -1, batch?.ToString());
        }
    }

    //each point rappresent a sprite quad
    internal struct CharVertex
    {
        /// <summary>
        /// POSITION : topleft corner position relative to string's origin
        /// signed short to work with pixel range [-32768 , 32767]
        /// </summary>
        public Vector2s destPos;
        /// <summary>
        /// TEXCOORD0 : topleft coordinate in the texture atlas, in pixel coordinates
        /// </summary>
        public Vector2s sourcePos;
        /// <summary>
        /// TEXCOORD1 : size of quad in the texture atlas, in pixel coordinates
        /// </summary>
        public Vector2s sourceSize;

        public override string ToString()
        {
            return String.Format("{0}|{1}|{2}", destPos.ToString(), sourcePos.ToString(), sourceSize.ToString());
        }
    }

}
