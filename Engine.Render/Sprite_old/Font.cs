﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Engine;
using Common.Maths;

using Common.Fonts;
using Common;
using System.Runtime.InteropServices;
using System.IO;


namespace Engine
{
    public class GraphicFont : GraphicChild
    {
        int capacity = 10;

        internal FontDictionary font;

        public string FontName => font.FontName;

        ViewportClip currentview;
        Texture2 texture;
        SamplerStateDx trilinear;
        InputLayoutDx inputlayout;

        VertexShaderDx vertexshader;
        PixelShaderDx pixelshader;
        GeometryShaderDx geometryshader;

        BlendStateDx blend;
        DepthStateDx nodepth;
        RasterizerStateDx solid;
        ConstantBuffer<fontConstants> fontglobals;

        VertexBuffer sprites;

        int sprite_index = 0;
        fontVertex[] tmp_sprites;

        D3Context context;

        /// <summary>
        /// </summary>
        public GraphicFont(D3Device device, FontDictionary font) : base(device, "GraphicFont[" + font.FontName + "]")
        {
            this.font = font;

            texture = ToDispose(Texture2.New(device, font.Texture));

            trilinear = device.SamplerStates.Trilinear;
            blend = device.BlendStates.Trasparent;
            nodepth = device.DepthStates.NoDepthTest;
            solid = device.RasterizeStates.Solid;

            var path = AppConfig.ContentPath + @"\Shaders\font.hlsl";

            vertexshader = device.VertexShaders.CreateOrGet(path, "VSMain");
            pixelshader = device.PixelShaders.CreateOrGet(path, "PSDistanceField");
            geometryshader = device.GeometryShaders.CreateOrGet(path, "GSMain");

            var vinput = new InputSlotLayout();
            vinput.Add(VertexElement.Create<Vector2s>("POSITION"));
            vinput.Add(VertexElement.Create<Vector2s>("TEXCOORD0"));
            vinput.Add(VertexElement.Create<Vector2s>("TEXCOORD1"));
            vinput.Add(VertexElement.Create<float>("TEXCOORD2"));
            vinput.Add(VertexElement.Create<uint>("COLOR"));

            inputlayout = device.InputStates.CreateOrGet(vertexshader, vinput);


            sprites = ToDispose(VertexBuffer.CreateDynamic<fontVertex>(device, capacity));

            fontglobals = ToDispose(ConstantBuffer<fontConstants>.Create(device));
            
            fontglobals.Data = new fontConstants() 
            { 
                viewportsize = new Vector2f(600, 800),
                texturesize = texture.Description.Size
            };


            tmp_sprites = new fontVertex[capacity];

        }

        fontVertex[] ChangeCapacity(fontVertex[] output, int size)
        {
            fontVertex[] array = new fontVertex[size];
            output.CopyTo(array, 0);
            return array;
        }

        public void BeginDraw(D3Context context)
        {
            this.context = context;
            sprite_index = 0;

        }

        public Vector2i Draw(string text, int x, int y, Vector4b color, float size = 1.0f)
        {
            Vector2i destsize = new Vector2i(0, 0);
            int begin = sprite_index;
            ForeachChar(text, ref tmp_sprites, ref sprite_index);

            for (int i = begin; i < sprite_index; i++)
            {
                tmp_sprites[i].color = color;
                tmp_sprites[i].scale = size;
                tmp_sprites[i].destPos.x = (short)(x + tmp_sprites[i].destPos.x * size);
                tmp_sprites[i].destPos.y = (short)(y + tmp_sprites[i].destPos.y * size);
            }

            return destsize;
        }

        /// <summary>
        /// End drawing and draw to context
        /// </summary>
        public void EndDraw()
        {
            if (context == null) throw new Exception("need begindraw");

            if (sprites.ElementCount < sprite_index)
            {
                RemoveAndDispose(ref sprites);
                sprites = ToDispose(VertexBuffer.CreateDynamic<fontVertex>(device, sprite_index));
            }
            sprites.SetData(context, tmp_sprites, sprite_index * Marshal.SizeOf<fontVertex>());

            currentview = context.CurrentViewport;

            var cache = fontglobals.Data;
            cache.viewportsize = new Vector2f(currentview.Width, currentview.Height);
            fontglobals.Data = cache;

            context.SetToVertexStage(fontglobals, 0);

            context.SetRenderState(blend);
            context.SetRenderState(nodepth);
            context.SetRenderState(solid);
            context.SetRenderState(inputlayout);
            context.SetShader(vertexshader);
            context.SetShader(geometryshader);
            context.SetShader(pixelshader);
            context.SetToPixelStage(texture, 0);
            context.SetToPixelStage(trilinear, 0);

            context.PrimitiveTopology = DxPrimitive.PointList;
            
            context.SetBuffer(sprites);
            context.Draw(sprite_index);

            context = null;
        }

        void ForeachChar(string text, ref fontVertex[] output, ref int outputoffset)
        {
            Vector2i Size = new Vector2i(0, 0);
            int xadvance = 0;
            int yadvance = 0;
            int lineHeight = font.LineHeightMinusPadding;

            foreach (char c in text)
            {
                if (c == '\n')
                {
                    xadvance = 0;
                    yadvance += lineHeight;
                    continue;
                }

                if (font.GetGlyph(c, out var glyph))
                {
                    var sprite = new fontVertex()
                    {
                        destPos = new Vector2s(glyph.xoffset + xadvance, glyph.yoffset + yadvance),
                        //destSize will be calcuated from sourceSize
                        sourcePos = new Vector2s(glyph.x, glyph.y),
                        sourceSize = new Vector2s(glyph.width, glyph.height)
                    };
 
                    if (outputoffset >= output.Length) 
                        output = ChangeCapacity(output, output.Length * 2);

                    output[outputoffset++] = sprite;

                    //not sure the padding math is correct
                    xadvance += (short)(glyph.xadvance - font.PaddingLeft - font.PaddingRight);
                }
            }
            Size.x = xadvance;
            Size.y = yadvance + lineHeight;
        }

        /// <summary>
        /// Per Frame constant buffer
        /// </summary>
        struct fontConstants
        {
            /// <summary>
            /// viewport rectangle [width, height], in float for convenience
            /// </summary>
            public Vector2f viewportsize;
            /// <summary>
            /// Size of texture, in float for convenience
            /// </summary>
            public Vector2f texturesize;
        }

        //each point rappresent a sprite quad
        struct fontVertex
        {
            /// <summary>
            /// POSITION : topleft corner position relative to string's origin
            /// signed short to work with pixel range [-32768 , 32767]
            /// </summary>
            public Vector2s destPos;
            /// <summary>
            /// TEXCOORD0 : topleft coordinate in the texture atlas, in pixel coordinates
            /// </summary>
            public Vector2s sourcePos;
            /// <summary>
            /// TEXCOORD1 : size of quad in the texture atlas, in pixel coordinates
            /// </summary>
            public Vector2s sourceSize;
            /// <summary>
            /// TEXCOORD2
            /// </summary>
            public float scale;
            /// <summary>
            /// COLOR
            /// </summary>
            public uint color;

            public override string ToString()
            {
                return String.Format("{0}|{1}|{2}", destPos.ToString(), sourcePos.ToString(), sourceSize.ToString());
            }
        }
    }
}
