﻿using System;

using SharpDX;
using SharpDX.Mathematics.Interop;
using D2D = SharpDX.Direct2D1;

using Common;
using Common.Maths;
using System.Drawing;

namespace Engine
{

    /// <summary>
    /// SharpDx extensions for value types
    /// </summary>
    public static class ValueTypeExtensions
    {
        #region comment
        /// <summary>
        /// extension method from my common maths to sharpdx raw structs
        /// </summary>
        const string o_o = "ineritdoc";
        #endregion

        /// <summary><inheritdoc cref="o_o"/></summary>
        public unsafe static Vector4 ToSharpDx(this Vector4f value)
        {
            return *(Vector4*)&value;
        }

        /// <summary><inheritdoc cref="o_o"/></summary>
        public unsafe static Vector3 ToSharpDx(this Vector3f value)
        {
            return *(Vector3*)&value;
        }
        /// <summary><inheritdoc cref="o_o"/></summary>
        public unsafe static Vector2 ToSharpDx(this Vector2f value)
        {
            return *(Vector2*)&value;
        }

        /// <summary><inheritdoc cref="o_o"/></summary>
        public static Vector2f ToCommon(this Size2F value)
        {
            return new Vector2f(value.Width, value.Height);
        }
        public unsafe static Size2 ToSharpDx(this Vector2i value)
        {
            return *(Size2*)&value;
        }
        public unsafe static Vector2i ToCommon(this Size2 value)
        {
            return *(Vector2i*)&value;
        }
        public static Size ToSystem(this Size2 value)
        {
            return new Size(value.Width, value.Height);
        }
        public static Size2 ToSharpDx(this Size value)
        {
            return new Size2(value.Width, value.Height);
        }


        /// <summary><inheritdoc cref="o_o"/></summary>
        public unsafe static RawVector4 ToRawVector(this Vector4f value)
        {
            return *(RawVector4*)&value;
        }
        /// <summary><inheritdoc cref="o_o"/></summary>
        public unsafe static RawVector3 ToRawVector(this Vector3f value)
        {
            return *(RawVector3*)&value;
        }
        /// <summary><inheritdoc cref="o_o"/></summary>
        public unsafe static RawVector2 ToRawVector(this Vector2f value)
        {
            return *(RawVector2*)&value;
        }
        /// <summary><inheritdoc cref="o_o"/></summary>
        public unsafe static RawColor4 ToRawColor(this Vector4f value)
        {
            return *(RawColor4*)&value;
        }
        /// <summary><inheritdoc cref="o_o"/></summary>
        public static RawColor4 ToRawColor(this Color4b value)
        {
            return new RawColor4(value.fR, value.fG, value.fB, value.fA);
        }
        /// <summary><inheritdoc cref="o_o"/></summary>
        public static RawColor4 ToRawColor(this System.Drawing.Color color)
        {
            return new RawColor4(color.R / 255.0f, color.G / 255.0f, color.B / 255.0f, color.A / 255.0f);
        }
       
        /// <summary><inheritdoc cref="o_o"/></summary>
        public unsafe static RawQuaternion ToRawQuaternion(this Quaternion4f value)
        {
            return *(RawQuaternion*)&value;
        }
        /// <summary><inheritdoc cref="o_o"/></summary>
        public unsafe static Quaternion4f ToCommon(this RawQuaternion value)
        {
            return *(Quaternion4f*)&value;
        }
        /// <summary>
        /// <inheritdoc cref="o_o"/><br/>
        /// <b>Directx matrix is trasposed</b>
        /// </summary>
        public unsafe static RawMatrix ToRawMatrix(this Matrix4x4f value)
        {
            Matrix4x4f matrix = value;
            matrix.Traspose();
            return *(RawMatrix*)&matrix;
        }
        /// <summary>
        /// <inheritdoc cref="o_o"/><br/>
        /// <b>Directx matrix is trasposed</b>
        /// </summary>
        public unsafe static Matrix4x4f ToCommon(this RawMatrix value)
        {
            Matrix4x4f matrix = *(Matrix4x4f*)&value;
            matrix.Traspose();
            return matrix;
        }

        /// <summary><inheritdoc cref="o_o"/></summary>
        public static RawRectangleF ToRawRectangleF(this Rectangle4f rect)
        {
            return new RawRectangleF(rect.x, rect.y, rect.x + rect.width, rect.y + rect.height);
        }
        /// <summary><inheritdoc cref="o_o"/></summary>
        public static Rectangle4f ToCommon(this RawRectangleF rect)
        {
            throw new NotImplementedException("not tested");
            //return new Rectangle4f(rect.Left, rect.Top, rect.Right - rect.Left, rect.Top - rect.Bottom);
        }
        /// <summary><inheritdoc cref="o_o"/></summary>
        public static RawRectangleF ToRawRectangleF(this Rectangle4i rect)
        {
            return new RawRectangleF(rect.x, rect.y, rect.x + rect.width, rect.y + rect.height);
        }

        public static RawRectangle ToRawRectangle( this Rectangle4i rect)
        {
            return new RawRectangle(rect.Left, rect.Top, rect.Right, rect.Bottom);
        }
        public unsafe static Rectangle4i ToCommon(this RawRectangle rect)
        {
            return new Rectangle4i(rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top);
        }

        /// <summary><inheritdoc cref="o_o"/></summary>
        public unsafe static RawViewport ToRawViewportInt(this ViewportClip value)
        {
            return *(RawViewport*)(&value);
        }
        public static RawViewportF ToRawViewportFloat(this ViewportClip value)
        {
            return new RawViewportF()
            {
                X = value.X,
                Y = value.Y,
                Width = value.Width,
                Height = value.Height,
                MinDepth = value.MinDepth,
                MaxDepth = value.MaxDepth
            };
        }
        public static ViewportClip ToCommon(this RawViewportF value)
        {
            return new ViewportClip(value.X, value.Y, value.Width, value.Height, value.MinDepth, value.MaxDepth);
        }

        /// <summary><inheritdoc cref="o_o"/></summary>
        public unsafe static ViewportClip ToCommon(this RawViewport value)
        {
            return *(ViewportClip*)(&value);
        }

        public unsafe static RawViewportF ToRawViewportFloat(this Viewport6f value)
        {
            return *(RawViewportF*)(&value);
        }

    }
}