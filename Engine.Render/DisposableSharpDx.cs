﻿using System;
using System.Diagnostics;

using Common;
using SharpDX;
using SharpDX.Direct3D11;

namespace Engine
{
    /// <summary>
    /// A disposable extension class that contain a debug name.
    /// The base class <see cref="Disposable"/> provides a list of disposable dependent objects
    /// </summary>
    public class DisposableDx : Disposable
    {
        protected string name;

        /// <summary>
        /// The complete debug name formatted as Hash:Name
        /// </summary>
        public string NameFull => name;
        /// <summary>
        /// The debug name without Hash string
        /// </summary>
        public virtual string Name
        {
            get
            {
                if (name != null)
                {
                    int i = name.IndexOf(':');
                    if (i >= 0) return name.Substring(i + 1);
                }
                return name;
            }
            set => name = ImmutableHash.ToString("X4") + ':' + value.Replace(':', '.');
        }
        /// <summary>
        /// Immutable Hash value generated at class constructor 
        /// </summary>
        public readonly int ImmutableHash;

        protected internal DisposableDx(string debugname = null)
        {
            ImmutableHash = base.GetHashCode();
            if (string.IsNullOrEmpty(debugname)) debugname = GetType().Name;
            Name = debugname;
        }

        /// <summary>
        /// return true if they are the same sharpdx reference by comparing the pointer
        /// </summary>
        public static bool CheckSameRef(CppObject A, CppObject B)
        {
            IntPtr aptr = (A == null) ? IntPtr.Zero : A.NativePointer;
            IntPtr bptr = (B == null) ? IntPtr.Zero : B.NativePointer;
            return aptr == bptr;
        }

        /// <summary>
        /// sintetic expression: return false if object is null or disposed
        /// </summary>
        public static bool IsValid(CppObject comobj) => comobj != null && !comobj.IsDisposed;

        /// <summary>
        /// The Disposing override for native sharpdx object applies also a debugname
        /// </summary>
        public T ToDispose<T>(T obj, string native_debugname)
        {
            if (obj is CppObject native)
            {
                native.Tag = name;
                if (obj is DeviceChild devicechild) devicechild.DebugName = native_debugname;
#if DEBUGMORE
                native.Disposing += dxObjectDisposing;
#endif
            }
            return base.ToDispose(obj);
        }


        void dxObjectDisposing(object sender, EventArgs e)
        {
            string _name = "";
            if (sender is CppObject native)
            {
                _name = native.ToString();
                native.Disposing -= dxObjectDisposing;
            }
            Debugg.Message($"native disposing: {_name}", 1);
        }

        /// <summary>
        /// When disposing, first dispose all collected disposable objects, then call its dispose instruction
        /// <code>
        /// protected override void Dispose(bool disposemanaged)
        /// {
        ///     base.Dispose(disposemanaged);
        ///     //write here dispose instructions
        /// }</code>
        /// </summary>
        protected override void Dispose(bool disposemanaged)
        {
#if DEBUGMORE
            Debugg.Print($"disposing: {Name}");
#endif
            base.Dispose(disposemanaged);
        }


        /// <summary>
        /// Use the IUnknown inteface instead Dispose to release sharpdx object and set reference to null.
        /// <inheritdoc cref="RemoveAndDispose{T}(T)"/>
        /// </summary>
        protected void ReleaseComObject<T>(ref T comobject) where T: ComObject
        {
            if (comobject == null || comobject.IsDisposed) return;

            comobject.Dispose();
            comobject = null;
            return;

            /*
            if (comobject is IUnknown unknown)
            {
                int refcount = unknown.Release();
                if (refcount > 0) Debugg.Print($"the sharpdx com object \"{comobject.GetType().Name}\" has pending resources = {refcount}", DebugInfo.Error);
                comobject = null;
            }
            else
            {
                Debugg.Print($"\"{Name}\" is not a shardx com object", DebugInfo.Error);
            }
            */
        }

        public override string ToString()
        {
            return Name;
        }

        public override int GetHashCode()
        {
            return ImmutableHash;
        }

        /// <summary>
        /// GC.Collect() call the finalizer at some point of life...
        /// </summary>
        ~DisposableDx()
        {
            if (!IsDisposed)
            {
                //this should never happen
                Debugg.Error($"Safety finalizing : {Name} hash:{GetHashCode().ToString("x4")}");
                Dispose();
            }
        }
    }
}
