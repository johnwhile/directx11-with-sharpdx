﻿using System;
using System.IO;
using Common;
using SharpDX.Direct3D11;


namespace Engine
{
    /// <summary>
    /// Base class for texture resources.
    /// </summary>
    public abstract class TextureBase : D3Resource
    {
        /// <summary>
        /// check the true value
        /// </summary>
        public const int MAX_TEXTURE_SLOTS = 16;

        internal protected enum SurfaceType
        {
            Texture,
            RenderTarget,
            SwapChainRenderTarget,
        }
        protected ShaderResourceView resourceViewer;

        /// <summary>
        /// The depth stride in bytes (number of bytes per depth slice).
        /// </summary>
        //internal readonly int DepthStride;
        /// <summary>
        /// The width stride in bytes (number of bytes per row).
        /// </summary>
        internal readonly int RowStride;
        /// <summary>
        /// Common description for this texture.
        /// </summary>
        public readonly TextureDescription Description;


        protected TextureBase(D3Device device, TextureDescription description, string debugname = null) : base(device, debugname)
        {
            Description = description;
            //RowStride = Description.Width * ((PixelFormat)Description.Format).SizeInBytes;
            RowStride = Description.Size.width * SharpDX.DXGI.FormatHelper.SizeOfInBytes(Description.Format);
        }

        /// <summary>
        /// Apply to vertex or pixel shader
        /// </summary>
        /// <param name="slot"></param>
        public void BindToShaderStage(CommonShaderStage shader, int slot)
        {
            if (slot >= MAX_TEXTURE_SLOTS) throw new ArgumentOutOfRangeException("Directx can use maximum " + MAX_TEXTURE_SLOTS + " slots for texture");
            if (!Description.BindFlags.HasFlag(BindFlags.ShaderResource)) throw new Exception("The texture is not created to shader's use");
            shader.SetShaderResource(slot, resourceViewer);
        }

        internal static BindFlags GetBindFlagsFromTextureFlags(TextureFlags flags)
        {
            var bindFlags = BindFlags.None;
            if (flags.HasFlag(TextureFlags.ShaderResource)) bindFlags |= BindFlags.ShaderResource;
            if (flags.HasFlag(TextureFlags.UnorderedAccess)) bindFlags |= BindFlags.UnorderedAccess;
            if (flags.HasFlag(TextureFlags.RenderTarget)) bindFlags |= BindFlags.RenderTarget;
            return bindFlags;
        }
        /// <summary>
        /// Calculates the mip map count from a requested level.
        /// </summary>
        /// <param name="requestedLevel">The requested level.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="depth">The depth.</param>
        /// <returns>The resulting mipmap count (clamp to [1, maxMipMapCount] for this texture)</returns>
        internal static int CalculateMipMapCount(MipMapCount requestedLevel, int width, int height = 0, int depth = 0)
        {
            int size = Math.Max(Math.Max(width, height), depth);
            int maxMipMap = 1 + (int)Math.Log(size, 2);

            return requestedLevel == 0 ? maxMipMap : Math.Min(requestedLevel, maxMipMap);
        }


        protected void SetShaderResourceView()
        {
            if (Description.BindFlags.HasFlag(BindFlags.ShaderResource))
            {
                RemoveAndDispose(ref resourceViewer);
                resourceViewer = ToDispose(new ShaderResourceView(device, Resource));
                resourceViewer.DebugName = "ShaderResourceView_of_" + ImmutableHash.ToString("X4");
            }
        }
    }
}
