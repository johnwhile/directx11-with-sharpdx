﻿using System;

using Common;
using Common.Maths;
using SharpDX.Direct3D11;
using DX = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;

namespace Engine
{
    /// <summary>
    /// 2D texture.
    /// </summary>
    public class Texture2 : TextureBase
    {
        public Texture2D texture;
        protected override Resource Resource => texture;
        public override IntPtr NativePointer => texture != null ? texture.NativePointer : IntPtr.Zero;
        public Vector2i Size => new Vector2i(texture.Description.Width, texture.Description.Height);
        public PixelFormat Format => (PixelFormat)texture.Description.Format; 
        
        protected internal Texture2(D3Device device, Texture2DDescription description, string debugname = null)
            : base(device, description, debugname)
        {
            texture = ToDispose(new Texture2D(device, description));
            texture.DebugName = "Texture2D_of_" + ImmutableHash.ToString("X4");
            SetShaderResourceView();
        }
        protected internal Texture2(D3Device device, Texture2D dxtexture, string debugname = null)
            : base(device, dxtexture.Description, debugname)
        {
            texture = ToDispose(dxtexture);
            texture.DebugName = "Texture2D_of_" + ImmutableHash.ToString("X4");
            SetShaderResourceView();
        }

        /// <remarks>remember to assign the correct slot in the pixel shader code <code>register(b[slot])</code></remarks>
        public void ApplyPS(int slot = 0)
        {
            BindToShaderStage(device.Context.d3contex.PixelShader, slot);
        }
        public void ApplyVS(int slot = 0)
        {
            BindToShaderStage(device.Context.d3contex.VertexShader, slot);
        }

        /// <summary>
        /// Creates a new texture from a <see cref="Texture2DDescription"/>.
        /// </summary>
        public static Texture2 New(D3Device device, TextureDescription description)
        {
            return new Texture2(device, description);
        }

        /// <summary>
        /// </summary>
        /// <param name="mipmaps">generate all mipleves</param>
        public static Texture2 New(D3Device device, string filename, bool mipmaps = false)
        {
            return new Texture2(device, TextureLoader.CreateTexture2DFromFile(device, filename, mipmaps));
        }


        protected static Texture2DDescription NewDescription(Vector2i size, PixelFormat format, TextureFlags textureFlags, int mipCount, int arraySize, ResourceUsage usage)
        {
            if ((textureFlags & TextureFlags.UnorderedAccess) != 0)
                usage = ResourceUsage.Default;

            var desc = new Texture2DDescription()
            {
                Width = size.width,
                Height = size.height,
                ArraySize = arraySize,
                SampleDescription = new DXGI.SampleDescription(1, 0),
                BindFlags = GetBindFlagsFromTextureFlags(textureFlags),
                Format = (DXGI.Format)format,
                MipLevels = CalculateMipMapCount(mipCount, size.width, size.height),
                Usage = usage,
                CpuAccessFlags = GetCpuAccess(usage),
                OptionFlags = ResourceOptionFlags.None
            };


            // If the texture is a RenderTarget + ShaderResource + MipLevels > 1, then allow for GenerateMipMaps method
            if ((desc.BindFlags & BindFlags.RenderTarget) != 0 && (desc.BindFlags & BindFlags.ShaderResource) != 0 && desc.MipLevels > 1)
            {
                desc.OptionFlags |= ResourceOptionFlags.GenerateMipMaps;
            }

            return desc;
        }

        /// <summary>
        /// <see cref="DXGI.Surface"/> casting operator. Remember to release this surface.
        /// </summary>
        public static implicit operator DXGI.Surface(Texture2 from)
        {
            if (from == null || from.texture==null) throw new NullReferenceException("DXGI.Surface casting from null reference");
            
            // Don't bother with multithreading here
            var dxgisurface = from.texture.QueryInterface<DXGI.Surface>();
            dxgisurface.DebugName = "dxgisurface_of_" + from.ImmutableHash.ToString("X4");
            return dxgisurface;
        }
    }
}
