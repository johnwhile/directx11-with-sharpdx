﻿using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.WIC;

using DX = SharpDX.Direct3D11;
using D2D = SharpDX.Direct2D1;
using PixelFormat = SharpDX.WIC.PixelFormat;

using Engine.Direct2D;
using System;

namespace Engine
{
    public static class TextureLoader
    {
        private static readonly ImagingFactory Imgfactory = new ImagingFactory();


        public static D2D.Bitmap LoadBitmap(string filename, D2WindowTarget wintarget)
        {
            var props = new BitmapProperties
            {
                PixelFormat = new D2D.PixelFormat(Format.R8G8B8A8_UNorm, D2D.AlphaMode.Premultiplied)
            };
            return D2D.Bitmap.FromWicBitmap(wintarget, LoadBitmap(filename), props);
        }
        public static Bitmap1 LoadBitmap(string filename, D2Context context, bool canread = false)
        {
            var props = new BitmapProperties1 
            { 
                PixelFormat = new D2D.PixelFormat(Format.R8G8B8A8_UNorm, D2D.AlphaMode.Premultiplied) 
            };
            if (canread) props.BitmapOptions |= BitmapOptions.CpuRead;
            return Bitmap1.FromWicBitmap(context, LoadBitmap(filename), props);
        }

        public static BitmapSource LoadBitmap(string filename)
        {
            if (filename == null) throw new ArgumentNullException();
            var d = new BitmapDecoder(Imgfactory, filename, DecodeOptions.CacheOnDemand);
            //gif has more frames
            var frame = d.GetFrame(0);
            var fconv = new FormatConverter(Imgfactory);
            fconv.Initialize( frame, SharpDX.WIC.PixelFormat.Format32bppPRGBA, BitmapDitherType.None, null, 0.0, BitmapPaletteType.Custom);
            return fconv;
        }

        public static Texture2D CreateTexture2DFromFile(DX.Device device, string filename, bool mipmaps = false)
        {
            var bSource = LoadBitmap(filename);
            return CreateTexture2DFromBitmap(device, bSource, mipmaps);
        }

        public static Texture2D CreateTexture2DFromBitmap(DX.Device device, BitmapSource bsource, bool mipmaps = false)
        {

            Texture2DDescription desc;
            desc.Width = bsource.Size.Width;
            desc.Height = bsource.Size.Height;
            desc.ArraySize = 1;
            desc.BindFlags = BindFlags.ShaderResource;
            desc.Usage = ResourceUsage.Default;
            desc.CpuAccessFlags = CpuAccessFlags.None;
            desc.Format = Format.R8G8B8A8_UNorm;
            desc.MipLevels = mipmaps ? 0 : 1;
            desc.OptionFlags = ResourceOptionFlags.None;
            desc.SampleDescription.Count = 1;
            desc.SampleDescription.Quality = 0;

            var s = new DataStream(bsource.Size.Height * bsource.Size.Width * 4, true, true);
            bsource.CopyPixels(bsource.Size.Width * 4, s);

            var rect = new DataRectangle(s.DataPointer, bsource.Size.Width * 4);

            var t2D = new DX.Texture2D(device, desc, rect);

            return t2D;
        }
    }
}
