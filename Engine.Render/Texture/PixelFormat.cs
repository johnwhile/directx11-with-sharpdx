﻿using System;
using System.Runtime.InteropServices;


using DXGI = SharpDX.DXGI;

namespace Engine
{
    /// <summary>
    /// Defines the format of data in a depth-stencil buffer.
    /// </summary>
    public enum DepthFormat
    {
        /// <summary>
        /// No depth stencil buffer.
        /// </summary>
        None = 0,

        /// <summary>
        /// A buffer that contains 16-bits of depth data.
        /// </summary>
        Depth16 = DXGI.Format.D16_UNorm,

        /// <summary>
        /// A 32 bit buffer that contains 24 bits of depth data and 8 bits of stencil data.
        /// </summary>	
        Depth24Stencil8 = DXGI.Format.D24_UNorm_S8_UInt,

        /// <summary>
        /// A buffer that contains 32-bits of depth data.
        /// </summary>
        Depth32 = DXGI.Format.D32_Float,

        /// <summary>
        /// A double 32 bit buffer that contains 32 bits of depth data and 8 bits padded with 24 zero bits of stencil data.
        /// </summary>
        Depth32Stencil8X24 = DXGI.Format.D32_Float_S8X24_UInt,
    }
    /// <summary>
    /// Defines the format of data in a backbuffer.
    /// </summary>
    public enum PixelFormat
    {
        Unknown = DXGI.Format.Unknown,
        B8G8R8A8_UNorm = DXGI.Format.B8G8R8A8_UNorm,
        B8G8R8X8_UNorm = DXGI.Format.B8G8R8X8_UNorm,
        R8G8B8A8_UNorm = DXGI.Format.R8G8B8A8_UNorm,
        R32G32B32_Float = DXGI.Format.R32G32B32_Float,
        R32G32B32_SInt = DXGI.Format.R32G32B32_SInt,
        R32G32B32_UInt = DXGI.Format.R32G32B32_UInt,
        R32G32B32A32_Float = DXGI.Format.R32G32B32_Float,
        R32G32B32A32_SInt = DXGI.Format.R32G32B32_SInt,
        R32G32B32A32_UInt = DXGI.Format.R32G32B32_UInt,
        R32_Typeless = DXGI.Format.R32_Typeless
    }

    /*

    /// <summary>
    /// DepthFormat is equivalent to <see cref="SharpDX.DXGI.Format"/>.
    /// Defines the format of data in a depth-stencil buffer.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 4)]
    public struct DepthFormat : IEquatable<DepthFormat>
    {
        /// <summary>
        /// Gets the value as a <see cref="SharpDX.DXGI.Format"/> enum.
        /// </summary>
        public readonly Format Value;

        private DepthFormat(Format format)
        {
            Value = format;
        }

        public int SizeInBytes { get { return FormatHelper.SizeOfInBytes(Value); } }

        /// <summary>
        /// No depth stencil buffer.
        /// </summary>
        public static readonly DepthFormat None = new DepthFormat(Format.Unknown);
        /// <summary>
        /// A buffer that contains 16-bits of depth data.
        /// </summary>  
        public static readonly DepthFormat Depth16 = new DepthFormat(Format.D16_UNorm);

        /// <summary>
        /// A 32 bit buffer that contains 24 bits of depth data and 8 bits of stencil data.
        /// </summary>       
        public static readonly DepthFormat Depth24Stencil8 = new DepthFormat(Format.D24_UNorm_S8_UInt);

        /// <summary>
        /// A buffer that contains 32-bits of depth data.
        /// </summary>  
        public static readonly DepthFormat Depth32 = new DepthFormat(Format.D32_Float);


        public static implicit operator Format(DepthFormat from) { return from.Value; }
        public static implicit operator DepthFormat(Format from) { return new DepthFormat(from); }
        public static bool operator ==(DepthFormat left, DepthFormat right) { return left.Equals(right); }
        public static bool operator !=(DepthFormat left, DepthFormat right) { return !left.Equals(right); }
        public bool Equals(DepthFormat other) { return Value == other.Value; }
        public override string ToString() { return string.Format("{0}={1}", Value, (int)Value); }
        public override int GetHashCode() { return Value.GetHashCode(); }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is DepthFormat && Equals((DepthFormat)obj);
        }


    }

    /// <summary>
    /// PixelFormat is equivalent to <see cref="SharpDX.DXGI.Format"/>.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Size = 4)]
    public struct PixelFormat : IEquatable<PixelFormat>
    {
        /// <summary>
        /// Gets the value as a <see cref="SharpDX.DXGI.Format"/> enum.
        /// </summary>
        public readonly Format Value;

        private PixelFormat(Format format)
        {
            Value = format;
        }

        public int SizeInBytes { get { return FormatHelper.SizeOfInBytes(Value); } }

        public static readonly PixelFormat Unknown = new PixelFormat(Format.Unknown);
        public static readonly PixelFormat B8G8R8A8_UNorm = new PixelFormat(Format.B8G8R8A8_UNorm);
        public static readonly PixelFormat B8G8R8X8_UNorm = new PixelFormat(Format.B8G8R8X8_UNorm);
        public static readonly PixelFormat R8G8B8A8_UNorm = new PixelFormat(Format.R8G8B8A8_UNorm);
        public static readonly PixelFormat R32G32B32_Float = new PixelFormat(Format.R32G32B32_Float);
        public static readonly PixelFormat R32G32B32_SInt = new PixelFormat(Format.R32G32B32_SInt);
        public static readonly PixelFormat R32G32B32_UInt = new PixelFormat(Format.R32G32B32_UInt);
        public static readonly PixelFormat R32G32B32A32_Float = new PixelFormat(Format.R32G32B32_Float);
        public static readonly PixelFormat R32G32B32A32_SInt = new PixelFormat(Format.R32G32B32_SInt);
        public static readonly PixelFormat R32G32B32A32_UInt = new PixelFormat(Format.R32G32B32_UInt);

        public static implicit operator Format(PixelFormat from)
        {
            return from.Value;
        }
        public static implicit operator PixelFormat(Format from)
        {
            return new PixelFormat(from);
        }
        public static implicit operator PixelFormat(SharpDX.Direct2D1.PixelFormat from)
        {
            return new PixelFormat(from.Format);
        }
        public static bool operator ==(PixelFormat left, PixelFormat right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(PixelFormat left, PixelFormat right)
        {
            return !left.Equals(right);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is PixelFormat && Equals((PixelFormat)obj);
        }
        public bool Equals(PixelFormat other)
        {
            return Value == other.Value;
        }
        public override string ToString()
        {
            return string.Format("{0}={1}", Value, (int)Value);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
    */
}
