﻿
using System;

using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Common;
using System.Diagnostics;

using DXGI = SharpDX.DXGI;
using Common.Maths;

namespace Engine
{
    public class DepthStencilBuffer : Texture2
    {
        DepthStencilView dephstencilview;

        public DepthStencilBuffer(D3Device Device, Texture2DDescription Description, DepthFormat depthFormat) : base(Device, Description)
        {
            dephstencilview = ToDispose(new DepthStencilView(Device, texture), "Depthstencilview_of_" + ImmutableHash.ToString("X4"));
        }
        /// <summary>
        /// Creates a new <see cref="DepthStencilBuffer" /> using multisampling.
        /// </summary>
        /// <param name="multiSampleCount">The multisample count.</param>
        /// <param name="format">Describes the format to use.</param>
        /// <param name="arraySize">Size of the texture 2D array, default to 1.</param>
        /// <param name="shaderResource">if set to <c>true</c> this depth stencil buffer can be set as an input to a shader (default: false).</param>
        public static DepthStencilBuffer New(D3Device Device, Vector2i size, MSAALevel multiSampleCount, DepthFormat format, bool shaderResource = false, int arraySize = 1)
        {
            return new DepthStencilBuffer(Device, NewDepthStencilBufferDescription(Device, size, format, multiSampleCount, arraySize, shaderResource), format);
        }

        protected static Texture2DDescription NewDepthStencilBufferDescription(
            D3Device device, Vector2i size, DepthFormat format, MSAALevel multiSampleCount, int arraySize, bool shaderResource)
        {
            var desc = NewDescription(size, PixelFormat.Unknown, TextureFlags.None, 1, arraySize, ResourceUsage.Default);
            desc.BindFlags |= BindFlags.DepthStencil;
            
            if (shaderResource)
            {
                desc.BindFlags |= BindFlags.ShaderResource;
            }

            // Sets the MSAALevel
            int maximumMSAA = (int)device.Features[format].MSAALevelMax;
            desc.SampleDescription.Count = Math.Max(1, Math.Min((int)multiSampleCount, maximumMSAA));

            var typelessDepthFormat = (Format)format;

            // If shader resource view are not required, don't use a TypeLess format
            if (shaderResource)
            {
                // Determine TypeLess Format and ShaderResourceView Format
                switch (format)
                {
                    case DepthFormat.Depth16:
                        typelessDepthFormat = DXGI.Format.R16_Typeless;
                        break;
                    case DepthFormat.Depth32:
                        typelessDepthFormat = DXGI.Format.R32_Typeless;
                        break;
                    case DepthFormat.Depth24Stencil8:
                        typelessDepthFormat = DXGI.Format.R24G8_Typeless;
                        break;
                    case DepthFormat.Depth32Stencil8X24:
                        typelessDepthFormat = DXGI.Format.R32G8X24_Typeless;
                        break;
                    default:
                        throw new InvalidOperationException(string.Format("Unsupported DepthFormat [{0}] for DEPTH DynamicVBuffer", format));
                }
            }

            desc.Format = typelessDepthFormat;

            return desc;
        }


        /// <summary>
        /// DepthStencilView casting operator, can be null.
        /// </summary>
        public static implicit operator DepthStencilView(DepthStencilBuffer from)
        {
            //return from == null ? throw new NullReferenceException("DepthStencilView casting from null reference") :  from.dephstencilview;
            return from?.dephstencilview;
        }
    }
}
