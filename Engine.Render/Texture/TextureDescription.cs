﻿using System;
using System.Runtime.InteropServices;

using SharpDX.Direct3D11;
using SharpDX.DXGI;

using Common.Maths;

namespace Engine
{
    /// <summary>
    /// Specifies usage of a texture.
    /// </summary>
    [Flags]
    public enum TextureFlags
    {
        /// <summary>
        /// None.
        /// </summary>
        None = 0,

        /// <summary>
        /// The texture will be used as a <see cref="SharpDX.Direct3D11.ShaderResourceView"/>.
        /// </summary>
        ShaderResource = 1,

        /// <summary>
        /// The texture will be used as a <see cref="SharpDX.Direct3D11.RenderTargetView"/>.
        /// </summary>
        RenderTarget = 2,

        /// <summary>
        /// The texture will be used as an <see cref="SharpDX.Direct3D11.UnorderedAccessView"/>.
        /// </summary>
        UnorderedAccess = 4,
    }
    
    public enum TextureDimension
    {
        Texture1D,
        Texture2D,
        Texture3D,
        TextureCube,
    }
    /// <summary>
    /// A Common description for all textures.
    /// </summary>
    /// <remarks>
    /// This class exposes the union of all fields exposed by fields in <see cref="SharpDX.Direct3D11.Texture1DDescription"/>, 
    /// <see cref="SharpDX.Direct3D11.Texture2DDescription"/>, <see cref="SharpDX.Direct3D11.Texture3DDescription"/>.
    /// It provides also 2-way implicit conversions for 1D, 2D, 3D textures descriptions.
    /// </remarks>
    [StructLayout(LayoutKind.Sequential)]
    public struct TextureDescription : IEquatable<TextureDescription>
    {
        /// <summary>
        /// The dimension of a texture.
        /// </summary>
        public TextureDimension Dimension;

        /// <summary>
        /// <para><b>Texture width (in texels).</b><br/>
        /// The  range is from 1 to <see cref="SharpDX.Direct3D11.Resource.MaximumTexture1DSize"/> (16384).<br/>
        /// 
        /// This field is valid for all textures: <see cref="Texture1D"/>, <see cref="Texture2D"/>, <see cref="Texture3D"/> and <see cref="TextureCube"/>.</para>
        /// <para><b>Texture height (in texels).</b><br/> 
        /// The  range is from 1 to <see cref="SharpDX.Direct3D11.Resource.MaximumTexture3DSize"/> (2048).<br/>
        /// This field is only valid for <see cref="Texture2D"/>, <see cref="Texture3D"/> and <see cref="Texture3D"/>.
        /// </para>
        /// <para>However, the range is actually constrained by the feature level at which you create the rendering device.<br/></para> 
        /// </summary>
        public Vector2i Size;

        /// <summary>	
        /// <dd> <p>Texture depth (in texels). The  range is from 1 to <see cref="SharpDX.Direct3D11.Resource.MaximumTexture3DSize"/> (2048).
        /// However, the range is actually constrained by the feature level at which you create the rendering device. </p> </dd>	
        /// </summary>	
        /// <remarks>
        /// This field is only valid for <see cref="Texture3D"/>.
        /// </remarks>
        public int Depth;

        /// <summary>	
        /// <dd> <p>Number of textures in the array. The  range is from 1 to <see cref="SharpDX.Direct3D11.Resource.MaximumTexture1DArraySize"/> (2048).
        /// However, the range is actually constrained by the feature level at which you create the rendering device. </p> </dd>	
        /// </summary>	
        /// <remarks>
        /// This field is only valid for <see cref="Texture1D"/>, <see cref="SharpDX.Direct3D11.Texture2D"/> and <see cref="TextureCube"/>
        /// </remarks>
        public int ArraySize;

        /// <summary>	
        /// <dd> <p>The maximum number of mipmap levels in the texture. See the remarks in <strong><see cref="SharpDX.Direct3D11.ShaderResourceViewDescription.Texture1DResource"/></strong>.
        /// Use 1 for a multisampled texture; or 0 to generate a full set of subtextures.</p> </dd>	
        /// </summary>	
        public int MipLevels;

        public Format Format;

        /// <summary>	
        /// <dd> <p>Structure that specifies multisampling parameters for the texture. See <strong><see cref="SharpDX.DXGI.SampleDescription"/></strong>.</p> </dd>	
        /// </summary>	
        /// <remarks>
        /// This field is only valid for <see cref="SharpDX.Direct3D11.Texture2D"/>.
        /// </remarks>
        public SampleDescription SampleDescription;

        /// <summary>	
        /// <dd> <p>Value that identifies how the texture is to be read from and written to.
        /// The most common value is <see cref="SharpDX.Direct3D11.ResourceUsage.Default"/>;
        /// see <strong><see cref="SharpDX.Direct3D11.ResourceUsage"/></strong> for all possible values.</p> </dd>	
        /// </summary>	
        public ResourceUsage Usage;

        /// <summary>	
        /// <dd> <p>Flags (see <strong><see cref="SharpDX.Direct3D11.BindFlags"/></strong>) for binding to pipeline stages.
        /// The flags can be combined by a logical OR. For a 1D texture, the allowable values are: <see cref="SharpDX.Direct3D11.BindFlags.ShaderResource"/>,
        /// <see cref="SharpDX.Direct3D11.BindFlags.RenderTarget"/> and <see cref="SharpDX.Direct3D11.BindFlags.DepthStencil"/>.</p> </dd>	
        /// </summary>	
        public BindFlags BindFlags;

        /// <summary>	
        /// <dd> <p>Flags (see <strong><see cref="SharpDX.Direct3D11.CpuAccessFlags"/></strong>) to specify the types of CPU access allowed.
        /// Use 0 if CPU access is not required. These flags can be combined with a logical OR.</p> </dd>	
        /// </summary>	
        public CpuAccessFlags CpuAccessFlags;

        /// <summary>	
        /// <dd> <p>Flags (see <strong><see cref="SharpDX.Direct3D11.ResourceOptionFlags"/></strong>) that identify other, 
        /// less common resource options. Use 0 if none of these flags apply. These flags can be combined with a logical OR.</p> </dd>	
        /// </summary>	
        public ResourceOptionFlags OptionFlags;

        /// <summary>
        /// Gets the staging description for this instance..
        /// </summary>
        /// <returns>A Staging description</returns>
        public TextureDescription ToStagingDescription()
        {
            var copy = this;
            copy.BindFlags = BindFlags.None;
            copy.CpuAccessFlags = CpuAccessFlags.Read | CpuAccessFlags.Write;
            copy.Usage = ResourceUsage.Staging;
            copy.OptionFlags = copy.Dimension == TextureDimension.TextureCube ? ResourceOptionFlags.TextureCube : ResourceOptionFlags.None;
            return copy;
        }

        public bool Equals(TextureDescription other)
        {
            return Dimension.Equals(other.Dimension) && Size == other.Size && Depth == other.Depth && ArraySize == other.ArraySize && MipLevels == other.MipLevels && Format.Equals(other.Format) && SampleDescription.Equals(other.SampleDescription) && Usage.Equals(other.Usage) && BindFlags.Equals(other.BindFlags) && CpuAccessFlags.Equals(other.CpuAccessFlags) && OptionFlags.Equals(other.OptionFlags);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            return obj is TextureDescription && Equals((TextureDescription)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Dimension.GetHashCode();
                hashCode = (hashCode * 397) ^ Size.GetHashCode();
                hashCode = (hashCode * 397) ^ Depth;
                hashCode = (hashCode * 397) ^ ArraySize;
                hashCode = (hashCode * 397) ^ MipLevels;
                hashCode = (hashCode * 397) ^ Format.GetHashCode();
                hashCode = (hashCode * 397) ^ SampleDescription.GetHashCode();
                hashCode = (hashCode * 397) ^ Usage.GetHashCode();
                hashCode = (hashCode * 397) ^ BindFlags.GetHashCode();
                hashCode = (hashCode * 397) ^ CpuAccessFlags.GetHashCode();
                hashCode = (hashCode * 397) ^ OptionFlags.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(TextureDescription left, TextureDescription right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(TextureDescription left, TextureDescription right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// Performs an explicit conversion from <see cref="Texture2DDescription"/> to <see cref="TextureDescription"/>.
        /// </summary>
        public static implicit operator TextureDescription(Texture1DDescription description)
        {
            return new TextureDescription
            {
                Dimension = TextureDimension.Texture1D,
                Size = new Vector2i(description.Width, 1),
                Depth = 1,
                MipLevels = description.MipLevels,
                ArraySize = description.ArraySize,
                Format = description.Format,
                SampleDescription = new SampleDescription(1, 0),
                Usage = description.Usage,
                BindFlags = description.BindFlags,
                CpuAccessFlags = description.CpuAccessFlags,
                OptionFlags = description.OptionFlags,
            };
        }

        /// <summary>
        /// Performs an explicit conversion from <see cref="TextureDescription"/> to <see cref="Texture2DDescription"/>.
        /// </summary>
        public static implicit operator Texture1DDescription(TextureDescription description)
        {
            return new Texture1DDescription
            {
                Width = description.Size.x,
                MipLevels = description.MipLevels,
                ArraySize = description.ArraySize,
                Format = description.Format,
                Usage = description.Usage,
                BindFlags = description.BindFlags,
                CpuAccessFlags = description.CpuAccessFlags,
                OptionFlags = description.OptionFlags,
            };
        }

        /// <summary>
        /// Performs an explicit conversion from <see cref="Texture2DDescription"/> to <see cref="TextureDescription"/>.
        /// </summary>
        public static implicit operator TextureDescription(Texture2DDescription description)
        {
            var dimension = (description.ArraySize == 6 && (description.OptionFlags & ResourceOptionFlags.TextureCube) != 0)
                                ? TextureDimension.TextureCube
                                : TextureDimension.Texture2D;

            return new TextureDescription
            {
                Dimension = dimension,
                Size = new Vector2i(description.Width, description.Height),
                Depth = 1,
                MipLevels = description.MipLevels,
                ArraySize = description.ArraySize,
                Format = description.Format,
                SampleDescription = description.SampleDescription,
                Usage = description.Usage,
                BindFlags = description.BindFlags,
                CpuAccessFlags = description.CpuAccessFlags,
                OptionFlags = description.OptionFlags,
            };
        }

        /// <summary>
        /// Performs an explicit conversion from <see cref="TextureDescription"/> to <see cref="Texture2DDescription"/>.
        /// </summary>
        public static implicit operator Texture2DDescription(TextureDescription description)
        {
            return new Texture2DDescription
            {
                Width = description.Size.x,
                Height = description.Size.y,
                MipLevels = description.MipLevels,
                ArraySize = description.ArraySize,
                Format = description.Format,
                SampleDescription = description.SampleDescription,
                Usage = description.Usage,
                BindFlags = description.BindFlags,
                CpuAccessFlags = description.CpuAccessFlags,
                OptionFlags = description.OptionFlags,
            };
        }

        /// <summary>
        /// Performs an explicit conversion from <see cref="Texture2DDescription"/> to <see cref="TextureDescription"/>.
        /// </summary>
        public static implicit operator TextureDescription(Texture3DDescription description)
        {
            return new TextureDescription
            {
                Dimension = TextureDimension.Texture3D,
                Size = new Vector2i(description.Width, description.Height),
                Depth = description.Depth,
                ArraySize = 1,
                MipLevels = description.MipLevels,
                Format = description.Format,
                SampleDescription = new SampleDescription(1, 0),
                Usage = description.Usage,
                BindFlags = description.BindFlags,
                CpuAccessFlags = description.CpuAccessFlags,
                OptionFlags = description.OptionFlags,
            };
        }

        /// <summary>
        /// Performs an explicit conversion from <see cref="TextureDescription"/> to <see cref="Texture3DDescription"/>.
        /// </summary>
        public static implicit operator Texture3DDescription(TextureDescription description)
        {
            return new Texture3DDescription
            {
                Width = description.Size.x,
                Height = description.Size.y,
                Depth = description.Depth,
                MipLevels = description.MipLevels,
                Format = description.Format,
                Usage = description.Usage,
                BindFlags = description.BindFlags,
                CpuAccessFlags = description.CpuAccessFlags,
                OptionFlags = description.OptionFlags,
            };
        }

        /*
        /// <summary>
        /// Performs an explicit conversion from <see cref="ImageDescription"/> to <see cref="TextureDescription"/>.
        /// </summary>
        public static implicit operator TextureDescription(ImageDescription description)
        {
            return new TextureDescription
            {
                Dimension = description.Dimension,
                Width = description.Width,
                Height = description.Height,
                Depth = description.Depth,
                ArraySize = description.ArraySize,
                MipLevels = description.MipLevels,
                Format = description.Format,
                SampleDescription = new SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.None,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = description.Dimension == TextureDimension.TextureCube ? ResourceOptionFlags.TextureCube : ResourceOptionFlags.None,
            };
        }

        /// <summary>
        /// Performs an explicit conversion from <see cref="ImageDescription"/> to <see cref="TextureDescription"/>.
        /// </summary>
        public static implicit operator ImageDescription(TextureDescription description)
        {
            return new ImageDescription
            {
                Dimension = description.Dimension,
                Width = description.Width,
                Height = description.Height,
                Depth = description.Depth,
                ArraySize = description.ArraySize,
                MipLevels = description.MipLevels,
                Format = description.Format,
            };
        }
        */
    }
}
