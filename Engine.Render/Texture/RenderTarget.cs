﻿using System;
using System.Diagnostics;

using Common;
using SharpDX.Direct3D11;

namespace Engine
{
    /// <summary>
    /// A RenderTarget front end to <b><see cref="Texture2D"/></b>, tipical used for BackBuffer.<br/>
    /// This class instantiates a <b><see cref="Texture2D"/></b> with the binding flags <b><see cref="BindFlags.RenderTarget"/></b>.<br/>
    /// This class is also castable to <b><see cref="RenderTargetView"/></b>.<br/>
    /// </summary>
    public class RenderTarget : Texture2
    {
        RenderTargetView rendertarghetview;

        internal RenderTarget(D3Device device, Texture2DDescription description, string debugname = null) :
            base(device, description, debugname)
        {
        }

        internal RenderTarget(D3Device device, Texture2D resource, string debugname = null) :
            base(device, resource, debugname)
        {
            Restore(resource);
        }


        public void Restore(Texture2D resource)
        {
            RemoveAndDispose(ref rendertarghetview);
            rendertarghetview = ToDispose(new RenderTargetView(device, resource), "RenderTargetView_of_" + ImmutableHash.ToString("X4"));
        }

        public static implicit operator RenderTargetView(RenderTarget from)
        {
            return from == null ? throw new NullReferenceException("RenderTargetView casting from null reference") : from.rendertarghetview;
        }
    }
}
