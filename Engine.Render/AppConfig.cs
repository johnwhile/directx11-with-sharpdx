﻿using System;
using System.IO;
using System.Xml;

using Common;

namespace Engine
{
    /// <summary>
    /// Get some globals information, external file's path etc...
    /// </summary>
    public static class AppConfig
    {
        /// <summary>
        /// The path of content folder where resources are stored. Each application must configure the correct path
        /// in "AppConfig.xml"
        /// </summary>
        public static string ContentPath = "";

        static AppConfig()
        {
            ContentPath = AppDomain.CurrentDomain.BaseDirectory;
            string filename = Path.Combine(ContentPath, "AppConfig.xml");
            if (!File.Exists(filename))
            {
                Debugg.Error("ATTENTION, the engine need the config file \"AppConfig.xml\"");
                return;
            }
            XmlDocument doc = new XmlDocument();
            doc.Load(filename);
            //i want to add more possible location of path of same folder for my 2 computers
            XmlNodeList list = doc.GetElementsByTagName("ContentPath");
            for (int i=0;i<list.Count;i++)
            {
                var path = Path.GetFullPath(list[i].InnerText);
                ContentPath = path;
                if (Directory.Exists(path)) break;
            }
        }
    }
}
