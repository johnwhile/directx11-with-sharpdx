﻿using System;
using Common;
using SharpDX.Direct3D11;

namespace Engine
{
    public class RasterizerStateCollector : StateCollector<RasterizerStateDx>
    {
        RasterizerStateDx _solid;
        RasterizerStateDx _wireframe;


        /// <summary>
        /// Get the default solid rasterizer (it's default)
        /// </summary>
        public RasterizerStateDx Solid
        {
            get
            {
                if (_solid == null)
                { 
                    _solid = CreateOrGet(RasterizerStateDescription.Default());
                    _solid.Name = "Rasterizer[solid]";
                }
                return _solid;
            }
        }
        /// <summary>
        /// Get the default wireframe rasterizer
        /// </summary>
        public RasterizerStateDx Wireframe
        {
            get
            {
                if (_wireframe == null)
                {
                    var descr = RasterizerStateDescription.Default();
                    descr.FillMode = FillMode.Wireframe;
                    _wireframe = CreateOrGet(descr);
                    _wireframe.Name = "Rasterizer[wireframe]";
                }
                return _wireframe;
            }
        }

        public RasterizerStateCollector(D3Device device) : base(device)
        {
        }

        /// <summary>
        /// Create a new state or get the one previously created
        /// </summary>
        /// <param name="descriptor">unique state has its unique descriptor</param>
        public RasterizerStateDx CreateOrGet(RasterizerStateDescription description)
        {
            var tmp_state = new RasterizerState(Device, description);

            if (Contain(tmp_state, out var state))
            {
                Debugg.Info($"Get the already created {state}");
                tmp_state.Dispose();
            }
            else
            {
                state = new RasterizerStateDx(Device, tmp_state, description, this);
                Debugg.Info($"Create new {state}");
                weakcollection.Add(tmp_state.NativePointer, new WeakReference<RasterizerStateDx>(state));
            }
            CleansReferences();
            return state;
        }
    }

    /// <summary>
    /// The rasterizer stage determines what, if any, pixels should be rendered and passes those pixels to the pixel shader stage.
    /// Along with the pixels to render, the rasterizer passes per-vertex values interpolated across each primitive.
    /// </summary>
    public class RasterizerStateDx : RenderStateBase
    {
        RasterizerState state;
        RasterizerStateCollector collector;
        public readonly RasterizerStateDescription Description;


        protected override DeviceChild Devicechild => state;
        public override IntPtr NativePointer => state != null ? state.NativePointer : IntPtr.Zero;

        /// <summary>
        /// Create a render state without collector.<br/>
        /// <b>use instead <see cref="RasterizerStateCollector.CreateOrGet(RasterizerStateDescription)"/></b>
        /// </summary>
        public RasterizerStateDx(D3Device device, RasterizerStateDescription description):
            this(device, new RasterizerState(device, description), description, null)
        {
        }
        /// <summary>
        /// Create a render state internaly
        /// </summary>
        internal RasterizerStateDx(D3Device device, RasterizerState native, RasterizerStateDescription description, RasterizerStateCollector collector) : base(device)
        {
            state = ToDispose(native);
            state.DebugName = "RasterizerState_of_" + ImmutableHash.ToString("X4");
            this.Description = description;
            this.collector = collector;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Dispose(bool disposemanaged)
        {
            if (collector != null && !collector.IsDisposing)
                collector.RemoveReference(state.NativePointer);
            base.Dispose(disposemanaged);
        }

        public static implicit operator RasterizerState(RasterizerStateDx from)
        {
            return from != null ? from.state : throw new ArgumentNullException("SamplerStateDx casting with null reference");
        }

    }
}
