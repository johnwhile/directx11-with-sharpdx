﻿using System;
using Common;
using SharpDX.Direct3D11;

namespace Engine
{
    public class SamplerStateCollector : StateCollector<SamplerStateDx>
    {
        SamplerStateDx _default;
        SamplerStateDx _pointclamp;
        SamplerStateDx _trilinear;

        /// <summary>
        /// Get the default sampler
        /// </summary>
        public SamplerStateDx Default
        {
            get
            {
                if (_default == null)
                {
                    _default = CreateOrGet(SamplerStateDescription.Default());
                    _default.Name = "SamplerState[default]";
                }
                return _default;
            }
        }
        /// <summary>
        /// Get the linear sampler
        /// </summary>
        public SamplerStateDx PointClamp
        {
            get
            {
                if (_pointclamp == null)
                {
                    var descr = SamplerStateDescription.Default();
                    descr.Filter = Filter.MinMagMipPoint;
                    descr.AddressV = TextureAddressMode.Clamp;
                    descr.AddressU = TextureAddressMode.Clamp;
                    _pointclamp = CreateOrGet(descr);
                    _pointclamp.Name = "SamplerState[linearclamp]";
                }
                return _pointclamp;
            }
        }

        /// <summary>
        /// Get the trilinear sampler
        /// </summary>
        public SamplerStateDx Trilinear
        {
            get
            {
                if (_trilinear == null)
                {
                    var descr = SamplerStateDescription.Default();
                    descr.Filter = Filter.MinMagMipLinear;
                    descr.AddressV = TextureAddressMode.Wrap;
                    descr.AddressU = TextureAddressMode.Wrap;
                    _trilinear = CreateOrGet(descr);
                    _trilinear.Name = "SamplerState[trilinear]";
                }
                return _trilinear;
            }
        }

        public SamplerStateCollector(D3Device device) : base(device)
        {
        }

        /// <summary>
        /// Create a new state or get the one previously created
        /// </summary>
        /// <param name="descriptor">unique state has its unique descriptor</param>
        public SamplerStateDx CreateOrGet(SamplerStateDescription description)
        {
            var tmp_state = new SamplerState(Device, description);

            if (Contain(tmp_state, out var state))
            {
                Debugg.Info($"Get the already created {state}");
                tmp_state.Dispose();
            }
            else
            {
                state = new SamplerStateDx(Device, tmp_state, description, this);
                Debugg.Info($"Create new {state}");
                weakcollection.Add(tmp_state.NativePointer, new WeakReference<SamplerStateDx>(state));
            }
            CleansReferences();
            return state;
        }
    }

    /// <summary>
    /// Samplers control how a color is retrieved from a texture.
    /// </summary>
    public class SamplerStateDx : RenderStateBase
    {
        SamplerState state;
        SamplerStateCollector collector;
        SamplerStateDescription description;

        protected override DeviceChild Devicechild => state;
        public override IntPtr NativePointer => state != null ? state.NativePointer : IntPtr.Zero;

        /// <summary>
        /// Create a render state without collector.<br/>
        /// <b>use instead <see cref="SamplerStateCollector.CreateOrGet(SamplerStateDescription)"/></b>
        /// </summary>
        public SamplerStateDx(D3Device device, SamplerStateDescription description):
            this(device, new SamplerState(device, description), description, null)
        {
        }
        /// <summary>
        /// Create a render state internaly
        /// </summary>
        internal SamplerStateDx(D3Device device, SamplerState native, SamplerStateDescription description, SamplerStateCollector collector) : base(device)
        {
            state = ToDispose(native);
            state.DebugName = "SamplerState_of_" + ImmutableHash.ToString("X4");
            this.description = description;
            this.collector = collector;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Dispose(bool disposemanaged)
        {
            if (collector != null && !collector.IsDisposing)
                collector.RemoveReference(state.NativePointer);
            base.Dispose(disposemanaged);
        }

        public static implicit operator SamplerState(SamplerStateDx from)
        {
            return from != null ? from.state : throw new ArgumentNullException("SamplerStateDx casting with null reference");
        }

    }
}
