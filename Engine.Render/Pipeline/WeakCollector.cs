﻿using Common;
using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;

namespace Engine
{
    public abstract class RenderStateBase : D3Resource
    {
        protected RenderStateBase(D3Device device, string debugname = null) : base(device, debugname) { }

        protected override Resource Resource => throw new NotSupportedException();

        protected abstract DeviceChild Devicechild { get; }
    }

    /// <summary>
    /// A weak reference implementation are used as memory manager
    /// </summary>
    public abstract class WeakCollector<TKey, TObject> : GraphicChild where TObject : RenderStateBase
    {
        protected readonly Dictionary<TKey, WeakReference<TObject>> weakcollection;

        protected WeakCollector(D3Device device, string debugname) : base(device, debugname)
        {
            weakcollection = new Dictionary<TKey, WeakReference<TObject>>();
        }
        /// <summary>
        /// Remove collected weak shader reference or disposed shader
        /// </summary>
        public void CleansReferences()
        {
            int count = weakcollection.Count;
            if (count == 0) return;

            var keys = new TKey[count];
            weakcollection.Keys.CopyTo(keys, 0);

            foreach (var key in keys)
            {
                var weakreference = weakcollection[key];

                if (!weakreference.TryGetTarget(out var @object) || @object.IsDisposed)
                {
                    Debugg.Warning($"purge collected or disposed object using key: {key}");
                    weakcollection.Remove(key);
                }
            }
        }

        public void RemoveReference(TKey key)
        {
            //avoid loop disposing
            if (!IsDisposing) weakcollection.Remove(key);
        }

        protected override T ToDispose<T>(T obj)
        {
            throw new NotSupportedException("Weak collector can't store strong references");
        }

        protected override void Dispose(bool disposemanaged)
        {
            base.Dispose(disposemanaged);

#if DEBUGMORE
            Debugg.Print($"dispose all alive weak object in {GetType().Name}", DebugInfo.Warning);
#endif
            //var values = new List<WeakReference<TObject>(weakcollection.Values);
            //foreach (var item in values)
            foreach(var item in weakcollection.Values)
            {
                if (item.TryGetTarget(out var @object) && !@object.IsDisposed)
                {
#if DEBUGMORE
                    Debugg.Print($"dispose alive {@object}");
#endif
                    @object.Dispose();
                }
            }
            weakcollection.Clear();
        }

    }

}
