﻿using System;
using SharpDX.Direct3D11;

namespace Engine
{
    /// <inheritdoc/>
    public class GeometryShaderCollector : ShaderCollector<GeometryShaderDx>
    {
        GeometryShaderDx _default;
        protected override string Profile => device?.SupportedProfiles?.GS;
        public GeometryShaderDx DefaultPointToQuad
        {
            get
            {
                if (_default == null)
                {
                    _default = CreateOrGet(ShaderCodeDescriptor.MyShaderDir + "gs.default.hlsl", "GS_PointToQuad");
                    _default.Name = "GeometryShader[quad]";
                }
                return _default;
            }
        }
        public GeometryShaderCollector(D3Device device) : base(device) { }
        protected override GeometryShaderDx CreateInstance(ShaderCodeDescriptor descriptor) => new GeometryShaderDx(this, descriptor);
    }

    /// <summary>
    /// Class front end to<see cref="VertexShader"/>
    /// </summary>
    public class GeometryShaderDx : ShaderBase
    {
        GeometryShader shader;
        GeometryShaderCollector collector;

        protected override DeviceChild Devicechild => shader;
        public override IntPtr NativePointer => shader != null ? shader.NativePointer : IntPtr.Zero;

        /// <summary>
        /// Pass a null reference to remove shader
        /// </summary>
        public readonly static GeometryShaderDx NULL = null;
            
        /// <summary>
        /// Create a geometry shader using a cache collector
        /// </summary>
        internal GeometryShaderDx(GeometryShaderCollector collector, ShaderCodeDescriptor description) :
            this(collector.Device, description)
        {
            this.collector = collector;
        }
        /// <summary>
        /// Create a vertex shader
        /// </summary>
        public GeometryShaderDx(D3Device device, ShaderCodeDescriptor description) : base(device, description)
        {
            shader = ToDispose(new GeometryShader(device, description.Compile()));
            shader.DebugName = "GeometryShader_of_" + ImmutableHash.ToString("X4");

            Description = description;
        }

        protected override void Dispose(bool disposemanaged)
        {
            if (collector != null && !collector.IsDisposing)
                collector.RemoveReference(Description);
            base.Dispose(disposemanaged);
        }

        public static implicit operator GeometryShader(GeometryShaderDx from)
        {
            return from != null ? from.shader : null;
            //return from != null ? from.shader : throw new ArgumentNullException("GeometryShaderDx casting with null reference");
        }
    }
}
