﻿using System;
using SharpDX.Direct3D11;

namespace Engine
{
    /// <inheritdoc/>
    public class PixelShaderCollector : ShaderCollector<PixelShaderDx>
    {
        PixelShaderDx _default_red;
        PixelShaderDx _default_texture;
        protected override string Profile => device?.SupportedProfiles?.PS;
        public PixelShaderDx DefaultPSred
        {
            get
            {
                if (_default_red == null)
                {
                    _default_red = CreateOrGet(ShaderCodeDescriptor.MyShaderDir + "ps.default.hlsl", "PS_DefaultRed");
                    _default_red.Name = "PixelShader[red]";
                }
                return _default_red;
            }
        }
        public PixelShaderDx DefaultPStexture
        {
            get
            {
                if (_default_texture == null)
                {
                    _default_texture = CreateOrGet(ShaderCodeDescriptor.MyShaderDir + "ps.default.hlsl", "PS_Texture");
                    _default_texture.Name = "PixelShader[texture]";
                }
                return _default_texture;
            }
        }
        public PixelShaderCollector(D3Device device) : base(device) { }
        protected override PixelShaderDx CreateInstance(ShaderCodeDescriptor descriptor) => new PixelShaderDx(this, descriptor);
    }

    /// <summary>
    /// Class front end to<see cref="PixelShader"/>
    /// </summary>
    public class PixelShaderDx : ShaderBase
    {
        PixelShader shader;
        PixelShaderCollector collector;

        protected override DeviceChild Devicechild => shader;
        public override IntPtr NativePointer => shader != null ? shader.NativePointer : IntPtr.Zero;

        /// <summary>
        /// Pass a null reference to remove shader
        /// </summary>
        public readonly static PixelShaderDx NULL = null;

        /// <summary>
        /// Create a pixel shader using a cache collector
        /// </summary>
        internal PixelShaderDx(PixelShaderCollector collector, ShaderCodeDescriptor description) :
            this(collector.Device, description)
        {
            this.collector = collector;
        }
        /// <summary>
        /// Create a pixel shader
        /// </summary>
        public PixelShaderDx(D3Device device, ShaderCodeDescriptor description) : base(device, description)
        {
            shader = ToDispose(new PixelShader(device, description.Compile()));
            shader.DebugName = "PixelShader_of_" + ImmutableHash.ToString("X4");
            Description = description;
        }
        protected override void Dispose(bool disposemanaged)
        {
            if (collector != null && !collector.IsDisposing)
                collector.RemoveReference(Description);
            base.Dispose(disposemanaged);
        }

        public static implicit operator PixelShader(PixelShaderDx from)
        {
            return from != null ? from.shader : null;
            //return from != null ? from.shader : throw new ArgumentNullException("PixelShaderDx casting with null reference");
        }
    }
}
