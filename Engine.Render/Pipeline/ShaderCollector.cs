﻿
using System;
using System.Collections.Generic;
using System.IO;

using SharpDX.D3DCompiler;

using Common;
using SharpDX.Direct3D11;
using SharpDX.Direct3D;

namespace Engine
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ShaderBase : RenderStateBase
    {
        public ShaderBase(D3Device device, ShaderCodeDescriptor description) : base(device)
        {
#if DEBUGMORE
            Debugg.Print($"{Name} init");
#endif
            Description = description;
        }
        public ShaderCodeDescriptor Description;

        protected override void Dispose(bool disposemanaged)
        {
#if DEBUGMORE
            Debugg.Print($"dispose {Name} and remove weak reference");
#endif
            base.Dispose(disposemanaged);
        }

    }

    /// <summary>
    /// The shader can be create with a dictionary to avoid duplicates.
    /// </summary>
    public abstract class ShaderCollector<S> : WeakCollector<ShaderCodeDescriptor, S> where S : ShaderBase
    {
        protected abstract string Profile { get; }
        protected ShaderCollector(D3Device device, string debugname = "ShaderCollector") : base(device, debugname) { }
        /// <summary>
        /// initialize the shader
        /// </summary>
        /// <param name="descriptor"></param>
        protected abstract S CreateInstance(ShaderCodeDescriptor descriptor);
        /// <summary>
        /// Create a new shader or get the one previously created
        /// </summary>
        public S CreateOrGet(string hlslfile, string entrypoint)
        {
            ShaderCodeDescriptor descriptor = new ShaderCodeDescriptor()
            {
                HlslFilename = hlslfile,
                Entrypoint = entrypoint,
                Profile = Profile
            };
            return CreateOrGet(descriptor);
        }
        /// <summary>
        /// Create a new shader or get the one previously created
        /// </summary>
        /// <param name="descriptor">unique shader has its unique descriptor</param>
        public S CreateOrGet(ShaderCodeDescriptor description)
        {
            if (string.IsNullOrEmpty(description.Profile)) throw new NotSupportedException("unknow shader profile, mabe be unsuported");
            S shader;

            //already created
            if (weakcollection.TryGetValue(description, out var weakReference))
            {
                //already created and still alive
                if (weakReference.TryGetTarget(out shader) && !shader.IsDisposed)
                {
                    Debugg.Info($"Get an already created {typeof(S).Name} : {description}");
                    return shader;
                }
                else
                {
                    //key exist so just reuse it
                    Debugg.Info($"Reassign a new initialization of {typeof(S).Name} : {description}");
                    shader = CreateInstance(description);
                    shader.Name = description.Entrypoint;
                    weakReference.SetTarget(shader);
                }
            }
            else
            {
                //first initialization
                Debugg.Info($"Create a new {typeof(S).Name} : {description}");
                shader = CreateInstance(description);
                shader.Name = description.Entrypoint;
                weakcollection.Add(description, new WeakReference<S>(shader));
            }
            CleansReferences();
            return shader;
        }
    }

    /// <summary>
    /// I'm using the maximum version for convenience and performance reasons. I'm testing only FeatureLevel.Level_11_0 (*s_5_0)
    /// </summary>
    public class SupportedShaderProfile
    {
        /// <summary> Direct Compute </summary>
        public string CS = null;
        /// <summary> Geometry Shader</summary>
        public string GS = null;
        /// <summary>Pixel Shader </summary>
        public string PS = null;
        /// <summary> Vertex Shader </summary>
        public string VS = null;
        /// <summary> Domain Shader </summary>
        public string DS = null;
        /// <summary> Hull Shader </summary>
        public string HS = null;

        public bool IsGeometryShaderSupported => GS != null;

        public SupportedShaderProfile(FeatureLevel level)
        {
            switch (level)
            {
                case FeatureLevel.Level_9_1:
                case FeatureLevel.Level_9_2:
                case FeatureLevel.Level_9_3: //check but it hasn't a practical use how
                    VS = "vs_2_0";
                    PS = "ps_2_0";
                    break;

                case FeatureLevel.Level_10_0:
                    VS = "vs_4_0";
                    PS = "ps_4_0";
                    GS = "gs_4_0";
                    CS = "cs_4_0";
                    break;
                case FeatureLevel.Level_10_1:
                    VS = "vs_4_1";
                    PS = "ps_4_1";
                    GS = "gs_4_1";
                    CS = "cs_4_1";
                    break;
                case FeatureLevel.Level_11_0:
                case FeatureLevel.Level_11_1:
                    VS = "vs_5_0";
                    PS = "ps_5_0";
                    GS = "gs_5_0";
                    CS = "cs_5_0";
                    DS = "ds_5_0";
                    HS = "hs_5_0";
                    break;
            }
        }
    }

    [Flags]
    public enum ShaderStageType : sbyte
    {
        NONE = -1,
        PS = 0,//mandatory
        VS = 1,
        GS = 2,
        PS_VS = PS | VS,
        PS_VS_GS = PS_VS | GS,
    }

    /// <summary>
    /// A shader code was compiled from a "*.hlsl" file and from function name, like technique in Directx9's Effect.
    /// Same descriptor define same shader code
    /// </summary>
    public struct ShaderCodeDescriptor
    {
        public static string MyShaderDir = AppConfig.ContentPath + @"\Shaders\";
#if DEBUG
        static readonly ShaderFlags Flags = ShaderFlags.Debug | ShaderFlags.SkipOptimization;
#else
        static readonly ShaderFlags Flags = ShaderFlags.None;
#endif
        public string HlslFilename;
        /// <summary>
        /// name of main function
        /// </summary>
        public string Entrypoint;
        /// <summary>
        /// example : vs_5_0
        /// </summary>
        public string Profile;

        public ShaderCodeDescriptor(string hlsl, string entrypoint, string profile)
        {
            HlslFilename = Path.GetFullPath(hlsl);
            Entrypoint = entrypoint;
            Profile = profile;
        }

        public ShaderBytecode Compile()
        {
            if (!File.Exists(HlslFilename)) throw new FileNotFoundException($"the shaderfile \"{HlslFilename}\" not found");

            var includeHandler = new HLSLFileInclude(Path.GetDirectoryName(HlslFilename));

            var code = ShaderBytecode.CompileFromFile(HlslFilename, Entrypoint, Profile, Flags, EffectFlags.None, null, includeHandler);

            if (code == null) throw new Exception($"compiling \"{Profile}\" file \"{HlslFilename}\" return null");
            return code;
        }

#region for Dictionary
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = HlslFilename.GetHashCode();
                hash = hash * 23 + Entrypoint.GetHashCode();
                hash = hash * 23 + Profile.GetHashCode();
                return hash;
            }
        }
        public override bool Equals(object obj)
        {
            if (obj is ShaderCodeDescriptor)
                return Equals((ShaderCodeDescriptor)obj);
            else return false;
        }
        public bool Equals(ShaderCodeDescriptor other)
        {
            return HlslFilename == other.HlslFilename && Entrypoint == other.Entrypoint && Profile == other.Profile;
        }
        public static bool operator ==(ShaderCodeDescriptor descr1, ShaderCodeDescriptor descr2)
        {
            return descr1.Equals(descr2);
        }
        public static bool operator !=(ShaderCodeDescriptor descr1, ShaderCodeDescriptor descr2)
        {
            return !descr1.Equals(descr2);
        }

        public override string ToString()
        {
            return $"{Path.GetFileName(HlslFilename)} {Entrypoint} {Profile}";
        }
#endregion
    }
}
