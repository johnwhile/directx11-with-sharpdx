﻿using Common;
using SharpDX.Direct3D11;
using System;
using System.Collections.Generic;


namespace Engine
{
    /// <summary>
    /// For each object created, the runtime checks to see if a previous object has the same state.
    /// If such a previous object exists, the runtime will return a pointer to previous instance instead of creating a duplicate object.<br/>
    /// The problem is that c # returns a new instance, you need a method to search internally for the same instance.
    /// I'm using as key the nativepointer <see cref="IntPtr"/> for renderstate objects
    /// </summary>
    public abstract class StateCollector<TState> : WeakCollector<IntPtr, TState> where TState : RenderStateBase
    {
        /// <summary>
        /// Directx constant = 4096, but i will use only maximum 32 states
        /// </summary>
        public const int MAX_STATES = 32;

        protected StateCollector(D3Device device, string debugname = "StateCollector") : base(device, debugname) { }
        
        /// <summary>
        /// The native pointer for disposed native object become IntPtr.Zero, but, if you create two RenderState
        /// with same description the native object is same for both instance but only for disposed object the native become dispose
        /// 
        /// sample: blendstate with same description
        /// <code>
        /// b1= 9054916
        /// b2= 9054916
        /// b1.Dispose()
        /// b1= 0
        /// b2= 9054916
        /// </code>
        /// </summary>
        protected bool Contain(DeviceChild native, out TState state)
        {
            state = null;
            var key = native.NativePointer;

            //key exist
            if (weakcollection.TryGetValue(key, out var weakReference))
            {
                //still alive
                if (weakReference.TryGetTarget(out state))
                    return true;
                else
                    weakcollection.Remove(key); //remove key for safety
            }
            //dead or key not created
            return false; 
        }
    }


}
