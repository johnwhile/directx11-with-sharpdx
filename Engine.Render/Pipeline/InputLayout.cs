﻿using System;
using System.Collections.Generic;

using Common;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D11;

namespace Engine
{
    public class InputLayoutCollector : StateCollector<InputLayoutDx>
    {
        public InputLayoutCollector(D3Device device) : base(device)
        {
        }

        /// <summary>
        /// Create a new state or get the one previously created
        /// </summary>
        /// <param name="descriptor">unique state has its unique descriptor</param>
        public InputLayoutDx CreateOrGet(VertexShaderDx validator, params InputSlotLayout[] slots)
        {
            if (validator == null) throw new NullReferenceException("InputLayout require the vertexshader as validator...");

            byte[] inputsignature = validator.Code.GetPart(ShaderBytecodePart.InputSignatureBlob);
            InputElement[] elements = VertexLayout.GetElements(slots);
            InputLayout tmp_native;

            try { tmp_native = new InputLayout(Device, inputsignature, elements); }
            catch (SharpDX.SharpDXException e)
            {
                Debugg.Message($"> Fail to validate input layout");
                throw e;
            }

            if (Contain(tmp_native, out var layout))
            {
                Debugg.Info($"Get the already created {layout}");
                tmp_native.Dispose();
            }
            else
            {
                layout = new InputLayoutDx(Device, tmp_native, slots, this);
                Debugg.Info($"Create new {layout}");
                weakcollection.Add(tmp_native.NativePointer, new WeakReference<InputLayoutDx>(layout));
            }
            CleansReferences();
            return layout;
        }
    }

    /// <summary>
    /// It stores the definition of vertex data for the input-assembler pipeline stage
    /// </summary>
    /// <remarks>
    /// vertex shader collection must be created first, because i need some default code...
    /// </remarks>
    public class InputLayoutDx : RenderStateBase
    {
        InputLayout layout;
        InputLayoutCollector collector;
        InputSlotLayout[] slots;
        public InputSlotLayout[] Slots => slots;


        protected override DeviceChild Devicechild => layout;
        public override IntPtr NativePointer => layout != null ? layout.NativePointer : IntPtr.Zero;

        /// <summary>
        /// Create a input layout without collector.<br/>
        /// <b>use instead <see cref="InputLayoutCollector.CreateOrGet(VertexShaderDx, InputSlotLayout[])"/></b>
        /// <br/><br/><seealso cref="InputLayoutDx(D3Device, byte[], InputSlotLayout[])"/>
        /// </summary>
        public InputLayoutDx(D3Device device, VertexShaderDx validator, params InputSlotLayout[] slots) :
            this(device, validator.Code, slots)
        { }

        /// <summary>
        /// <inheritdoc cref="InputLayoutDx(D3Device, VertexShaderDx, InputSlotLayout[])"/>
        /// </summary>
        /// <param name="code">a way to get ShaderBytecodePart.InputSignatureBlob ?</param>
        public InputLayoutDx(D3Device device, ShaderBytecode code, params InputSlotLayout[] slots) :
            this(device, (byte[])code.GetPart(ShaderBytecodePart.InputSignatureBlob), slots)
        { }


        /// <summary>
        /// <inheritdoc cref="InputLayoutDx(D3Device, VertexShaderDx, InputSlotLayout[])"/>
        /// </summary>
        /// <param name="slots">example for instancing, you can create 2 buffer and 2 layout, I prefer to keep them separate</param>
        /// <param name="inputsignature">the total byte[] code or only the inputsignatureblob. directx require a vertex shader code to validate the first time the creation of the interface</param>
        /// <exception cref="SharpDX.SharpDXException">if the inputlayout doesn't match the shader code, an exception is thrown</exception>
        internal InputLayoutDx(D3Device device, byte[] inputsignature, params InputSlotLayout[] slots) : base(device)
        {
            InputLayout native;
            try
            {
                native = new InputLayout(device, inputsignature, VertexLayout.GetElements(slots));
            }
            catch (SharpDX.SharpDXException e)
            {
                Debugg.Message($"> Fail to validate input layout {Name}");
                throw e;
            }
            Initialize(native, slots, null);
        }

        /// <summary>
        /// Create a input layout internaly
        /// </summary>
        internal InputLayoutDx(D3Device device, InputLayout native, InputSlotLayout[] slots, InputLayoutCollector collector) : base(device)
        {
            Initialize(native, slots, null);
        }


        private void Initialize(InputLayout native, InputSlotLayout[] slots, InputLayoutCollector collector)
        {
            layout = ToDispose(native);
            layout.DebugName = "InputLayout_of_" + ImmutableHash.ToString("X4");
            this.slots = slots;
            this.collector = collector;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Dispose(bool disposemanaged)
        {
            if (collector != null && !collector.IsDisposing)
                collector.RemoveReference(layout.NativePointer);
            base.Dispose(disposemanaged);
        }

        public static implicit operator InputLayout(InputLayoutDx from)
        {
            return from != null ? from.layout : throw new ArgumentNullException("InputLayoutDx casting with null reference");
        }

    }
}
