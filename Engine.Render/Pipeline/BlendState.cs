﻿using System;
using Common;
using SharpDX.Direct3D11;

namespace Engine
{
    public class BlendStateCollector : StateCollector<BlendStateDx>
    {
        BlendStateDx _default;
        BlendStateDx _trasparent;
        /// <summary>
        /// Get the a blend state with default descriptor
        /// </summary>
        public BlendStateDx Default
        {
            get 
            {
                if (_default == null)
                {
                    _default = CreateOrGet(BlendStateDescription.Default());
                    _default.Name = "BlendState[default]";
                }
                return _default;
            }

        }
        /// <summary>
        /// Get the a blend state with alpha blending enabled
        /// </summary>
        public BlendStateDx Trasparent
        {
            get
            {
                if (_trasparent == null)
                {
                    var descr = BlendStateDescription.Default();
                    descr.RenderTarget[0].IsBlendEnabled = true;
                    descr.RenderTarget[0].SourceBlend = BlendOption.SourceAlpha;
                    descr.RenderTarget[0].DestinationBlend = BlendOption.InverseSourceAlpha;
                    _trasparent = CreateOrGet(descr);
                    _trasparent.Name = "BlendState[trasparent]";
                }
                return _trasparent;
            }
        }
        public BlendStateCollector(D3Device device) : base(device) { }
        /// <summary>
        /// Create a new state or get the one previously created
        /// </summary>
        /// <param name="descriptor">unique state has its unique descriptor</param>
        public BlendStateDx CreateOrGet(BlendStateDescription description)
        {
            var tmp_state = new BlendState(Device, description);

            if (Contain(tmp_state, out var state))
            {
                Debugg.Info($"Get the already created {state}");
                tmp_state.Dispose();
            }
            else
            {
                state = new BlendStateDx(Device, tmp_state, description, this);
                Debugg.Info($"Create new {state}");
                weakcollection.Add(tmp_state.NativePointer, new WeakReference<BlendStateDx>(state));
            }
            CleansReferences();
            return state;
        }
    }

    /// <summary>
    /// The blend state is a collection of states used to control blending using formula:
    /// <para><code>Dest = (Source * sourceFactor) {Operation} (Dest * destFactor)</code></para>
    /// </summary>
    public class BlendStateDx : RenderStateBase
    {
        BlendState state;
        BlendStateCollector collector;
        BlendStateDescription description;

        protected override DeviceChild Devicechild => state;

        public override IntPtr NativePointer => state!=null ? state.NativePointer : IntPtr.Zero;

        /// <summary>
        /// Create a render state without collector.<br/>
        /// <b>use instead <see cref="BlendStateCollector.CreateOrGet(BlendStateDescription)"/></b>
        /// </summary>
        public BlendStateDx(D3Device device, BlendStateDescription description):
            this(device, new BlendState(device, description), description, null)
        {
        }
        /// <summary>
        /// Create a render state internaly
        /// </summary>
        internal BlendStateDx(D3Device device, BlendState native, BlendStateDescription description, BlendStateCollector collector) : base(device)
        {
            state = ToDispose(native);
            state.DebugName = "BlendState_of_" + ImmutableHash.ToString("X4");
            this.description = description;
            this.collector = collector;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Dispose(bool disposemanaged)
        {
            if (collector != null && !collector.IsDisposing)
                collector.RemoveReference(state.NativePointer);
            base.Dispose(disposemanaged);
        }

        public static implicit operator BlendState(BlendStateDx from)
        {
            return from != null ? from.state : throw new ArgumentNullException("BlendStateDx casting with null reference");
        }

    }
}
