﻿using System;
using Common;
using SharpDX.Direct3D11;

namespace Engine
{
    public class DepthStateCollector : StateCollector<DepthStateDx>
    {
        DepthStateDx _default;
        DepthStateDx _nodepthtest;

        /// <summary>
        /// Get the a depth stencil state with default descriptor
        /// </summary>
        public DepthStateDx Default
        {
            get 
            {
                if (_default == null) _default = CreateOrGet(DepthStencilStateDescription.Default());
                _default.Name = "DepthState[default]";
                return _default;
            }

        }

        /// <summary>
        /// Get the a depthstencil state with depth test disabled
        /// </summary>
        public DepthStateDx NoDepthTest
        {
            get
            {
                if (_nodepthtest == null)
                {
                    var descr = DepthStencilStateDescription.Default();
                    descr.IsDepthEnabled = false;
                    _nodepthtest = CreateOrGet(descr);
                    _nodepthtest.Name = "DepthState[nodepthtest]";
                }
                return _nodepthtest;
            }
        }

        public DepthStateCollector(D3Device device) : base(device)
        {
        }

        /// <summary>
        /// Create a new state or get the one previously created
        /// </summary>
        /// <param name="descriptor">unique state has its unique descriptor</param>
        public DepthStateDx CreateOrGet(DepthStencilStateDescription description)
        {
            var tmp_state = new DepthStencilState(Device, description);

            if (Contain(tmp_state, out var state))
            {
                Debugg.Info($"Get the already created {state}");
                tmp_state.Dispose();
            }
            else
            {
                state = new DepthStateDx(Device, tmp_state, description, this);
                Debugg.Info($"Create new {state}");
                weakcollection.Add(tmp_state.NativePointer, new WeakReference<DepthStateDx>(state));
            }
            CleansReferences();
            return state;
        }
    }

    /// <summary>
    /// The depth-stencil test determines whether or not a given pixel should be drawn.
    /// </summary>
    public class DepthStateDx : RenderStateBase
    {
        DepthStencilState state;
        DepthStateCollector collector;
        DepthStencilStateDescription description;

        protected override DeviceChild Devicechild => state;

        public override IntPtr NativePointer => state != null ? state.NativePointer : IntPtr.Zero;

        /// <summary>
        /// Create a render state without collector.<br/>
        /// <b>use instead <see cref="DepthStateCollector.CreateOrGet(DepthStencilStateDescription)"/></b>
        /// </summary>
        public DepthStateDx(D3Device device, DepthStencilStateDescription description):
            this(device, new DepthStencilState(device, description), description, null)
        {
        }
        /// <summary>
        /// Create a render state internaly
        /// </summary>
        internal DepthStateDx(D3Device device, DepthStencilState native, DepthStencilStateDescription description, DepthStateCollector collector) : base(device)
        {
            state = ToDispose(native);
            state.DebugName = "DepthState_of_" + ImmutableHash.ToString("X4");

            this.description = description;
            this.collector = collector;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Dispose(bool disposemanaged)
        {
            if (collector != null && !collector.IsDisposing)
                collector.RemoveReference(state.NativePointer);
            base.Dispose(disposemanaged);
        }

        public static implicit operator DepthStencilState(DepthStateDx from)
        {
            return from != null ? from.state : throw new ArgumentNullException("DepthStateDx casting with null reference");
        }

    }
}
