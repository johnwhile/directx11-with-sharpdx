﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Common;
using SharpDX;
using SharpDX.D3DCompiler;

namespace Engine
{
    /// <summary>
    /// IMPORTANT: When creating a new shader file use "Save As...", "Save with encoding", 
    /// and then select "Western European (Windows) - Codepage 1252" as the 
    /// D3DCompiler cannot handle the default encoding of "UTF-8 with signature"
    /// </summary>
    public class HLSLFileInclude : CallbackBase, Include
    {
        public readonly Stack<string> CurrentDirectory;
        public readonly List<string> IncludeDirectories;


        public HLSLFileInclude(string initialDirectory)
        {
            IncludeDirectories = new List<string>();
            CurrentDirectory = new Stack<string>();
            CurrentDirectory.Push(initialDirectory);
        }

        /// <summary>
        /// <see cref="Include"/> members
        /// </summary>
        public Stream Open(IncludeType type, string fileName, Stream parentStream)
        {
            var currentDirectory = CurrentDirectory.Peek();

            if (currentDirectory == null) currentDirectory = Environment.CurrentDirectory;

            var filePath = fileName;

            if (!Path.IsPathRooted(filePath))
            {
                var directoryToSearch = new List<string> { currentDirectory };
                directoryToSearch.AddRange(IncludeDirectories);
                foreach (var dirPath in directoryToSearch)
                {
                    var selectedFile = Path.Combine(dirPath, fileName);
                    if (File.Exists(selectedFile))
                    {
                        filePath = selectedFile;
                        break;
                    }
                }
            }

            if (filePath == null || !File.Exists(filePath))
            {
                throw new FileNotFoundException(string.Format("Unable to find file [{0}]", filePath ?? fileName));
            }

            /*
            using (var reader = new StreamReader(filePath, Encoding.Default, true))
            {
                if (reader.Peek() >= 0) // you need this!
                    reader.Read();

                Debugg.Print(reader.CurrentEncoding, DebugInfo.Warning);
            }*/

            Stream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);

            CurrentDirectory.Push(Path.GetDirectoryName(filePath));
            return fs;
        }
        
        /// <summary>
        /// <see cref="Include"/> members
        /// </summary>
        public void Close(Stream stream)
        {
            stream.Dispose();
            CurrentDirectory.Pop();
        }
    }
}