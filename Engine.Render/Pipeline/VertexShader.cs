﻿using System;

using SharpDX.D3DCompiler;
using SharpDX.Direct3D11;

namespace Engine
{
    /// <inheritdoc/>
    public class VertexShaderCollector : ShaderCollector<VertexShaderDx>
    {
        VertexShaderDx _default;
        protected override string Profile => device?.SupportedProfiles?.VS;
        public VertexShaderDx Default
        {
            get
            {
                if (_default == null)
                {
                    _default = CreateOrGet(ShaderCodeDescriptor.MyShaderDir + "vs.default.hlsl", "VS_Default");
                    _default.Name = "VertexShader[default]";
                }
                return _default;
            }
        }
        public VertexShaderCollector(D3Device device) : base(device) { }
        protected override VertexShaderDx CreateInstance(ShaderCodeDescriptor descriptor) => new VertexShaderDx(this, descriptor);
    }

    /// <summary>
    /// Class front end to <see cref="VertexShader"/>
    /// </summary>
    public class VertexShaderDx : ShaderBase
    {
        VertexShader shader;
        VertexShaderCollector collector;
        ShaderBytecode code;
        /// <summary>
        /// require to store the compiled code for inputlayout 
        /// </summary>
        public ShaderBytecode Code => code;

        protected override DeviceChild Devicechild => shader;

        public override IntPtr NativePointer => shader != null ? shader.NativePointer : IntPtr.Zero;

        /// <summary>
        /// Pass a null reference to remove shader
        /// </summary>
        public readonly static VertexShaderDx NULL = null;

        /// <summary>
        /// Create a vertex shader using a cache collector
        /// </summary>
        internal VertexShaderDx(VertexShaderCollector collector, ShaderCodeDescriptor description) :
            this(collector.Device, description)
        {
            this.collector = collector;
        }
        /// <summary>
        /// Create a vertex shader
        /// </summary>
        public VertexShaderDx(D3Device device, ShaderCodeDescriptor description) : base(device, description)
        {
            shader = ToDispose(new VertexShader(device, code = description.Compile()));
            shader.DebugName = "VertexShader_of_" + ImmutableHash.ToString("X4");
            Description = description;
        }

        /// <summary>
        /// Not Implemented...
        /// Instead creating every time a new InputLayout, get it from shader file
        /// </summary>
        private VertexElement[] ExtractVertexInputLayout()
        {
            VertexElement[] elements;

            using (var vsReflect = new ShaderReflection(code))
            {
                int count = vsReflect.Description.InputParameters;
                elements = new VertexElement[count];

                for (int i = 0; i < count; i++)
                {
                    var descr = vsReflect.GetInputParameterDescription(i);

                    elements[i] = new VertexElement();
                    elements[i].SemanticName = descr.SemanticName;
                    elements[i].SemanticIndex = (byte)descr.SemanticIndex;

                    byte usage = (byte)descr.UsageMask;
                    int size = 0;
                    for (byte b = 1; b < 16; b <<= 1) if ((usage & b) > 0) size++;

                    switch (descr.ComponentType)
                    {
                        case RegisterComponentType.UInt32:
                            break;
                        case RegisterComponentType.SInt32:
                            break;
                        case RegisterComponentType.Float32:
                            break;
                        case RegisterComponentType.Unknown:
                        default:
                            throw new Exception("m_shaderfile component type issue");
                    }

                }
            }

            throw new NotImplementedException();
        }

        protected override void Dispose(bool disposemanaged)
        {
            if (collector != null && !collector.IsDisposing)
                collector.RemoveReference(Description);
            base.Dispose(disposemanaged);
        }

        public static implicit operator VertexShader(VertexShaderDx from)
        {
            return from != null ? from.shader : null;
            //return from != null ? from.shader : throw new ArgumentNullException("VertexShaderDx casting with null reference");
        }
    }
}
