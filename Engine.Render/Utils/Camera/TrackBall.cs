﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using Common;
using Common.Maths;
using Common.Inputs;

namespace Engine.Tools
{
    /// <summary>
    /// The Event's Controller of trackballcamera, need a DirectMouse and window control.
    /// </summary>
    public class MyTrackBall : TrackBallCameraMath
    {
        IMouse input;
        Control control;

        public MyTrackBall(Control control, IMouse input, Matrix4x4f view, Matrix4x4f proj)
            : base(new ViewportClip(control.ClientRectangle), view, proj)
        {
            InitEvents(control,input);
        }

        /// <summary>
        /// </summary>
        /// <param name="control">The ball require information about size of panel, at this moment viewport match with clientrectangle</param>
        public MyTrackBall(Control control, IMouse input, Vector3f Eye, Vector3f Targhet, float near, float far)
            : base(new ViewportClip(control.ClientRectangle), Eye, Targhet, near, far)
        {
            InitEvents(control,input);
        }

        ~MyTrackBall()
        {
            DeleteEvents();
        }

        void InitEvents(Control control, IMouse input)
        {
            this.input = input;
            input.OnMouseDown += OnMouseDown;
            input.OnMouseUp += OnMouseUp;
            input.OnMouseWheel += OnMouseWheel;
            input.OnMouseMove += OnMouseMove;

            this.control = control;
            control.Resize += OnControlResize;
        }

        void DeleteEvents()
        {
            input.OnMouseDown -= OnMouseDown;
            input.OnMouseUp -= OnMouseUp;
            input.OnMouseWheel -= OnMouseWheel;
            input.OnMouseMove -= OnMouseMove;
            control.Resize -= OnControlResize;
        }

        protected override void GetMouseCoord(out Vector2i pos)
        {
            WindowMouse.GetPosition(out pos, control.Handle);
        }

        void OnMouseDown(object sender, MyMouseArg arg)
        {
            if (arg.key == MouseKey.Left)
            {
                base.MouseTraslateDown();
            }
            else if (arg.key == MouseKey.Right)
            {
                base.MouseRotateDown();
            }

        }

        void OnMouseUp(object sender, MyMouseArg arg)
        {
            base.MouseUp();
        }

        void OnMouseWheel(object sender, MyMouseArg arg)
        {
            //base.MouseWheeling(sender.MouseMovement.z);
        }

        void OnMouseMove(object sender, MyMouseArg arg)
        {
            if (mousing != Mousing.None)
            {
                base.MouseMoving();
            }
        }

        void OnControlResize(object sender, EventArgs e)
        {
            Control ctrl = (Control)sender;
            ViewportClip viewport = new ViewportClip(control.ClientRectangle);
            base.ViewportResize(viewport);
        }


    }
}
