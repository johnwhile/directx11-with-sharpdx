﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Engine
{
    internal class GraphicResourceCollector : LinkedList <GraphicChild> , IDisposable
    {
        public GraphicResourceCollector():base()
        {

        }
        public new void AddLast(GraphicChild item)
        {
            Debug.Print("ADD TO DEVICE COLLECTOR : " + item.Name);
            base.AddLast(item);
        }
        public new bool Remove(GraphicChild item)
        {
            bool removed = base.Remove(item);
            if (removed) Debug.Print("REMOVED FROM DEVICE COLLECTOR : " + item.Name);
            return removed;
        }

        void removeAndDisposeAll()
        {
            while (Count > 0)
            {
                var item = Last.Value;

                if (item.IsDisposed) Debug.Print("item " + item.Name + " already disposed");
                item.Dispose();
                Remove(item);
            }
            Clear();
        }

        public void Dispose()
        {
            removeAndDisposeAll();
        }

        ~GraphicResourceCollector()
        {
            removeAndDisposeAll();
        }
    }
}
