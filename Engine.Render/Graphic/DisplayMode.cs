﻿using System;
using System.Collections.Generic;

using DXGI = SharpDX.DXGI;

using Common.Maths;

namespace Engine
{
    public enum ScreenMode
    {
        Windowed = 0,
        Fullscreen = 1,
        FakeFullscreen = 2
    }


    /// <summary>
    /// Simple information about screen
    /// </summary>
    public struct DisplayMode : IComparable<DisplayMode>, IEquatable<DisplayMode>
    {
        public PixelFormat Format;
        public Vector2i Size;
        /// <summary>
        /// Can be example Hz:60317/1000. 
        /// 0/0 is legal and become 0/1. 
        /// 0/N become zero
        /// </summary>
        public Vector2i RefreshRate;

        public DisplayMode(Vector2i size, PixelFormat format, Vector2i refreshrate)
        {
            Size = size;
            Format = format;
            if (refreshrate.y == 0) refreshrate = new Vector2i(0, 1);
            if (refreshrate.x == 0) refreshrate = new Vector2i(0, 0);
            RefreshRate = refreshrate;
        }

        /// <summary>
        /// Gets the aspect ratio (width / height).
        /// </summary>
        public float AspectRatio => Size.height > 0 ? Size.width / (float)Size.height : 0;

        public static DisplayMode Default => new DisplayMode(new Vector2i(800, 600), PixelFormat.R8G8B8A8_UNorm, new Vector2i(60, 1));
        
        /// <summary>
        /// return -1 if greater, 1 if smaller, this because display mode list will be sort from smaller to bigger
        /// </summary>
        public int CompareTo(DisplayMode other)
        {
            int comp = compare(other.Size, Size);
            if (comp==0) comp = compare(other.RefreshRate, RefreshRate);
            return comp;
        }

        public bool Equals(DisplayMode other)
        {
            return other.CompareTo(this) == 0;
        }

        int compare(Vector2i s0, Vector2i s1)
        {
            //compare areas
            return (s0.x * s0.y).CompareTo(s1.x * s1.y);
        }


        public static implicit operator DisplayMode(DXGI.ModeDescription descr)
        {
            return new DisplayMode(new Vector2i(descr.Width, descr.Height), (PixelFormat)descr.Format, new Vector2i(descr.RefreshRate.Numerator, descr.RefreshRate.Denominator));
        }


        public override int GetHashCode()
        {
            unchecked
            {
                int hash = Size.GetHashCode();
                hash = (hash * 397) ^ RefreshRate.GetHashCode();
                hash = (hash * 397) ^ Format.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return string.Format($"Display : {Size} {(DXGI.Format)Format} Hz:{RefreshRate.x/(float)RefreshRate.y}");
        }
    }
}
