﻿using System;
using System.Collections.Generic;
using Common;
using Common.Maths;

using SharpDX;
using SharpDX.DXGI;


namespace Engine
{
    /// <summary>
    /// The monitor
    /// </summary>
    public class GraphicOutput : DisposableDx
    {
        Output output;
        DisplayMode currentDisplayMode;
        DisplayMode[] supportedDisplayModes;

        /// <summary>
        /// Gets the adapter this output is attached.
        /// </summary>
        public readonly GraphicAdapter Adapter;
        /// <summary>
        /// Index of current monitor in the list
        /// </summary>
        public readonly int MonitorIndex;


        /// <summary>
        /// Initializes a new instance of <see cref="GraphicOutput" />.
        /// </summary>
        internal GraphicOutput(GraphicAdapter adapter, Output monitor, int monitorindex)
        {
            Adapter = adapter;
            MonitorIndex = monitorindex;
            output = ToDispose(monitor);

            InitializeSupportedDisplayModes();
            foreach(var mode in supportedDisplayModes)
            {
                //Debugg.Print(mode);
            }

            if (!TryFindMatchingDisplayMode(Format.R8G8B8A8_UNorm, out currentDisplayMode))
            {
                currentDisplayMode = default(DisplayMode);
                Debugg.Error("Cant find a valid DisplayMode for default format R8G8B8A8_UNorm");
            }
            else
            {
                Debugg.Message(currentDisplayMode);
            }
        }

        /// <summary>
        /// Find the display mode that most closely matches the requested display mode.
        /// </summary>
        public DisplayMode FindClosestMatchingDisplayMode(D3Device Device, DisplayMode mode)
        {
            ModeDescription closestDescription;

            ModeDescription descr = new ModeDescription()
            {
                Width = mode.Size.width,
                Height = mode.Size.height,
                RefreshRate = new Rational(mode.RefreshRate.x, mode.RefreshRate.y),
                Format = (Format)mode.Format,
                Scaling = DisplayModeScaling.Unspecified,
                ScanlineOrdering = DisplayModeScanlineOrder.Unspecified
            };

            output.GetClosestMatchingMode(Device, descr, out closestDescription);

            return closestDescription;
        }

        /// <summary>
        /// Gets the current display mode.
        /// </summary>
        public DisplayMode CurrentDisplayMode => currentDisplayMode;

        /// <summary>
        /// Returns a collection of supported display modes for this <see cref="GraphicOutput"/>.
        /// </summary>
        public DisplayMode[] SupportedModes => supportedDisplayModes;

        /// <summary>
        /// Enumerates all available display modes for this output and stores them in <see cref="SupportedModes"/>.
        /// </summary>
        void InitializeSupportedDisplayModes()
        {
            List<DisplayMode> modesAvailable = new List<DisplayMode>();
            Dictionary<string, DisplayMode> modesMap = new Dictionary<string, DisplayMode>();

            // Returns one of the following DXGI_ERROR. It is rare, but possible, that the display modes available can change immediately after calling this method,
            // in which case DXGI_ERROR_MORE_DATA is returned (if there is not enough room for all the display modes).
            try
            {
                foreach (Format dxgiFormat in Enum.GetValues(typeof(Format)))
                {
                    ModeDescription[] modes = output.GetDisplayModeList(dxgiFormat, DisplayModeEnumerationFlags.Interlaced | DisplayModeEnumerationFlags.Scaling);

                    foreach (ModeDescription mode in modes)
                    {
                        if (mode.Scaling == DisplayModeScaling.Unspecified)
                        {
                            string key = dxgiFormat + ";" + mode.Width + ";" + mode.Height + ";" + mode.RefreshRate.Numerator + ";" + mode.RefreshRate.Denominator;

                            DisplayMode oldMode;

                            if (!modesMap.TryGetValue(key, out oldMode))
                            {
                                DisplayMode displayMode = new DisplayMode()
                                {
                                    Size = new Vector2i(mode.Width, mode.Height),
                                    Format = (PixelFormat)mode.Format,
                                    RefreshRate = new Vector2i(mode.RefreshRate.Numerator, mode.RefreshRate.Denominator)
                                };
                                modesMap.Add(key, displayMode);
                                modesAvailable.Add(displayMode);
                            }
                        }
                    }
                }
            }
            catch (SharpDXException dxgiException)
            {
                if (dxgiException.ResultCode != ResultCode.NotCurrentlyAvailable)
                    throw;
            }
            supportedDisplayModes = modesAvailable.ToArray();
        }

        /// <summary>
        /// Tries to find a display mode that has the same size as the current <see cref="OutputDescription"/> associated with this instance
        /// of the specified format.
        /// </summary>
        bool TryFindMatchingDisplayMode(Format format, out DisplayMode result)
        {
            result = default(DisplayMode);
            foreach (DisplayMode supported in SupportedModes)
            {
                result = supported;
                var bound = output.Description.DesktopBounds;
                Vector2i size = new Vector2i(bound.Right - bound.Left, bound.Bottom - bound.Top);
                if (supported.Size == size && (Format)supported.Format == format) return true;
            }
            return false;
        }

        /// <summary>
        /// Adapter casting operator.
        /// </summary>
        public static implicit operator Output(GraphicOutput from)
        {
            return from != null ? from.output : throw new NullReferenceException("GraphicOutput with null reference");
        }

        public override string ToString()
        {
            return $"{output.Description.DeviceName} <{output.Description.DesktopBounds.ToCommon()}>";
        }
    }
}
