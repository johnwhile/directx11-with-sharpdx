﻿using System;

using DXGI = SharpDX.DXGI;

namespace Engine
{
    /// <summary>
    /// Multisample count level.
    /// </summary>
    public enum MSAALevel : int
    {
        /// <summary>
        /// No multisample.
        /// </summary>
        None = 1,

        /// <summary>
        /// Multisample count of 2 pixels.
        /// </summary>
        X2 = 2,

        /// <summary>
        /// Multisample count of 4 pixels.
        /// </summary>
        X4 = 4,

        /// <summary>
        /// Multisample count of 8 pixels.
        /// </summary>
        X8 = 8
    }

    /*
    /// <summary>
    /// using SharpDX.DXGI.Format
    /// </summary>
    public enum Format
    {
        Unknown = 0,
        // Riepilogo:
        //      A four-component, 32-bit unsigned-normalized-integer format that supports
        //     8 bits per channel including alpha.
        R8G8B8A8_UNorm = 28,
        R8G8B8A8_UNorm_SRgb = 29,
    }
    
    public static class FormatExt
    {
        public static SharpDX.DXGI.Format ToDxgi(this Format foo)
        {
            return (SharpDX.DXGI.Format)foo;
        }
    }
    */


}
