﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

using DX = SharpDX;
using DXGI = SharpDX.DXGI;
using D3D = SharpDX.Direct3D11;

using Common;
using System.Diagnostics;
using SharpDX.DXGI;

namespace Engine
{
    /// <summary>
    /// The static method initialize all available graphic cards.
    /// </summary>
    public class GraphicAdapter : DisposableDx
    {
        Adapter adapter;
        
        /// <summary>
        /// <summary>
        /// Video Memory in MB, if compiled for x86 return maximum 3GB, compiled for x64 can return greater (and true) value
        /// </summary>
        public int DedicatedVideoMemoryMB { get; private set; }

        /// <summary>
        /// Index of current graphic card
        /// </summary>
        public int AdapterIndex { get; private set; }

        /// <summary>
        /// Gets the <see cref="GraphicOutput"/> attached to this adapter at the specified index.
        /// </summary>
        public List<GraphicOutput> Outputs;

        /// <summary>
        /// </summary>
        internal GraphicAdapter(int ordinal, Adapter adapter)
        {
            AdapterIndex = ordinal;
            this.adapter = ToDispose(adapter);

            Name = adapter.Description.Description.Trim('\0'); //Convert the name of the video card from 128 character array
            DedicatedVideoMemoryMB = (int)(adapter.Description.DedicatedVideoMemory / 1024 / 1024);

            Outputs = new List<GraphicOutput>();

            //Debugg.Print(ToString(), DebugInfo.Warning);
            //Debugg.Indent++;
            // numbers of monitors or similar that current card are using
            for (int i = 0; i < adapter.GetOutputCount(); i++)
            {
                var monitor = ToDispose(new GraphicOutput(this, adapter.GetOutput(i), i));
                Outputs.Add(monitor);
                //Debugg.Print(monitor.ToString());
            }
            //Debugg.Indent--;
        }

        /// <summary>
        /// Adapter casting operator.
        /// </summary>
        public static implicit operator Adapter(GraphicAdapter from)
        {
            return from != null ? from.adapter : throw new ArgumentNullException("GraphicAdapter casting with null reference");
        }

        public override string ToString()
        {
            return string.Format($"\"{Name}\" [{AdapterIndex}] {DedicatedVideoMemoryMB} MB");
        }
    }

}
