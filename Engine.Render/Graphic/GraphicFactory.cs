﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Common;

using SharpDX.DXGI;

namespace Engine
{
    public class IndigentExeption : Exception { public IndigentExeption(string message) : base(message) { } }

    public class GraphicFactory : DisposableDx
    {
        static GraphicFactory singleton;
        
        static GraphicFactory()
        {
            singleton = singleton ?? new GraphicFactory();
            Application.ApplicationExit += delegate (object sender, EventArgs args) { singleton.Dispose(); };
        }

        /// <summary>
        /// All available graphic card used by your computer, the first is the Default graphic card.
        /// </summary>
        public static List<GraphicAdapter> Adapters;

        /// Is always the first adapter of AllGraphicCards list. For my purpose i don't need to use more than one card
        /// </summary>
        public static GraphicAdapter DefaultAdapter => Adapters != null && Adapters.Count > 0 ? Adapters[0] : null;


        private GraphicFactory()
        {
            Debugg.Info("GraphicFactory init");
            Adapters = new List<GraphicAdapter>();
            using (var factory = new Factory1())
            {
                int count = factory.GetAdapterCount1();
                if (count == 0) throw new IndigentExeption("Poor guy, no graphic card ?");
                for (int i = 0; i < count; i++)
                {
                    var card = ToDispose(new GraphicAdapter(i, factory.Adapters1[i]));
                    Adapters.Add(card);
                }
            }
        }

        protected override void Dispose(bool disposemanaged)
        {
            Debugg.Info("GraphicFactory disposing");
            base.Dispose(disposemanaged);
        }
    }
}
