﻿using System;
using SharpDX.Direct3D;


namespace Engine
{
    /// <summary>
    /// The settings used in creation of the graphics device.
    /// </summary>
    public class GraphicInformation
    {
        /// <summary>
        /// Only Directx11 will be used but the FeatureLevel 10.0 support is required
        /// </summary>
        public static FeatureLevel[] DefaultGraphicsProfile = new FeatureLevel[] {
            FeatureLevel.Level_11_1,
            FeatureLevel.Level_11_0,
            FeatureLevel.Level_10_1,
            FeatureLevel.Level_10_0
        };

        /// <summary>
        /// The graphics adapter on which the graphics device will be created.
        /// </summary>
        /// <remarks>
        /// This is only valid on desktop systems where multiple graphics 
        /// adapters are possible.  Defaults to <see cref="GraphicsAdapter.DefaultAdapter"/>.
        /// </remarks>
        public GraphicAdapter Adapter { get; set; }

        /// <summary>
        /// The requested graphics device feature set. 
        /// </summary>
        //public GraphicsProfile GraphicsProfile { get; set; }

        /// <summary>
        /// The list of feature levels to accept
        /// </summary>
        public FeatureLevel[] PreferredGraphicsProfile = DefaultGraphicsProfile;

        /// <summary>
        /// The settings that define how graphics will be presented to the display.
        /// </summary>
        public PresentationParameters PresentationParameters { get; set; }
    }
}
