﻿using System;
using System.Windows.Forms;

using Common;
using Common.Windows;
using Common.Maths;

using SharpDX.DXGI;

namespace Engine
{
    /// <summary>
    /// The settings that define how graphics will be presented to the display.
    /// </summary>
    public class PresentationParameters
    {
        public PixelFormat BackBufferFormat;
        public DepthFormat DepthStencilFormat;

        public int BufferCount { get; private set; }

        /// <summary>
        /// the current backbuffer size
        /// </summary>
        public Vector2i BackBufferSize;
        /// <summary>
        /// the backbuffer size used when switch to windowed mode (updated in ChangeClientSize event)
        /// </summary>
        public Vector2i PreferredBackBufferSize;
        /// <summary>
        /// the monitor size
        /// </summary>
        public Vector2i ScreenSize;

        /// <summary>
        /// Get or set the handle of the window that will present the back buffer.
        /// </summary>
        /// <summary>
        /// A window object is platform dependent: On Windows Desktop: This could a low level window/control handle (IntPtr),
        /// or directly a Winform Control object.
        /// </summary>
        public Control OutputWnd;

        /// <summary>
        /// structure describing the refresh rate in hertz
        /// </summary>
        public Rational RefreshRate;

        /// <summary>
        /// Synchronize with vertical retrace: if enabled the first argument instructs DXGI to block until VSync, putting the application to sleep until the next VSync. 
        /// This ensures we don't waste any CPU/GPU cycles rendering frames that will never be displayed to the screen.
        /// </summary>
        public bool Vsync
        {
            get { return PresentationInterval > PresentInterval.Immediate; }
            set { PresentationInterval = value ? PresentInterval.One : PresentInterval.Immediate; }
        }
        /// <summary>
        /// Indicates whether the DepthBuffer should be created with the ShaderResource flag. Default is false.
        /// </summary>
        public bool DepthBufferShaderResource;
        /// <summary>
        /// Gets or sets a value indicating whether the application is in full screen mode (true hardware fullscreen).
        /// Swapchain use this value also to switch from windowed mode to fullscreen when creating
        /// </summary>
        public bool IsFullScreen;
        /// <summary>
        /// A member of the DXGI_USAGE enumerated type that describes the surface usage and CPU access options for the back buffer.
        /// The back buffer can  be used for shader input or render-target output.
        /// </summary>
        public Usage RenderTargetUsage;

        /// <summary>
        ///   Gets or sets the maximum rate at which the swap chain's back buffers can be presented to the front buffer.
        /// </summary>
        public PresentInterval PresentationInterval = PresentInterval.Immediate;
        
        /// <summary>
        ///   A member of the <see cref="SharpDX.DXGI.SwapChainFlags" />
        ///   enumerated type that describes options for swap-chain behavior.
        /// </summary>
        public SwapChainFlags Flags;

        /// <summary>
        /// Initializes a new instance of the <see cref="PresentationParameters" /> class with default values.
        /// </summary>
        /// <param name="preferredWidth">prefered backbuffer size in windowed mode, in fullscreen will match the screen</param>
        /// <param name="outputWnd">window to render, if is a child control can render only in windowed mode</param>
        public PresentationParameters(Control outputWnd, Vector2i preferredSize)
        {
            OutputWnd = outputWnd;
            PreferredBackBufferSize = preferredSize;
            BackBufferSize = PreferredBackBufferSize;
            BackBufferFormat = PixelFormat.R8G8B8A8_UNorm;
            DepthStencilFormat = DepthFormat.Depth24Stencil8;
            MultiSampleCount = MSAALevel.None;
            IsFullScreen = false;
            RefreshRate = new Rational(60, 1); // by default
            RenderTargetUsage = Usage.BackBuffer | Usage.RenderTargetOutput;
            Flags = SwapChainFlags.AllowModeSwitch;
            BufferCount = 1;
            ScreenSize = Screen.PrimaryScreen.Bounds.Size;
        }


        /// <summary>
        /// https://docs.microsoft.com/en-us/windows/win32/api/dxgi/ne-dxgi-dxgi_swap_effect
        /// </summary>
        public SwapChainDescription1 GetSwapChainDescription_new()
        {
            var desc = GetSwapChainDescription_old();

            BufferCount = 2;
            desc.BufferCount = BufferCount;
            desc.SwapEffect = SwapEffect.FlipDiscard;
            desc.SampleDescription = new SampleDescription(1, 0);
            return desc;
        }
        public SwapChainDescription1 GetSwapChainDescription_old()
        {
            BufferCount = 1;

            var desc = new SwapChainDescription1();

            desc.Width = BackBufferSize.width;
            desc.Height = BackBufferSize.height;
            desc.Format = (Format)BackBufferFormat;
            desc.BufferCount = BufferCount;
            desc.SwapEffect = SwapEffect.Discard;
            desc.Usage = RenderTargetUsage;
            desc.Flags = Flags;
            desc.SampleDescription = new SampleDescription((int)MultiSampleCount, 0);

            
            return desc;
        }

        public SwapChainFullScreenDescription GetSwapChainnDescriptionFullScreen()
        {
            return new SwapChainFullScreenDescription()
            {
                RefreshRate = new Rational(60, 1),
                Scaling = DisplayModeScaling.Centered,
                Windowed = true
            };
        }


        public MSAALevel MultiSampleCount { get; set; }

        /// <summary>
        /// Directx11 swapchain can't switch to fullscreen if windows is a child of form
        /// </summary>
        public bool IsOutputWndMainForm
        {
            get { return OutputWnd.Parent == null; }
        }

        public ViewportClip DefaultViewport
        {
            get { return new ViewportClip(BackBufferSize); }
        }

        public override string ToString()
        {
            return 
                $"Size   : {BackBufferSize}\n" +
                $"format : {BackBufferFormat}\n" +
                $"depth  : {DepthStencilFormat}\n" +
                $"fullscreen: {IsFullScreen}";
        }
        /// <summary>
        /// <see cref="OutputWnd"/> should be a copy by value or reference ?
        /// </summary>
        public PresentationParameters ShallowCopy()
        {
            return (PresentationParameters)MemberwiseClone();
        }
    }
}
