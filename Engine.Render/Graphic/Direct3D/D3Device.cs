﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

using SharpDX;
using SharpDX.Direct3D11;

using DX = SharpDX.Direct3D11;
using D3D = SharpDX.Direct3D;
using DXGI = SharpDX.DXGI; 

using Common;

namespace Engine
{
    /// <summary>
    /// Graphic Device (Directx11.1)
    /// </summary>
    public class D3Device : DisposableDx
    {
        /// <summary>
        /// Graphic card used to create this device
        /// </summary>
        public GraphicAdapter Adapter;

        internal Device5 d3device;
        internal D3Context context;
        internal BlendStateCollector blends;
        internal DepthStateCollector depthstencil;
        internal RasterizerStateCollector rasterize;
        internal InputLayoutCollector inputs;
        internal SamplerStateCollector samplers;
        internal PixelShaderCollector pixelshaders;
        internal VertexShaderCollector vertexshaders;
        internal GeometryShaderCollector geometryshaders;

        public D3Context Context =>
            context != null ? context : context = ToDispose(new D3Context(this));
       
        public BlendStateCollector BlendStates =>
            blends == null ? blends = ToDispose(new BlendStateCollector(this)) : blends;
       
        public DepthStateCollector DepthStates =>
            depthstencil == null ? depthstencil = ToDispose(new DepthStateCollector(this)) : depthstencil;
        
        public RasterizerStateCollector RasterizeStates =>
            rasterize == null ? rasterize = ToDispose(new RasterizerStateCollector(this)) : rasterize;
        
        public SamplerStateCollector SamplerStates =>
            samplers == null ? samplers = ToDispose(new SamplerStateCollector(this)) : samplers;

        public VertexShaderCollector VertexShaders =>
            vertexshaders == null ? vertexshaders = ToDispose(new VertexShaderCollector(this)) : vertexshaders; 
       
        public GeometryShaderCollector GeometryShaders=> 
            (geometryshaders == null && SupportedProfiles.IsGeometryShaderSupported) ? 
            geometryshaders = ToDispose(new GeometryShaderCollector(this)): geometryshaders;
       
        public PixelShaderCollector PixelShaders =>
            pixelshaders == null ? pixelshaders = ToDispose(new PixelShaderCollector(this)) : pixelshaders;

        public InputLayoutCollector InputStates
        {
            get
            {
                if (vertexshaders == null) vertexshaders = new VertexShaderCollector(this);
                if (inputs == null) inputs = ToDispose(new InputLayoutCollector(this));
                return inputs;
            }
        }


        /// <summary>
        /// Gets or sets the current presenter use by the <see cref="Present"/> method. (for now only swapchain)
        /// </summary>
        //public SwapChainPresenter Presenter { get; set; }

        static D3Device()
        {
            //Configuration.EnableObjectTracking = true;
        }

        public D3Device(GraphicAdapter adapter)
            : this(adapter, GraphicInformation.DefaultGraphicsProfile)
        {
            
        }

        /// <summary>
        /// </summary>
        public D3Device(GraphicAdapter adapter, params D3D.FeatureLevel[] graphicProfiles)
        {
            Debugg.Info("Device init using " + adapter.ToString());

            Adapter = adapter;

            // support for Direct2D
            DeviceCreationFlags d3dflag = DeviceCreationFlags.BgraSupport | DeviceCreationFlags.SingleThreaded;
#if DEBUG
            d3dflag |= DeviceCreationFlags.Debug;
#endif
            if (Device.GetSupportedFeatureLevel() < D3D.FeatureLevel.Level_10_0)
                throw new NotSupportedException("the FeatureLevel.Level_10_0 support is required");



            try
            {
                if (Adapter == null)
                {
                    using (Device device = new Device(D3D.DriverType.Hardware, d3dflag, graphicProfiles))
                        d3device = ToDispose(device.QueryInterface<Device5>());
                }
                else
                {
                    using (Device device = new Device(Adapter, d3dflag, graphicProfiles))
                        d3device = ToDispose(device.QueryInterface<Device5>());
                }
            }
            catch (SharpDXException e)
            {
                Debugg.Error("fail to create d3device, try to create a new one without debug flag: " + e.Message);
                // Try again without the debug flag. This allows debug builds to run
                // on machines that don't have the debug runtime installed.
                d3dflag &= ~DeviceCreationFlags.Debug;
                using (Device device = new Device(D3D.DriverType.Hardware, d3dflag, graphicProfiles))
                    d3device = ToDispose(device.QueryInterface<Device5>());
            }
            Debugg.Message($"Device created with profile : {d3device.FeatureLevel}");

            Features = new DeviceFeatures(this);

            SupportedProfiles = new SupportedShaderProfile(d3device.FeatureLevel);

        }

        /// <summary>
        /// Gets the status of this device.
        /// </summary>
        public DeviceStatus DeviceStatus
        {
            get
            {
                Result result = d3device.DeviceRemovedReason;

                switch((uint)result.Code)
                {
                    case 0x887A0006: return DeviceStatus.Hung;
                    case 0x887A0005: return DeviceStatus.Removed;
                    case 0x887A0007: return DeviceStatus.Reset;
                    case 0x887A0020: return DeviceStatus.InternalError;
                    case 0x887A0001: return DeviceStatus.InvalidCall;
                }

                if (result.Success) return DeviceStatus.Normal;
                else throw new Exception("Unknown result code for Device status");
            }
        }

        /// <summary>
        /// Gets the features supported by this <see cref="DX.Device"/>.
        /// </summary>
        public readonly DeviceFeatures Features;
        /// <summary>
        /// Gets the supported shaders by current <see cref="Features"/>
        /// </summary>
        public readonly SupportedShaderProfile SupportedProfiles;


        protected override void Dispose(bool disposeManaged)
        {
            Debugg.Indent++;
            Debugg.Info("Device start disposing");      
            //dispose all resources attacked
            base.Dispose(disposeManaged);
            Debugg.Indent--;

        }
        public static implicit operator Device5(D3Device from)
        {
            return from != null ? from.d3device : throw new NullReferenceException($"D3Device casting with null reference");
        }

        /// <summary>
        /// Helps finding any remaining unreleased references via console output, which can be
        /// enabled by using Dx Control Panel and adding the app there (DX11) and also enabling
        /// native debugging output in the properties of the project to debug. Then see Output window.
        /// </summary>
        [Conditional("DEBUG")]
        public void ReportLiveDeviceObjects()
        {
            using (var deviceDebug = new DeviceDebug(d3device))
            {
                // http://sharpdx.org/forum/4-general/1241-reportliveobjects
                // Contains several Refcount: 0 lines. This cannot be avoided, but is still be useful to
                // find memory leaks (all objects should have Refcount=0, the device still has RefCount=3)
                deviceDebug.ReportLiveDeviceObjects(ReportingLevel.Summary | ReportingLevel.IgnoreInternal);
            }
        }
    }
}