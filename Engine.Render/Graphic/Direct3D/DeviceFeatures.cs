﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using DXGI = SharpDX.DXGI;
using DX = SharpDX.Direct3D11;
using D3D = SharpDX.Direct3D;

namespace Engine
{
    public struct DeviceFeatures
    {
        private readonly FeaturesPerFormat[] mapFeaturesPerFormat;


        /// <summary>
        /// <see cref="Format"/> to exclude from the features test.
        /// </summary>
        private readonly static List<DXGI.Format> ObsoleteFormatToExcludes = new List<DXGI.Format>()
        {
            DXGI.Format.R1_UNorm,
            DXGI.Format.B5G6R5_UNorm,
            DXGI.Format.B5G5R5A1_UNorm
        };


        internal DeviceFeatures(D3Device Device)
        {
            DX.Device device = Device;

            mapFeaturesPerFormat = new FeaturesPerFormat[256];

            // Check global features
            Level = device.FeatureLevel;
            HasComputeShaders = device.CheckFeatureSupport(DX.Feature.ComputeShaders);
            HasDoublePrecision = device.CheckFeatureSupport(DX.Feature.ShaderDoubles);
            device.CheckThreadingSupport(out HasMultiThreadingConcurrentResources, out this.HasDriverCommandLists);

            // Check features for each DXGI.Format
            foreach (var format in Enum.GetValues(typeof(DXGI.Format)))
            {
                var dxgiFormat = (DXGI.Format)format;
                var maximumMSAA = MSAALevel.None;
                var computeShaderFormatSupport = DX.ComputeShaderFormatSupport.None;
                var formatSupport = DX.FormatSupport.None;

                if (!ObsoleteFormatToExcludes.Contains(dxgiFormat))
                {
                    maximumMSAA = GetMaximumMSAASampleCount(device, dxgiFormat);
                    if (HasComputeShaders)
                        computeShaderFormatSupport = device.CheckComputeShaderFormatSupport(dxgiFormat);

                    formatSupport = device.CheckFormatSupport(dxgiFormat);
                }

                mapFeaturesPerFormat[(int)dxgiFormat] = new FeaturesPerFormat(dxgiFormat, maximumMSAA, computeShaderFormatSupport, formatSupport);
            }
        }

        /// <summary>
        /// Gets the maximum MSAA sample count for a particular <see cref="PixelFormat" />.
        /// </summary>
        /// <param name="device">The device.</param>
        /// <param name="pixelFormat">The pixelFormat.</param>
        /// <returns>The maximum multisample count for this pixel pixelFormat</returns>
        /// <msdn-id>ff476499</msdn-id>
        ///   <unmanaged>HRESULT ID3D11Device::CheckMultisampleQualityLevels([In] DXGI_FORMAT Format,[In] unsigned int SampleCount,[Out] unsigned int* pNumQualityLevels)</unmanaged>
        ///   <unmanaged-short>ID3D11Device::CheckMultisampleQualityLevels</unmanaged-short>
        private static MSAALevel GetMaximumMSAASampleCount(DX.Device device, DXGI.Format pixelFormat)
        {
            int maxCount = 1;
            for (int i = 1; i <= 8; i *= 2)
            {
                if (device.CheckMultisampleQualityLevels(pixelFormat, i) != 0)
                    maxCount = i;
            }
            return (MSAALevel)maxCount;
        }
        /// <summary>
        /// Features level of the current device.
        /// </summary>
        public D3D.FeatureLevel Level;

        /// <summary>
        /// Boolean indicating if this device supports compute shaders, unordered access on structured buffers and raw structured buffers.
        /// </summary>
        public readonly bool HasComputeShaders;

        /// <summary>
        /// Boolean indicating if this device supports shaders double precision calculations.
        /// </summary>
        public readonly bool HasDoublePrecision;

        /// <summary>
        /// Boolean indicating if this device supports concurrent resources in multithreading scenarios.
        /// </summary>
        public readonly bool HasMultiThreadingConcurrentResources;

        /// <summary>
        /// Boolean indicating if this device supports command lists in multithreading scenarios.
        /// </summary>
        public readonly bool HasDriverCommandLists;

        /// <summary>
        /// Gets the <see cref="FeaturesPerFormat" /> for the specified <see cref="SharpDX.DXGI.Format" />.
        /// </summary>
        /// <param name="dxgiFormat">The DXGI format.</param>
        /// <returns>Features for the specific format.</returns>
        public FeaturesPerFormat this[DXGI.Format dxgiFormat]
        {
            get { return mapFeaturesPerFormat[(int)dxgiFormat]; }
        }
        public FeaturesPerFormat this[DepthFormat dxgiFormat]
        {
            get { return mapFeaturesPerFormat[(int)dxgiFormat]; }
        }
        /// <summary>
        /// The features exposed for a particular format.
        /// </summary>
        public struct FeaturesPerFormat
        {
            internal FeaturesPerFormat(DXGI.Format format, MSAALevel maximumMSAALevel, DX.ComputeShaderFormatSupport computeShaderFormatSupport, DX.FormatSupport formatSupport)
            {
                Format = format;
                this.MSAALevelMax = maximumMSAALevel;
                ComputeShaderFormatSupport = computeShaderFormatSupport;
                FormatSupport = formatSupport;
            }

            /// <summary>
            /// The <see cref="SharpDX.DXGI.Format"/>.
            /// </summary>
            public readonly DXGI.Format Format;

            /// <summary>
            /// Gets the maximum MSAA sample count for a particular <see cref="PixelFormat"/>.
            /// </summary>
            public readonly MSAALevel MSAALevelMax;

            /// <summary>	
            /// Gets the unordered resource support options for a compute shader resource.	
            /// </summary>	
            /// <msdn-id>ff476135</msdn-id>	
            /// <unmanaged>D3D11_FORMAT_SUPPORT2</unmanaged>	
            /// <unmanaged-short>D3D11_FORMAT_SUPPORT2</unmanaged-short>	
            public readonly DX.ComputeShaderFormatSupport ComputeShaderFormatSupport;

            /// <summary>
            /// Support of a given format on the installed video device.
            /// </summary>
            public readonly DX.FormatSupport FormatSupport;

            public override string ToString()
            {
                return string.Format("Format: {0}, MSAALevelMax: {1}, ComputeShaderFormatSupport: {2}, FormatSupport: {3}", Format, this.MSAALevelMax, ComputeShaderFormatSupport, FormatSupport);
            }
        }
    }
}
