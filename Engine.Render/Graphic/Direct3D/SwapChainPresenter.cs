﻿#define NEW_RESIZE

using System;
using System.Diagnostics;

using SharpDX;
using SharpDX.DXGI; 
using DXGI = SharpDX.DXGI;
using SharpDX.Mathematics.Interop;
using SharpDX.Direct3D11;

using Common;
using Common.Maths;
using Common.Windows;
using System.Windows.Forms;
using Engine.Direct2D;
using SharpDX.Direct2D1;

namespace Engine
{
    /// <summary>
    /// Graphics presenter for SwapChain (Directx11.0)
    /// </summary>
    public partial class SwapChainPresenter : Presenter
    {
        bool screenswithing = false;
        bool d2drawing = false;

        D2BitmapTarget d2target;
        RenderTarget backbuffer;
        SwapChain2 swapchain;
        RawColor4 clearcolor = Color4b.CornflowerBlue.ToRawColor();

        public override RenderTarget BackBuffer => backbuffer;

        /// <summary>
        /// Get the Native Fullscreen state
        /// </summary>
        public bool IsFullScreen
        {
            get { return swapchain!=null ? swapchain.IsFullScreen : false; }
        }

        /// <summary>
        /// Creates a DepthStencilBuffer, create the swapchain and get the backbuffer
        /// </summary>
        /// <param name="PresentParameters">It will be a shallow copy so no reference are used</param>
        /// <param name="D2Device">optional if you want enable the direct2d functionality</param>
        public SwapChainPresenter(D3Device D3Device, PresentationParameters PresentParameters, D2Device D2Device = null)
            : base(D3Device, PresentParameters, D2Device)
        {
            //can't start at first time in fullscreen mode, you have to switch after creating in windowed
            Description.IsFullScreen = false;

            //remember the first size used for windowed state
            Description.PreferredBackBufferSize = Description.BackBufferSize;

            CreateSizeDependentResources();
        }

        /// <summary>
        /// Create a new swapchain.
        /// </summary>
        /// <remarks>
        /// If you are swithing in fullscreen i read that it's seems to be more reliable create a new one then trying to change the current swap-chain, 
        /// so this implementation does not manage the resize in fullscreen mode
        /// </remarks>
        protected void CreateSizeDependentResources()
        {
            Debugg.Info($"Swapchain create or init with size : {Description.BackBufferSize}");

            int width = Description.BackBufferSize.width;
            int height = Description.BackBufferSize.height;
            Format format = (Format)Description.BackBufferFormat;

            var d3dDevice = Device.d3device;
            var d3dContext = Device.Context;

            d3dContext?.SetRenderTargets(null, null);

            RemoveAndDispose(ref backbuffer);
            RemoveAndDispose(ref depthStencilbuffer);

            // release direct2 target !
            bool reassignnewtarget = false;
            if (IsValid(d2device) && d2device.Context.Target == d2target)
            {
                d2device.Context.Target = null;
                reassignnewtarget = true;
            }
            RemoveAndDispose(ref d2target);


            if (IsValid(swapchain) && !Description.IsFullScreen)
            {
                Debugg.Info("Swapchain resize buffers");
                swapchain.ResizeBuffers(Description.BufferCount, width, height, format, Description.Flags);
            }
            else
            {
                Debugg.Message("Swapchain can't resize buffers, create new one");

                RemoveAndDispose(ref swapchain);

                var desc = Description.GetSwapChainDescription_new();
                var fulldesc = Description.GetSwapChainnDescriptionFullScreen();

                // Rather than create a new DXGI Factory we should reuse
                // the one that has been used internally to create the device
                using (var dxgiDevice2 = d3dDevice.QueryInterface<DXGI.Device2>())
                using (var dxgiAdapter = dxgiDevice2.Adapter)
                using (var dxgiFactory2 = dxgiAdapter.GetParent<DXGI.Factory2>())
                {
                    var HASH = GetHashCode().ToString("X4");
                    //SPECIFIED CREATION FOR HWND, ONLY FOR DESKTOP APPLICATION

                    //swapchain = ToDispose(new SwapChain1(dxgiFactory2, d3dDevice, Description.OutputWnd.Handle, ref desc, fulldesc, null));

                    using (var m_swapchain = new SwapChain1(dxgiFactory2, d3dDevice, Description.OutputWnd.Handle, ref desc, fulldesc, null))
                        swapchain = ToDispose(m_swapchain.QueryInterface<SwapChain2>(), "swapchain2_of_" + ImmutableHash.ToString("X4"));

                    //Ignore all windows events (ALT-ENTER will be custom implemented)
                    var hwnd = Description.OutputWnd.GetMainForm();
                    dxgiFactory2.MakeWindowAssociation(hwnd.Handle, WindowAssociationFlags.IgnoreAltEnter);
                }

                //go to fullscreen only after create swapchain in windowed
                if (Description.IsFullScreen)
                {
                    var resizeMode = new ModeDescription(Description.ScreenSize.width, Description.ScreenSize.height, new Rational(0, 0), Format.Unknown);

                    // Before fullscreen switch
                    swapchain.ResizeTarget(ref resizeMode);
                    // Switch to full screen

                    swapchain.IsFullScreen = true;
                    //swapchain.SetFullscreenState(true, null);
                    // The MSDN documentation says to call resize targets again with a zeroed refresh rate after setting the mode: 
                    // https://msdn.microsoft.com/en-us/library/windows/desktop/ee417025(v=vs.85).aspx.
                    swapchain.ResizeTarget(ref resizeMode);

                    // This is really important to call ResizeBuffers AFTER switching to IsFullScreen 
                    swapchain.ResizeBuffers(Description.BufferCount, 0, 0, Format.Unknown, SwapChainFlags.AllowModeSwitch);

                    SwapChainDescription swapchainDescr = swapchain.Description;
                    Description.BackBufferSize.width = swapchainDescr.ModeDescription.Width;
                    Description.BackBufferSize.height = swapchainDescr.ModeDescription.Height;
                    Description.BackBufferFormat = (PixelFormat)swapchainDescr.ModeDescription.Format;

                    Debugg.Message($"Swapchain: fullscreen report:\n {Description}");
                }
            }
            backbuffer = ToDispose(new RenderTarget(Device, swapchain.GetBackBuffer<Texture2D>(0)));

            //reassign direct2 target !
            if (IsValid(d2device))
            {
                d2target = ToDispose(D2Bitmap.New(d2device, backbuffer));
                if (reassignnewtarget) d2device.Context.Target = d2target;
            }

            CreateDepthStencilBuffer();

            DefaultViewport = new ViewportClip(0, 0, Description.BackBufferSize.width, Description.BackBufferSize.height, 0, 1);
        }

        /// <summary>
        /// The correct sequence to entering in fullscreen mode
        /// </summary>
        /// <remarks>ensure disposing all the Device-size-dependent resources</remarks>
        public void EnterFullscreen()
        {
            screenswithing = true;

            if (!IsValid(swapchain))
                throw new ArgumentNullException("to fullscreen switching: swapchain is disposed !");

            if (!Description.IsOutputWndMainForm)
                throw new ArgumentException(
                    "swapchain can't switch to fullscreen if it's a child's windows, need association with main Form" +
                    "\n" +
                    "DXGI ERROR: IDXGISwapChain::SetFullscreenState: Fullscreen is not allowed for swapchains targetting a child window." +
                    "Applications are advised to create a second swapchain targeting a non-child window. [ MISCELLANEOUS ERROR #82: ]");

            swapchain.GetFullscreenState(out var isCurrentlyFullscreen, out _);

            if (isCurrentlyFullscreen)
            {
                Debugg.Message("Swapchain: already in fullscreen");
                return;
            }
            Debugg.Message("Swapchain: switching to fullscreen");

            // Bring the control up before attempting to switch to full screen.
            // Otherwise things get real weird, real fast.
            if (!Description.OutputWnd.Visible)
                Description.OutputWnd.Visible = true;

            // this flag is used in CreateDeviceDependentResources()
            Description.IsFullScreen = true;
            // Destroy and recreate the full swapchain in case of fullscreen switch
            // It seems to be more reliable then trying to change the current swap-chain.
            CreateSizeDependentResources();

            if (!swapchain.IsFullScreen) throw new Exception("swapchain didn't go in fullscreen mode");

            screenswithing = false;
        }

        /// <summary>
        /// The correct sequence to exiting from fullscreen mode
        /// </summary>
        public void ExitFullscreen()
        {
            screenswithing = true;

            Debugg.Message("Swapchain: switching to windowed");

            Description.IsFullScreen = false;
            swapchain.IsFullScreen = false;

            Resize(Description.PreferredBackBufferSize, Description.BackBufferFormat);

            var resizeMode = new ModeDescription(Description.BackBufferSize.width, Description.BackBufferSize.height, new Rational(0, 0), (Format)Description.BackBufferFormat);
            
            swapchain.ResizeTarget(ref resizeMode);

            screenswithing = false;
        }

        /// <summary>
        /// Resizes the current presenter, by resizing the backbuffer and the depth stencil buffer.
        /// It's not called by EnterFullScreen.<br/>
        /// <b>Swapchain cannot be resized unless all outstanding buffer references have been released</b> 
        /// but not need to dispose <see cref="D2Device"/> and <see cref="D2Context"/>.
        /// </summary>
        /// <param name="format">backbuffer format, the depthstencilbuffer format is defined in <see cref="PresentationParameters"/></param>
        public bool Resize(Vector2i size, PixelFormat? pxformat = null)
        {
            if (Description.IsFullScreen)
            {
                Debugg.Message("Swapchain: can't be resized in fullscreen mode");
                return true;
            }

            if (size.width <= 0 || size.height <= 0) throw new ArgumentException("backbuffer Size can't be zero");

            //mantain same backbufferformat if you don't make a decision
            var format = pxformat == null ? Description.BackBufferFormat : (PixelFormat)pxformat;

            if (size == Description.BackBufferSize && format == Description.BackBufferFormat)
            {
                Debugg.Warning("Swapchain: resizing with same Size, do nothing !");
                return true;
            }

            Description.BackBufferSize = size;
            Description.BackBufferFormat = format;


            // update cached size if Resize are called manualy when client resizing
            if (!Description.IsFullScreen)
            {
                Description.PreferredBackBufferSize = size;
            }
            Debugg.Info($"Swapchain: resizing to {Description.BackBufferSize}");

            CreateSizeDependentResources();

            return true;
        }

        /// <summary>
        /// Before start drawing, it assigns the backbuffer, depthstencilbuffer and viewport size
        /// </summary>
        public override bool BeginDraw(D3Context context)
        {
            if (!base.BeginDraw(context)) return false;
            //if (IsValid(d2device)) d2device.Context.Target = d2target;
            context.SetViewports(DefaultViewport);
            //reset to the whole backbuffer size
            context.SetScissor(DefaultViewport.Rectangle);
            return true;
        }

        /// <summary>
        /// Before start drawing, it assigns the backbuffer's bitmap target for <see cref="D2Context"/>.<br/>
        /// Stop drawing with <b><see cref="D2Context.EndDraw()"/></b>
        /// </summary>
        public bool BeginDraw2D(D2Context context)
        {
            if (!IsValid(d2device)) return false;
            d2device.Context.Target = d2target;
            d2drawing = d2device.Context.BeginDraw();
            return d2drawing;
        }

        /// <summary>
        /// Presents the Backbuffer to the screen. If was called the BeginDraw method of <see cref="D2Context"/>, it call endDraw
        /// </summary>
        /// <exception cref="SharpDXException"></exception>
        public override void EndDrawsAndPresent()
        {
            if (d2drawing) d2device.Context.EndDraw();

            //add time before present when use vsync for a correct performance analysis 
            base.EndDrawsAndPresent();

            if (screenswithing) return;

            Result result;
            try
            {
                var t0 = TimerTick.Ticks;
                result = swapchain.Present((int)Description.PresentationInterval, PresentFlags.None);
                Stats.PresentingCost = TimerTick.Ticks - t0;
            }
            catch (SharpDXException ex)
            {
                // If the device was removed either by a disconnect or a driver upgrade, we 
                // must completely reinitialize the renderer.
                if (ex.ResultCode == DXGI.ResultCode.DeviceRemoved)
                    throw new Exception("DeviceRemoved");
                else if (ex.ResultCode == DXGI.ResultCode.DeviceReset)
                    throw new Exception("DeviceReset");
                else
                    throw;
            }

            if (!result.Success)
            {
                Debugg.Message($"Swapchain: ERROR when presenting\nreport: {result}");
            }
           
        }

        /// <summary>
        /// Clears the default render target and depth stencil buffer attached to the current <see cref="Presenter"/>.
        /// </summary>
        /// <param name="options">Options for clearing a buffer.</param>
        /// <param name="color">Set this four-component color value in the buffer.</param>
        /// <param name="depth">Set this depth value in the buffer.</param>
        /// <param name="stencil">Set this stencil value in the buffer.</param>
        public override void Clear(Color4b? color = null, float depth = 1, byte stencil = 0)
        {
            RawColor4 rawcolor = color == null ? clearcolor : color.Value.ToRawColor();
            
            var context = Device.Context;
            if (context != null)
            {
                context.d3contex.ClearDepthStencilView(depthStencilbuffer, DepthStencilClearFlags.Depth | DepthStencilClearFlags.Stencil, depth, stencil);
                context.d3contex.ClearRenderTargetView(backbuffer, rawcolor);
            }
        }

        /// <summary>
        ///  ensure disposing all the Device-size-dependent resources
        /// </summary>
        protected override void Dispose(bool disposeManagedResources)
        {
            // Direct3D is actually incapable of closing when in fullscreen mode.
            // This is due to certain threading issues that occur behind the scenes.
            // To correctly close down, we must make sure that we are in windowed mode.
            // We can use the function SetFullscreenState() to do this.
            if (swapchain != null && !swapchain.IsDisposed) swapchain.SetFullscreenState(false, null);

            base.Dispose(disposeManagedResources);
        }
    }
}
