﻿using System;
using System.Diagnostics;

using SharpDX.DXGI;
using SharpDX;
using SharpDX.Mathematics.Interop;
using SharpDX.Direct2D1;

using Common;
using Common.Maths;
using SharpDX.Direct3D11;
using System.Windows.Forms;
using Engine.Direct2D;

namespace Engine
{
    public class PresenterStats
    {
        /// <summary>
        /// Ticks from begin to end drawing
        /// </summary>
        public long FrameCost = 0;
        /// <summary>
        /// Ticks of Present() call
        /// </summary>
        public long PresentingCost = 0;


        public void Reset()
        {
            FrameCost = 0;
            PresentingCost = 0;
        }
    }


    /// <summary>
    /// Defines flags that describe the relationship between the adapter refresh rate and the rate at which Present operations are completed. 
    /// </summary>
    public enum PresentInterval
    {
        /// <summary>
        /// The runtime updates the window client area immediately, and might do so more than once during the adapter refresh period. Present operations might be affected immediately. This option is always available for both windowed and full-screen swap chains.
        /// </summary>
        Immediate = 0,

        /// <summary>
        /// The driver waits for the vertical retrace period (the runtime will beam trace to prevent tearing). Present operations are not affected more frequently than the screen refresh rate; the runtime completes one Present operation per adapter refresh period, at most. This option is always available for both windowed and full-screen swap chains.
        /// </summary>
        One = 1,

        /// <summary>
        /// The driver waits for the vertical retrace period. Present operations are not affected more frequently than every second screen refresh. 
        /// </summary>
        Two = 2,

        /// <summary>
        /// Equivalent to setting <see cref="One"/>.
        /// </summary>
        Default = One,
    }
    
    /// <summary>
    /// Describes how data will be displayed to the screen.
    /// </summary>
    public abstract class Presenter : GraphicChild
    {
        public PresenterStats Stats;

        long beginticks = 0;

        /// <summary>
        /// How many ticks from <see cref="BeginDraw(D3Context)"/> to <see cref="EndDrawsAndPresent"/> 
        /// but before <see cref="SwapChain.Present(int, PresentFlags)"/> because in Vsync it's affected by thread sleep time to match the screen refresh rate
        /// </summary>
        

        protected DepthStencilBuffer depthStencilbuffer;

        protected readonly D2Device d2device;

        /// <summary>
        /// Gets the description of this presenter.
        /// </summary>
        public PresentationParameters Description { get; private set; }

        /// <summary>
        /// Gets the default back buffer for this presenter. Attention, all resources that use this surface must be disposed
        /// </summary>
        public abstract RenderTarget BackBuffer { get; }
        /// <summary>
        /// Gets the render targhet assigned to this backbuffer, if the <see cref="D2Device"/> is created, the renderer use the Direct2Context methods
        /// Attention, it will be disposed at backbuffer disposing
        /// </summary>
        //public RenderTarget2D Renderer2d => renderer2d;

        /// <summary>
        /// Gets the default depth stencil buffer for this presenter.
        /// </summary>
        public DepthStencilBuffer DepthStencilBuffer
        {
            get => depthStencilbuffer;
            protected set => depthStencilbuffer = value;
        }
        /// <summary>
        /// </summary>
        /// <param name="PresentParameters">It will be a shallow copy so no reference are used</param>
        /// <param name="d2device">optional if you want enable the direct2d functionality</param>
        protected Presenter(D3Device device, PresentationParameters PresentParameters, D2Device d2device = null, string debugname = null) : base(device, debugname)
        {
            Description = PresentParameters.ShallowCopy();
            this.d2device = d2device;
            Stats = new PresenterStats();
        }

        /// <summary>
        /// Assign the backbuffer, depthstencilbuffer and set the current viewport size
        /// </summary>
        public virtual bool BeginDraw(D3Context context)
        {
            Stats.Reset();
            beginticks = TimerTick.Ticks;
            context.SetRenderTargets(BackBuffer, DepthStencilBuffer);
            return true;
        }
        /// <summary>
        /// Clear the backbuffer and depthstencilbuffer
        /// </summary>
        public abstract void Clear(Color4b? color = null, float depth = 1, byte stencil = 0);

        /// <summary>
        /// Presents the Backbuffer to the screen.
        /// </summary>
        public virtual void EndDrawsAndPresent()
        {
            Stats.FrameCost = TimerTick.Ticks - beginticks;
            //if (!begindrawOK) throw new Exception("You call EndDraw but BeginDraw failed or was not called");
        }

        /// <summary>
        /// Default viewport that covers the whole presenter surface.
        /// </summary>
        public ViewportClip DefaultViewport { get; protected set; }


        /// <summary>
        /// Get the control mouse position in backbuffer coordinates
        /// </summary>
        public Vector2f MousePosition(Vector2f mouseControlPosition, Vector2i clientsize)
        {
            if (clientsize.IsZero) return default;
            return new Vector2f(
                mouseControlPosition.x / clientsize.x * Description.BackBufferSize.x,
                mouseControlPosition.y / clientsize.y * Description.BackBufferSize.y);
        }



        /// <summary>
        /// Change size mean recreate it. The size must be the same of backbuffer
        /// </summary>
        /// <exception cref="ArgumentException">depthstencilbuffer can't have a zero size</exception>
        protected void CreateDepthStencilBuffer()
        {
            if (Description.DepthStencilFormat == DepthFormat.None)
            {
                Debugg.Error("DepthStencilBuffer format is None, can't create it");
                return;
            }

            if (Description.BackBufferSize.width * Description.BackBufferSize.height <= 0)
                throw new ArgumentException("DepthStencilBuffer Size cannot be zero");

            Debugg.Info($"Depthstencilbuffer create {Description.BackBufferSize} {Description.DepthStencilFormat}");
            
            RemoveAndDispose(ref depthStencilbuffer);
            depthStencilbuffer = ToDispose(DepthStencilBuffer.New(
                Device,
                Description.BackBufferSize,
                Description.MultiSampleCount,
                Description.DepthStencilFormat,
                Description.DepthBufferShaderResource));
        }

    }
}
