﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Common;
using SharpDX;
using SharpDX.Direct3D11;

namespace Engine
{
    /// <summary>
    /// A device dependent resources, the base class for all resources of this "big family".
    /// </summary>
    public abstract class GraphicChild : DisposableDx
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        protected readonly D3Device device;

        /// <summary>
        /// </summary>
        /// <param name="debugname">a good practice is to give a name to debug</param>
        protected GraphicChild(D3Device device, string debugname = null) : base(debugname)
        {
            if (device == null)
#if DEBUG
            Debug.Print("Device can't be null");
#else
            throw new ArgumentNullException("Device can't be null");
#endif
            this.device = device;
        }
        /// <summary>
        /// change device is not allowed
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        public D3Device Device
        {
            get { return device; }

            private set 
            {
                //just because I want to keep this method private
                //var methodInfo = device.GetType().GetMethod("ToDispose", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                //methodInfo = methodInfo.MakeGenericMethod(typeof(GraphicChild));
                //methodInfo.Invoke(device, new object[] { this , Name});
            }
        }
    }


    /// <summary>
    /// A device dependent resources
    /// Resources provide data to the pipeline and define what is rendered during your scene.
    /// Typically, resources include texture data, vertex data, and shader data
    /// </summary>
    public abstract class D3Resource : GraphicChild
    {
        /// <summary>
        /// <inheritdoc cref="GraphicChild"/><br/>
        /// Resources provide data to the pipeline and define what is rendered during your scene.<br/>
        /// Typically, resources include texture data, vertex data, and shader data
        /// </summary>
        protected D3Resource(D3Device device, string debugname = null) : base(device, debugname) { }

        /// <summary>
        /// The attached resource to this instance (for casting purpose)
        /// </summary>
        protected abstract Resource Resource { get; }
        /// <summary>
        /// If disposed is <see cref="IntPtr.Zero"/>
        /// </summary>
        public abstract IntPtr NativePointer { get; }

        /// <summary>
        /// Implicit casting operator to <see cref="Resource"/>
        /// </summary>
        public static implicit operator Resource(D3Resource resource)
        {
            if (resource.Resource == null) throw new ArgumentNullException("SharpDX.Direct3D11.Native is null");
            return resource.Resource;
        }

        protected static CpuAccessFlags GetCpuAccess(BufferAccess access)
        {
            var cpu = CpuAccessFlags.None;
            if (access.HasFlag(BufferAccess._CPU_write)) cpu |= CpuAccessFlags.Write;
            if (access.HasFlag(BufferAccess._CPU_read)) cpu |= CpuAccessFlags.Read;
            return cpu;
        }
        protected static ResourceUsage GetUsage(BufferAccess access)
        {
            switch(access)
            {
                case BufferAccess.Immutable: return ResourceUsage.Immutable;
                case BufferAccess.Dynamic: return ResourceUsage.Dynamic;
                case BufferAccess.Staging: return ResourceUsage.Staging;
                case BufferAccess.Default: return ResourceUsage.Default;
                default: throw new ArgumentException($"Unknow BufferAccess {access} flag");
            }
        }

        /// <summary>
        /// Gets the CPU access flags from the <see cref="ResourceUsage"/>.
        /// </summary>
        protected static CpuAccessFlags GetCpuAccess(ResourceUsage usage)
        {
            switch (usage)
            {
                case ResourceUsage.Dynamic: return CpuAccessFlags.Write;
                case ResourceUsage.Staging: return CpuAccessFlags.Read | CpuAccessFlags.Write;
                case ResourceUsage.Immutable: 
                default: return CpuAccessFlags.None;
            }
        }

        /// <summary>
        /// Only native pointer define the class equality. If the native resource is re-initialized, the class is defined as different
        /// </summary>
        public bool Equals(D3Resource other)
        {
            return Equals(this, other);
        }
        public override bool Equals(object obj) => Equals(obj as D3Resource);

        public static bool Equals(D3Resource ref1, D3Resource ref2)
        {
            if (ref1 == null && ref2 != null) return false;
            if (ref2 == null && ref1 != null) return false;
            if (ref1 == null && ref2 == null) return true;
            return ref1.NativePointer == ref2.NativePointer;
        }
        public override int GetHashCode()
        {
            return NativePointer != IntPtr.Zero ? NativePointer.GetHashCode() : base.GetHashCode();
        }

    }


    /// <summary>
    /// A shared resource obeys different rules: when owner disposing it reduce reference by one and can be disposed 
    /// only after reference count equal to zero
    /// </summary>
    public abstract class D3SharedResource : D3Resource
    {
        protected List<object> references;
        protected int ReferenceCount => references.Count;

        protected D3SharedResource(D3Device device, string debugname = null) : base(device, debugname)
        {
            references = new List<object>();
        }

        public void AddReference(object owner)
        {
            if (!references.Contains(owner)) references.Add(owner);
        }
        public void ReleaseReference(object owner)
        {
            references.Remove(owner);
        }

        public override void Dispose()
        {
            if (references.Count == 0)
            {
                base.Dispose();
            }
        }
    }
}
