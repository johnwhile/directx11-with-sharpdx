﻿
using Common;
using Common.Maths;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.Mathematics.Interop;
using System;

namespace Engine
{
    public class ContextStats
    {
        public int DrawCount;
        public int AssignVBufferCount;
        public int AssignIBufferCount;
        public int AssignCBufferDataCount;
        public int AssignCBufferCount;
        public int AssignVShaderCount;
        public int AssignPShaderCount;
        public int AssignGShaderCount;
        public int AssignBlend;
        public int AssignDepth;
        public int AssignRasterizer;
        public int AssignInputlayout;

        public void Reset()
        {
            DrawCount = 0;
            AssignVBufferCount = 0;
            AssignIBufferCount = 0;
            AssignCBufferDataCount = 0;
            AssignCBufferCount = 0;
            AssignVShaderCount = 0;
            AssignPShaderCount = 0;
            AssignGShaderCount = 0;
            AssignBlend = 0;
            AssignDepth = 0;
            AssignRasterizer = 0;
            AssignInputlayout = 0;
        }
        public override string ToString()
        {
            return $"draws {DrawCount}\nvb {AssignVBufferCount}\nvs {AssignVShaderCount}";
        }
    }


    /// <summary>
    /// The SharpDX.Direct3D11.DeviceContext
    /// </summary>
    public class D3Context : GraphicChild
    {
        const int MAXSLOTS = 16;
        public ContextStats Stats = new ContextStats();

        internal DeviceContext1 d3contex;
        ViewportClip currentViewport;
        
        /// <summary>
        /// Get "relative" viewport of context
        /// </summary>
        public ViewportClip CurrentViewport => currentViewport;

        public D3Context(D3Device device) : base(device)
        {
            //d3contex = ToDispose(device.d3device.ImmediateContext.QueryInterface<DeviceContext1>());
            d3contex = ToDispose(device.d3device.ImmediateContext1);

            current_vb = new IntPtr[MAXSLOTS];
            current_vs_samplers = new IntPtr[MAXSLOTS];
            current_ps_samplers = new IntPtr[MAXSLOTS];
            current_vs_constants = new IntPtr[MAXSLOTS];
            current_ps_constants = new IntPtr[MAXSLOTS];
            current_gs_constants = new IntPtr[MAXSLOTS];
            current_vs_textures = new IntPtr[MAXSLOTS];
            current_ps_textures = new IntPtr[MAXSLOTS];
        }

        #region states setting optimization
        IntPtr[] current_vb;

        IntPtr current_ps;
        IntPtr current_vs;
        IntPtr current_gs;
        IntPtr current_blend;
        IntPtr current_depth;
        IntPtr current_rasterizer;
        IntPtr current_inputlayout;

        IntPtr[] current_vs_constants;
        IntPtr[] current_ps_constants;
        IntPtr[] current_gs_constants;

        IntPtr[] current_vs_samplers;
        IntPtr[] current_ps_samplers;
        IntPtr[] current_vs_textures;
        IntPtr[] current_ps_textures;

        /// <summary>
        /// automatic check if it has already been assigned, from literature to optimize is better don't set it twice
        /// </summary>
        bool applyifneed(ref IntPtr current, D3Resource resource)
        {
            IntPtr newref = resource?.NativePointer ?? IntPtr.Zero;
            bool need = current != newref;
            current = newref;
            return need;
        }
        void setContantBuffer(ConstantBuffer constbuffer, CommonShaderStage stage, int slot)
        {
            constbuffer.BindToShaderStage(stage, slot);
            Stats.AssignCBufferCount++;
        }

        public void SetRenderState(BlendStateDx state)
        {
            if (!applyifneed(ref current_blend, state)) return;
            d3contex.OutputMerger.BlendState = state;
            Stats.AssignBlend++;
        }
        public void SetRenderState(DepthStateDx state)
        {
            if (applyifneed(ref current_depth, state)) { d3contex.OutputMerger.DepthStencilState = state; Stats.AssignDepth++; }
        }
        public void SetRenderState(RasterizerStateDx state)
        {
            if (applyifneed(ref current_rasterizer, state)) {d3contex.Rasterizer.State = state; Stats.AssignRasterizer++; }
        }
        public void SetRenderState(InputLayoutDx state)
        {
            if (applyifneed(ref current_inputlayout, state)) { d3contex.InputAssembler.InputLayout = state; Stats.AssignInputlayout++; }
        }

        /// <summary>
        /// passing a <see cref="VertexShaderDx.NULL"/> is equivalent of casting (<see cref="VertexShaderDx"/>)<b>null</b>
        /// </summary>
        public void SetShader(VertexShaderDx vs)
        {
            if (applyifneed(ref current_vs, vs)) { d3contex.VertexShader.Set(vs); Stats.AssignVShaderCount++; }
        }
        /// <summary>
        /// <inheritdoc cref="SetShader(VertexShaderDx)"/>
        /// </summary>
        public void SetShader(PixelShaderDx ps)
        {
            if (applyifneed(ref current_ps, ps)) {d3contex.PixelShader.Set(ps); Stats.AssignPShaderCount++; }
        }
        /// <summary>
        /// <inheritdoc cref="SetShader(VertexShaderDx)"/>
        /// </summary>
        public void SetShader(GeometryShaderDx gs)
        {
            if (applyifneed(ref current_gs, gs)) {d3contex.GeometryShader.Set(gs); Stats.AssignGShaderCount++; }
        }
        public void SetBuffer(VertexBuffer buffer, int slot = 0)
        {
            if (buffer == null) return;
            if (!applyifneed(ref current_vb[slot], buffer)) return;
            buffer.Bind(this, slot);
            Stats.AssignVBufferCount++;
        }
        public void SetBuffer(IndexBuffer buffer, int slot = 0)
        {
            if (buffer == null) return;
            buffer.Bind(this, slot);
            Stats.AssignIBufferCount++;
        }        
        /// <summary>
        /// not implemented the vertex texture
        /// </summary>
        public void SetToVertexStage(TextureBase texture, int slot = 0)
        {
            if (texture == null) return;
            if (applyifneed(ref current_vs_textures[slot], texture)) texture.BindToShaderStage(d3contex.VertexShader, slot);
        }
        /// <summary>
        /// not implemented the vertex texture
        /// </summary>
        public void SetToVertexStage(SamplerStateDx sampler, int slot = 0)
        {
            if (sampler == null) return;
            if (applyifneed(ref current_vs_samplers[slot], sampler)) d3contex.VertexShader.SetSampler(slot, sampler);
        }
        /// <summary>
        /// Set resource to specified pipeline stage, perform also a write if necessary
        /// </summary>
        public void SetToVertexStage(ConstantBuffer constbuffer, int slot = 0)
        {
            if (constbuffer == null) return;
            constbuffer.Update(this);
            if (applyifneed(ref current_vs_constants[slot], constbuffer)) 
                setContantBuffer(constbuffer, d3contex.VertexShader, slot);
        }
        public void SetToVertexStage(ConstantBuffer[] buffers, ushort validSlots = ushort.MaxValue)
        {
            if (buffers == null) return;
            for (int i = 1, j = 1; j < validSlots + 1; j <<= 1, i++)
                if ((j & validSlots) != 0)
                    SetToVertexStage(buffers[i], i);
        }
        public void SetToVertexStage(StructuredBuffer buffer, int slot = 0)
        {
            if (buffer == null) return;
            buffer.BindToShaderStage(d3contex.VertexShader, slot);
        }

        /// <summary>
        /// <inheritdoc cref="SetToVertexStage(ConstantBuffer, int)"/>
        /// </summary>
        public void SetToPixelStage(ConstantBuffer constbuffer, int slot = 0)
        {
            if (constbuffer == null) return;
            constbuffer.Update(this);
            if (applyifneed(ref current_ps_constants[slot], constbuffer)) 
                setContantBuffer(constbuffer, d3contex.PixelShader, slot);
        }
        public void SetToPixelStage(StructuredBuffer buffer, int slot = 0)
        {
            if (buffer == null) return;
            buffer.BindToShaderStage(d3contex.PixelShader, slot);
        }
        public void SetToPixelStage(TextureBase texture, int slot = 0)
        {
            if (texture == null) return;
            if (applyifneed(ref current_ps_textures[slot], texture)) texture.BindToShaderStage(d3contex.PixelShader, slot);
        }
        public void SetToPixelStage(TextureBase[] textures, ushort validSlots = ushort.MaxValue)
        {
            if (textures == null) return;
            for (int i = 1, j = 1; j < validSlots + 1; j <<= 1, i++)
                if ((j & validSlots) != 0) SetToPixelStage(textures[i], i);
        }
        public void SetToPixelStage(SamplerStateDx sampler, int slot = 0)
        {
            if (sampler == null) return;
            if (applyifneed(ref current_ps_samplers[slot], sampler)) d3contex.PixelShader.SetSampler(slot, sampler);
        }
        public void SetToPixelStage(SamplerStateDx[] samplers, ushort validSlots = ushort.MaxValue)
        {
            if (samplers == null) return;
            for (int i = 1, j = 1; j < validSlots + 1; j <<= 1, i++)
                if ((j & validSlots) != 0) SetToPixelStage(samplers[i], i);
        }

        /// <summary>
        /// <inheritdoc cref="SetToVertexStage(ConstantBuffer, int)"/>
        /// </summary>
        public void SetToGeometryStage(ConstantBuffer constbuffer, int slot = 0)
        {
            if (constbuffer == null) return;
            constbuffer.Update(this);
            if (applyifneed(ref current_gs_constants[slot], constbuffer))
                constbuffer.BindToShaderStage(d3contex.GeometryShader, slot);
        }


        #endregion

        /// <summary>
        /// </summary>
        /// <param name="vertexCount">Number of vertices to draw.</param>
        /// <param name="startVertex">Index of the first vertex, which is usually an offset in a vertex buffer.</param>
        public void Draw(int vertexCount, int startVertex = 0)
        {
            Stats.DrawCount++;
            d3contex.Draw(vertexCount, startVertex);
        }
        /// <summary>
        /// </summary>
        /// <param name="indexCount">Number of indices to use to draw.</param>
        /// <param name="startIndex">Index of the first index</param>
        /// <param name="vertexOffset">An integer value to be added to the indices used in this draw call before the vertices are fetched.<br/>
        /// Used when vertexbuffer is a concatenation of multiple geometries.</param>
        public void DrawIndexed(int indexCount, int startIndex = 0, int vertexOffset = 0)
        {
            Stats.DrawCount++;
            d3contex.DrawIndexed(indexCount, startIndex, vertexOffset);
        }
        public void DrawInstanced(int vertexCount, int instances, int startVertex = 0, int startInstance = 0)
        {
            Stats.DrawCount++;
            d3contex.DrawInstanced(vertexCount, instances, startVertex, startInstance);
        }
        /// <summary>
        /// This method resets any device context to the default settings.
        /// This sets all input/output resource slots, shaders, input layouts, predications, scissor rectangles, depth-stencil state,
        /// rasterizer state, blend state, sampler state, and viewports to <strong><c>null</c></strong>.
        /// The primitive topology is set to UNDEFINED.
        /// For a scenario where you would like to clear a list of commands recorded so far, call <strong><see cref="SharpDX.Direct3D11.DeviceContext.FinishCommandListInternal"/></strong> and throw away the resulting <strong><see cref="SharpDX.Direct3D11.CommandList"/></strong>.
        /// </summary>
        public void ClearState()
        {
            d3contex.ClearState();
            d3contex.Flush();
            current_blend = IntPtr.Zero;
            current_depth = IntPtr.Zero;
            current_rasterizer = IntPtr.Zero;
            current_inputlayout = IntPtr.Zero;
            Array.Clear(current_vb, 0, MAXSLOTS);
            Array.Clear(current_vs_samplers, 0, MAXSLOTS);
            Array.Clear(current_ps_samplers, 0, MAXSLOTS);
            Array.Clear(current_vs_constants, 0, MAXSLOTS);
            Array.Clear(current_vs_textures, 0, MAXSLOTS);
            Array.Clear(current_ps_textures, 0, MAXSLOTS);
        }

        public DxPrimitive PrimitiveTopology
        {
            get => (DxPrimitive)d3contex.InputAssembler.PrimitiveTopology;
            set => d3contex.InputAssembler.PrimitiveTopology = (PrimitiveTopology)value;
        }

        /// <summary>
        /// Binds a depth-stencil buffer and a set of render targets to the output-merger stage.
        /// set both null to reset.
        /// </summary>
        public void SetRenderTargets(RenderTarget renderTargetView, DepthStencilBuffer depthStencilBuffer = null)
        {
            if (depthStencilBuffer == null && renderTargetView == null)
            {
                d3contex.OutputMerger.ResetTargets();
                return;
            }

            if (renderTargetView == null) throw new ArgumentNullException("renderTargetView can't be null");

            if (depthStencilBuffer != null)
            {
                if (depthStencilBuffer.Size != renderTargetView.Size)
                    throw new Exception("The RenderTargetView is not compatible, need same Size of DepthStencilView");
            }

            d3contex.OutputMerger.SetTargets(depthStencilBuffer, renderTargetView);

        }

        /// <summary>
        /// Binds a set of viewports to the rasterizer stage.
        /// </summary>
        public void SetViewports(ViewportClip viewport)
        {
            currentViewport = viewport;
            d3contex.Rasterizer.SetViewport(viewport.ToRawViewportFloat());
        }
        /// <summary>
        /// <inheritdoc cref="SetViewports(ViewportClip)"/>
        /// </summary>
        public void SetViewports(Rectangle4i rectangle, float mindepth = 0, float maxdepth = 1)
        {
            SetViewports(new ViewportClip(rectangle, mindepth, maxdepth));
            /*
            currentViewport.X = rectangle.x;
            currentViewport.Y = rectangle.y;
            currentViewport.Width = rectangle.width;
            currentViewport.Height = rectangle.height;
            currentViewport.MinDepth = mindepth;
            currentViewport.MaxDepth = maxdepth;
            d3contex.Rasterizer.SetViewport(rectangle.x, rectangle.y, rectangle.width, rectangle.height, mindepth, maxdepth);
            */
        }
        /// <summary>
        /// ScissorEnable must be set to <b>true</b> in the rasterizer state <b><see cref="RasterizerStateDescription.IsScissorEnabled"/></b>
        /// </summary>
        public void SetScissor(Rectangle4i rectangle)
        {
            // according from https://github.com/sharpdx/SharpDX/blob/master/Source/SharpDX.Mathematics/Rectangle.cs
            d3contex.Rasterizer.SetScissorRectangle(rectangle.x, rectangle.y, rectangle.x + rectangle.width, rectangle.y + rectangle.height);
            //var check = d3contex.Rasterizer.GetScissorRectangles<RawRectangle>();
        }

        /// <summary>
        /// </summary>
        public ViewportClip GetViewports()
        {
            RawViewportF raw = d3contex.Rasterizer.GetViewports<RawViewportF>()[0];
            return raw.ToCommon();
        } 


        public static implicit operator DeviceContext(D3Context from)
        {
            return from != null ? from.d3contex : throw new NullReferenceException($"D3Context casting with null reference");
        }

        protected override void Dispose(bool disposeManaged)
        {
            // Clean some states

            if (!d3contex.IsDisposed)
            {
                d3contex.OutputMerger.ResetTargets();
                d3contex.ClearState();
                d3contex.Flush();
            }

            base.Dispose(disposeManaged);

            /*
            d3contex.OutputMerger.Dispose();
            d3contex.Rasterizer.Dispose();
            d3contex.InputAssembler.Dispose();
            d3contex.VertexShader.Dispose();
            d3contex.PixelShader.Dispose();
            d3contex.GeometryShader.Dispose();
            d3contex.HullShader.Dispose();
            */
            //context and device are mutual referenced, so they have always 1 pending resource
            //ReleaseComObject(ref d3contex);
        }
    }
}
