﻿using System;

using Common;
using Common.Maths;

using D2D = SharpDX.Direct2D1;
using SharpDX.Direct2D1;
using SharpDX.DXGI;
using System.IO;

namespace Engine.Direct2D
{
    public class D2Context : D2GraphicTargetBase
    {
        internal D2Device device;
        internal DeviceContext d2context;
        D2BitmapTarget target;

        public D2BitmapTarget Target
        {
            get
            {
                return target;
            }
            set
            {
                if (target != value)
                {
                    OnRenderTargetReleased();
                    target = value;
                    d2context.Target = target;
                    OnRenderTargetAssigned();
                }

            }
        }

        /// <summary>
        /// native draws methods
        /// </summary>
        protected override D2D.RenderTarget rendertarget => d2context;

        /// <summary>
        /// </summary>
        /// <param name="device">attacked device</param>
        public D2Context(D2Device device) : base()
        {
            this.device = device;
            CreateDeviceDependentResources();
        }
        /// <summary>
        /// Device2 is indipendent from Backbuffer but require it to draw
        /// </summary>
        public void CreateDeviceDependentResources()
        {
            RemoveAndDispose(ref d2context);
            d2context = ToDispose(new DeviceContext(device, DeviceContextOptions.None));
            // Set D2D text anti-alias mode to Grayscale to ensure proper rendering of text on intermediate surfaces.
            d2context.TextAntialiasMode = TextAntialiasMode.Grayscale;
        }

        public override bool BeginDraw()
        {
            if (!base.BeginDraw()) return false;
            if (target == null || target.IsDisposed) throw new ArgumentException("Require a valid D2Context's Target");
            return true;
        }

        public override Bitmap LoadImage(string filename, bool canread = false)
        {
            if (!File.Exists(filename)) throw new FileNotFoundException(filename);

            var props = new BitmapProperties1()
            {
                PixelFormat = new D2D.PixelFormat(Format.R8G8B8A8_UNorm, D2D.AlphaMode.Premultiplied),
                DpiX = d2context.DotsPerInch.Width,
                DpiY = d2context.DotsPerInch.Height,
                BitmapOptions = canread ? BitmapOptions.CpuRead : BitmapOptions.None
            };

            return Bitmap1.FromWicBitmap(d2context, TextureLoader.LoadBitmap(filename), new BitmapProperties1());
        }

        public static implicit operator DeviceContext(D2Context from)
        {
            return from != null ? from.d2context : throw new NullReferenceException("D2Context casting with null reference");
        }

        protected override void Dispose(bool disposemanaged)
        {
            //ReleaseRenderTarget();
            Target = null;
            base.Dispose(disposemanaged);
        }
    }
}
