﻿
using SharpDX.Direct2D1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using D2D = SharpDX.Direct2D1;
using DXGI = SharpDX.DXGI;

namespace Engine.Direct2D
{
    /// <summary>
    /// A target variant of bitmap surface, used only to render with <see cref="DeviceContext"/>
    /// </summary>
    public class D2BitmapTarget : D2Bitmap
    {
        internal D2BitmapTarget(D2Device device) : base(device.Context)
        {

        }
    }
}
