﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Windows.Forms;

using Common;
using Common.Maths;
using SharpDX.Direct2D1;
using SharpDX.WIC;
using DW = SharpDX.DirectWrite;


namespace Engine.Direct2D
{
    /// <summary>
    /// The ID2D1Factory interface provides the starting point for Direct2D. 
    /// In general, an object created from a single instance of a factory object can be used with other resources
    /// created from that instance, but not with resources created by other factory instances
    /// </summary>
    /// <remarks>
    /// Only one static implementation are necessary
    /// </remarks> 
    internal class Direct2Factory : DisposableDx
    {
        static Direct2Factory singleton;

        Factory1 d2factory;
        DW.Factory dwfactory;
        ImagingFactory2 wicfactory;
        Vector2f dpi;

        Dictionary<string, D2Font> fonts;


        /// <summary>
        /// Direct2D Factory
        /// </summary>
        public static Factory1 D2DFactory => singleton.d2factory;
        /// <summary>
        /// Direct Write Factory
        /// </summary>
        public static DW.Factory DWFactory => singleton.dwfactory;
        /// <summary>
        /// Windows Imaging Component Factory
        /// </summary>
        public static ImagingFactory2 WICFactory => singleton.wicfactory;
        /// <summary>
        /// default is dpi = 96.0f both for vertical and horizontal
        /// https://learn.microsoft.com/en-us/windows/win32/direct2d/how-to--size-a-window-properly-for-high-dpi-displays
        /// </summary>
        public static Vector2f Dpi => singleton.dpi;

        static Direct2Factory()
        {
            singleton = singleton ?? new Direct2Factory();
            Application.ApplicationExit += delegate (object sender, EventArgs args) { singleton.Dispose(); };
        }

        private Direct2Factory() : base("Direct2Factory")
        {
            Debugg.Info("Direct2Factory init");

            DebugLevel d2dflag = DebugLevel.None;
#if DEBUG
            d2dflag = DebugLevel.Warning;
#endif
            d2factory = ToDispose(new Factory1(FactoryType.SingleThreaded, d2dflag));
            dwfactory = ToDispose(new DW.Factory(DW.FactoryType.Shared));
            wicfactory = ToDispose(new ImagingFactory2());

            // Deprecated API ID2D1Factory::GetDesktopDpi. Instead, DisplayProperties::LogicalDpi should be used for Windows Store apps and
            // GetDpiForWindow should be used for Win32 apps.
            using (var graphics = System.Drawing.Graphics.FromHwnd(IntPtr.Zero))
            {
                dpi.x = graphics.DpiX;
                dpi.y = graphics.DpiY;
            }
            //dpi = d2factory.DesktopDpi.ToCommon();

            fonts = new Dictionary<string, D2Font>();
        }

        internal static D2Font GetOrCreateFont(string fontname = "Calibri" , int fontsize = 14)
        {
            string key = fontname.ToLower() + fontsize;
            if (!singleton.fonts.TryGetValue(key, out D2Font font))
            {
                font = singleton.ToDispose(new D2Font(fontname, fontsize));
                singleton.fonts.Add(key, font);
            }
            return font;
        }

        protected override void Dispose(bool disposemanaged)
        {
            Debugg.Info("Direct2Factory disposing");
            base.Dispose(disposemanaged);
        }
    }
}
