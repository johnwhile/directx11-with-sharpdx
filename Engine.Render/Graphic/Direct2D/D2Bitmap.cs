﻿
using System;
using System.IO;

using Common.Maths;

using SharpDX.Direct2D1;
using D2D = SharpDX.Direct2D1;
using DXGI = SharpDX.DXGI;

namespace Engine.Direct2D
{
    public class D2Bitmap : D2GraphicResource
    {
        /// <summary>
        /// It's <see cref="Bitmap1"/> if using <see cref="D2Context"/> as render target
        /// </summary>
        protected Bitmap bitmap;
        protected override Resource resource => bitmap;

        protected D2Bitmap(D2GraphicTargetBase target, string debugname = null) : base(target, debugname)
        {
        }

        public Vector2i Size => bitmap != null ? bitmap.PixelSize.ToCommon() : Vector2i.Zero;

        /// <summary>
        /// TO TEST
        /// </summary>
        public static D2Bitmap New(D2GraphicTargetBase target, Vector2i size, PixelFormat format, bool candraw = true, bool canread = false)
        {
            var Bitmap = new D2Bitmap(target);

            if (target is D2Context context)
            {
                var properties = new BitmapProperties1()
                {
                    PixelFormat = new D2D.PixelFormat((DXGI.Format)format, AlphaMode.Premultiplied),
                    DpiX = context.device.Dpi.x,
                    DpiY = context.device.Dpi.y,
                    BitmapOptions = BitmapOptions.None
                };
                if (!candraw) properties.BitmapOptions |= BitmapOptions.CannotDraw;
                if (canread) properties.BitmapOptions |= BitmapOptions.CpuRead;

                Bitmap.bitmap = new Bitmap1(context, size.ToSharpDx(), properties);
            }
            else
            {
                var properties = new BitmapProperties()
                {
                    PixelFormat = new D2D.PixelFormat((DXGI.Format)format, AlphaMode.Premultiplied),
                    DpiX = Direct2Factory.Dpi.x,
                    DpiY = Direct2Factory.Dpi.y
                };
                Bitmap.bitmap = new Bitmap(target, size.ToSharpDx(), properties);
            }

            return Bitmap;
        }

        /// <summary>
        /// Create a Direct2D image. 
        /// </summary>
        /// <param name="canread">used only on <see cref="D2Context"/> case</param>
        public static D2Bitmap New(D2GraphicTargetBase target, string filename, bool canread = false)
        {
            D2Bitmap Bitmap = null;
            string name = Path.GetFileNameWithoutExtension(filename);
            if (target is D2Context context)
            {
                Bitmap = new D2Bitmap(context, name);
                Bitmap.bitmap = TextureLoader.LoadBitmap(filename, context, canread);
            }
            else if (target is D2WindowTarget wintarget)
            {
                Bitmap = new D2Bitmap(wintarget, name);
                Bitmap.bitmap = TextureLoader.LoadBitmap(filename, wintarget);
            }
            else
            {
                throw new ArgumentException($"target {target} not implemented");
            }
            return Bitmap;
        }

        /// <summary>
        /// Create a Direct2D Render Target bitmap linked to the swapchain. 
        /// </summary>
        public static D2BitmapTarget New(D2Device device, RenderTarget backbuffer)
        {
            var D2BitmapTarget = new D2BitmapTarget(device);

            // Now we set up the Direct2D render target bitmap linked to the swapchain. 
            // Whenever we render to this bitmap, it will be directly rendered to the 
            // swapchain associated with the window.
            var properties = new BitmapProperties1()
            {
                PixelFormat = new D2D.PixelFormat(backbuffer.Description.Format, AlphaMode.Premultiplied),
                DpiX = device.Dpi.x,
                DpiY = device.Dpi.y,
                BitmapOptions = BitmapOptions.Target | BitmapOptions.CannotDraw
            };

            // Direct2D needs the dxgi version of the backbuffer surface pointer.
            // Get a D2D surface from the DXGI back buffer to use as the D2D render target.
            using (DXGI.Surface dxgiBackBuffer = backbuffer)
            {
                dxgiBackBuffer.DebugName = "dxgiBackBuffer_of_" + D2BitmapTarget.ImmutableHash.ToString("X4");
                D2BitmapTarget.bitmap = new Bitmap1(device.Context, dxgiBackBuffer, properties);
            }

            return D2BitmapTarget;
        }


        public static implicit operator Bitmap(D2Bitmap from)
        {
            return from != null ? from.bitmap : null;
            //return from != null ? from.bitmap : throw new ArgumentNullException("D2Bitmap casting with null reference");
        }

        protected override void Dispose(bool disposemanaged)
        {
            ReleaseComObject(ref bitmap);
            base.Dispose(disposemanaged);
        }
    }
}
