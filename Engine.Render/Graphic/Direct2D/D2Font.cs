﻿using System;
using System.Diagnostics;

using Common;
using Common.Maths;
using SharpDX;
using SharpDX.Direct2D1;
using SharpDX.DirectWrite;


namespace Engine.Direct2D
{
    /// <summary>
    /// Device indipendent.
    /// The Direct Write TextFormat interface describes the font and paragraph properties used to format text, and it describes locale information.
    /// </summary>
    public class D2Font : DisposableDx
    {
        public readonly TextFormat Format;   
        /// <summary>
        /// immutable property, em-height = fontsize*8/6
        /// </summary>
        public readonly int FontSize;
        /// <summary>
        /// immutable property
        /// </summary>
        public readonly string FontName;

        internal D2Font(string fontname = "Calibri", int fontsize = 14) : base($"D2Font[{fontname}{fontsize}")
        {
            FontName = fontname;
            FontSize = fontsize;
            Format = ToDispose(new TextFormat(Direct2Factory.DWFactory, fontname, fontsize));
            Format.Tag = "TextFormat_of_" + $"{fontname}{fontsize}";
            Format.TextAlignment = TextAlignment.Leading;
            Format.ParagraphAlignment = ParagraphAlignment.Near;
        }

        /// <summary>
        /// internally generates a list of used fonts
        /// </summary>
        public static D2Font GetFont(string fontname = "Calibri", int fontsize = 14)
        {
            return Direct2Factory.GetOrCreateFont(fontname, fontsize);
        }
    }
}
