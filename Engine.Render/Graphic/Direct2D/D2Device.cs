﻿using System;

using Common;
using Common.Maths;
using SharpDX.Direct2D1;
using D3D = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;


namespace Engine.Direct2D
{

    /// <summary>
    /// </summary>
    public class D2Device : GraphicChild
    {
        Device d2device;
        D2Context context;
        
        /// <summary>
        /// This method specifies the mapping from pixel space to device-independent space
        /// for the render target. If both dpiX and dpiY are 0, the factory-read system DPI
        /// is chosen. If one parameter is zero and the other unspecified, the DPI is not
        /// changed. For SharpDX.Direct2D1.WindowRenderTarget, the DPI defaults to the most
        /// recently factory-read system DPI. The default value for all other render targets
        /// is 96 DPI.
        /// </summary>
        public Vector2f Dpi { get; private set; }
        public D2Context Context => context;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="device">attacked device</param>
        public D2Device(D3Device device) : base(device, "D2Device")
        {
            CreateDeviceDependentResources(device);
        }


        public void CreateDeviceDependentResources(D3Device device)
        {
            Debugg.Info("GraphicDevice2d init");

            ReleaseComObject(ref d2device);
            RemoveAndDispose(ref context);

            D3D.Device d3device = device;

            if (!d3device.CreationFlags.HasFlag(D3D.DeviceCreationFlags.BgraSupport))
                throw new ArgumentException("The 3DDevice must be created with Bgra Support");

            using (var dxgi = d3device.QueryInterface<DXGI.Device>())
                d2device = new Device(Direct2Factory.D2DFactory, dxgi);

            context = ToDispose(new D2Context(this));

            Dpi = context.d2context.DotsPerInch.ToCommon();
        }


        protected override void Dispose(bool disposemanaged)
        {
            base.Dispose(disposemanaged);
            ReleaseComObject(ref d2device);
        }

        public static implicit operator Device(D2Device from)
        {
            return from == null ? null : from.d2device;
        }
    }
}