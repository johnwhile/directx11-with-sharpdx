﻿using System;
using System.Windows.Forms;

using SharpDX;
using SharpDX.Direct2D1;
//using SharpDX.WIC;

using Common;
using Common.Maths;

using Engine;

namespace Engine.Direct2D
{
    /// <summary>
    /// Device-indipendent Direct2 renderer, only used in debug testing when no <see cref="D3Device"/> creation is required.<br/>
    /// <b>Use instead <see cref="D2Context"/></b>
    /// </summary>
    public class D2WindowTarget : D2GraphicTargetBase
    {
        WindowRenderTarget wintarget;
        protected override SharpDX.Direct2D1.RenderTarget rendertarget => wintarget;

        /// <summary>
        ///If you want to use CheckWindowState to determine the current window state, you should call CheckWindowState after every
        ///EndDraw call and ignore its return value. This call will ensure that your next call to CheckWindowState state will return the actual window state. ?
        /// </summary>
        public WindowState prevstate;

        /// <summary>
        /// Create a windows render target 
        /// </summary>
        /// <param name="hwnd"></param>
        public D2WindowTarget(Control hwnd)
        {
            SetControl(hwnd);

        }

        public void SetControl(Control hwnd)
        {
            Debugg.Info("D2WindowTarget init");

            OnRenderTargetReleased();
            ReleaseComObject(ref wintarget);
            
            HwndRenderTargetProperties wtp = new HwndRenderTargetProperties()
            {
                Hwnd = hwnd.Handle,
                PixelSize = hwnd.ClientSize.ToSharpDx(),
                PresentOptions = PresentOptions.Immediately
            };
            wintarget = new WindowRenderTarget(Direct2Factory.D2DFactory, new RenderTargetProperties(), wtp);
            OnRenderTargetAssigned();
        }


        public void Resize(Vector2i size)
        {
            Debugg.Info($"D2WindowTarget resizing to {size}");
            wintarget.Resize(size.ToSharpDx());
        }

        public override void EndDraw()
        {
            base.EndDraw();
            //If you want to use CheckWindowState to
            //determine the current window state, you should call CheckWindowState after every
            //EndDraw call and ignore its return value. This call will ensure that your next
            //call to CheckWindowState state will return the actual window state.?
            prevstate = wintarget.CheckWindowState();
        }

        protected override void Dispose(bool disposemanaged)
        {
            ReleaseComObject(ref wintarget);
            base.Dispose(disposemanaged);
        }

    }
}
