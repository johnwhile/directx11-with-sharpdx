﻿using System;
using System.Windows.Forms;

using SharpDX;
using SharpDX.Direct2D1;
using D2 = SharpDX.Direct2D1;
using SharpDX.Mathematics.Interop;
//using SharpDX.WIC;

using Common;
using Common.Maths;


namespace Engine.Direct2D
{
    /// <summary>
    /// The direct2d render class utils. This class restores its components by calling<br/>
    /// <b><see cref="RenderTargetReleasing"/></b> and <b><see cref="RenderTargetAssigning"/></b> events.
    /// </summary>
    public abstract class D2GraphicTargetBase : DisposableDx
    {
        protected bool beginOK = false;
        protected abstract D2.RenderTarget rendertarget { get; }
        //for internal use
        protected SolidColorBrush solidbrush;

        Rectangle4f viewport;
        public Rectangle4f Viewport => viewport;

        public D2GraphicTargetBase() : base("D2GraphicTargetBase")
        {

        }

        public event Action RenderTargetReleasing;
        public event Action RenderTargetAssigning;

        protected virtual void OnRenderTargetReleased()
        {
            //i prefer releasing all dependent resources before releasing target
            RenderTargetReleasing?.Invoke();
            RemoveAndDispose(ref solidbrush);
        }
        protected virtual void OnRenderTargetAssigned()
        {
            if (rendertarget == null) throw new NullReferenceException();
            RemoveAndDispose(ref solidbrush);
            solidbrush = ToDispose(new SolidColorBrush(rendertarget, Color4b.Black.ToRawColor(), new BrushProperties() { Opacity = 1 }));
            viewport = new Rectangle4f(rendertarget.Size.ToCommon());
            
            RenderTargetAssigning?.Invoke();
        }

        public virtual void Clear(Color4b color)
        {
            if (!IsValid(rendertarget)) throw new Exception("invalid rendertarget");
            rendertarget.Clear(color.ToRawColor());
        }

        public virtual bool BeginDraw()
        {
            if (!IsValid(rendertarget)) throw new Exception("invalid rendertarget");
            rendertarget.BeginDraw();
            beginOK = true;
            return beginOK;
        }
        public virtual void EndDraw()
        {
            if (!beginOK) throw new Exception($"You call EndDraw in {GetType().Name} but BeginDraw failed or was not called");
            rendertarget.EndDraw();
            beginOK = false;
        }

        public void DrawText(D2Font font, string text, Vector4b color, int x, int y)
        {
            int px = (int)(font.FontSize * 8f / 6f);
            DrawText(font, text, color, new Rectangle4i(x, y, (int)viewport.size.x - x, (int)viewport.size.y - y));
        }
        public void DrawText(D2Font font, string text, Color4b color, Rectangle4i? destination = null)
        {
            if (string.IsNullOrWhiteSpace(text)) return;

            var dest = destination.HasValue ? destination.Value.ToRawRectangleF() : viewport.ToRawRectangleF();

            solidbrush.Color = color.ToRawColor();

            //rendertarget.DrawRectangle(dest, solidbrush, 1);
            rendertarget.DrawText(text, font.Format, dest, solidbrush);

        }
        public void FillRectangle(Rectangle4i rect, Color4b color, Rectangle4i? clip = null)
        {
            solidbrush.Color = color.ToRawColor();
            if (clip.HasValue) rendertarget.PushAxisAlignedClip(clip.Value.ToRawRectangleF(), AntialiasMode.PerPrimitive);
            rendertarget.FillRectangle(rect.ToRawRectangleF(), solidbrush);
            if (clip.HasValue) rendertarget.PopAxisAlignedClip();

        }
        public void DrawRectangle(Rectangle4i rect, Color4b color, float stroke = 1, Rectangle4i? clip = null)
        {
            solidbrush.Color = color.ToRawColor();
            if (clip.HasValue) rendertarget.PushAxisAlignedClip(clip.Value.ToRawRectangleF(), AntialiasMode.PerPrimitive);
            rendertarget.DrawRectangle(rect.ToRawRectangleF(), solidbrush, stroke);
            if (clip.HasValue) rendertarget.PopAxisAlignedClip();

        }
        public void DrawImage(D2Bitmap image, float opacity = 1)
        {
            DrawImage((Bitmap)image, null, null, opacity);
        }
        public void DrawImage(D2Bitmap image, Rectangle4i? destination, Rectangle4i? source, float opacity = 1)
        {
            DrawImage((Bitmap)image, destination, source, opacity);
        }
        internal void DrawImage(Bitmap image, Rectangle4i? destination, Rectangle4i? source, float opacity = 1)
        {
            var srcF = source.HasValue ? source.Value.ToRawRectangleF(): new RectangleF(0,0, image.PixelSize.Width, image.PixelSize.Height);
            var destF = destination.HasValue ? destination.Value.ToRawRectangleF() : srcF;
            rendertarget.DrawBitmap(image, destF, opacity, BitmapInterpolationMode.Linear, srcF);
        }


        /// <summary>
        /// For <see cref="D2Context"/> return the new <see cref="Bitmap1"/> generation's version
        /// </summary>
        /// <param name="canread">not supported on rendertarget creation, use Bitmap1 instead</param>
        public virtual Bitmap LoadImage(string filename, bool canread = false)
        {
            if (!IsValid(rendertarget)) throw new Exception("rendertarget not valid");
            return Bitmap.FromWicBitmap(rendertarget, TextureLoader.LoadBitmap(filename));
        }

        public static implicit operator D2.RenderTarget(D2GraphicTargetBase from)
        {
            return from != null ? from.rendertarget : throw new NullReferenceException("D2GraphicTargetBase casting with null reference");
        }

    }
}
