﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX.Direct2D1;

namespace Engine.Direct2D
{    
    public abstract class D2GraphicResource : DisposableDx
    {
        public D2GraphicTargetBase target { get; private set; }
        protected abstract Resource resource { get; }

        public D2GraphicResource(D2GraphicTargetBase target, string debugname = null) : base(debugname)
        {
            this.target = target;
        }

        public static implicit operator Resource(D2GraphicResource from)
        {
            return from != null ? from.resource : throw new NullReferenceException("D2GraphicResource casting with null reference");
        }

        public override int GetHashCode()
        {
            if (IsValid(resource) && resource.NativePointer != IntPtr.Zero)
                return resource.NativePointer.GetHashCode();
            else return base.GetHashCode();
        }

    }
}
