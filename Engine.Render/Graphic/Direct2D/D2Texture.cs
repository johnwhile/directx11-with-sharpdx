﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Direct2D
{
    public enum RestoreMode
    {
        Immediate,
        FirstUse
    }

    public class D2Texture
    {
        D2TextureManager manager;
        D2Bitmap image;

        public string Filename { get; private set; }
        public RestoreMode Restore { get; set; }
        
        public D2Bitmap Image
        {
            get
            {
                if (image == null && Restore == RestoreMode.FirstUse)
                    manager.Restore(this);
                return image;
            }
            internal set => image = value;
        }

        internal D2Texture(string filename, RestoreMode restore)
        {
            Restore = restore;
            Filename = filename;
            if (!File.Exists(filename)) throw new FileNotFoundException();
        }

        public static D2Texture CreateTexture(D2TextureManager manager, string filename, RestoreMode restore)
        {
            var texture = manager.CreateTexture(filename, restore);
            texture.manager = manager;
            return texture;
        }
    }
}
