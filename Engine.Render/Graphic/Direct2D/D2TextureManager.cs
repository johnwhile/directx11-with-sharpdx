﻿using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Direct2D
{
    public class D2TextureManager : DisposableDx
    {
        D2GraphicTargetBase rendertarget;
        List<D2Texture> textures;

        public D2TextureManager(D2GraphicTargetBase rendertarget) : base(null)
        {
            rendertarget.RenderTargetAssigning += RenderTargetAssigning;
            rendertarget.RenderTargetReleasing += RenderTargetReleasing;
            this.rendertarget = rendertarget;
            textures = new List<D2Texture>();
        }

        public D2Texture CreateTexture(string filename, RestoreMode restore)
        {
            var texture = new D2Texture(filename, restore);

            if (texture.Restore == RestoreMode.Immediate)
            {
                var name = Path.GetFileName(filename);
                texture.Image = ToDispose(D2Bitmap.New(rendertarget, filename, false));
            }
            textures.Add(texture);
            return texture;
        }

        private void RenderTargetReleasing()
        {
            foreach(var texture in textures)
            {
                RemoveAndDispose(texture.Image);
                texture.Image = null;
            }

        }

        private void RenderTargetAssigning()
        {
            foreach (var texture in textures)
            {
                if (texture.Restore == RestoreMode.Immediate)
                {
                    var name = Path.GetFileName(texture.Filename);
                    texture.Image = ToDispose(D2Bitmap.New(rendertarget, texture.Filename, false));
                }
            }
        }
        /// <summary>
        /// restore the D2Texture class
        /// </summary>
        public void Restore(D2Texture texture)
        {
            RemoveAndDispose(texture.Image);
            texture.Image = ToDispose(D2Bitmap.New(rendertarget, texture.Filename, false));
        }

        protected override void Dispose(bool disposemanaged)
        {
            Debugg.Info("D2TextureManager disposing");
            if (rendertarget!=null)
            {
                rendertarget.RenderTargetAssigning -= RenderTargetAssigning;
                rendertarget.RenderTargetReleasing -= RenderTargetReleasing;
            }
            rendertarget = null;
            base.Dispose(disposemanaged);
        }
    }
}
