﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Common;
using Common.Maths;

namespace Engine
{

    public abstract class RendererBase : GraphicChild
    {
        public RendererBase(D3Device device): base(device)
        {
        }
        /// <summary>
        /// Create any resources that depend on the device or device context
        /// </summary>
        protected abstract void CreateDeviceDependentResources();

        /// <summary>
        /// Create any resources that depend upon the size of the render target
        /// </summary>
        protected abstract void CreateSizeDependentResources();

        /// <summary>
        /// Each descendant of RendererBase performs a frame
        /// render within the implementation of DoRender
        /// </summary>
        public abstract void Render(D3Context context);
    }
}
