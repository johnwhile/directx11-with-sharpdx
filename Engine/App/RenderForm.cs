﻿//#define INSPECTING_ORDER_OF_EVENTS

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;

using Common.Windows;
using Common.WinNative;

namespace Common
{
    /// <summary>
    /// My Form extensions, can be used also for usual Visual Studio Design
    /// https://github.com/sharpdx/SharpDX/blob/master/Source/SharpDX.Desktop/RenderForm.cs
    /// </summary>
    [ToolboxItem(false)]
    public partial class RenderForm : Form
    {
        Size cachedSize;
        FormWindowState previousWindowState;

        /// <summary>
        /// when user are resizing stop calling change size
        /// </summary>
        public bool isUserResizing;
        bool allowUserResizing;
        bool isBackgroundFirstDraw;
        bool isSizeChangedWithoutResizeBegin;

        public bool IsInDesigneMode => DesignMode && FormUtils.IsInDesignMode(this);

        public Keys FullScreenKey => Keys.Alt | Keys.Enter;


        /// <summary>
        /// Initializes a new instance of the <see cref="RenderForm"/> class.
        /// </summary>
        public RenderForm(int Width = 800, int Height = 600)
        {
            Text = "RenderForm";
            ClientSize = new Size(Width, Height);
            BackColor = Color.CornflowerBlue;

            ResizeRedraw = true;
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);

            previousWindowState = FormWindowState.Normal;
            AllowUserResizing = true;
            AllowPaintBackground = false;
#if INSPECTING_ORDER_OF_EVENTS
            AppActivated += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's AppActivated Event"); };
            AppDeactivated += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's AppDeactivated Event"); };
            MonitorChanged += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's MonitorChanged Event"); };
            PauseRendering += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's PauseRendering Event"); };
            ResumeRendering += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's ResumeRendering Event"); };
            Screensaver += delegate (object s, CancelEventArgs e) { Debug.WriteLine("RenderForm's Screensaver Event"); };
            SystemResume += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's SystemResume Event"); };
            SystemSuspend += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's SystemSuspend Event"); };
            UserResized += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's UserResized Event"); };

            Activated += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's Activated Event"); };
            Deactivate += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's Deactivated Event"); };
            ResizeEnd += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's ResizeEnd Event"); };
            ResizeBegin += delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's ResizeBegin Event"); };
            ClientSizeChanged+= delegate (object s, EventArgs e) { Debug.WriteLine("RenderForm's ClientSizeChanged Event"); };
#endif

        }

        /// <summary>
        /// Occurs when [app activated].
        /// </summary>
        public event EventHandler<EventArgs> AppActivated;

        /// <summary>
        /// Occurs when [app deactivated].
        /// </summary>
        public event EventHandler<EventArgs> AppDeactivated;

        /// <summary>
        /// Occurs when [monitor changed].
        /// </summary>
        public event EventHandler<EventArgs> MonitorChanged;

        /// <summary>
        /// Occurs when [pause rendering].
        /// </summary>
        public event EventHandler<EventArgs> PauseRendering;

        /// <summary>
        /// Occurs when [resume rendering].
        /// </summary>
        public event EventHandler<EventArgs> ResumeRendering;

        /// <summary>
        /// Occurs when [screensaver].
        /// </summary>
        public event EventHandler<CancelEventArgs> Screensaver;

        /// <summary>
        /// Occurs when [system resume].
        /// </summary>
        public event EventHandler<EventArgs> SystemResume;

        /// <summary>
        /// Occurs when [system suspend].
        /// </summary>
        public event EventHandler<EventArgs> SystemSuspend;

        /// <summary>
        /// Occurs when [user resized].
        /// </summary>
        public event EventHandler<EventArgs> UserResized;

        /// <summary>
        /// Occurs when [Alt+Enter].
        /// UserResized event are disabled because swapchain takes care of the resizing
        /// </summary>
        public event EventHandler<EventArgs> FullscreenSwitching;

        /// <summary>
        /// Gets or sets a value indicating whether this form can be resized by the user. See remarks.
        /// </summary>
        /// <remarks>
        /// This property alters <see cref="Form.FormBorderStyle"/>, 
        /// for <c>true</c> value it is <see cref="FormBorderStyle.Sizable"/>, 
        /// for <c>false</c> - <see cref="FormBorderStyle.FixedSingle"/>.
        /// </remarks>
        public bool AllowUserResizing
        {
            get
            {
                return allowUserResizing;
            }
            set
            {
                if (allowUserResizing != value)
                {
                    allowUserResizing = value;
                    MaximizeBox = allowUserResizing;
                    FormBorderStyle = IsFullscreen ? FormBorderStyle.None
                    : allowUserResizing ? FormBorderStyle.Sizable : FormBorderStyle.FixedSingle;
                }
            }
        }


        /// <summary>
        /// Set to false in case the control is drawn by the swapchain
        /// </summary>
        public bool AllowPaintBackground { get; set; } = false;

        /// <summary>
        /// Gets a value indicationg whether the current render form is in fullscreen mode.
        /// </summary>
        public bool IsFullscreen { get; protected set; } = false;


        private bool OnFullscreenSwithing(bool toFullscreen)
        {
            try
            {
                isUserResizing = true;
                if (FullscreenSwitching != null)
                    FullscreenSwitching(this, EventArgs.Empty);
                isUserResizing = false;
                return true;
            }
            catch
            {
                return false;
            }

        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.ResizeBegin"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnResizeBegin(EventArgs e)
        {
            isUserResizing = true;

            base.OnResizeBegin(e);
            cachedSize = Size;
            OnPauseRendering(e);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.ResizeEnd"/> event.
        /// </summary>
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);

            isUserResizing = false;

            if (cachedSize != Size)
            {
                OnUserResized(EventArgs.Empty);
                // UpdateScreen();
            }
            OnResumeRendering(e);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Load"/> event.
        /// </summary>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            // UpdateScreen();
        }

        /// <summary>
        /// Paints the background of the control.
        /// </summary>
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            if (!isBackgroundFirstDraw || AllowPaintBackground)
            {
                base.OnPaintBackground(e);
                isBackgroundFirstDraw = true;
            }
        }

        /// <summary>
        /// Raises the Pause Rendering event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnPauseRendering(EventArgs e) { PauseRendering?.Invoke(this, e); }

        /// <summary>
        /// Raises the Resume Rendering event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnResumeRendering(EventArgs e) { ResumeRendering?.Invoke(this, e); }

        /// <summary>
        /// Raises the User resized event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnUserResized(EventArgs e)
        {
            if (!isUserResizing && UserResized != null) UserResized(this, e);
        }
        private void OnMonitorChanged(EventArgs e) { MonitorChanged?.Invoke(this, e); }

        /// <summary>
        /// Raises the On App Activated event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnAppActivated(EventArgs e) { AppActivated?.Invoke(this, e); }
        /// <summary>
        /// Raises the App Deactivated event
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnAppDeactivated(EventArgs e) { AppDeactivated?.Invoke(this, e); }
        /// <summary>
        /// Raises the System Suspend event
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnSystemSuspend(EventArgs e) { SystemSuspend?.Invoke(this, e); }
        /// <summary>
        /// Raises the System Resume event
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnSystemResume(EventArgs e) { SystemResume?.Invoke(this, e); }
        /// <summary>
        /// Raises the <see cref="E:Screensaver"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void OnScreensaver(CancelEventArgs e) { Screensaver?.Invoke(this, e); }

        protected override void OnClientSizeChanged(EventArgs e)
        {
            base.OnClientSizeChanged(e);

            if (!isUserResizing && (isSizeChangedWithoutResizeBegin || cachedSize != Size))
            {
                isSizeChangedWithoutResizeBegin = false;
                cachedSize = Size;
                OnUserResized(EventArgs.Empty);
                //UpdateScreen();
            }
        }

        /// <summary>
        /// Override windows message loop handling.
        /// </summary>
        protected override void WndProc(ref Message m)
        {
            //Debugg.WriteLine(m.ToString());

            long wparam = m.WParam.ToInt64();
            long lparam = m.LParam.ToInt64();
       
            switch ((WM)m.Msg)
            {
                case WM.SIZE:
                    {
                        //Debug.WriteLine(string.Format("[{0}]", (SIZE)wparam));

                        if ((SIZE)wparam == SIZE.MINIMIZED)
                        {
                            previousWindowState = FormWindowState.Minimized;
                            OnPauseRendering(EventArgs.Empty);
                        }
                        else
                        {
                            RawRectangle rect;
                            WinApi.GetClientRect(m.HWnd, out rect);
                            if (rect.Bottom - rect.Top == 0)
                            {
                                // Rapidly clicking the task bar to minimize and restore a window
                                // can cause a WM_SIZE message with SIZE_RESTORED when 
                                // the window has actually become minimized due to rapid change
                                // so just ignore this message
                            }
                            else if ((SIZE)wparam == SIZE.MAXIMIZED)
                            {
                                if (previousWindowState == FormWindowState.Minimized)
                                    OnResumeRendering(EventArgs.Empty);

                                previousWindowState = FormWindowState.Maximized;

                                OnUserResized(EventArgs.Empty);

                                //UpdateScreen();
                                cachedSize = Size;
                            }
                            else if ((SIZE)wparam == SIZE.RESTORED)
                            {
                                //Debug.Print(string.Format("SIZE RESTORED {0}", Size.ToString()));

                                if (previousWindowState == FormWindowState.Minimized)
                                    OnResumeRendering(EventArgs.Empty);

                                if (!isUserResizing && (Size != cachedSize || previousWindowState == FormWindowState.Maximized))
                                {
                                    previousWindowState = FormWindowState.Normal;

                                    // Only update when cachedSize is != 0
                                    if (cachedSize != Size.Empty)
                                    {
                                        isSizeChangedWithoutResizeBegin = true;
                                    }
                                }
                                else
                                    previousWindowState = FormWindowState.Normal;
                            }
                        }
                        break;
                    }
                case WM.ACTIVATEAPP:
                    {
                        if (wparam != 0)
                            OnAppActivated(EventArgs.Empty);
                        else
                            OnAppDeactivated(EventArgs.Empty);
                        break;
                    }
                case WM.POWERBROADCAST:
                    {
                        if ((PBT)wparam == PBT.APMQUERYSUSPEND)
                        {
                            OnSystemSuspend(EventArgs.Empty);
                            m.Result = new IntPtr(1);
                            return;
                        }
                        else if ((PBT)wparam == PBT.APMRESUMESUSPEND)
                        {
                            OnSystemResume(EventArgs.Empty);
                            m.Result = new IntPtr(1);
                            return;
                        }
                    }
                    break;
                //case WM.MENUCHAR:
                    //m.Result = new IntPtr(MNC.CLOSE << 16); // IntPtr(MAKELRESULT(0, MNC_CLOSE));
                    //return;

                case WM.SYSCOMMAND:
                    {
                        wparam &= 0xFFF0;
                        if ((SC)wparam == SC.MONITORPOWER || (SC)wparam == SC.SCREENSAVE)
                        {
                            var e = new CancelEventArgs();
                            OnScreensaver(e);
                            if (e.Cancel)
                            {
                                m.Result = IntPtr.Zero;
                                return;
                            }
                        }
                        break;
                    }

                case WM.DISPLAYCHANGE:
                    OnMonitorChanged(EventArgs.Empty);
                    break;

                case WM.CHAR:
                    {
                        break;
                    }
                case WM.NCHITTEST:
                {
                        break;
                }
            }

            base.WndProc(ref m);
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == (Keys.Menu | Keys.Alt) || keyData == Keys.F10)
                return true;

            if (keyData == FullScreenKey)
            {
                IsFullscreen = !IsFullscreen;
                if (OnFullscreenSwithing(IsFullscreen))
                    return true;
            }
            return base.ProcessDialogKey(keyData);
        }

        MouseEventArgs getmousestate(long wparam, long lparam)
        {
            var button = (MK)wparam;
            MouseButtons buttons = MouseButtons.None;
            if ((MK.LBUTTON & button) > 0) buttons |= MouseButtons.Left;
            if ((MK.RBUTTON & button) > 0) buttons |= MouseButtons.Right;
            if ((MK.MBUTTON & button) > 0) buttons |= MouseButtons.Middle;
            short x = unchecked((short)lparam);
            short y = unchecked((short)(lparam >> 16));

            return new MouseEventArgs(buttons, 1, x, y, 0);
        }
    }
}
