﻿
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Common.Windows
{
    /// <summary>
    /// From https://github.com/sharpdx/SharpDX/blob/master/Source/SharpDX.Desktop/RenderControl.cs
    /// A Control designed to rendering
    /// </summary>
    [ToolboxItem(false)]
    public partial class RenderControl : Control
    {
        enum RenderResult
        {
            /// <summary>
            /// Don't use Render()
            /// </summary>
            NotRender,
            /// <summary>
            /// Require call Render()
            /// </summary>
            NeedRender,
            /// <summary>
            /// Render() return true
            /// </summary>
            RenderSuccess,
            /// <summary>
            /// Render() return false, it's possible draw a error image
            /// </summary>
            RenderError
        }

        Pen bigred = new Pen(Color.Red, 8);
        RenderResult method = RenderResult.NeedRender;
        bool isAppActive = true;
        bool allowOnPaint = true;
        bool allowOptimizedBuffer = false;
        bool fullscreen = false;
        protected bool isDesignMode = false;


        public RenderControl()
        {

            Name = "SharpDX RenderControl";

            // don't touch this function otherwise Designer editor will not work.
            InitializeComponent();

            isDesignMode = this.DesignMode || FormUtils.IsInDesignMode(this);

            // WM_ERASEBKGND are ignored so OnPaintBackground and OnPaint are called both on WM_PAINT message. Need UserPaint to true
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            // if false OnPaint will be skip
            SetStyle(ControlStyles.UserPaint, true);

            // don't call OnPaintBackground when rendering
            SetStyle(ControlStyles.Opaque, true);
            // draw also when resizing
            SetStyle(ControlStyles.ResizeRedraw, true);

            // very good improvement from NET 1.1, not require custom implementation also for high draw frequency
            // the problem is that erase the directx rendering....
            SetStyle(ControlStyles.OptimizedDoubleBuffer, false);
            DoubleBuffered = false;
            allowOptimizedBuffer = false;


            if (isDesignMode)
            {
                allowOnPaint = true;
            }
        }

        /// <summary>
        /// Sometime DesignMode flag doesn't return the true value
        /// </summary>
        protected bool IsInDesignMode
        {
            get { return DesignMode || FormUtils.IsInDesignMode(this); }
        }

        /// <summary>
        /// If false, don't call OnPaint event, device take full control of painting event.
        /// Don't take effect in DesignMode.
        /// Only Render() function can draw.
        /// </summary>
        public bool AllowOnPaint
        {
            get { return allowOnPaint; }
            set { allowOnPaint = value; SetStyle(ControlStyles.UserPaint, value); UpdateStyles(); }
            /*
            set 
            {
                allowOnPaint = value || IsInDesignMode;
                SetStyle(ControlStyles.UserPaint, allowOnPaint);
                UpdateStyles();
            }*/
        }
        /// <summary>
        /// isn't compatible with Render() and RenderOrPaint().
        /// </summary>
        public bool AllowOptimizedDoubleBuffer
        {
            get { return allowOptimizedBuffer; }
            set { allowOptimizedBuffer = value; SetStyle(ControlStyles.OptimizedDoubleBuffer, value); UpdateStyles(); }
            /*
            set 
            {
                allowOnPaint = value || IsInDesignMode;
                SetStyle(ControlStyles.UserPaint, allowOnPaint);
                UpdateStyles();
            }*/
        }
       
        /// <summary>
        /// You need to set also "AllowUserResizing"
        /// </summary>
        public bool IsFullscreen
        {
            get { return fullscreen; }
            set { fullscreen = value; }
        }

        /// <summary>
        /// </summary>
        public event EventHandler<EventArgs> DoRender;

        /// <summary>
        /// Draw to form and return true if all is correct. This function doesn't have any relation
        /// with Paint event and must not call invalidate. If "Paint" flag is enable, OnPaint can call it.
        /// </summary>
        protected virtual bool Render()
        {
            if (DoRender == null) return false;
            try
            {
                DoRender?.Invoke(this, EventArgs.Empty);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Paint to form and return true if all is correct. This function is called only by OnPaint
        /// </summary>
        protected virtual bool NetPaint(PaintEventArgs e)
        {
            return false;
        }

        /// <summary>
        /// Draw to form with "Render()". If "AllowOnPaint" is true, and "Render" return false, it draw an error image on "OnPaint()".
        /// </summary>
        public void RenderOrPaint()
        {
            method = RenderResult.NeedRender;

            if (!isAppActive) return;

            if (!Render())
            {
                method = RenderResult.RenderError;
                Invalidate();
            }
            else
            {
                method = RenderResult.RenderSuccess;
            }
        }

        /// <summary>
        /// Draw to form with only "Paint()" function. "AllowOnPaint" must be true. This function is used only when you need to debug the
        /// application with net painting, i don't know if is possible mixing render and paint 
        /// </summary>
        public void OnlyNetPaint()
        {
            method = RenderResult.NotRender;
            if (!isAppActive) return;
            Invalidate(true);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //Console.WriteLine("OnPaint " + method.ToString());

            Graphics g = e.Graphics;

            if (isDesignMode)
            {
                g.Clear(BackColor);
                SizeF textsize = g.MeasureString(Name, SystemFonts.DefaultFont);
                g.DrawString(Name, SystemFonts.DefaultFont, Brushes.Black,
                    (ClientSize.Width - textsize.Width) / 2, (ClientSize.Height - textsize.Height) / 2);
            }
            else
            {
                if (method != RenderResult.NotRender)
                {
                    if (method == RenderResult.NeedRender)
                    {
                        method = Render() ? RenderResult.RenderSuccess : RenderResult.RenderError;
                    }

                    if (method == RenderResult.RenderError)
                    {

                        g.Clear(Color.White);
                        g.DrawLine(bigred, 0, 0, ClientSize.Width, ClientSize.Height);
                        g.DrawLine(bigred, ClientSize.Width, 0, 0, ClientSize.Height);
                        SizeF textsize = e.Graphics.MeasureString("Directx Rendering Error !", SystemFonts.DefaultFont);
                        g.DrawString("Directx Rendering Error !", SystemFonts.DefaultFont, Brushes.Black,
                                (ClientSize.Width - textsize.Width) / 2, (ClientSize.Height - textsize.Height) / 2);
                    }
                    method = RenderResult.NeedRender;
                }
            }

            NetPaint(e);

            base.OnPaint(e);
        }

    }
}
