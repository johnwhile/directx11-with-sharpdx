﻿using System;
using System.Windows.Forms;

using Common;
using Common.Maths;
using SharpDX.DXGI;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;


namespace Engine
{
    public delegate void ResizeEventHandler(Vector2i size);
    public delegate void GameMouseHandler(Vector2f position, MouseButtons mouse);
    public delegate void GameKeyHandler(KeyEventArgs args);
    public delegate void GameWindowRenderHandler(SwapChainPresenter presenter);
    public delegate void GameUpdateHandler(GameTime time);

    /// <summary>
    /// Manages one swapchain to a form or to a generic control
    /// </summary>
    public class GameWindow : Disposable
    {
        static int instancecounter = 0;

        public string Name;

        internal GameApplication application;
        internal SwapChainPresenter presenter;
        protected Form parentForm;
        protected bool resizing = false;
        PreFullscreenState preFullscreenState;

        protected Vector2i prevsize; //avoid resizing backbuffer or rendertarghet to same size

        public bool IsActive { get; private set; }
        public bool IsFullScreen { get; private set; }

        /// <summary>
        /// Gets or sets whether if you are using the next vertical sync presenting.
        /// </summary>
        public bool VSync
        {
            get => presenter.Description.Vsync;
            set => presenter.Description.Vsync = value;
        }

        /// <summary>
        /// The output where presenter render to
        /// </summary>
        public Control Control { get; private set; }
        /// <summary>
        /// The <see cref="Form"/> that contain <see cref="Control"/>, <u>can be the same reference</u>
        /// </summary>
        public Form ParentForm => parentForm;

        /// <summary>
        /// mouse event args are relative to output panel but backbuffer can have different size and ratio
        /// </summary>
        Vector2f GetMousePositionRelativeToBackBufferSpace()
        {
            Common.Inputs.WindowMouse.GetPosition(out var mouse, Control.Handle);
            presenter.MousePosition(mouse, Control.ClientSize);
            return mouse;
        }

        #region Contructors
        /// <summary>
        /// </summary>
        /// <param name="application"></param>
        /// <param name="control">can be <see cref="Control"/>, <see cref="Form"/> or <see cref="RenderForm"/></param>
        public GameWindow(GameApplication application, Control control)
        {
            if (application == null || control == null) throw new NullReferenceException("Control or Application can't be null");

            Name = "GameWindows_" + instancecounter++;
            this.application = application;
            Control = control;

            //it's itself if control is a form
            parentForm = control.FindForm();
            // if not the same class, control is a child control of parentform
            bool isChild = control != parentForm;

            if (control is Form form)
            {
                if (control is RenderForm myform)
                {
                    initializeAs(myform);
                }
                else
                {
                    initializeAs(form);
                }
            }
            else
            {
                initializeAs(control);
            }
            parentForm.FormClosing += OnClosing;
            parentForm.Activated += OnActivated;
            parentForm.Deactivate += OnDeactivate;

            //fix an issue when form was already activated
            if (parentForm == Form.ActiveForm) IsActive = true;

            preFullscreenState = default;
            preFullscreenState.location = control.Location;
            preFullscreenState.size = control.ClientSize;
            preFullscreenState.border = parentForm.FormBorderStyle;
        }

        private void initializeAs(Form form)
        {
            form.SizeChanged += OnClientSizeChanged;
            form.ResizeBegin += OnFormSizeBegin;
            form.ResizeEnd += OnFormSizeEnd;
            form.FormClosing += OnClosing;
            form.Activated += OnActivated;
            form.Deactivate += OnDeactivate;
            form.KeyDown += OnKeyDown;
        }

        private void initializeAs(RenderForm myform)
        {
            myform.SizeChanged += OnClientSizeChanged;
            myform.ResizeBegin += OnFormSizeBegin;
            myform.ResizeEnd += OnFormSizeEnd;
            myform.FormClosing += OnClosing;
            myform.Activated += OnActivated;
            myform.Deactivate += OnDeactivate;
            myform.KeyDown += OnKeyDown;
        }
        private void initializeAs(Control control)
        {
            // not found a solution to implement onresizebegin and onresizeend only for control
            // but still they are called by parentForm events
            control.SizeChanged += delegate (object sender, EventArgs e)
            {
                System.Threading.Thread.Sleep(200);
                OnClientSizeChanged(control, e);
            };
            parentForm.ResizeBegin += OnFormSizeBegin;
            parentForm.ResizeEnd += OnFormSizeEnd;
            parentForm.FormClosing += OnClosing;
            parentForm.Activated += OnActivated;
            parentForm.Deactivate += OnDeactivate;
            control.KeyDown += OnKeyDown;
        }
        #endregion

        /// <summary>
        /// use the <see cref="Control.BackColor"/> as clear color
        /// </summary>
        public virtual void Draw()
        {
            var context = application.Manager.Device3D.Context;
            var stats = context.Stats;
            var viewport = presenter.DefaultViewport;
            if (!application.Manager.CheckDeviceStatus()) return;
            if (presenter==null || !presenter.BeginDraw(context)) return;
            presenter.Clear(Control.BackColor);
            Drawing?.Invoke(presenter);
            presenter.EndDrawsAndPresent();
        }

        #region Events
        protected virtual void OnClientSizeChanged(object sender, EventArgs e)
        {
            Vector2i clientsize = Control.ClientSize;

            if (!resizing && clientsize != prevsize)
            {
                Debugg.Message($"{Name} OnClientSizeChanged: {clientsize}");

                if (IsFullScreen)
                {
                    Debugg.Error($"Can't resize {Name}, Game is in fullscreen state, resize is disabled");
                    return;
                }

                presenter?.Resize(clientsize);

                ClientSizeChanged?.Invoke(clientsize);
                prevsize = clientsize;
            }
        }
        private void OnFormSizeBegin(object sender, EventArgs e)
        {
            Debugg.Message("OnFormSizeBegin");
            prevsize = Control.ClientSize;
            resizing = true;
        }
        private void OnFormSizeEnd(object sender, EventArgs e)
        {
            Debugg.Message("OnFormSizeEnd");
            resizing = false;
            OnClientSizeChanged(Control, e);
        }
        protected void OnClosing(object sender, EventArgs e)
        {
            Closing?.Invoke(sender, e);
        }
        protected void OnActivated(object sender, EventArgs e)
        {
            Debugg.Message($"Activate : {Name}");
            IsActive = true;
            Activated?.Invoke(sender, e);
        }
        protected void OnDeactivate(object sender, EventArgs e)
        {
            Debugg.Message($"Deactivate : {Name}");
            IsActive = false;
            Deactivated?.Invoke(sender, e);
        }
        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            MouseUp?.Invoke(GetMousePositionRelativeToBackBufferSpace(), e.Button);
        }
        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            MouseDown?.Invoke(GetMousePositionRelativeToBackBufferSpace(), e.Button);
        }
        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            MouseMove?.Invoke(GetMousePositionRelativeToBackBufferSpace(), e.Button);
        }
        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            KeyDown?.Invoke(e);
        }
        
        public event GameWindowRenderHandler Drawing;
        /// <summary>
        /// When form size change
        /// </summary>
        public event ResizeEventHandler ClientSizeChanged;
        /// <summary>
        /// When form get focus or maximized
        /// </summary>
        public event EventHandler<EventArgs> Activated;
        /// <summary>
        /// When form lost focus or minimized
        /// </summary>
        public event EventHandler<EventArgs> Deactivated;
        /// <summary>
        /// When form are closing
        /// </summary>
        public event EventHandler<EventArgs> Closing;

        //TODO: implement a input manager class
        public event GameMouseHandler MouseDown;
        public event GameMouseHandler MouseUp;
        public event GameMouseHandler MouseMove;
        public event GameKeyHandler KeyDown;
        #endregion


        public void SwitchFullscreen(FullScreenMode mode = FullScreenMode.Hardware)
        {
            if (mode == FullScreenMode.Hardware)
            {
                Debugg.Info("Game hardware fullscreen switching");
                if (application.Manager.HardwareFullscreenSwitching(presenter, !IsFullScreen)) IsFullScreen = !IsFullScreen;
            }
            else
            {
                Debugg.Info("Game borderless fullscreen switching");
                if (BorderlessFullscreenSwitching(!IsFullScreen)) IsFullScreen = !IsFullScreen;
            }
        }

        /// <summary>
        /// Returns true if successful
        /// </summary>
        /// <param name="fullscreen">the state to apply</param>
        private bool BorderlessFullscreenSwitching(bool fullscreen)
        {
            if (Control is Form form)
            {
                if (fullscreen)
                {
                    preFullscreenState = new PreFullscreenState()
                    {
                        location = form.Location,
                        size = form.ClientSize,
                        border = form.FormBorderStyle
                    };

                    form.FormBorderStyle = FormBorderStyle.None;
                    form.WindowState = FormWindowState.Maximized;

                    OnClientSizeChanged(form, null);

                }
                else
                {
                    form.FormBorderStyle = preFullscreenState.border;
                    form.ClientSize = preFullscreenState.size;
                    form.Location = preFullscreenState.location;
                    form.WindowState = FormWindowState.Normal;

                    OnClientSizeChanged(form, null);
                }
                return true;
            }
            return false;
        }

        protected override void Dispose(bool disposemanaged)
        {
            Debugg.Message($"{Name} disposing (is already disposed : {IsDisposed})");
            
            Deactivated?.Invoke(this, EventArgs.Empty);

            application.RemoveOutputWindow(this);

            base.Dispose(disposemanaged);
        }


        struct PreFullscreenState
        {
            public Vector2i size;
            public Vector2i location;
            public FormBorderStyle border;
        }
    }
}
