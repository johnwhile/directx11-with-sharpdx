﻿using System;
using System.Collections.Generic;

using Common;
using Common.Maths;

using System.Windows.Forms;

using Common.Windows;
using Common.Tools;

namespace Engine
{

    //https://learn.microsoft.com/it-it/windows/win32/direct2d/direct2d-quickstart
    public abstract class DesktopApplication : Disposable
    {
        protected Form m_attachedloopform;

        public DesktopApplication()
        {
            Application.ApplicationExit += OnApplicationExit;
            Location = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
        }

        /// <summary>
        /// The directory name where the current game runs.
        /// </summary>
        public readonly string Location;
       
        /// <summary>
        /// Require a form to work correctly
        /// </summary>
        public virtual void RunLoop(Form mainform = null)
        {
            if (m_attachedloopform != null)
            {
                Debugg.Message("Application exit from current form");
                Application.Idle -= OnIdle;
                Application.Exit();
            }

            m_attachedloopform = mainform;

            //mainform can be null
            Debugg.Message("Application RunLoop");
            Application.Idle += OnIdle;
            Application.Run(m_attachedloopform);
            Debugg.Message("Application StopLoop");
            Application.Idle -= OnIdle;
        }

        protected abstract void EachFrame();


        /// <summary>
        /// Run game loop when the app becomes Idle.
        /// </summary>
        /// <param name="sender">Is the thread id</param>
        void OnIdle(object sender, EventArgs e)
        {
            do
            {
                EachFrame();
            }
            while (FormUtils.IsAppStillIdle && m_attachedloopform != null && !m_attachedloopform.IsDisposed);
        }

        /// <summary>
        /// When application exiting
        /// </summary>
        public event EventHandler<EventArgs> Exiting;
       
       
        protected void OnApplicationExit(object sender, EventArgs e)
        {
            Debugg.Message("Application Exiting");
            Application.ApplicationExit -= OnApplicationExit;

            var pMb = MemoryHelp.GetMBMemoryPressure().ToString("0.00");
            Debugg.Info("Application Closing");
            Debugg.Success($"Disposable used: {MaxInstancesCount}");
            Debugg.Success($"Memory pressure: {pMb} MB");


            Exiting?.Invoke(this, e);
        }

        protected override void Dispose(bool disposemanaged)
        {
            Debugg.Info($"Application disposing (disposed : {IsDisposed})");
            base.Dispose(disposemanaged);

            Application.Idle -= OnIdle;
        }
    }
}
