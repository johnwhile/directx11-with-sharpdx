﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.Remoting.Contexts;
using System.Windows.Forms;

using Common;
using Common.Maths;
using static System.Net.Mime.MediaTypeNames;

namespace Engine
{
    public abstract class GameApplication : DesktopApplication
    {
        /// <summary>
        /// TODO: test different vsync for multi-gamewindows
        /// </summary>
        bool VSync = false;

        protected GameTime gametime;
        protected TimerTick timer;

        double accumulatedMsSleepTime = 0;
        TimeSpan inactiveSleepTime = TimeSpan.Zero;//TimeSpan.FromMilliseconds(100);
        TimeSpan elapsedTime = TimeSpan.Zero;
        
        /// <summary>
        /// Set to zero for no targhet, it will render at the maximum frame rate
        /// </summary>
        public TimeSpan TargetElapsedTime { get; set; } = TimeSpan.FromMilliseconds(1000.0 / 60.0);

        public readonly List<GameWindow> Windows;
        
        /// <summary>
        /// The graphic devices manager
        /// </summary>
        public GraphicDeviceManager Manager { get; private set; }

        /// <summary>
        /// Type of fullscreen to use
        /// </summary>
        public FullScreenMode FullScreenMode { get; set; } = FullScreenMode.Hardware;

        /// <summary>
        /// Initialize the device and device's manager
        /// </summary>
        public GameApplication() : base()
        {
            gametime = new GameTime();
            timer = new TimerTick();
            Windows = new List<GameWindow>(1);

            Manager = ToDispose(new GraphicDeviceManager(true));
            Debugg.Message(Manager.Device3D.Adapter.Outputs[0].CurrentDisplayMode);
        }


        public void AddOutputWindow(GameWindow window, bool vsync = false)
        {
            if (!Windows.Contains(window))
            {
                Windows.Add(window);
                window.presenter = Manager.CreatePresenter(window.Control, vsync, false);
                VSync |= vsync;
            }
        }

        public void RemoveOutputWindow(GameWindow window)
        {
            if (Windows.Contains(window))
            {
                Windows.Remove(window);
                Manager.RemoveSwapChain(window.presenter);
            }
        }

        /// <summary>
        /// If you pass null form, it attacs the game loop to the first <see cref="GameWindow"/> added
        /// </summary>
        public override void RunLoop(Form mainform = null)
        {
            if (mainform == null && Windows.Count > 0) 
                mainform = Windows[0].ParentForm;

            base.RunLoop(mainform);
        }

        public void SwitchFullscreen(GameWindow window)
        {
            if (Windows.Contains(window))
            {
                //If exist a different fullscreen output, exit from it before continue
                foreach (GameWindow current in Windows)
                    if (current.IsFullScreen && current != window)
                        current.SwitchFullscreen();

                window.SwitchFullscreen(FullScreenMode);
            }
        }



        protected virtual void Draw()
        {
            void DrawSingle(GameWindow window)
            {
                window.Draw();
                var presenter = window.presenter;
                GameTime.PerformanceInfo.SetDrawConsuming(presenter.Stats.FrameCost + presenter.Stats.PresentingCost);
                GameTime.PerformanceInfo.SetPresentConsuming(presenter.Stats.PresentingCost);
            }

            //If exist a fullscreen outputs, draw only it
            foreach (GameWindow window in Windows)
                if (window.IsFullScreen) 
                { 
                    DrawSingle(window); 
                    return;
                }
            
            //if not exist fullscreen outputs, draw all
            foreach (GameWindow window in Windows) DrawSingle(window);
        }

        protected abstract void Update(GameTime time);

        protected override void EachFrame()
        {
            long t0 = 0;
            long t1 = 0;
            long t2 = 0;
            gametime.Frame();

            //reduce cpu usage
            elapsedTime = TimeSpan.FromTicks(gametime.ElapsedTicks);

            if (!VSync && elapsedTime < TargetElapsedTime)
            {
                var sleeptime = TargetElapsedTime - elapsedTime;
                accumulatedMsSleepTime += sleeptime.TotalMilliseconds;

                if (accumulatedMsSleepTime > TimerHelper.LowestSleepThreshold)
                {
                    t0 = TimerTick.Ticks;
                    TimerHelper.SleepForNoMoreThan(accumulatedMsSleepTime);
                    t1 = TimerTick.Ticks;
                    accumulatedMsSleepTime -= TimerTick.GetMSFromTick(t1 - t0);
                    if (accumulatedMsSleepTime < 0) accumulatedMsSleepTime = 0;
                }
            }

            t0 = TimerTick.Ticks;
            Update(gametime);
            t1 = TimerTick.Ticks;
            Draw();
            t2 = TimerTick.Ticks;

            GameTime.PerformanceInfo.SetUpdateConsuming(t1 - t0);
            GameTime.PerformanceInfo.SetElapsedFrame(gametime.ElapsedTicks);

            Manager.Device3D.Context.Stats.Reset();
        }


        protected override void Dispose(bool disposemanaged)
        {
            while (Windows.Count > 0)
            {
                RemoveOutputWindow(Windows[Windows.Count - 1]);
            }
            base.Dispose(disposemanaged);
        }
    }
}
