﻿using System;

using Common;

namespace Engine
{
    /// <summary>
    /// 
    /// </summary>
    public class GameTimePerformance
    {
        long ticks_for_avarage = TimerTick.GetTicksFromMS(1000);
        long elapsed_for_avarage;
        uint avarage_frames;
        long drawsTicksSum;
        long presentTicksSum;
        long updatesTicksSum;
        double draws_nanosec;
        double updates_nanosec;
        double present_nanosec;


        long OneSecond = TimerTick.GetTicksFromMS(1000);
        uint fps_frames;
        long elapsed_for_fps;
        double fps;

        internal void SetElapsedFrame(long ticks)
        {
            if (elapsed_for_fps > OneSecond)
            {
                fps = fps_frames / TimerTick.GetMSFromTick(elapsed_for_fps) * 1000.0;
                elapsed_for_fps = 0;
                fps_frames = 0;
            }
            if (elapsed_for_avarage > ticks_for_avarage)
            {
                draws_nanosec = TimerTick.GetMSFromTick(drawsTicksSum / avarage_frames) * 1000;
                updates_nanosec = TimerTick.GetMSFromTick(updatesTicksSum / avarage_frames) * 1000;
                present_nanosec = TimerTick.GetMSFromTick(presentTicksSum / avarage_frames) * 1000;
                drawsTicksSum = 0;
                updatesTicksSum = 0;
                presentTicksSum = 0;
                elapsed_for_avarage = 0;
                avarage_frames = 0;
            }
            elapsed_for_fps += ticks;
            elapsed_for_avarage += ticks;
            fps_frames++;
            avarage_frames++;
        }

        public void SetDrawConsuming(long ticks)
        {
            drawsTicksSum += ticks;
        }
        public void SetPresentConsuming(long ticks)
        {
            presentTicksSum += ticks;
        }
        internal void SetUpdateConsuming(long ticks)
        {
            updatesTicksSum += ticks;
        }

        /// <summary>
        /// Frame in one seconds
        /// </summary>
        public float FPS => (float)fps;

        /// <summary>
        /// Get the avarage time for rendering.
        /// </summary>
        /// <remarks>
        /// for an accurate analysis it is necessary to exclude the <see cref="Presenter.EndDrawsAndPresent"/> when using
        /// vsync
        /// </remarks>
        public float DrawNanoSeconds => (float)draws_nanosec;

        /// <summary>
        /// Get the avarage time for all updates
        /// </summary>
        public float UpdateNanoSeconds => (float)updates_nanosec;

        public override string ToString()
        {
            return string.Format("FPS: {0} draw: {1}ns update: {2}ns",
                FPS.ToString("0.0"), DrawNanoSeconds.ToString("0"), UpdateNanoSeconds.ToString("0"));
        }
    }


    /// <summary>
    /// Current timing used for variable-step (real time) or fixed-step (game time) games.
    /// All values expressed as ticks
    /// </summary>
    public class GameTime
    {
        public static readonly GameTimePerformance PerformanceInfo;

        long previousTicks;
        long startTicks;

        static GameTime()
        {
            PerformanceInfo = new GameTimePerformance();
        }

        public GameTime()
        {
            previousTicks = 0;
            ResetStartTicks();
        }
        public void ResetStartTicks()
        {
            startTicks = TimerTick.Ticks;
        }

        /// <summary>
        /// Gets the amount of game time since the start of the game.
        /// </summary>
        public long TotalTicks { get; private set; }
        /// <summary>
        /// Gets the amount of game time since the start of the game.
        /// </summary>
        public TimeSpan TotalTime => TimeSpan.FromTicks(TotalTicks);

        /// <summary>
        /// Gets the current frame count since the start of the game.
        /// </summary>
        public int TotFrameCount { get; private set; }

        /// <summary>
        /// Set the time of one frame, used to calculate the elapsed time.
        /// </summary>
        public long ElapsedTicks { get; internal set; }
        /// <summary>
        /// Get the <see cref="ElapsedTicks"/> in milliseconds. It's the main value for update call
        /// </summary>
        public double ElapsedTime => TimerTick.GetMSFromTick(ElapsedTicks);

        /// <summary>
        /// It must be called on a regular basis at every *tick*.
        /// </summary>
        public void Frame()
        {
            TotFrameCount++;
            var currentTicks = TimerTick.Ticks - startTicks;
            ElapsedTicks = currentTicks - previousTicks;
            previousTicks = currentTicks;
            TotalTicks += ElapsedTicks;
        }

        public override string ToString()
        {
            return $"Total: {TotalTime}";
        }

    }
}
