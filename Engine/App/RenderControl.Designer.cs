﻿namespace Common.Windows
{
    partial class RenderControl
    {
        /// <summary> 
        /// Variabile di progettazione necessaria.
        /// </summary>
        protected System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary> 
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        protected virtual void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }

        #endregion
    }
}
