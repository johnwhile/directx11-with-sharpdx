﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;


using Common;
using Common.Maths;
using Engine.Effect;

namespace Engine
{
    public abstract class StaticBatchRenderer : GraphicChild
    {
        protected IndexBuffer IBuffer;
        protected VertexBuffer VBuffer;

        public StaticBatchRenderer(D3Device device) : base(device, "StaticBatchRenderer")
        {
        }

        public List<Mesh> Meshes = new List<Mesh>();

        public abstract void CreateDeviceDependentResources();

        protected virtual void CreateIndexBuffer()
        {
            int maxindex = Meshes.Max(x => x?.GetMaxIndex() ?? 0);
            int indicescount = Meshes.Sum(x => x?.IndicesCount ?? 0);
            int offset = 0;

            if (maxindex <= ushort.MaxValue)
            {
                ushort[] indices = new ushort[indicescount];
                foreach (var mesh in Meshes)
                {
                    if (mesh == null) continue;
                    ushort[] meshindices = mesh.GetIndexBuffer16();
                    Array.Copy(meshindices, 0, indices, offset, meshindices.Length);
                    offset += meshindices.Length;
                }

                IBuffer = ToDispose(IndexBuffer.CreateImmutable(Device, indices));
            }
            else
            {
                int[] indices = new int[indicescount];
                foreach (var mesh in Meshes)
                {
                    if (mesh == null) continue;
                    int[] meshindices = mesh.GetIndexBuffer32();
                    Array.Copy(meshindices, 0, indices, offset, meshindices.Length);
                    offset += meshindices.Length;
                }
                IBuffer = ToDispose(IndexBuffer.CreateImmutable(Device, indices));
            }

            IBuffer.Name = "StaticMeshIndicesBuffer";
        }
        protected abstract void CreateVertexBuffer();
    }

    public class StaticMeshBatchRenderer : StaticBatchRenderer
    {
        public StaticMeshBatchRenderer(D3Device device) : base(device)
        {
        }

        public override void CreateDeviceDependentResources()
        {
            CreateIndexBuffer();
            CreateVertexBuffer();
        }

        protected override void CreateVertexBuffer()
        {
            int verticescount = Meshes.Sum(x => x?.VerticesCount ?? 0);
            Vertex[] vertices = new Vertex[verticescount];
            int offset = 0;

            foreach (var mesh in Meshes)
            {
                if (mesh == null) continue;

                for (int i = 0; i < mesh.VerticesCount; i++) vertices[i + offset].position = mesh.Vertices[i];

                if (mesh.HasTexCoords) for (int i = 0; i < mesh.VerticesCount; i++) vertices[i + offset].texcoord = mesh.TexCoords[i];
                if (mesh.HasNormals) for (int i = 0; i < mesh.VerticesCount; i++)
                        vertices[i + offset].normals = mesh.Normals[i];
                        //vertices[i + offset].normals = UnitVectorPacker32.EncodeX15Y15Z1(mesh.Normals[i]);
                if (mesh.HasColors) for (int i = 0; i < mesh.VerticesCount; i++) vertices[i + offset].color = mesh.Colors[i];

                offset += mesh.VerticesCount;
            }
            VBuffer = ToDispose(VertexBuffer.CreateImmutable(Device, vertices));
        }
        
        public void Draw(D3Context context, EffectShading shading)
        {
            context.SetBuffer(VBuffer);
            context.SetBuffer(IBuffer);

            int vertexoffset = 0;
            int indexoffset = 0;
            for (int i = 0; i < Meshes.Count; i++)
            {
                var mesh = Meshes[i];
                if (mesh==null) continue;

                var material = shading.Materials;
                material.Diffuse = mesh.Diffuse;
                shading.Materials = material;
                shading.manager.Constants.Materials.Update(context);


                var transform = shading.Transforms;
                transform.World = mesh.Transform;
                transform.WorldInverseTraspose = Matrix4x4f.WorldInverseTraspose(mesh.Transform);
                transform.WorldViewProjection = shading.Ambients.ViewProjection * mesh.Transform;
                shading.Transforms = transform;
                shading.manager.Constants.Transforms.Update(context);

                context.PrimitiveTopology = mesh.Topology.ToSharpDx();

                context.DrawIndexed(mesh.IndicesCount, indexoffset, vertexoffset);

                indexoffset += mesh.IndicesCount;
                vertexoffset += mesh.VerticesCount;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        struct Vertex
        {
            public Vector3f position;
            public Vector2f texcoord;
            public Vector3f normals;
            public Color4b color;
        }
    }

    public class StaticSplineBatchRenderer : StaticBatchRenderer
    {
        public StaticSplineBatchRenderer(D3Device device) : base(device)
        {
        }

        public override void CreateDeviceDependentResources()
        {
            CreateIndexBuffer();
            CreateVertexBuffer();
        }
        protected override void CreateVertexBuffer()
        {
            int verticescount = Meshes.Sum(x => x.VerticesCount);
            Vertex[] vertices = new Vertex[verticescount];
            int offset = 0;

            foreach (var mesh in Meshes)
            {
                for (int i = 0; i < mesh.VerticesCount; i++) vertices[i + offset].position = mesh.Vertices[i];
                if (mesh.HasColors) for (int i = 0; i < mesh.VerticesCount; i++) vertices[i + offset].color = mesh.Colors[i];
                offset += mesh.VerticesCount;
            }
            VBuffer = ToDispose(VertexBuffer.CreateImmutable(Device, vertices));
        }

        public void Draw(D3Context context, EffectSpline splining)
        {
            context.SetBuffer(VBuffer);
            context.SetBuffer(IBuffer);

            int vertexoffset = 0;
            int indexoffset = 0;
            for (int i = 0; i < Meshes.Count; i++)
            {
                var mesh = Meshes[i];

                var transform = splining.Transforms;
                transform.World = mesh.Transform;
                transform.WorldInverseTraspose = Matrix4x4f.WorldInverseTraspose(mesh.Transform);
                transform.WorldViewProjection = splining.Ambients.ViewProjection * mesh.Transform;
                splining.Transforms = transform;

                splining.manager.Constants.Transforms.Update(context);

                context.PrimitiveTopology = mesh.Topology.ToSharpDx();

                context.DrawIndexed(mesh.IndicesCount, indexoffset, vertexoffset);

                indexoffset += mesh.IndicesCount;
                vertexoffset += mesh.VerticesCount;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        struct Vertex
        {
            public Vector3f position;
            public Color4b color;
        }
    }
}
