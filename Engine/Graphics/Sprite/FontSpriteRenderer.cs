﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Common;
using Common.Fonts;
using Common.Gui;
using Common.Maths;

using SharpDX.Direct3D11;

namespace Engine
{
    public class FontSpriteRenderer : GraphicChild
    {
        string currentext;
        FontDictionary font;
        StructuredBuffer GlyphSources;
        Texture2 texture;
        SamplerStateDx sampler;

        VertexBuffer sprites;
        ConstantBuffer<VSGLOBALS> vs_constants;
        ConstantBuffer<float> ps_constants;

        BlendStateDx trasparent;
        DepthStateDx nodepthtest;
        RasterizerStateDx solid;
        RasterizerStateDx wireframe;
        InputLayoutDx inputlayout;
        
        VertexShaderDx vs;
        VertexShaderDx vs_debug;

        bool useAlphaChn = false;
        bool useDistancefield = false;

        PixelShaderDx ps_debug;
        PixelShaderDx ps_raw;
        PixelShaderDx ps_raw_alpha;
        PixelShaderDx ps_df_smooth;
        PixelShaderDx ps_df_smooth_alpha;
        GeometryShaderDx gs;

        List<VERTEX> vertices;

        Rectangle4i area;

        /// <summary>
        /// Structude buffer contains basic glyth data
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        struct GLYPH
        {
            public Rectangle4i source; //texcoord of font texture
            public Vector2i offset;
        }


        [StructLayout(LayoutKind.Sequential)]
        struct VERTEX
        {
            public Vector2i position;
            public int sourceindex;
        }

        //respect the packing rule for a constant buffer
        [StructLayout(LayoutKind.Sequential)]
        struct VSGLOBALS
        {
            public Rectangle4i viewport;
            public Vector2i origin;
            public Vector2i texturesize;
            public Vector4b color;
            public float scale;
            // 0.25f / (spread * scale)
            public float smoothing;
            private float _pad0;
        }


        public FontSpriteRenderer(D3Device device, bool IsDistanceFieldVersion = false) : base(device, "FontSpriteRenderer")
        {

            
            useDistancefield = IsDistanceFieldVersion;


            //MipMapLinearNearest
            sampler = device.SamplerStates.Trilinear;

            var filter = SamplerStateDescription.Default();
            filter.Filter = Filter.MinMagMipLinear;
            filter.AddressV = TextureAddressMode.Wrap;
            filter.AddressU = TextureAddressMode.Wrap;

            sampler = device.SamplerStates.CreateOrGet(filter);


            


            //render states
            trasparent = device.BlendStates.Trasparent;
            nodepthtest = device.DepthStates.NoDepthTest;

            //solid and wireframe with scissor enable
            var solid_descr = device.RasterizeStates.Solid.Description;
            solid_descr.IsScissorEnabled = false;
            solid = device.RasterizeStates.CreateOrGet(solid_descr);

            wireframe = device.RasterizeStates.Wireframe;


            //shaders
            string hlsl = $"{AppConfig.ContentPath}\\Shaders\\Font.hlsl";
            vs = device.VertexShaders.CreateOrGet(hlsl, "VS_main");
            vs_debug = device.VertexShaders.CreateOrGet(hlsl, "VS_debug");

            ps_debug = device.PixelShaders.CreateOrGet(hlsl, "PS_debug");
            ps_raw = device.PixelShaders.CreateOrGet(hlsl, "PS_raw");
            ps_raw_alpha = device.PixelShaders.CreateOrGet(hlsl, "PS_raw_alpha");
            ps_df_smooth = device.PixelShaders.CreateOrGet(hlsl, "PS_df_smooth");
            ps_df_smooth_alpha = device.PixelShaders.CreateOrGet(hlsl, "PS_df_smooth_alpha");

            gs = device.GeometryShaders.CreateOrGet(hlsl, "GS_quad");

            //validate input layout
            var layout = new InputSlotLayout(
                VertexElement.Create<Vector2i>("ANCHOR"),
                VertexElement.Create<int>("INDEX"));

            inputlayout = device.InputStates.CreateOrGet(vs, layout);


            //build some sprites
            vs_constants = ToDispose(ConstantBuffer<VSGLOBALS>.Create(device));
            ps_constants = ToDispose(ConstantBuffer<float>.Create(device));

        }

        public FontDictionary Font
        {
            get => font;
            set
            {
                if (font != value)
                {
                    font = value;
                    //texture source rectangles
                    var sourcesrect = new GLYPH[font.RawGlyphs.Count];

                    for (int i = 0; i < font.RawGlyphs.Count; i++)
                    {
                        var g = font.RawGlyphs[i];
                        sourcesrect[i] = new GLYPH()
                        {
                            source = new Rectangle4i(g.x - 1, g.y - 1, g.width + 2, g.height + 2),
                            offset = new Vector2i(g.xoffset, g.yoffset),
                        };
                    }
                    RemoveAndDispose(ref GlyphSources);
                    GlyphSources = ToDispose(StructuredBuffer.Create(device, sourcesrect));
                    currentext = null;

                    useAlphaChn = System.IO.Path.GetExtension(font.Texture) == ".png";
                    //texture and texture's sampler
                    RemoveAndDispose(ref texture);
                    texture = ToDispose(Texture2.New(device, font.Texture));

                }
            }
        }



        void UpdateVertices(D3Context context, string text, float scale)
        {
            if (vertices == null) vertices = new List<VERTEX>(text.Length);
            vertices.Clear();

            int xadvance = 0;
            int yadvance = 0;
            
            area = Rectangle4i.Undefined;

            foreach (char c in text)
            {
                if (c == '\n')
                {
                    xadvance = 0;
                    yadvance += Font.LineHeight;
                    continue;
                }
                else if (Font.GetGlyph(c, out var glyph, out var index))
                {
                    var v = new VERTEX()
                    {
                        position = new Vector2i(glyph.xoffset + xadvance, glyph.yoffset + yadvance),
                        sourceindex = index
                    };

                    area.Add(new Rectangle4i(v.position.x, v.position.y, glyph.width, glyph.height));

                    xadvance += (short)(glyph.xadvance - Font.PaddingLeft - Font.PaddingRight);

                    vertices.Add(v);
                }
            }
            if (vertices.Count != 0)
            {
                if (sprites == null || sprites.IsDisposed || sprites.ElementCount < vertices.Count)
                {
                    RemoveAndDispose(sprites);
                    sprites = ToDispose(VertexBuffer.Create(Device, vertices.Capacity, vertices.ToArray(), 0, -1, BufferAccess.Dynamic));
                }
                else
                {
                    sprites.SetData(context, vertices.ToArray());
                }
            }
        }
        

        public Rectangle4i DrawString(D3Context context, ViewportClip presenterClip, string text, int x, int y, float scale = 1 , bool showireframe = false)
        {
            if (currentext != text)
            {
                currentext = text;
                UpdateVertices(context, currentext, scale);
            }

            var drawrectangle = area;
            drawrectangle.position += new Vector2i(x, y);


            var data = vs_constants.Data;

            data.origin = new Vector2i(x, y);
            data.texturesize = texture.Size;
            data.scale = scale;
            data.viewport = presenterClip.Rectangle;
            vs_constants.Data = data;

            ps_constants.Data = 0.04f; //smoothing
            context.SetToPixelStage(ps_constants, 3);

            context.SetViewports(presenterClip);
            context.SetScissor(drawrectangle);
            context.SetRenderState(trasparent);
            context.SetRenderState(nodepthtest);
            
            context.SetRenderState(inputlayout);
            context.SetToPixelStage(sampler);
            context.SetToPixelStage(texture);
            context.SetToVertexStage(GlyphSources, 1);
            context.SetBuffer(sprites);      
            context.SetShader(vs);
            context.SetShader(gs);

            context.SetToVertexStage(vs_constants, 2);

            if (showireframe)
            {
                //wireframe
                data.color = Color4b.Green;
                vs_constants.Data = data;
                vs_constants.Update(context);

                context.SetShader(ps_debug);
                context.SetRenderState(wireframe);
                context.Draw(vertices.Count);
            }

            //font
            data.color = Color4b.Red;
            vs_constants.Data = data;
            vs_constants.Update(context);

            context.SetRenderState(solid);

            if (useDistancefield) context.SetShader(useAlphaChn ? ps_df_smooth_alpha : ps_df_smooth);
            else context.SetShader(useAlphaChn ? ps_raw_alpha : ps_raw);

            context.PrimitiveTopology = DxPrimitive.PointList;
            context.Draw(vertices.Count);

            /*
            vs_constants.Data.origin = drawrectangle.position;
            vs_constants.Data.texturesize = drawrectangle.size;
            context.SetToVertexStage(vs_constants);
            context.SetShader(vs_debug);
            context.SetShader(ps_debug);
            context.SetRenderState(wireframe);
            context.Draw(1);
            */

            context.SetShader(VertexShaderDx.NULL);
            context.SetShader(PixelShaderDx.NULL);
            context.SetShader(GeometryShaderDx.NULL);

            return drawrectangle;
        }
    }
}
