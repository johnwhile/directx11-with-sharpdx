﻿using System;
using System.Runtime.InteropServices;

using Common;
using Common.Fonts;
using Common.Maths;
using SharpDX.Direct3D;

namespace Engine
{
    public class SpriteBatch : GraphicChild
    {
        const int MINSIZE = 10;
        public int Capacity => DynVertexBuffer != null && !DynVertexBuffer.IsDisposed ? DynVertexBuffer.ElementCount : 0;
        public int Count => sprites.Count;

        protected SpritePipeline pipeline;
        protected VertexBuffer DynVertexBuffer;
        protected SpriteGlobals globals;

        //protected ConstantBuffer cbuffer;
        protected ConstantBuffer<SpriteGlobals> DynConstantBuffer;

        protected StructBuffer<SpriteVertex> sprites;

        public Texture2 Texture
        {
            get => pipeline.texture;
            set => pipeline.texture = value;
        }

        public SpriteBatch(D3Device device, int capacity) : base(device)
        {
            capacity = capacity < MINSIZE ? MINSIZE : capacity;

            pipeline = ToDispose(new SpritePipeline(Device));

            DynVertexBuffer = ToDispose(VertexBuffer.CreateDynamic<SpriteVertex>(device, capacity));

            //cbuffer = ToDispose(ConstantBuffer.New(device, Marshal.SizeOf<SpriteGlobals>(), true));
            DynConstantBuffer = ToDispose(ConstantBuffer<SpriteGlobals>.Create(device));

            globals = new SpriteGlobals();

            sprites = new StructBuffer<SpriteVertex>(capacity);
        }

        /// <summary>
        /// Add a sprite vertex to draw's queue, in pixel coordinates
        /// </summary>
        public void AddSprite(Rectangle4i destination, ViewportClip viewport, Rectangle4f source, Vector4b color)
        {
            AddSprite( Mathelp.ScreenToClipSpace(destination, viewport.Size), source, color);
        }
        /// <summary>
        /// Add a sprite vertex to draw's queue, in homogeneus clip space
        /// </summary>
        public void AddSprite(Rectangle4f destination, Rectangle4f source, Vector4b color)
        {
            var sprite = new SpriteVertex()
            {
                destination = destination,
                source = source,
                color = color
            };
            sprites.Add(sprite);
        }

        /// <summary>
        /// Extract sprite from font
        /// </summary>
        public void AddString(string text, FontDictionary font, ViewportClip viewport, Vector2i offset, Vector4b color)
        {
            Vector2i Size = new Vector2i(0, 0);
            int xadvance = 0;
            int yadvance = 0;
            int lineHeight = font.LineHeightMinusPadding;

            foreach (char c in text)
            {
                if (c == '\n')
                {
                    xadvance = 0;
                    yadvance += lineHeight;
                    continue;
                }

                if (font.GetGlyph(c, out var glyph))
                {
                    var destination = new Rectangle4i(
                        glyph.xoffset + xadvance,
                        glyph.yoffset + yadvance,
                        glyph.width,
                        glyph.height);

                    destination.position += offset;

                    //normalize source to [0,1] uv coordinates
                    var source = new Rectangle4f(
                        (float)glyph.x / font.ScaleWidth,
                        (float)glyph.y / font.ScaleHeight,
                        (float)glyph.width / font.ScaleWidth,
                        (float)glyph.height / font.ScaleHeight);

                    //not sure the padding math is correct
                    xadvance += (short)(glyph.xadvance - font.PaddingLeft - font.PaddingRight);

                    AddSprite(destination, viewport, source, color);
                }
            }
            Size.x = xadvance;
            Size.y = yadvance + lineHeight;
        }


        public void Clear()
        {
            sprites.Clear();
        }


        public void Render(D3Context context, Rectangle4i? destination = null)
        {
            if (sprites.Count <= 0) return;

            if (sprites.Count > DynVertexBuffer.ElementCount)
            {
                RemoveAndDispose(DynVertexBuffer);
                DynVertexBuffer = ToDispose(VertexBuffer.CreateDynamic(Device, sprites.Capacity, sprites.Buffer, 0, sprites.Count));
            }
            else
            {
                //var t0 = TimerTick.Ticks;
                DynVertexBuffer.SetData(context, (SpriteVertex[])sprites);
                //DynVertexBuffer.WriteData(sprites.array, 0, true);
                //var t1 = TimerTick.Ticks;

                //Debugg.Print($"Update setdata buffer in {TimerTick.GetMSFromTick(t1 - t0)}", DebugInfo.Warning);
            }

            if (Texture != null) globals.texture = Texture.Size;
            globals.viewport = destination.HasValue ? destination.Value : context.CurrentViewport.Rectangle;

            Flush(context);
        }

        void Flush(D3Context context)
        {
            DynConstantBuffer.Data = globals;
            context.SetToVertexStage(DynConstantBuffer);
            context.SetBuffer(DynVertexBuffer);
            pipeline.SetToContext(context);
            context.Draw(Count);
        }
        protected override void Dispose(bool disposemanaged)
        {
            sprites.Clear();
            base.Dispose(disposemanaged);
        }

        [StructLayout(LayoutKind.Sequential)]
        protected struct SpriteGlobals
        {
            public Rectangle4i viewport;

            public Vector2f texture;
            public float pad0;
            public float pad1;
        }


    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SpriteVertex
    {
        // Destination rectangle using omogeneus coordinates
        public Rectangle4f destination;
        // texture source rectangle
        public Rectangle4f source;
        // optional color
        public Vector4b color;
    }


    public class SpritePipeline : GraphicChild
    {
        public SamplerStateDx sampler;
        public Texture2 texture;

        public VertexShaderDx vertexshader;
        public GeometryShaderDx geometryshader;
        public PixelShaderDx pixelshader;

        public BlendStateDx blend;
        public DepthStateDx depthstencil;
        public RasterizerStateDx rasterizer;
        public InputLayoutDx inputLayout;

        public InputSlotLayout layout;


        public SpritePipeline(D3Device device) : base(device)
        {
            blend = device.BlendStates.Trasparent;
            depthstencil = device.DepthStates.NoDepthTest;
            rasterizer = device.RasterizeStates.Solid;
            sampler = device.SamplerStates.Trilinear;

            //Rectangle4f or Verctor4f are the same
            layout = new InputSlotLayout(
                VertexElement.Create<Vector4f>("POSITION"),
                VertexElement.Create<Vector4f>("TEXCOORD"),
                VertexElement.Create<Vector4b>("COLOR"));

            string hlsl = $"{AppConfig.ContentPath}\\Shaders\\sprite.hlsl";

            vertexshader = device.VertexShaders.CreateOrGet(hlsl, "VS_normalized_viewport");
            pixelshader = device.PixelShaders.CreateOrGet(hlsl, "PS_textured"); //PS_color
            geometryshader = device.GeometryShaders.CreateOrGet(hlsl, "GS_quad");
            
            inputLayout = device.InputStates.CreateOrGet(vertexshader, layout);
            
        }

        public void SetToContext(D3Context context)
        {
            context.SetRenderState(blend);
            context.SetRenderState(depthstencil);
            context.SetRenderState(rasterizer);

            context.SetToPixelStage(sampler);
            context.SetToPixelStage(texture);

            context.SetRenderState(inputLayout);
            context.SetShader(vertexshader);
            context.SetShader(geometryshader);
            context.SetShader(pixelshader);

            context.PrimitiveTopology = DxPrimitive.PointList;
        }

        protected override void Dispose(bool disposemanaged)
        {
            //pipeline isn't owner of texture 
            texture = null;
            base.Dispose(disposemanaged);
        }
    }

}
