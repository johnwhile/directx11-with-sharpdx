﻿
using Common;

namespace Engine
{
    /// <summary>
    /// The instruction to render
    /// </summary>
    public abstract class Pipeline : GraphicChild
    {
        public DxPrimitive topology;
        public VertexShaderDx vertexshader;
        public PixelShaderDx pixelshader;
        public BlendStateDx blend;
        public DepthStateDx depthstencil;
        public RasterizerStateDx rasterizer;
        public InputLayoutDx inputLayout;
        public SamplerStateDx sampler;

        /// <summary>
        /// </summary>
        public Pipeline(D3Device device) : base(device)
        {
            blend = device.BlendStates.Trasparent;
            depthstencil = device.DepthStates.NoDepthTest;
            rasterizer = device.RasterizeStates.Solid;
            sampler = device.SamplerStates.Trilinear;

        }

        public virtual void SetToContext(D3Context context)
        {
            context.SetRenderState(blend);
            context.SetRenderState(depthstencil);
            context.SetRenderState(rasterizer);
            context.SetRenderState(inputLayout);

            context.SetShader(pixelshader);
            context.SetToPixelStage(sampler);

            context.SetShader(vertexshader);
            context.SetShader(pixelshader);

            context.PrimitiveTopology = DxPrimitive.TriangleList;
        }
    }
}
