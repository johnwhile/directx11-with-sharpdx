﻿using System;
using System.Diagnostics;



using SharpDX.Direct3D11;

using Common;
using Common.Maths;
using Engine.Direct2D;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Engine
{

    /// <summary>
    /// The main enptry point for directx: manage the graphic's devices and manage the shawpchains
    /// </summary>
    public class GraphicDeviceManager : DisposableDx
    {
        D2Device device2d;
        D3Device device3d;

        /// <summary>
        /// for internal use.
        /// </summary>
        public readonly List<SwapChainPresenter> Presenters;

        public D3Device Device3D=> device3d;
        public D2Device Device2D => device2d;

        public event Action<object> DeviceCreated;
        public event Action<object> DeviceDisposing;
        public event Action<object> DeviceLost;

        /// <summary>
        /// Initialize both <see cref="D3Device"/> and <see cref="D2Device"/> devices.
        /// </summary>
        public GraphicDeviceManager(bool enableDirect2d = true)
        {
            Presenters = new List<SwapChainPresenter>();

            OnDeviceDisposing(device3d);

            RemoveAndDispose(ref device2d);
            RemoveAndDispose(ref device3d);

            if (device3d != null) device3d.Disposing -= OnDeviceDisposing;
            device3d = ToDispose(new D3Device(GraphicFactory.DefaultAdapter));
            device3d.Disposing += OnDeviceDisposing;

            //device3d.Presenter = presenter;

            if (enableDirect2d) device2d = ToDispose(new D2Device(device3d));
            //device2d.Context.SetRenderTarget(presenter.BackBuffer);

            OnDeviceCreated(device3d);
        }

        /// <summary>
        /// Initialize both <see cref="D3Device"/> and <see cref="D2Device"/> devices and create the first swapchain
        /// </summary>
        public GraphicDeviceManager(Control output, bool vsync = false, bool fullscreen = false, bool enableDirect2d = true) : 
            this(enableDirect2d)
        {
            CreatePresenter(output, vsync, fullscreen);
        }

        /// <summary>
        /// Add a new presenter in the swap chain, with relative output, into <see cref="Presenters"/> list.<br/>
        /// <b>Don't call <see cref="Disposable.ToDispose{T}(T)"/>, this manage take care of that.</b>
        /// </summary>
        /// <param name="vsync"></param>
        /// <param name="fullscreen">start as fullscreen (It's start as windowed then switch to fullscreen)</param>
        public SwapChainPresenter CreatePresenter(Control output, bool vsync = false, bool fullscreen = false)
        {
            foreach(var presenter in Presenters)
            {
                if (presenter.Description.OutputWnd == output) throw new ArgumentException("A swapchain for this control already exist");
            }


            var param = new PresentationParameters(output, output.ClientSize)
            {
                Vsync = vsync,
                IsFullScreen = fullscreen,
            };
            var swapchain = ToDispose(new SwapChainPresenter(device3d, param, device2d));

            Presenters.Add(swapchain);
            return swapchain;
        }

        /// <summary>
        /// Dispose this swapchain and remove it from <see cref="Presenters"/> list.
        /// </summary>
        /// <param name="swapchain"></param>
        public void RemoveSwapChain(SwapChainPresenter swapchain)
        {
            Presenters.Remove(swapchain);
            RemoveAndDispose(swapchain);
        }

        public bool HardwareFullscreenSwitching(SwapChainPresenter presenter, bool fullscreen)
        {
            if (fullscreen) presenter.EnterFullscreen();
            else presenter.ExitFullscreen();
            return true;
        }
        /// <summary>
        /// Before start drawing check the device status
        /// </summary>
        public bool CheckDeviceStatus()
        {
            if (device3d == null) return false;

            switch (device3d.DeviceStatus)
            {
                case DeviceStatus.Normal:
                    return true;

                default:
                    OnDeviceLost(device3d);
                    System.Threading.Thread.Sleep(20);
                    throw new NotImplementedException("device3d lost not implemented");
            }
        }

        protected virtual void OnDeviceCreated(object sender)
        {
            DeviceCreated?.Invoke(sender);
        }

        protected virtual void OnDeviceDisposing(object sender)
        {
            DeviceDisposing?.Invoke(sender);

            if (device3d == null) return;
            device3d.Disposing -= OnDeviceDisposing;

            foreach(var swapchain in Presenters) RemoveAndDispose(swapchain);
            Presenters.Clear();

            RemoveAndDispose(ref device2d);
            RemoveAndDispose(ref device3d);
        }

        protected virtual void OnDeviceLost(object sender)
        {
            Debugg.Warning("Manager devicelost");
            OnDeviceDisposing(sender);
            DeviceLost?.Invoke(sender);

            throw new NotImplementedException();
        }

        protected override void Dispose(bool disposemanaged)
        {
            Debugg.Indent++;
            if (device3d != null) device3d.Disposing -= OnDeviceDisposing;
            Debugg.Info("GraphicDeviceManager start disposing");
#if DEBUGMORE
            RemoveToDispose(device3d);
            base.Dispose(disposemanaged);
            device3d.Context.Dispose();
            device3d.ReportLiveDeviceObjects();
            device3d.Dispose();
#else
            base.Dispose(disposemanaged);
#endif
            Debugg.Indent--;
        }
    }

}
