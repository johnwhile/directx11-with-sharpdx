﻿using Common.Maths;
using System.Collections.Generic;
using System.Runtime.InteropServices;

/// 
/// 16byte Alligned structures
/// 
namespace Engine.Effect
{
    //respect the hlsl packing rule
    [StructLayout(LayoutKind.Sequential)]
    public struct EffectTransform
    {
        public Matrix4x4f World;
        public Matrix4x4f WorldInverseTraspose;
        public Matrix4x4f WorldViewProjection;
    };

    [StructLayout(LayoutKind.Explicit)]
    public struct EffectAmbient
    {
        [FieldOffset(0)]
        public Matrix4x4f View;
        [FieldOffset(64)]
        public Matrix4x4f Projection;
        [FieldOffset(128)]
        public Matrix4x4f ViewProjection;
        [FieldOffset(192)]
        public Vector3f CameraPosition;
    };

    [StructLayout(LayoutKind.Explicit)]
    public struct EffectLight
    {
        [FieldOffset(0)]
        public Vector3f LightPosition;
        [FieldOffset(16)]
        public Vector3f LightDirection;
        [FieldOffset(32)]
        public Vector4f LightColor;
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct EffectMaterial
    {
        //uncompressed colors
        public Vector4f Ambient;
        public Vector4f Diffuse;
        public Vector4f Specular;
        public Vector4f Emissive;
        public Matrix4x4f UVTransform;
        public float SpecularPower;
        public int HasTexture;
    };



    /// <summary>
    /// Common Constants Buffers, seealso <i>"common.hlsl"</i>
    /// </summary>
    public class EffectConstants : GraphicChild
    {
        public ConstantBuffer<EffectTransform> Transforms;
        public ConstantBuffer<EffectAmbient> Ambients;
        public ConstantBuffer<EffectLight> Lights;
        public ConstantBuffer<EffectMaterial> Materials;


        public EffectConstants(D3Device device, string debugname = null) : base(device, debugname)
        {
            CreateDeviceDependentResources(device);
        }


        void CreateDeviceDependentResources(D3Device device)
        {
            Transforms = ToDispose(ConstantBuffer<EffectTransform>.Create(device), "constant_transform");
            Ambients = ToDispose(ConstantBuffer<EffectAmbient>.Create(device), "constant_ambient");
            Lights = ToDispose(ConstantBuffer<EffectLight>.Create(device), "constant_light");
            Materials = ToDispose(ConstantBuffer<EffectMaterial>.Create(device), "constant_material");


            //some default values
            Transforms.Data = new EffectTransform()
            {
                World = Matrix4x4f.Identity,
                WorldInverseTraspose = Matrix4x4f.Identity,
            };

            Lights.Data = new EffectLight()
            {
                LightDirection = -Vector3f.Normalize(Vector3f.UnitY + Vector3f.UnitX),
            };

            Materials.Data = new EffectMaterial()
            {
                Ambient = new Vector4f(0.2f),
                Diffuse = Color4b.Toy,
                Emissive = Color4b.Black,
                Specular = Color4b.White,
                SpecularPower = 20f
            };
        }
    }
}
