﻿using Common.Maths;
using System.Runtime.InteropServices;
using static Engine.Effect.Effect;
using System.Runtime.Remoting.Contexts;

namespace Engine.Effect
{
    public abstract class EffectShading : Effect
    {
        protected InputSlotLayout layout;

        public EffectShading(EffectManager mananger, string name = null) : base(mananger, name)
        {
            layout = new InputSlotLayout(
                VertexElement.Create<Vector3f>("POSITION"),
                VertexElement.Create<Vector2f>("TEXCOORD"),
                VertexElement.Create<Vector3f>("NORMAL"),
                VertexElement.Create<Color4b>("COLOR"));
        }
    }

    public class SmoothShading : EffectShading
    {
        public SmoothShading(EffectManager mananger, string name = null) : base(mananger, name)
        {
            blend = device.BlendStates.Default;
            depthstencil = device.DepthStates.Default;
            rasterizer = device.RasterizeStates.Solid;
            sampler = device.SamplerStates.Trilinear;


            string filename = manager.ShaderDir + "Mesh\\SmoothShading.hlsl";

            var vs = device.VertexShaders.CreateOrGet(filename, "VS_SmoothShading");
            var ps_diffuse = device.PixelShaders.CreateOrGet(filename, "PS_SmoothShading_Diffuse");
            var ps_texture = device.PixelShaders.CreateOrGet(filename, "PS_SmoothShading_Texture");


            inputLayout = device.InputStates.CreateOrGet(vs, layout);
            inputLayout.Name = "MeshShadingLayout";

            Techniques = new Technique[]
            {
                new Technique(vs, ps_diffuse),
                new Technique(vs, ps_texture)
            };
        }


        public override void SetToContext(D3Context context, int technique = 0)
        {
            base.SetToContext(context, technique);

            context.SetToVertexStage(manager.Constants.Transforms, 0);
            context.SetToVertexStage(manager.Constants.Ambients, 1);

            context.SetToPixelStage(manager.Constants.Lights, 2);
            context.SetToPixelStage(manager.Constants.Materials, 3);
        }

    }
    
    public class FlatShading : EffectShading
    {
        public FlatShading(EffectManager mananger, string name = "FlatShading") : base(mananger, name)
        {
            blend = device.BlendStates.Default;
            depthstencil = device.DepthStates.Default;
            rasterizer = device.RasterizeStates.Solid;
            sampler = device.SamplerStates.Trilinear;

            string filename = manager.ShaderDir + "Mesh\\FlatShading.hlsl";

            var vs = device.VertexShaders.CreateOrGet(filename, "VS_FlatShading");
            var gs = device.GeometryShaders.CreateOrGet(filename, "GS_FlatShading");
            
            var ps_diffuse = device.PixelShaders.CreateOrGet(filename, "PS_FlatShading_Diffuse");
            var ps_diffuse_wire = device.PixelShaders.CreateOrGet(filename, "PS_FlatShading_Diffuse_Wireshading2");
            var ps_texture = device.PixelShaders.CreateOrGet(filename, "PS_FlatShading_Texture");

            inputLayout = device.InputStates.CreateOrGet(vs, layout);
            inputLayout.Name = "MeshShadingLayout";

            Techniques = new Technique[]
            {
                new Technique(vs, ps_diffuse, gs),
                new Technique(vs, ps_diffuse_wire, gs),
                new Technique(vs, ps_texture, gs)
            };
        }

        public override void SetToContext(D3Context context, int technique = 0)
        {
            base.SetToContext(context, technique);

            context.SetToVertexStage(manager.Constants.Transforms, 0);
            context.SetToGeometryStage(manager.Constants.Transforms, 0);

            context.SetToVertexStage(manager.Constants.Ambients, 1);

            context.SetToGeometryStage(manager.Constants.Lights, 2);
            context.SetToPixelStage(manager.Constants.Lights, 2);

            context.SetToPixelStage(manager.Constants.Materials, 3);
        }
    }
}
