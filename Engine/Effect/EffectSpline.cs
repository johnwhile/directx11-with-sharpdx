﻿using Common.Maths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Effect
{
    public class EffectSpline : Effect
    {
        protected InputSlotLayout layout;

        public EffectSpline(EffectManager manager, string name = "SplineEffect") : base(manager, name)
        {
            blend = device.BlendStates.Default;
            depthstencil = device.DepthStates.Default;
            rasterizer = device.RasterizeStates.Solid;
            sampler = device.SamplerStates.Trilinear;


            layout = new InputSlotLayout(
                VertexElement.Create<Vector3f>("POSITION"),
                VertexElement.Create<Color4b>("COLOR"));

            string filename = manager.ShaderDir + "Spline\\Spline.hlsl";

            var vs = device.VertexShaders.CreateOrGet(filename, "VS_Spline");
            var ps = device.PixelShaders.CreateOrGet(filename, "PS_Spline");


            inputLayout = device.InputStates.CreateOrGet(vs, layout);
            inputLayout.Name = "SplineLayout";

            Techniques = new Technique[]
            {
                new Technique(vs, ps),
            };
        }

        public override void SetToContext(D3Context context, int technique = 0)
        {
            base.SetToContext(context, technique);

            context.SetToVertexStage(manager.Constants.Transforms, 0);
            context.SetToVertexStage(manager.Constants.Ambients, 1);
        }
    }
}
