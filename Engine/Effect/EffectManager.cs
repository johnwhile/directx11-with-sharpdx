﻿using Common.Maths;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine.Effect
{
    public class EffectManager : GraphicChild
    {
        public readonly string ShaderDir;

        public EffectConstants Constants;


        public EffectManager(D3Device device, string debugname = null) : base(device, debugname)
        {
            Constants = ToDispose(new EffectConstants(device));
            ShaderDir = AppConfig.ContentPath + "\\Shaders\\";

        }

    }
}
