﻿
using Common.Maths;

namespace Engine.Effect
{
    public abstract class Effect : GraphicChild
    {
        public EffectManager manager;

        public Technique[] Techniques;

        protected SamplerStateDx sampler;
        protected BlendStateDx blend;
        protected DepthStateDx depthstencil;
        protected RasterizerStateDx rasterizer;
        protected InputLayoutDx inputLayout;

        public Effect(EffectManager manager, string name = null) : base(manager.Device, name)
        {
            this.manager = manager;
        }

        public EffectTransform Transforms
        {
            get => manager.Constants.Transforms.Data;
            set => manager.Constants.Transforms.Data = value;
        }

        public EffectAmbient Ambients
        {
            get => manager.Constants.Ambients.Data;
            set => manager.Constants.Ambients.Data = value;
        }

        public EffectLight Lights
        {
            get => manager.Constants.Lights.Data;
            set => manager.Constants.Lights.Data = value;
        }

        public EffectMaterial Materials
        {
            get => manager.Constants.Materials.Data;
            set => manager.Constants.Materials.Data = value;
        }

        public virtual void SetToContext(D3Context context, int technique = 0)
        {
            //pass null to remove
            context.SetShader(Techniques[technique].vs);
            context.SetShader(Techniques[technique].ps);
            context.SetShader(Techniques[technique].gs);

            context.SetRenderState(blend);
            context.SetRenderState(depthstencil);
            context.SetRenderState(rasterizer);
            context.SetRenderState(inputLayout);
        }

        public class Technique
        {
            public string Name = "TechniqueX";

            public VertexShaderDx vs;
            public PixelShaderDx ps;
            public GeometryShaderDx gs;

            public Technique(VertexShaderDx vs, PixelShaderDx ps, GeometryShaderDx gs = null)
            {
                this.vs = vs;
                this.ps = ps;
                this.gs = gs;
            }
        }
    }
}
