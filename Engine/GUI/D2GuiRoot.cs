﻿using Common.Maths;
using Common.Gui;

namespace Engine.Direct2D
{
    public class D2GuiRoot : GuiRoot
    {
        D2GuiGraphicRenderer renderer;

        public D2GuiRoot(GuiManager manager, D2GuiGraphicRenderer renderer) : base(manager, default(Rectangle4i))
        {
            this.renderer = renderer;
        }

        public void Draw()
        {
            renderer?.DrawRectangle(Destination, Color4b.Black);
        }
    }
}
