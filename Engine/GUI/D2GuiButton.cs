﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

using Common.Maths;
using Common.Gui;

namespace Engine.Direct2D
{
    [DebuggerDisplay("{ToString()}")]
    public class D2GuiButton : GuiButton
    {
        public D2GuiGraphicRenderer renderer;

        public D2GuiButton(GuiContainer parent, D2GuiGraphicRenderer renderer) : base(parent, default(Rectangle4i))
        {
            this.renderer = renderer;
        }

    }
}
