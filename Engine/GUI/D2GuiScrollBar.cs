﻿using System;
using System.Windows.Forms;

using Common.Maths;
using Common.Gui;
using Common.Inputs;

namespace Engine.Direct2D
{
    public class D2GuiScrollBar : GuiScrollBar
    {
        public D2GuiGraphicRenderer renderer;

        public D2GuiScrollBar(GuiContainer parent, GuiScrollType type, D2GuiGraphicRenderer renderer) : base(parent, type)
        {
            this.renderer = renderer;
            GuiCursor = type == GuiScrollType.Vertical ? MouseCursor.HSplit : MouseCursor.VSplit;
        }
    }

    public class D2GuiScrollSlider : GuiSlider
    {
        D2GuiGraphicRenderer renderer;

        public D2GuiScrollSlider(GuiScrollBar parent, D2GuiGraphicRenderer renderer) : base(parent)
        {
            this.renderer = renderer;
            GuiCursor = parent.scrolltype == GuiScrollType.Vertical ? MouseCursor.SizeNS : MouseCursor.SizeWE;
        }

    }
}
