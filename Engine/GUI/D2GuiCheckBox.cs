﻿using System;
using System.Diagnostics;
using System.Drawing;

using Common.Maths;
using Common.Gui;

namespace Engine.Direct2D
{
    /// <summary>
    /// A sample checkbox rendered by <see cref="System.Drawing.Graphics"/> renderer.
    /// The <see cref="renderer"/> value must be assigned
    /// </summary>
    [DebuggerDisplay("{ToString()}")]
    public class D2GuiCheckBox : GuiCheckBox
    {
        public D2GuiGraphicRenderer renderer;

        public D2GuiCheckBox(GuiContainer parent, D2GuiGraphicRenderer renderer) : base(parent)
        {
            this.renderer = renderer;

        }

    }
}
