﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Common.Maths;
using Common.Gui;

namespace Engine.Direct2D
{
    /// <summary>
    /// A sample textbox rendered by <see cref="System.Drawing.Graphics"/> renderer.
    /// The <see cref="renderer"/> value must be assigned
    /// </summary>
    public class D2GuiComboBox : GuiComboBox
    {
        D2GuiGraphicRenderer renderer;

        public D2GuiComboBox(GuiContainer parent, D2GuiGraphicRenderer renderer) : base(parent)
        {
            this.renderer = renderer;

        }

    }
    public class D2GuiButtonsList : GuiButtonsList
    {
        D2GuiGraphicRenderer renderer;

        public D2GuiButtonsList(GuiComboBox parent, D2GuiGraphicRenderer renderer) : base(parent)
        {
            this.renderer = renderer;
        }

        public void Draw()
        {
            if (renderer == null) return;
            int rows = owner.Items.Count;
            int dy = Destination.height / rows;

            renderer.FillRectangle(Destination, Color4b.White);
            renderer.DrawRectangle(Destination, Color4b.Black);
            
            for (int i = 0; i < rows; i++)
            {
                string text = owner.Items[i].ToString();
                Rectangle4i rect = Destination;
                rect.height = dy;
                rect.y = Destination.y + dy * i;

                if (owner.SelectedItem == i)
                {
                    renderer.FillRectangle(rect, Color4b.Blue);
                    renderer.DrawString(text, rect, Color4b.White);
                }
                else
                {
                    renderer.DrawString(text, rect, Color4b.Black);
                }
                renderer.DrawRectangle(rect, Color4b.Gray);
            }
        }
    }
}
