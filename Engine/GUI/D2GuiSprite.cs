﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common.Maths;

namespace Engine.Direct2D
{
    public class D2GuiSprite
    {
        Rectangle4i destination;
        Vector4b errorcolor = new Vector4b(0, 0, 0, 150);
        D2GuiGraphicRenderer renderer;

        public D2Texture texture;
        public Rectangle4i source;
        
        public D2GuiSprite(D2GuiGraphicRenderer renderer)
        {
            this.renderer = renderer;
        }

        public void Draw(Vector2i position)
        {
            destination.size = source.size;
            destination.position = position;

            if (renderer == null) return;

            if (texture != null)
            {
                renderer.DrawImage(texture.Image, destination, source);
            }
            else
            {
                renderer.FillRectangle(destination, errorcolor);
            }
        }
    }
}
