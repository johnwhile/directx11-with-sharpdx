﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Common.Maths;
using Common.Gui;
using Common.Gui.SystemGraphic;

namespace Engine.Direct2D
{
    /// <summary>
    /// A sample textbox rendered by <see cref="System.Drawing.Graphics"/> renderer.
    /// The <see cref="renderer"/> value must be assigned
    /// </summary>
    public class D2GuiText : GuiEditText
    {
        D2GuiGraphicRenderer renderer;

        public D2GuiText(GuiContainer parent, D2GuiGraphicRenderer renderer, string initialText = "") : base(parent, initialText)
        {
            this.renderer = renderer;
        }

        public override void Draw(GraphicsRenderer renderer, bool debug = false)
        {
            throw new NotImplementedException();
        }

        public override void InitDefaultComponents()
        {
            throw new NotImplementedException();
        }
    }
}
