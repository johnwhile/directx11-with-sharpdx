﻿using System;
using System.Collections.Generic;


using Common;
using Common.Gui;
using Common.Maths;

using Engine;
using D2D = SharpDX.Direct2D1;

namespace Engine.Direct2D
{
    /// <summary>
    /// Basic direct2d renderer for gui graphics
    /// </summary>
    public class D2GuiGraphicRenderer : DisposableDx
    {
        D2GraphicTargetBase rendertarget;
        
        public D2Font Font;  

        public D2GuiGraphicRenderer(D2GraphicTargetBase renderTarget)
        {
            rendertarget = renderTarget;
            Font = D2Font.GetFont("Arial", 10);
        }



        public void DrawImage(D2Bitmap image, Rectangle4i destination, Rectangle4i source)
        {
            rendertarget.DrawImage(image, destination, source);
        }

        public void FillRectangle(Rectangle4i destination, Color4b fillcolor, Rectangle4i? clipregion = null)
        {
            rendertarget.FillRectangle(destination, fillcolor, clipregion);
        }
        public void DrawRectangle(Rectangle4i destination, Color4b bordercolor, int thickness = 1, Rectangle4i? clipregion = null)
        {
            rendertarget.DrawRectangle(destination, bordercolor, thickness, clipregion);
        }

        public void DrawString(string text, Rectangle4i destination, Color4b color)
        {
            rendertarget.DrawText(Font, text, color, destination);
        }
        public void DrawString(string text, int x, int y, Color4b color)
        {
            rendertarget.DrawText(Font, text, color, x, y);
        }
    }
}
