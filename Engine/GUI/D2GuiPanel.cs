﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using Common.Maths;
using Common.Gui;
using Common.Gui.SystemGraphic;

namespace Engine.Direct2D
{
    /// <summary>
    /// A sample panel rendered by <see cref="System.Drawing.Graphics"/> renderer.
    /// The <see cref="renderer"/> value must be assigned
    /// </summary>
    [DebuggerDisplay("{ToString()}")]
    public class D2GuiPanel : GuiPanel
    {
        public D2GuiGraphicRenderer renderer;

        public D2Texture texture;
        public Rectangle4i source;

        public D2GuiPanel(GuiContainer parent, D2GuiGraphicRenderer renderer) : base(parent, default(Rectangle4i))
        {
            this.renderer = renderer;


        }
    }
}
