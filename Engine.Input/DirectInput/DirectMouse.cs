﻿using System;
using System.Collections.Generic;
using System.Text;

using DX = SharpDX;
using DI = SharpDX.DirectInput;

using SharpDX;
using SharpDX.DirectInput;

using System.Windows.Forms;
using Common.Inputs;

using Point = System.Drawing.Point;

using Common.WinNative;

namespace Engine.Inputs
{
    /// <summary>
    /// The Mouse input manager, the acquiring is done at the first Update, you can also disable acquiring with
    /// EnableAcquiring = false BEFORE update
    /// </summary>
    /// <remarks>
    /// MOVEMENT :
    /// The data returned for the x-axis and y-axis of a mouse, indicates the movement of the mouse itself, not the cursor pixel position.
    /// The units of measurement are based on the values returned by the mouse hardware and have nothing to do with pixels
    /// or any other form of screen measurement, for a tipical computer mouse the movement==pixel, but can be different for other hardwares like jeypad.
    /// Also axis not match with screen X Y axis
    /// </remarks>
    public class MouseDirectInput : DirectInputDevice, IMouse
    {
        const int BUTTONCOUNT = 8;

        DI.Mouse mouse;
        DI.MouseState states;
        DI.DeviceAxisMode axisMode;
        //bool[] prevPress;
        

        MyMouseState current = MyMouseState.Empty;
        MyMouseState previous = MyMouseState.Empty;

        MyMouseArg[] mouseArgs = new MyMouseArg[BUTTONCOUNT];
        
        MyMouseArg mouseArgsEmpty = new MyMouseArg();

        int screenW = Screen.PrimaryScreen.Bounds.Width;
        int screenH = Screen.PrimaryScreen.Bounds.Height;

        bool isAxisRelative;

        public MyMouseState GetMouseState
        {
            get { return current; }
        }

        public event MyMouseEventHandler OnMouseStateChanged;
        public event MyMouseEventHandler OnMouseDoubleClick;
        public event MyMouseEventHandler OnMouseDown;
        public event MyMouseEventHandler OnMouseUp;
        public event MyMouseEventHandler OnMouseMove;
        public event MyMouseEventHandler OnMouseWheel;


        /// <summary>
        /// i don't know that is DI.DirectInput, but is always better initialize only once the same class
        /// </summary>
        /// <param name="mainwindow">SharpDX require Form handle otherwise crash</param>
        internal MouseDirectInput(Form mainwindow, DI.DirectInput dxi, InputUsage usage = InputUsage.DefaultMouse, DI.DeviceAxisMode axisMode = DI.DeviceAxisMode.Relative)
            : base(mainwindow, usage)
        {     
            device = mouse = new DI.Mouse(dxi);

            this.usage = usage;


            // Because a mouse is a relative device - unlike a joystick, it does not have a home position - relative data is returned by default.
            mouse.Properties.AxisMode = axisMode;
            System.Diagnostics.Trace.WriteLine(axisMode);
            
            this.axisMode = axisMode;
            isAxisRelative = axisMode == DeviceAxisMode.Relative;


            // just a memo: not valid for keyboard or mouse
            //if (defaultLevel == (DI.CooperativeLevel.Exclusive | DI.CooperativeLevel.Background)) throw new ArgumentException("Level not valid");

            // DISCL_EXCLUSIVE = not share with other programs
            // DISCL_NONEXCLUSIVE = However if you want other applications to have access to keyboard input while your program 
            // is running you can set it to non-exclusive.
            // Just remember that if it is non-exclusive and you are running in a windowed mode then you will need to check 
            // for when the device loses focus and when it re-gains that focus so it can re-acquire the device for use again.
            // This focus loss generally happens when other windows become the main focus over your window or your window is minimized.
            mouse.SetCooperativeLevel(mainwindow.Handle, (DI.CooperativeLevel)usage);


            states = new DI.MouseState();
            current = new MyMouseState();
            previous = new MyMouseState();
            prevPress = new bool[BUTTONCOUNT];

            mouseArgs[0] = new MyMouseArg(MouseKey.Left);
            mouseArgs[1] = new MyMouseArg(MouseKey.Right);
            mouseArgs[2] = new MyMouseArg(MouseKey.Middle);
            mouseArgs[3] = new MyMouseArg(MouseKey.XBtn1);
            mouseArgs[4] = new MyMouseArg(MouseKey.XBtn2);
            for (int i = 5; i < BUTTONCOUNT; i++) mouseArgs[i] = new MyMouseArg(MouseKey.Unknow);
        }

        /// <summary>
        /// MainFocusWindow define the focus state of main windows when use NonExclusive flag
        /// </summary>
        /// <param name="mainwindow">SharpDX require Form handle otherwise crash</param>
        public MouseDirectInput(Form mainwindow, InputUsage usage = InputUsage.DefaultMouse, bool absolute = false)
            : this(mainwindow, new DI.DirectInput(), usage, absolute ? DI.DeviceAxisMode.Absolute : DI.DeviceAxisMode.Relative) { }

        /// <summary>
        /// Read the device states
        /// </summary>
        public override bool Update()
        {
            if (!TryReadState()) return false;

            for (int i = 0; i < 5; i++) mouseArgs[i].state = current;
            mouseArgsEmpty.state = current;

            if (OnMouseDown != null)
            {
                if (current.Left == KeyState.Down) OnMouseDown(KeyState.Down, mouseArgs[0]);
                if (current.Right == KeyState.Down) OnMouseDown(KeyState.Down, mouseArgs[1]);
                if (current.Middle == KeyState.Down) OnMouseDown(KeyState.Down, mouseArgs[2]);
                if (current.XButton1 == KeyState.Down) OnMouseDown(KeyState.Down, mouseArgs[3]);
                if (current.XButton2 == KeyState.Down) OnMouseDown(KeyState.Down, mouseArgs[4]); 
            }

            if (OnMouseUp != null)
            {
                if (current.Left == KeyState.Up) OnMouseDown(KeyState.Up, mouseArgs[0]);
                if (current.Right == KeyState.Up) OnMouseDown(KeyState.Up, mouseArgs[1]);
                if (current.Middle == KeyState.Up) OnMouseDown(KeyState.Up, mouseArgs[2]);
                if (current.XButton1 == KeyState.Up) OnMouseDown(KeyState.Up, mouseArgs[3]);
                if (current.XButton2 == KeyState.Up) OnMouseDown(KeyState.Up, mouseArgs[4]); 
            }

            if (OnMouseMove != null && (current.Move.x != 0 || current.Move.y != 0))
                OnMouseMove(WM.MOUSEMOVE, mouseArgsEmpty); 

            if (OnMouseWheel != null && current.Delta != 0)
                OnMouseWheel(WM.MOUSEWHEEL, mouseArgsEmpty);


            if (OnMouseDoubleClick != null) throw new NotImplementedException();

            if (OnMouseStateChanged != null)
            {
                if (current.Left != KeyState.None) OnMouseStateChanged(current.Left, mouseArgs[0]);
                if (current.Right != KeyState.None) OnMouseStateChanged(current.Right, mouseArgs[1]);
                if (current.Middle != KeyState.None) OnMouseStateChanged(current.Middle, mouseArgs[2]);
                if (current.XButton1 != KeyState.None) OnMouseStateChanged(current.XButton1, mouseArgs[3]);
                if (current.XButton2 != KeyState.None) OnMouseStateChanged(current.XButton2, mouseArgs[4]);
            }

            return true;
        }

        protected override void Read()
        {
            previous = current;

            // store previous state
            for (int i = 0; i < BUTTONCOUNT; i++) prevPress[i] = states.Buttons[i];
            int prevX = states.X;
            int prevY = states.Y;
            int prevZ = states.Z;

            // get new state
            mouse.GetCurrentState(ref states);

            // calculate mouse data
            if (isAxisRelative)
            {
                // if relative, the new state is the relative to previous state
                // so X match with movement.
                current.Move.x = states.X;
                current.Move.y = states.Y;
                current.Delta = states.Z;
                current.Position = Cursor.Position;
            }
            else
            {
                // if absolute, the movement must be calculated from previous state
                current.Move.x = states.X - prevX;
                current.Move.y = states.Y - prevY;
                current.Delta = states.Z - prevZ;

                // states.X is the cursor position relative to mouse device zero and 
                // can be any int32 value, so need to get it by window
                current.Position = Cursor.Position;
            }

            // evaluate buttons states
            current.DownButtons = MouseEnum.None;
            current.Left = getStateForButton(0);
            current.Right = getStateForButton(1);
            current.Middle = getStateForButton(2);
            current.XButton1 = getStateForButton(3);
            current.XButton2 = getStateForButton(4);

            if (states.Buttons[0]) current.DownButtons |= MouseEnum.Left;
            if (states.Buttons[1]) current.DownButtons |= MouseEnum.Right;
            if (states.Buttons[2]) current.DownButtons |= MouseEnum.Middle;
            if (states.Buttons[3]) current.DownButtons |= MouseEnum.XButton1;
            if (states.Buttons[4]) current.DownButtons |= MouseEnum.XButton2;


            //byte btn = 1;
            //for (int i = 0; i < BUTTONCOUNT; i++, btn <<= 1)
            //    if (states.Buttons[i]) current.DownButtons |= (MouseEnum)btn;
        }

        /// <summary>
        /// </summary>
        /// <param name="buttonID">mach with MouseKey</param>
        KeyState getStateForButton(int buttonID)
        {
            bool currDown = states.Buttons[buttonID];
            bool prevDown = prevPress[buttonID];

            if (currDown)
            {
                if (!prevDown)
                    return KeyState.Down;
                else
                    return KeyState.Hold;
            }
            else
            {
                if (!prevDown)
                    return KeyState.None;
                else
                    return KeyState.Up;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                mouse.Unacquire();
                if (!mouse.IsDisposed) mouse.Dispose();
                for (int i = 0; i < BUTTONCOUNT; i++) prevPress[i] = false;
            }
        }
    }

}
