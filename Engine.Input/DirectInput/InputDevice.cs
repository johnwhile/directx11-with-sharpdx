﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using SharpDX;
using DX = SharpDX;
using DI = SharpDX.DirectInput;

using Common;

using SharpDX.DirectInput;

namespace Engine.Inputs
{
    [Flags]
    public enum InputUsage : byte
    {
        /// <summary>
        /// Disable acquiring when windows is in background or when focus lost, i don't want interferences with other applications
        /// </summary>
        DefaultKeyboard = Background | NonExclusive,

        /// <summary>
        /// Disable acquiring when windows is not focused, this generate a failure in Acquire()
        /// </summary>
        DefaultMouse = Foreground | NonExclusive,

        //     The application is requesting an exclusive access to the device. If the exclusive
        //     access is authorized, no other instance of the device can get an exclusive
        //     access to the device while it is acquired. Note that non-exclusive access
        //     to the input device is always authorized, even when another application has
        //     an exclusive access. In exclusive mode, an application that acquires the
        //     mouse or keyboard device must unacquire the input device when it receives
        //     a windows event message WM_ENTERSIZEMOVE or WM_ENTERMENULOOP. Otherwise,
        //     the user won't be able to access to the menu or move and resize the window.
        Exclusive = 1,

        /// <summary>
        /// The application is requesting a non-exclusive access to the device. There
        /// is no interference even if another application is using the same device. 
        /// </summary>
        NonExclusive = 2,


        //     The application is requesting a foreground access to the device. If the foreground
        //     access is authorized and the associated window moves to the background, the
        //     device is automatically unacquired.
        Foreground = 4,


        /// <summary>
        /// The application is requesting a background access to the device. If background
        /// access is authorized, the device can be acquired even when the associated
        /// window is not the active window.
        /// </summary>
        Background = 8,

        //     The application is requesting to disable the Windows logo key effect. When
        //     this flag is set, the user cannot perturbate the application. However, when
        //     the default action mapping UI is displayed, the Windows logo key is operating
        //     as long as that UI is opened. Consequently, this flag has no effect in this
        //     situation.
        NoWinKey = 16
    }

    /// <summary>
    /// The initial state of EnableAcquiring is false because acquiring will be done at first update
    /// </summary>
    /// <example>
    /// if (DirectInputDevice.DeviceInputCanWork(this);)
    /// {
    ///     if (!mouse.IsAcquired) mouse.EnableAcquiring();
    ///     if (!mouse.Update()) ;// throw new Exception("CAN?T UPDATE MOUSE DEVICE");
    ///     }
    ///     else
    ///     {
    ///         if (mouse.IsAcquired) mouse.StopAcquiring();
    ///     }
    /// }
    /// </example>
    public abstract class DirectInputDevice : SharpDX.DisposeBase
    {
        protected InputUsage usage;
        protected Form windows;
        protected Control control;
        protected DI.Device device;

        bool acquiring_error = false;
        bool isacquired = false;

        string classtype = "InputDevice";

        // boolean arrays are faster than BitArray or bit operations.
        internal bool[] prevPress, currPress;

        protected DirectInputDevice(Control control, InputUsage usage)
        {
            prevPress = null;
            currPress = null;

            classtype = this.GetType().Name.ToString();
            windows = control.FindForm();
            this.usage = usage;            
            isacquired = false;

            if (usage == (InputUsage.Background | InputUsage.Exclusive))
            {
                throw new NotImplementedException("Sharpdx doesn't want this flag");
            }
        }

        /// <summary>
        /// Read the device's state
        /// </summary>
        /// <returns>return true if success</returns>
        public abstract bool Update();

        /// <summary>
        /// Stop acquiring in all cases
        /// </summary>
        public void StopAcquiring()
        {
            if (isacquired)
            {
                try_unacquire();
                acquiring_error = false;
            }
        }

        /// <summary>
        /// If success, acquiring only once
        /// </summary>
        public void EnableAcquiring()
        {
            if (!isacquired)
            {
                acquiring_error = !try_acquire();
            }
        }



        /// <summary>
        /// TODO: focused form sometime can't acquire ???
        /// If form is not on topmost, deactivated or not visible, the device can't be acquirable, is a good
        /// thing unacquire it when not necessary.
        /// </summary>
        public static bool DeviceCanAcquire(Form application, InputUsage usage)
        {
            if (application.WindowState == FormWindowState.Minimized) return false;

            if ((usage & InputUsage.Background) != 0)
            {
                return true;
            }
            if ((usage & InputUsage.Foreground) != 0)
            {
                if (!application.Focused) return false;
            }

            return application != null && Form.ActiveForm == application;
        }

        /// <summary>
        /// Return the state of device
        /// </summary>
        public bool IsAcquired
        {
            get { return isacquired; }
        }

        public InputUsage InputUsage
        {
            get { return usage; }
        }

        /// <summary>
        /// Call before Update, return false if can't read input device
        /// </summary>
        protected bool TryReadState()
        {
            if (acquiring_error) return false;

            if (!isacquired)
            {
                acquiring_error = !try_acquire();
                isacquired = !acquiring_error;

                if (acquiring_error)
                {
                    try_unacquire();
                    return false;
                }
            }
            if (!try_read()) return false;

            return true;
        }

        /// <summary>
        /// </summary>
        void try_unacquire()
        {
            System.Diagnostics.Trace.WriteLine("Try UnAcquire " + classtype);
#if DEBUG
            try
            {
                device.Unacquire();
            }
            catch (DX.SharpDXException ex)
            {
                System.Diagnostics.Trace.WriteLine("Can't " + classtype + " UnAcquire " + ex.Descriptor.Code + "\n" + ex.ToString());
                Console.WriteLine("Can't " + classtype + " UnAcquire " + ex.Descriptor.Code + "\n" + ex.ToString());
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Can't " + classtype + " UnAcquire " + ex.Message);
                Console.WriteLine("Can't " + classtype + " UnAcquire " + ex.Message);
            }
#else
            device.Unacquire();
#endif
            isacquired = false;
        }

        /// <summary>
        /// I can't find a way to know if mouse or keyboard is acquirable, just use try catch
        /// </summary>
        bool try_acquire()
        {
            
#if DEBUG
            try
            {
                System.Diagnostics.Trace.WriteLine("Try Acquire " + classtype);
                device.Acquire();
                isacquired = true;
            }
            catch (DX.SharpDXException ex)
            {
                System.Diagnostics.Trace.WriteLine("Can't " + classtype + " Acquire " + ex.Descriptor.Code + "\n" + ex.ToString());
                Console.WriteLine("Can't " + classtype + " Acquire " + ex.Descriptor.Code + "\n" + ex.ToString());
                isacquired = false;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Can't " + classtype + " Acquire " + ex.Message);
                Console.WriteLine("Can't " + classtype + " Acquire " + ex.Message);
                isacquired = false;
            }

            return isacquired;

#else
            device.Acquire();
            if (device!=null) device.Poll();
            isacquired = true;
            return true;
#endif
        }

        /// <summary>
        /// try to get input device state, if can't, is possible you forget to acquire with "try_acquire()"
        /// </summary>
        bool try_read()
        {
#if DEBUG
            try
            {
                Read();
                return true;
            }
            catch (DX.SharpDXException ex)
            {
                System.Diagnostics.Trace.WriteLine("Can't " + classtype + " Read State " + ex.ResultCode.ToString());
                Console.WriteLine("Can't " + classtype + " Read State " + ex.ResultCode.ToString());
                return false;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Can't " + classtype + " Read State " + ex.Message);
                Console.WriteLine("Can't " + classtype + " Read State " + ex.Message);
                return false;
            }
#else
            Read();
            return true;
#endif
        }

        protected abstract void Read();

        ~DirectInputDevice() { Dispose(); }

    }

    /// <summary>
    /// A generic input device manage in the same way all time of button (from mouse, keyboard, joystick, etc...)
    /// </summary>
    /// <typeparam name="T">the type of button</typeparam>
    //public abstract class DirectInputDevice<T> : DirectInputDevice
    //{
    //    protected int BUTTONCOUNT = 0;
    //    protected T[] buttons;

 
    //    /// <summary>
    //    /// All key pressed, no distinction between KeyDown or KeyHold 
    //    /// </summary>
    //    public event DxButtonEventHandler<T> OnKeyPress;
    //    /// <summary>
    //    /// Occurs when keyboard button is been pressed first time
    //    /// </summary>
    //    public event DxButtonEventHandler<T> OnKeyDown;
    //    /// <summary>
    //    /// Occurs when keyboard button is been release after pressed.
    //    /// </summary>
    //    public event DxButtonEventHandler<T> OnKeyUp;
    //    /// <summary>
    //    /// Occurs when keyboard button continue to be pressed.
    //    /// </summary>
    //    public event DxButtonEventHandler<T> OnKeyHold;

    //    /// <summary>
    //    /// i don't know that is DI.DirectInput, but is always better initialize only once the same class
    //    /// </summary>
    //    protected DirectInputDevice(Control control, InputUsage usage, int numofbuttons)
    //        : base(control, usage)
    //    {
    //        BUTTONCOUNT = numofbuttons;
    //        prevPress = new bool[BUTTONCOUNT];
    //        currPress = new bool[BUTTONCOUNT];
    //        buttons = new T[BUTTONCOUNT];
    //    }

       

    //    /// <summary>
    //    /// Read the keyboard's device states, a elapsed time in ms was used to understand
    //    /// the time passed between two frames, example to get multi-key press (work in progress)
    //    /// </summary>
    //    public bool UpdateState(double elapsed = 0.0)
    //    {
    //        if (!Update()) return false;

    //        if (OnKeyPress != null)
    //            for (int i = 0; i < BUTTONCOUNT; i++)
    //                if (buttons[i].IsPressed) OnKeyPress(this, buttons[i]);

    //        if (OnKeyDown != null)
    //            for (int i = 0; i < BUTTONCOUNT; i++)
    //                if (buttons[i].IsFirstTimeDown) OnKeyDown(this, buttons[i]);

    //        if (OnKeyUp != null)
    //            for (int i = 0; i < BUTTONCOUNT; i++)
    //                if (buttons[i].IsFirstTimeUp) OnKeyUp(this, buttons[i]);

    //        if (OnKeyHold != null)
    //            for (int i = 0; i < BUTTONCOUNT; i++)
    //                if (buttons[i].IsHolding) OnKeyHold(this, buttons[i]);

    //        return true;
    //    }
    //}
}
