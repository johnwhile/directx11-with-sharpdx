﻿//#define DEBUG_


using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using DX = SharpDX;
using DI = SharpDX.DirectInput;


namespace Engine.Inputs
{

    ///// <summary>
    ///// Very efficient input than using windows.forms.onkeydown events
    ///// </summary>
    //public class KeyboardDevice : DirectInputDevice<KeyButtonEventArg>
    //{
    //    // the keyboard device
    //    DI.Keyboard keyboard;
    //    DI.KeyboardState states;


    //    /// <summary>
    //    /// i don't know that is DI.DirectInput, but is always better initialize only once the same class
    //    /// </summary>
    //    /// <param name="mainwindow">SharpDX require Form handle otherwise crash</param>
    //    internal KeyboardDevice(Form mainwindow, DI.DirectInput dxi, InputUsage usage = InputUsage.DefaultKeyboard)
    //        : base(mainwindow, usage, 237)
    //    {
    //        // preinitializing the unique class to avoid unnecessary new()
    //        for (int i = 0; i < BUTTONCOUNT; i++)
    //            base.buttons[i] = new KeyButtonEventArg(this, (DIKeys)i);

    //        device = keyboard = new DI.Keyboard(dxi);

    //        keyboard.SetCooperativeLevel(mainwindow.Handle, (DI.CooperativeLevel)usage);
    //        states = new DI.KeyboardState();
    //    }

    //    /// <summary>
    //    /// MainFocusWindow define the focus state of main windows when use NonExclusive flag
    //    /// </summary>
    //    /// <param name="mainwindow">SharpDX require Form handle otherwise crash</param>
    //    public KeyboardDevice(Form mainwindow, InputUsage usage = InputUsage.DefaultKeyboard)
    //        : this(mainwindow, new DI.DirectInput(), usage) { }


    //    /// <summary>
    //    /// Read the keyboard's device states
    //    /// </summary>
    //    public override bool Update()
    //    {
    //        if (!TryReadState()) return false;

    //        // update all pressed keys before do events
    //        for (int i = 0; i < BUTTONCOUNT; i++)
    //        {
    //            prevPress[i] = currPress[i];
    //            currPress[i] = states.IsPressed((DI.Key)i);
    //        }

    //        return true;
    //    }

    //    /// <summary>
    //    /// Return informations about keyboard button at current state
    //    /// </summary>
    //    public KeyButtonEventArg GetButtonState(DIKeys button)
    //    {
    //        return buttons[(int)button];
    //    }


    //    protected override void UnAcquire()
    //    {
    //        keyboard.Unacquire();
    //    }

    //    protected override void Acquire()
    //    {
    //        keyboard.Acquire();
    //    }

    //    protected override void Read()
    //    {
    //        keyboard.GetCurrentState(ref states);
    //    }


    //    protected override void Dispose(bool disposing)
    //    {
    //        if (!IsDisposed)
    //        {
    //            keyboard.Unacquire();
    //            if (!keyboard.IsDisposed) keyboard.Dispose();
    //            for (int i = 0; i < BUTTONCOUNT; i++) prevPress[i] = false;
    //        }
    //    }
    //}
}
