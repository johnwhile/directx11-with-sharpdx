﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using DX = SharpDX;
using DI = SharpDX.DirectInput;


namespace Engine
{

    ///// <summary>
    ///// Menage the keyboard and mouse device using DirecInput
    ///// </summary>
    //public class MyDirectInputs
    //{
    //    DI.DirectInput directinput;

    //    Form window;
    //    Control control;

    //    public readonly KeyboardDevice keyboard;
    //    public readonly DirectMouse mouse;

    //    /// <summary>
    //    /// The control must be Focused to output inputs
    //    /// </summary>
    //    public MyDirectInputs(Control control)
    //    {
    //        this.control = control;
    //        this.window = control.FindForm();
    //        directinput = new DI.DirectInput();

    //        InputUsage usage = InputUsage.Background | InputUsage.NonExclusive;

    //        keyboard = new KeyboardDevice(window, directinput, usage);
    //        mouse = new DirectMouse(window, directinput, usage, DI.DeviceAxisMode.Relative);
    //    }


    //    bool update(bool focused, double elapsed)
    //    {
    //        System.Diagnostics.Trace.WriteLine("INPUTS UPDATE");
    //        System.Diagnostics.Trace.Indent();

    //        // acquire and unacquire can be done here because i suppose 
    //        // doesn't change many time during run of program

    //        keyboard.CanAcquire = focused;
    //        mouse.CanAcquire = focused;

    //        if (focused)
    //        {
    //            if (mouse.Update() && keyboard.UpdateState(elapsed))
    //            {
    //                System.Diagnostics.Trace.Unindent();
    //                return true;
    //            }
    //            else
    //            {
                   
    //            }
    //        }
    //        else
    //        {
    //            System.Diagnostics.Trace.WriteLine("DirectInput can't acquire because app not focused");
    //        }
    //        System.Diagnostics.Trace.Unindent();
    //        return false;
    //    }

    //    /// <summary>
    //    /// Control the update frequency, return true is all is ok, false is inputmanager not read the device state
    //    /// </summary>
    //    /// <param name="elapsed">NOT IMPLEMENTED, will be used example for double clic</param>
    //    public bool Update(double elapsed = 0.0)
    //    {
    //        return update(DirectInputDevice.DeviceInputCanWork(window), elapsed);
    //    }

    //    public bool Update(MyForm mainApp, double elapsed = 0.0)
    //    {
    //        System.Diagnostics.Trace.WriteLine("INPUTS UPDATE");
    //        System.Diagnostics.Trace.Indent();

    //        bool focused = mainApp.CanReceiveInputs;

    //        if (!focused)
    //        {
    //            keyboard.CanAcquire = false;
    //            mouse.CanAcquire = false;

    //            System.Diagnostics.Trace.WriteLine("DirectInput can't acquire because app not focused");
    //            System.Diagnostics.Trace.WriteLine(mainApp.ToString());
    //            System.Diagnostics.Trace.Unindent();
    //            return false;
    //        }
    //        else
    //        {
    //            keyboard.CanAcquire = true;
    //            mouse.CanAcquire = true;

    //            if (mouse.Update() && keyboard.UpdateState(elapsed))
    //            {
    //                System.Diagnostics.Trace.Unindent();
    //                return true;
    //            }
    //            else
    //            {

    //            }
    //            System.Diagnostics.Trace.Unindent();
    //            return false;
    //        }
    //    }
        
    //    public bool Update(Form mainApp, double elapsed = 0.0)
    //    {
    //        return update(mainApp.TopMost & DirectInputDevice.DeviceInputCanWork(mainApp), elapsed);
    //    }
    //}
}
