﻿
using System;
using System.Collections.Generic;
using System.Text;

using Common.Inputs;

namespace Engine.Inputs
{
    public abstract class InputManager
    {
        IntPtr window;

        public readonly IMouse mouse;
        public readonly IKeyboard keyboard;
        
        
        public InputManager(IMouse mouse, IKeyboard keyboard, IntPtr window)
        {
            this.window = window;
            this.mouse = mouse;
            this.keyboard = keyboard;
        }

        public void Update()
        {
            mouse.Update();
            keyboard.Update();
        }

    }
}
