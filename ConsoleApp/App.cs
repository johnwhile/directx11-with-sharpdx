﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Engine;
using SharpDX.DXGI;

namespace ConsoleApp
{
    public class App : GameApplication
    {
        public App(Control control, bool vsync, bool startFullscreen) : base()
        {
        }

        protected override void Update(GameTime time)
        {
            Windows[0].Control.Text = "FPS " + GameTime.PerformanceInfo.FPS.ToString("0.0");
        }
    }
}
