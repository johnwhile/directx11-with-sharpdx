﻿using System;
using System.Windows.Forms;
using System.Diagnostics;

using Common;
using Engine;

using SharpDX;
using SharpDX.DXGI;
using SharpDX.Direct3D11;
using SharpDX.Mathematics;
using Engine.Direct2D;
using Common.Maths;

namespace ConsoleApp
{ 
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*
            using (var form = new TestForm())
            {
                form.Size = new System.Drawing.Size(800, 600);
                form.Text = "TestForm";
                form.Show();
                form.InitializeGraphics();
                Application.Run(form);
            }
            */

            using (var form = new TestForm())
            {
                form.Size = new System.Drawing.Size(800, 600);
                form.Text = "TestForm";
                form.Show();
                form.splitContainer1.Dispose();
                form.splitContainer1 = null;
                form.BackColor = Color4b.CornflowerBlue;

                using (var app = new GameTest(form))
                {
                    int fps = 20;
                    app.TargetElapsedTime = TimeSpan.FromMilliseconds(1000.0 / fps);
                    app.RunLoop();
                }
            }
        }
    }

    public class GameTest : GameApplication
    {
        D2Font font;
        Vector2f point;

        public GameTest(Control control) : base()
        {
            font = D2Font.GetFont("Calibri");

            var window1 = new GameWindow(this, control);

            AddOutputWindow(window1);

            window1.Drawing += Window1_Drawing;
        }

        private void Window1_Drawing(SwapChainPresenter presenter)
        {
            var context2d = Manager.Device2D.Context;

            if (presenter.BeginDraw2D(context2d))
            {
                context2d.DrawText(font, "DrawString Panel", Color4b.Black, (int)point.x, (int)point.y);
                //FirstPresenter.EndDraw2D();
            }
        }

        protected override void Update(GameTime time)
        {
            float ms = (float)time.ElapsedTime;
            ms /= 100;
            point += new Vector2f(ms * 2, ms * 4);
        }
    }
}
