﻿using System;
using System.Windows.Forms;

using Engine;
using Engine.Direct2D;

using Common;
using Common.Maths;

namespace ConsoleApp
{
    public class TestForm : Form
    {
        public SplitContainer splitContainer1;

        GraphicDeviceManager manager;
        D2Font font;

        public TestForm()
        {
            InitializeComponent();
        }

        public void InitializeGraphics()
        {
            manager = new GraphicDeviceManager();
            var presenter = manager.CreatePresenter(splitContainer1.Panel1);

            font = D2Font.GetFont("Calibri");

            splitContainer1.Panel1.ClientSizeChanged += delegate(object sender, EventArgs args)
            {
                presenter.Resize(splitContainer1.Panel1.ClientSize);
            };

            splitContainer1.Panel1.Paint += delegate (object sender, PaintEventArgs e)
            {
                if (presenter.BeginDraw(manager.Device3D.Context))
                {
                    presenter.Clear(Color4b.CornflowerBlue);

                    if (manager.Device2D.Context.BeginDraw())
                    {
                        manager.Device2D.Context.DrawText(font, "DrawString Panel", Color4b.Black, 10, 10);
                        manager.Device2D.Context.EndDraw();
                    }

                    presenter.EndDrawsAndPresent();
                }
            };
        }

        protected override void Dispose(bool disposing)
        {
            Debugg.Warning("DISPOSING FORM");
            manager?.Dispose();
            base.Dispose(disposing);
        }


        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitContainer1.Size = new System.Drawing.Size(839, 491);
            this.splitContainer1.SplitterDistance = 407;
            this.splitContainer1.TabIndex = 0;
            // 
            // FormApp
            // 
            this.ClientSize = new System.Drawing.Size(839, 491);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FormApp";
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
        }
    }


}
