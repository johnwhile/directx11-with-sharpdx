﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Engine;
using Engine.Direct2D;

using Common;
using Common.Fonts;
using Common.Maths;

namespace Engine
{
    public partial class DistantFieldEditor : Form
    {
        public SwapChainPresenter presenter;
        GraphicDeviceManager manager;


        public DistantFieldEditor()
        {
            InitializeComponent();
            Text = "DistantFieldEditor";
        }
        public void InitGraphics(GraphicDeviceManager manager)
        {
            presenter = manager.CreatePresenter(this);
            this.manager = manager;
            ResizeEnd += ResizePresent;
            FormClosing += Close;

        }

        private void ResizePresent(object sender, EventArgs e)
        {
            presenter.Resize(ClientSize);
            Invalidate();
        }

        private void Close(object sender, FormClosingEventArgs e)
        {
            manager.RemoveSwapChain(presenter);
        }
    }
}
