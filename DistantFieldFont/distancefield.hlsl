﻿Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);

cbuffer Globals : register(b0)
{
    //0.04
    float smoothing;
};

/* draw a big triangle with vertex [-1,1] [3,1] [-1,-3]
topology = TriangleList
Draw(3)
*/
//output values must match with pixelshader's inputs
void VS_screenquad(in uint VertID : SV_VertexID, out float4 Pos : SV_Position, out float2 Tex : TEXCOORD0)
{
    // Texture coordinates range [0, 2], but only [0, 1] appears on screen.
    Tex = float2((VertID << 1) & 2, VertID & 2);
    Pos = float4(lerp(float2(-1, 1), float2(1, -1), Tex), 0, 1);
    //Pos = float4(Tex * float2(2, -2) + float2(-1, 1), 0, 1);
}


float4 PS(float4 position : SV_Position, float2 texcoord : TEXCOORD0) : SV_Target
{
    //distance 
    // 0.0 = far away from the letter
    // 0.5 = right on the edge
    // 1.0 = well inside it
    float distance = Texture.Sample(Sampler, texcoord).r;
    //gives a smooth transition around 0.5 to provide antialiasing, the right smoothing value for crisp fonts is 0.25f / (spread * scale)
    //where spread is the value you used when generating the font and scale is the scale you’re drawing it at
    float alpha = smoothstep(0.5 - smoothing, 0.5 + smoothing, distance);
    return float4(0,0,0, alpha);
}