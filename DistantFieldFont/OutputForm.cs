﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common.Maths;
using Engine.Direct2D;

namespace Engine
{
    public partial class OutputForm : Form
    {
        public SwapChainPresenter presenter;
        GraphicDeviceManager manager;
        public DistantFieldPipeline pipeline;

        public float Value;

        public OutputForm(int width, int height)
        {
            Text = "OutputForm";
            InitializeComponent();
            ClientSize = new Size(width, height);

            trackBar1.Value = 3;
            trackBar1.ValueChanged += TrackBar1_ValueChanged;
        }

        private void TrackBar1_ValueChanged(object sender, EventArgs e)
        {
            Value = (float)trackBar1.Value / trackBar1.Maximum;
            Debug.Print(Value.ToString());
            Invalidate();
        }

        public void InitGraphics(GraphicDeviceManager manager)
        {
            presenter = manager.CreatePresenter(this);
            this.manager = manager;
            ResizeEnd += ResizePresent;
            FormClosing += Close;
            pipeline = new DistantFieldPipeline(manager.Device3D);
 

        }

        private void ResizePresent(object sender, EventArgs e)
        {
            presenter.Resize(ClientSize);
            Invalidate();
        }

        private void Close(object sender, FormClosingEventArgs e)
        {
            manager.RemoveSwapChain(presenter);
        }
    }
}
