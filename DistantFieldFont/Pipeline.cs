﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    public class DistantFieldPipeline : Pipeline
    {
        public ConstantBuffer<float> setting;
        public Texture2 texture;


        public DistantFieldPipeline(D3Device device) : base(device)
        {
            //string filename = AppConfig.ContentPath + "\\Shaders\\distancefield.hlsl";
            string filename = "distancefield.hlsl";

            pixelshader = device.PixelShaders.CreateOrGet(filename, "PS");
            vertexshader = device.VertexShaders.CreateOrGet(filename, "VS_screenquad");

            setting = ConstantBuffer<float>.Create(Device);
        }

        public override void SetToContext(D3Context context)
        {
            context.SetRenderState(blend);
            context.SetRenderState(depthstencil);
            context.SetRenderState(rasterizer);
            context.SetRenderState(inputLayout);

            context.SetShader(pixelshader);
            context.SetToPixelStage(sampler);
            context.SetToPixelStage(texture);

            context.SetShader(vertexshader);
            context.SetShader(pixelshader);

            context.SetToPixelStage(setting);

            context.PrimitiveTopology = DxPrimitive.TriangleList;
        }


    }
}
