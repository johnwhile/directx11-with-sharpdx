﻿using Common.Fonts;
using Common.Maths;
using Engine.Direct2D;
using System;
using System.Windows.Forms;

namespace Engine
{
    internal static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (var manager = new GraphicDeviceManager())
            {
                var form1 = new DistantFieldEditor();
                form1.Show();
                form1.ClientSize = new System.Drawing.Size(800, 600);
                form1.InitGraphics(manager);


                FontDictionary glyphs = FontDictionary.GetOrLoad("Arial", AppConfig.ContentPath + "\\Fonts\\Arial_distancefield.fnt");
                var image = D2Bitmap.New(manager.Device2D.Context, glyphs.Texture);

                D2Font calibri = D2Font.GetFont("Calibri", 20);

                form1.Paint += delegate (object sender, PaintEventArgs args)
                {
                    var context = manager.Device3D.Context;
                    var presenter = form1.presenter;

                    if (presenter.BeginDraw(context))
                    {
                        presenter.Clear(Color4b.CornflowerBlue);

                        var d2context = manager.Device2D.Context;
                        var viewport = d2context.Viewport;

                        if (presenter.BeginDraw2D(d2context))
                        {
                            d2context.DrawImage(image, viewport, null);
                            //d2context.DrawText(font, Text, Vector4b.Black, ClientSize.Width / 2, ClientSize.Height / 2);
                            //d2context.EndDraw();
                            //presenter.EndDraw2D();
                        }

                        presenter.EndDrawsAndPresent();
                    }
                };


                var form2 = new OutputForm(256, 256);
                form1.AddOwnedForm(form2);
                form2.Show();
                form2.InitGraphics(manager);

                form2.pipeline.texture = Texture2.New(manager.Device3D, glyphs.Texture);

                form2.Paint += delegate (object sender, PaintEventArgs args)
                {
                    var context = manager.Device3D.Context;
                    var presenter = form2.presenter;
                    var pipeline = form2.pipeline;



                    var d2context = manager.Device2D.Context;
                    if (presenter.BeginDraw2D(d2context))
                    {
                        d2context.DrawText(calibri, "Test", Color4b.Green, presenter.DefaultViewport.Size.width / 2, presenter.DefaultViewport.Size.height / 2);
                        //d2context.EndDraw();
                        //presenter.EndDraw2D();
                    }


                    if (presenter.BeginDraw(context))
                    {
                        presenter.Clear(Color4b.Blue);

                        pipeline.setting.Data = form2.Value;
                        
                        pipeline.SetToContext(context);

                        context.Draw(3);

                        presenter.EndDrawsAndPresent();
                    }
                };



                Application.Run(form1);

                form2.Close();
                form1.Close();

                //fontsprite = new FontSprite(manager.Device3D, "Arial_distancefield");
            }
        }
    }
}
