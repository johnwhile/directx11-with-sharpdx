#define WHITE float4(1,1,1,1);
#define BLACK float4(0,0,0,1);
#define BLUE  float4(0,1,0,1);

float4x4 WorldViewProj;


float3 GetEyeFromCamera(float4x4 matrice)
{
	return float3(matrice._m30,matrice._m31,matrice._m32);
}

float3 GetTrueNormal(float3 normal , float3x3 worldinversetraspose )
{
	return normalize(mul(normal, worldinversetraspose));
}
