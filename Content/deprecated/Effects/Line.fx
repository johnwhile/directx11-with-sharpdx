//-----------------------------------------------------------------------------
// Basic Shader program for line, require only position and color
//-----------------------------------------------------------------------------

#include "baseshader.fx"

float4 VertexColor = BLACK;

struct VS_OUTPUT
{   
    float4  Position : POSITION;
	float4  Color    : COLOR0;
};

VS_OUTPUT VShader(float4 Position : POSITION )
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	output.Position = mul(Position, WorldViewProj);
	output.Color = VertexColor;
	return output;
}

VS_OUTPUT VShaderColor(float4 Position : POSITION , float4 Color : COLOR)
{
	VS_OUTPUT output = (VS_OUTPUT)0; 

	output.Position = mul(Position, WorldViewProj);
	output.Color = Color;
	return output;
}


float4 PShader(float4 Color : COLOR) : COLOR0
{
	return Color;
}

//-----------------------------------------------------------------------------
// Techniques.
//-----------------------------------------------------------------------------
technique TechniqueColor
{
    pass p0
    {
        VertexShader = compile vs_2_0 VShader();
		PixelShader =  compile ps_2_0 PShader();
    }
}
technique TechniqueVertexColor
{
	pass p0
    {
        VertexShader = compile vs_2_0 VShaderColor();
		PixelShader =  compile ps_2_0 PShader();
    }
}