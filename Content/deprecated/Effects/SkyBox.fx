float4x4 WorldViewProj;
 
Texture SkyBoxTexture;

samplerCUBE SkyBoxSampler = sampler_state 
{ 
   texture = <SkyBoxTexture>; 
   magfilter = LINEAR; 
   minfilter = LINEAR; 
   mipfilter = LINEAR; 
   AddressU = Wrap; 
   AddressV = Wrap; 
   AddressW = Wrap;
};
 
struct VS_INPUT
{
    float4 Position : POSITION0;
};
 
struct VS_OUTPUT
{
	// screen position of pixel
	float4 Position : SV_POSITION;
	// untrasformed vertex position (but interpolated in the triangle of course)
    float4 SkyPosition  : TEXCOORD0;
};
 
VS_OUTPUT VShader_textured(VS_INPUT input)
{
    VS_OUTPUT output;
 
	input.Position.w = 1.0f;
    
	// when pixelshader make homogeneus multiplication, z/w = 1, depth will be always 1.
	// this eliminates the projection matrix optimization because force deph = 1 also when greater 
	// than far or smaller than near planes (z>1 or z<0)
	output.Position = mul(input.Position, WorldViewProj);
	output.Position.z = output.Position.w;
	
	// the untrasformed position was used to get the cubemap value
	output.SkyPosition = input.Position;

    return output;
}
 

VS_OUTPUT VShader_white(VS_INPUT input)
{
    VS_OUTPUT output;
	input.Position.w = 1.0f;
	output.Position = mul(input.Position, WorldViewProj);
	output.Position.z = output.Position.w;
	output.SkyPosition = input.Position;
    return output;
}


float4 PShader_textured(VS_OUTPUT input) : COLOR0
{
	// input.SkyPosition requre a normalization ???
    return texCUBE(SkyBoxSampler, input.SkyPosition);
}

float4 PShader_white(VS_OUTPUT input) : COLOR0
{
    return float4(1,1,1,1);
}


technique SkyBoxSolid
{
    pass Pass0
    {
        VertexShader = compile vs_2_0 VShader_textured();
        PixelShader = compile ps_2_0 PShader_textured();
    }
}

technique SkyBoxSolidWireframe
{
	pass Pass0
    {
        VertexShader = compile vs_2_0 VShader_textured();
        PixelShader = compile ps_2_0 PShader_textured();
    }
    pass Pass1
    {
        VertexShader = compile vs_2_0 VShader_white();
        PixelShader = compile ps_2_0 PShader_white();
    }
}