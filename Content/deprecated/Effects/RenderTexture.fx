
texture Texture0;
texture Texture1;

sampler TextureSampler0 = sampler_state 
{ 
	texture = <Texture0>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter = LINEAR; 
	AddressU  = CLAMP; 
	AddressV  = CLAMP;
};
sampler TextureSampler1 = sampler_state 
{ 
	texture = <Texture1>; 
	magfilter = LINEAR; 
	minfilter = LINEAR; 
	mipfilter = LINEAR; 
	AddressU  = CLAMP; 
	AddressV  = CLAMP;
};
//-----------------------------------------------------------------------------
// Basic Shader program to render one (or more) texture to the backbuffer
// (or in general the first render targhet).
// the VertexStream is a simple quad of positions
/*
new Vector2(0, 0);
new Vector2(1, 0);
new Vector2(1, 1);
new Vector2(0, 0);
new Vector2(1, 1);
new Vector2(0, 1);
*/
//----------------------------------------------------------------------------

struct VS_OUTPUT
{   
	float4 Position : POSITION;
	float2 TexCoord : TEXCOORD;
};

// is better use POSITION or TEXCOORD ?
VS_OUTPUT VShaderQuad(float2 inPosition : POSITION)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	// remeber to split y coordinate because texture use topleft corner and you have to convert into view coordinate
	// where z is the depth and the center x,y of quad are in origin x=0 y=0
	output.Position = float4(inPosition.x * 2.0f - 1.0f, 1.0f - inPosition.y * 2.0f , 0 , 1);
	output.TexCoord = inPosition;
	return output;
}

float4 PShaderQuad(float2  inTexCoord  : TEXCOORD) : COLOR0
{
	return tex2D(TextureSampler0, inTexCoord);
}


// World with Quad geometry
// need to disable zbuffer, is not necessary
technique TechniqueRenderTexture
{
    pass p0
    {
        VertexShader = compile vs_2_0 VShaderQuad();
		PixelShader =  compile ps_2_0 PShaderQuad();
		CullMode = CCW;
		ZEnable = false;
    }
}
// World with SpriteBatch
technique TechniqueRenderTexture2
{
    pass p0
    {
		PixelShader =  compile ps_2_0 PShaderQuad();
		CullMode = None;
    }
}