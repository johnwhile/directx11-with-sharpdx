﻿#include "common.hlsl"

Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);

//vertex shader globals
cbuffer VsGlobals : register(b2)
{
    //[xoffset, yoffset, width, height]
    int4 viewport;
    int2 origin;
    int2 texturesize;
    uint color;
    float scale;
};

//pixel shader globals
cbuffer PsGlobals : register(b3)
{
    float smoothing;
};

//------------------------------------------------------------------
//                      GLYPH MAP
//------------------------------------------------------------------
struct GlyphStruct
{
    int4 source; //texcoord of font texture
    int2 offset; //not used
};

StructuredBuffer<GlyphStruct> Glyphs : register(t1);

//------------------------------------------------------------------
//                      INPUT-OUTPUT STRUCTS
//------------------------------------------------------------------
//each point rappresent a sprite quad
struct VSinput
{
    int2 position : ANCHOR;
    int source : INDEX;
};

struct GSinput
{
    // destination is in uniform clip space [-1,1]
    float4 Destination : POSITION;
    // texture in UV coord. [0,1]
    float4 TexCoord : TEXCOORD0;
    float4 Color : COLOR;
};

struct PSinput
{
    float4 Position : SV_Position;
    float2 TexCoord : TEXCOORD0;
    float4 Color : COLOR;
};

//------------------------------------------------------------------
//                      VERTEX SHADER
//------------------------------------------------------------------
GSinput VS_main(VSinput input)
{
    GSinput output = (GSinput) 0;
    
    //source rectangle of sprite in the texture
    float4 src = Glyphs[input.source].source;
    float2 offset = Glyphs[input.source].offset;
    
    //dest rectangle is in pixel, convert to clip space
    float4 dest = float4(input.position * scale + origin, src.zw * scale);
    output.Destination = PixelToClipSpace(dest, viewport.zw);
    
    //convert source rectangle in [0,1] coords
    output.TexCoord = src / texturesize.xyxy;
    output.Color = Unpack32bitColor(color);
    return output;
}

//output as rectangle using vs_globals
GSinput VS_debug()
{
    GSinput output = (GSinput) 0;
    
    //dest rectangle is in pixel, convert to clip space
    float4 dest = float4(origin, texturesize * scale);
    output.Destination = PixelToClipSpace(dest, viewport.zw);
    output.TexCoord = float4(0, 0, 0, 0);
    output.Color = Unpack32bitColor(color);
    return output;
}

//------------------------------------------------------------------
//                      GEOMETRY SHADER
//------------------------------------------------------------------
// trianglestrip [012][213]
/*
 v0___v1
  |  /|
  | / |
v2|/__|v3
*/
[maxvertexcount(4)]
void GS_quad(point GSinput vertices[1], inout TriangleStream<PSinput> outStream)
{
    PSinput v = (PSinput) 0;
    GSinput p = vertices[0];

    float2 TL = p.Destination.xy;
    float2 BR = float2(TL.x + p.Destination.z, TL.y - p.Destination.w);
    float2 TLuv = p.TexCoord.xy;
    float2 BRuv = p.TexCoord.xy + p.TexCoord.zw;
    
    v.Position = float4(0, 0, 0, 1);
    v.Color = p.Color;
    
    //v0 top-left
    v.Position.xy = TL;
    v.TexCoord = TLuv;
    outStream.Append(v);
    
    //v1 top-right
    v.Position.xy = float2(BR.x, TL.y);
    v.TexCoord = float2(BRuv.x, TLuv.y);
    outStream.Append(v);
  
    //v2 bottom-left
    v.Position.xy = float2(TL.x, BR.y);
    v.TexCoord = float2(TLuv.x, BRuv.y);
    outStream.Append(v);
    
    //v3 bottom-right
    v.Position.xy = BR;
    v.TexCoord = BRuv;
    outStream.Append(v);
}
//------------------------------------------------------------------
//                      PIXEL SHADER
//------------------------------------------------------------------
float4 PS_debug(PSinput input) : SV_Target
{
    return input.Color;
}

//Raw output with interpolation.
float4 PS_raw(PSinput input) : SV_Target
{
    return float4(input.Color.rgb, Texture.Sample(Sampler, input.TexCoord).r);
}
float4 PS_raw_alpha(PSinput input) : SV_Target
{
    return float4(input.Color.rgb, Texture.Sample(Sampler, input.TexCoord).a);
}
//distance field font method (black-white texture)
float4 PS_df_smooth(PSinput input) : SV_Target
{
    //distance 
    // 0.0 = far away from the letter
    // 0.5 = right on the edge
    // 1.0 = well inside it
    float distance = Texture.Sample(Sampler, input.TexCoord).r;
    //gives a smooth transition around 0.5 to provide antialiasing, the right smoothing value for crisp fonts is 0.25f / (spread * scale)
    //where spread is the value you used when generating the font and scale is the scale you’re drawing it at
    float alpha = smoothstep(0.5 - smoothing, 0.5 + smoothing, distance);
    
    return float4(input.Color.rgb, input.Color.a * alpha);
}
//distance field font method (texture with alpha channel)
float4 PS_df_smooth_alpha(PSinput input) : SV_Target
{
    float distance = Texture.Sample(Sampler, input.TexCoord).a;
    float alpha = smoothstep(0.5 - smoothing, 0.5 + smoothing, distance);
    return float4(input.Color.rgb, input.Color.a * alpha);
}