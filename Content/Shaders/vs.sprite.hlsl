﻿
// Constant buffer to be updated by application per object
cbuffer PerObject : register(b0)
{
    float2 location;
    float scale;

};

struct VS_Input
{
    int2 Position : POSITION0;
    float4 Vertex : TEXCOORD0;
    float4 TexCoord : TEXCOORD1;
};


struct VS_Output
{
    float4 Position : SV_Position;
    float2 TextCoord : TEXCOORD;
};

VS_Output VS_SpriteOld(VS_Input input)
{
    VS_Output output = (VS_Output) 0;
    
    //build quad vertices
    output.Position = float4(0, 0, 0, 1);  
    output.Position.x = input.Position.x * input.Vertex.x + input.Position.x * input.Vertex.z;
    output.Position.y = input.Position.y * input.Vertex.y + input.Position.y * input.Vertex.w;
    
    
    output.TextCoord = input.TexCoord;
    return output;
}

VS_Output VS_Sprite(VS_Input input, uint VID : SV_VertexID)
{
    VS_Output output = (VS_Output) 0;

    uint n = VID;
    
    if (n==0)
    {
        output.Position = float4(input.Vertex.x, input.Vertex.y, 0, 1);
    }
    else if (n==1)
    {
        output.Position = float4(input.Vertex.z, input.Vertex.y, 0, 1);
    }
    else if (n==2)
    {
        output.Position = float4(input.Vertex.x, input.Vertex.w, 0, 1);
    }
    else if (n==3)
    {
        output.Position = float4(input.Vertex.z, input.Vertex.w, 0, 1);
    }
    
   
    output.TextCoord = input.TexCoord;
    
    return output;
}