﻿#include "..\common.hlsl"


cbuffer PerObject : register(b0)
{
    Transform transform;
};

cbuffer PerFrame : register(b1)
{
    Ambient ambient;
};



struct PS_Spline_input
{
    float4 Position : SV_Position;
    float4 Color : COLOR;
};


PS_Spline_input VS_Spline(VS_spline_input input, uint VID : SV_VertexID)
{
    PS_Spline_input output = (PS_Spline_input) 0;
    output.Position = mul(float4(input.position, 1), transform.WorldViewProjection);
    output.Color = Unpack32bitColor(input.color);
    return output;
}

float4 PS_Spline(PS_Spline_input input) : SV_Target
{
    return input.Color;
}