﻿// Globals for texture sampling
Texture2D ShaderTexture : register(t0);
SamplerState Sampler : register(s0);


// Sample the pixel color using the sampler and texture
// using the input texture coordinate 
float4 PS_DebugRed(float4 position : SV_Position) : SV_Target
{
    return float4(1, 0, 0, 1);
}

// Sample the pixel color using the sampler and texture
// using the input texture coordinate 
float4 PS_Sprite(float4 position : SV_Position, float2 texcoord : TEXCOORD0) : SV_Target
{
    float4 color = ShaderTexture.Sample(Sampler, texcoord);
    return color;
}

// Sample the pixel color using the sampler and texture
// using the input texture coordinate 
float4 PS_SpriteFont(float4 position : SV_Position, float2 texcoord : TEXCOORD0) : SV_Target
{
    float4 color = ShaderTexture.Sample(Sampler, texcoord);
    
    color.a = color.r;
    if (color.r > 0)
        color.rgb = 1;
    return color;
}


    
