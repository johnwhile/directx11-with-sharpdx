﻿// Globals for texture sampling
Texture2D ShaderTexture : register(t0);
SamplerState Sampler : register(s0);


// A mandatory Pixel Shader
float4 PS_DefaultRed() : SV_Target
{
    return float4(1, 0, 0, 1);
}


float4 PS_Pos2Col_test1(float4 position : SV_Position) : SV_Target
{
    float r = position.x;
    float g = position.y;
    return float4(r, g, 0, 1.0f);
}

float4 PS_Pos2Col(float4 position : SV_Position, float4 color : COLOR) : SV_Target
{
    return color;
}

// Sample the pixel color using the sampler and texture
// using the input texture coordinate 
float4 PS_Texture(float4 position : SV_Position, float2 texcoord : TEXCOORD0) : SV_Target
{
    return ShaderTexture.Sample(Sampler, texcoord);
}


    
