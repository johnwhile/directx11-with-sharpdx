﻿


// Pixel shader input
struct PS_Input
{
    float4 Position : SV_Position;
    float2 UV : TEXCOORD0;
    float Energy : ENERGY;
};

//you can't attach a semantic to a type, only a variable, so it's necessary build a utility struct
// Geometry shader input
struct GS_Input
{
    float4 Position : SV_Position;
    float Radius : RADIUS;
    float Energy : ENERGY;
};

[maxvertexcount(4)]
void GS_PointToQuad(point GS_Input p[1], inout TriangleStream<PS_Input> outStream)
{
    PS_Input v = (PS_Input) 0;
    
    outStream.Append(v);
    outStream.Append(v);
    outStream.Append(v);
    outStream.Append(v);
    outStream.RestartStrip();
}