﻿#include "common.hlsl"


// Globals for texture sampling
Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);


cbuffer SpriteGlobals : register(b0)
{
    //[xoffset, yoffset, width, height]
    float4 viewport;
    float2 texturesize;
};


//each point rappresent a sprite quad
struct VSinput
{
    //topleft corner position relative to string's origin, in pixel relative to viewport
    float4 destination : POSITION;
    float4 source : TEXCOORD;
    //byte4 not supported
    int color : COLOR;
};

//you can't attach a semantic to a type, only a variable, so it's necessary build a utility struct
struct GSinput
{
    float4 destination : POSITION;
    float4 source : TEXCOORD;
    float4 color : COLOR;
};

struct PSinput
{
    float4 Position : SV_Position;
    float2 TexCoord : TEXCOORD0;
    float4 Color : COLOR;
};

//destination and texture coords are in pixel units
GSinput VS_pixel(VSinput input)
{
    GSinput output = (GSinput) 0;
    
    //output.TL = SceenToViewport(input.destination.xy, viewporsize);
    //output.BR = SceenToViewport(input.destination.xy + input.destination.zw, viewporsize);   
    output.source = input.source / texturesize.xyxy;
    output.color = Unpack32bitColor(input.color);
    return output;
}

//destination are in uniform clip space, texture in UV coord
GSinput VS_normalized(VSinput input)
{
    GSinput output = (GSinput) 0;
    
    output.destination = input.destination;
    output.source = input.source;
    output.color = Unpack32bitColor(input.color);
    return output;
}

//destination are in uniform clip space, texture in UV coord.
//converts space into custom viewport
GSinput VS_normalized_viewport(VSinput input)
{
    GSinput output = (GSinput) 0;
    
    output.destination = input.destination;
    output.source = input.source;
    output.color = Unpack32bitColor(input.color);
    return output;
}



/* trianglestrip [012][213]
 v0___v1
  |  /|
  | / |
v2|/__|v3
*/
[maxvertexcount(4)]
void GS_quad(point GSinput vertices[1], inout TriangleStream<PSinput> outStream)
{
    PSinput v = (PSinput) 0;
    GSinput p = vertices[0];

    float2 TL = p.destination.xy;
    float2 BR = float2(TL.x + p.destination.z, TL.y - p.destination.w);
    float2 TLuv = p.source.xy;
    float2 BRuv = p.source.xy + p.source.zw;
    
    v.Position = float4(0, 0, 0, 1);
    v.Color = p.color;
    
    //v0 top-left
    v.Position.xy = TL;
    v.TexCoord = TLuv;
    outStream.Append(v);
    
    //v1 top-right
    v.Position.xy = float2(BR.x, TL.y);
    v.TexCoord = float2(BRuv.x, TLuv.y);
    outStream.Append(v);
  
    //v2 bottom-left
    v.Position.xy = float2(TL.x, BR.y);
    v.TexCoord = float2(TLuv.x, BRuv.y);
    outStream.Append(v);
    
    //v3 bottom-right
    v.Position.xy = BR;
    v.TexCoord = BRuv;
    outStream.Append(v);
}


float4 PS_color(PSinput input) : SV_Target
{
    return input.Color;
}

float4 PS_textured(PSinput input) : SV_Target
{
    float4 color = Texture.Sample(Sampler, input.TexCoord);   
    //color.rgb = input.Color.rgb * color.a;
    return color;
}