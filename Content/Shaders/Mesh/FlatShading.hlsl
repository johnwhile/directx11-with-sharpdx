﻿#include "meshshading.hlsl"


struct PSinputFlat
{
    float4 Position : SV_Position;
    float2 TexCoord : TEXCOORD0;
    float3 Vertex : TEXCOORD1;
    float Light : TEXCOORD2;
    float2 BaricentricCoord : TEXCOORD3;
    
};


PSinputFlat VS_FlatShading(VS_mesh_input input, uint VID : SV_VertexID)
{
    PSinputFlat output = (PSinputFlat) 0;
    
    output.Vertex = input.position;
    output.Position = mul(float4(input.position, 1), transform.WorldViewProjection);
    output.TexCoord = input.texcoord;

    return output;
}

[maxvertexcount(3)]
void GS_FlatShading(triangle PSinputFlat IN[3], inout TriangleStream<PSinputFlat> triStream)
{
    // Compute the normal
    float3 v01 = IN[1].Vertex - IN[0].Vertex;
    float3 v02 = IN[2].Vertex - IN[0].Vertex;
    
    float3 normal = cross(v01, v02);
    normal = mul(normal, (float3x3) transform.WorldInverse);
    normal = normalize(normal);

    // Compute diffuse light
    float diffuse_light = max(0, dot(normal, light.LightDirection));

    // wireframe 
    IN[0].BaricentricCoord = float2(1, 0);
    IN[1].BaricentricCoord = float2(0, 1);
    IN[2].BaricentricCoord = float2(0, 0);
    
    for (int i = 0; i < 3; i++)
    {
        IN[i].Light = diffuse_light;
        triStream.Append(IN[i]);
    }
}

float4 PS_FlatShading_Diffuse_Wireshading2(PSinputFlat input) : SV_Target
{
    float3 barys;
    //rebuild missing z
    barys.xy = input.BaricentricCoord;
    barys.z = 1 - barys.x - barys.y;
    //near edge if one of three components are near zero
    float near = min(barys.x, min(barys.y, barys.z));

    /*
    if (near < 0.01)
        near = 0;
    else
        near = 1;
    */
    near = smoothstep(0, 0.01, near);
    
    return material.Diffuse * near * input.Light;
}



float4 PS_FlatShading_Diffuse_Wireshading(PSinputFlat input) : SV_Target
{
    float3 barys;
    barys.xy = input.BaricentricCoord;
    barys.z = 1 - barys.x - barys.y;
    float minBary = min(barys.x, min(barys.y, barys.z));


    //abs(ddx(minBary)) + abs(ddy(minBary))
    float delta = fwidth(minBary);
    
    //minBary = smoothstep(0, delta, minBary);
    minBary = smoothstep(delta, 2 * delta, minBary);
    
    return material.Diffuse * minBary * input.Light;
}


float4 PS_FlatShading_Diffuse(PSinputFlat input) : SV_Target
{
    // normalize(cross(ddy(input.Vertex), ddx(input.Vertex)));
    
    return material.Diffuse * input.Light;
}

float4 PS_FlatShading_Texture(PSinputFlat input) : SV_Target
{
    return Texture.Sample(Sampler, input.TexCoord) * input.Light;
}