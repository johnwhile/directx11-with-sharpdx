#include "..\common.hlsl"

// Globals for texture sampling
Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);


cbuffer PerObject : register(b0)
{
    Transform transform;
};
cbuffer PerFrame : register(b1)
{
    Ambient ambient;
};

cbuffer PerFrame2 : register(b2)
{
    Light light;
};
cbuffer PerMaterial : register(b3)
{
    Material material;
};