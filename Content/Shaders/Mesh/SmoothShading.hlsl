﻿#include "meshshading.hlsl"


struct PSinputSmooth
{
    float4 Position : SV_Position;
    float2 TexCoord : TEXCOORD;
    float3 WorldNormal : NORMAL;
};


PSinputSmooth VS_SmoothShading(VS_mesh_input input, uint VID : SV_VertexID)
{
    PSinputSmooth output = (PSinputSmooth) 0;
    
    output.Position = mul(float4(input.position, 1), transform.World);
    output.Position = mul(output.Position, ambient.ViewProjection);
    output.TexCoord = input.texcoord;
    output.WorldNormal = mul(input.normal, (float3x3) transform.WorldInverse);
    return output;
}


float4 PS_SmoothShading_Diffuse(PSinputSmooth input) : SV_Target
{
    // Normalize our vectors as they are not 
    // guaranteed to be unit vectors after interpolation
    float3 normal = normalize(input.WorldNormal);
    float3 lightdir = -light.LightDirection;
    
    return Lambert(material.Diffuse, normal, lightdir);
}
float4 PS_SmoothShading_Texture(PSinputSmooth input) : SV_Target
{
    return Texture.Sample(Sampler, input.TexCoord);
}