﻿
// Vertex Shader output structure consisting of the
// transformed position and original color
// This is also the pixel shader input

struct VS_OUT
{
    float4 Position : SV_POSITION;
    float4 Color : COLOR;
};

// At a minimum, the vertex shader must accept a variable with the POSITION semantic
// and must return a variable with the SV_Position semantic.
float4 VS_Default(float4 position : POSITION) : SV_POSITION
{
    return float4(position.xy, 0, 1);
}

VS_OUT VS_Untrasformed(float4 position : POSITION)
{
    VS_OUT output = (VS_OUT) 0;
    output.Position = position;
    output.Color.r = (position.x + 1) / 2.0;
    output.Color.g = (1 - position.y) / 2.0;
    
    return output;
}