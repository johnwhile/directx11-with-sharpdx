﻿
// Globals for texture sampling
Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);

static const float smoothEdge0 = 0.4;
static const float smoothEdge1 = 0.8;


//per font
cbuffer FontGlobals : register(b0)
{
    //[xoffset, yoffset, width, height]
    float2 viewporsize;
    float2 texturesize;
};
//per strings
cbuffer StringGlobals : register(b1)
{
    //origin point of the string, in viewport space coordinates
    float2 origin;
    float scale;
    int color;
};

//convert a pixel coordinate into view space
//viewport in format: [x,y,width,height]
float2 SceenToViewport(float2 screen, float2 viewporsize)
{
    /*
    float2 scale = 2.0 * screen.xy / viewport.zw;
    scale.x = scale.x - 1;
    scale.y = 1 - scale.y;
    return scale;
    */
    return float2((2.0 * screen.x / viewporsize.x) - 1.0, 1.0 - (2.0 * screen.y / viewporsize.y));
}

float4 Unpack32bitColor(int packed)
{
    float4 color = (float4) 0;  
    color.r = packed & 0xF;packed >>= 8;
    color.g = packed & 0xF;packed >>= 8;
    color.b = packed & 0xF;packed >>= 8;
    color.a = packed & 0xF;
    return color;
}

//each point rappresent a sprite quad
struct VSinput
{
    //topleft corner position relative to string's origin, in pixel relative to viewport
    min16int2 pxoffset : POSITION;  
    //topleft coordinate in the texture atlas ( [0,1] uv coordinate)
    min16int2 texorigin : TEXCOORD0;
    //size of quad in the texture atlas, used also to calculate size of quad in view space ( [0,1] uv coordinate)
    min16int2 texsize : TEXCOORD1;
};

//you can't attach a semantic to a type, only a variable, so it's necessary build a utility struct
struct GSinput
{
    float2 TopLeft : POSITION0;
    float2 BottomRight : POSITION1;
    float2 texorigin : TEXCOORD0;
    float2 texsize : TEXCOORD1;
    float4 color : COLOR;
};

struct PSinput
{
    float4 Position : SV_Position;
    float2 TexCoord : TEXCOORD0;
    float4 Color : COLOR;
};


GSinput VSMain(VSinput input)
{
    GSinput output = (GSinput) 0;
    
    float2 pos = input.pxoffset * scale + origin;
    float2 size = input.texsize * scale;

    
    output.TopLeft = SceenToViewport(pos, viewporsize);
    output.BottomRight = SceenToViewport(pos + size, viewporsize);
    

    output.texorigin = float2(input.texorigin) / texturesize;
    output.texsize = float2(input.texsize) / texturesize;
    output.color = Unpack32bitColor(color);
    return output;
}

/* trianglestrip [012][213]
 v0___v1
  |  /|
  | / |
v2|/__|v3
*/
[maxvertexcount(4)]
void GSMain(point GSinput vertices[1], inout TriangleStream<PSinput> outStream)
{
    PSinput v = (PSinput) 0;
    GSinput p = vertices[0];

    v.Position = float4(0, 0, 0, 1);
    v.Color = p.color;
    
    //v0
    v.Position.xy = p.TopLeft;
    v.TexCoord = p.texorigin;
    outStream.Append(v);
    
    //v1
    v.Position.xy = float2(p.BottomRight.x, p.TopLeft.y);
    v.TexCoord = p.texorigin + float2(p.texsize.x, 0);
    outStream.Append(v);
  
    //v2
    v.Position.xy = float2(p.TopLeft.x, p.BottomRight.y);
    v.TexCoord = p.texorigin + float2(0, p.texsize.y);
    outStream.Append(v);
    
    //v3
    v.Position.xy = float2(p.BottomRight.x, p.BottomRight.y);
    v.TexCoord = p.texorigin + float2(p.texsize.x, p.texsize.y);
    outStream.Append(v);
}


float4 PSDebug(PSinput input) : SV_Target
{
    return float4(1, 0, 0, 1);
}

float4 PSMain(PSinput input) : SV_Target
{
    float4 color = Texture.Sample(Sampler, input.TexCoord);
    color.rgb = input.Color.rgb;
    return color;
}

float4 PSDistanceField(PSinput input) : SV_Target
{
    float4 color = Texture.Sample(Sampler, input.TexCoord);
    color.rgb = input.Color.rgb;
    color.a = smoothstep(smoothEdge0, smoothEdge1, color.a);
    return color;
}