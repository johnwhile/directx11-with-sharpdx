//---------------------------------------------------------------------------------------------
// IMPORTANT: When creating a new shader file use "Save As...", "Save with encoding", 
// and then select "Western European (Windows) - Codepage 1252" as the 
// D3DCompiler cannot handle the default encoding of "UTF-8 with signature"
//---------------------------------------------------------------------------------------------
//the math performed is designed to work per-component
/*
float4 v = a*b;
vx = ax * bx;
vy = ay * by;
vz = az * bz;
vw = aw * bw;

float d = dot(a,b);
d = ax*bx + ay*by * ...
*/

//mull remark
/*
mul(vector, matrix) = mul(traspose(matrix),vector)

The matrix must be to the RIGHT of the multiply operator
The vertex or vector must to the LEFT of the operator

*/

// make sure dont use half datatype becouse by default is mapped to fp32 in HLSL
#define Half min16float
#define Half2 min16float2
#define Half3 min16float3
#define Half4 min16float4
#define Half3x3 min16float3x3
#define Half3x4 min16float3x4
/*
    float4 pos = float4(input.position, 1);
    pos = mul(World, pos);
    pos = mul(View, pos);
    pos = mul(Projection, pos);
*/

//by default hlsl use column major
#pragma pack_matrix( column_major )
//#pragma pack_matrix( row_major )


struct VS_mesh_input
{
    float3 position : POSITION;
    float2 texcoord : TEXCOORD;
    float3 normal : NORMAL;
    uint color : COLOR;
};
struct VS_spline_input
{
    float3 position : POSITION;
    uint color : COLOR;
};

struct Ambient
{
    float4x4 View;
    float4x4 Projection;
    float4x4 ViewProjection;
    float3 CameraPosition;
};

struct Light
{
    float3 LightPosition;
    float3 LightDirection;
    float4 LightColor;
};

struct Material
{
    float4 Ambient;
    float4 Diffuse;
    float4 Specular;
    float4 Emissive;
    float4x4 UVTransform;
    float SpecularPower;
    int HasTexture;
};

struct Transform
{
    // World matrix to calculate lighting in world space
    float4x4 World;
    // Inverse of world, used for
    // bringing normals into world space, especially
    // necessary where non-uniform scaling has been applied
    float4x4 WorldInverse;
    float4x4 WorldViewProjection;
};


// compact color
float4 Unpack32bitColor(uint packed)
{
    float4 color = (float4) 0;
    color.r = packed & 0xFF;
    packed >>= 8;
    color.g = packed & 0xFF;
    packed >>= 8;
    color.b = packed & 0xFF;
    packed >>= 8;
    color.a = packed & 0xFF;
    color /= 255.0;
    return color;
}
// common and efficient normal approximation
float3 Unpack32bitNormal(uint packed)
{
    float3 normal = (float3) 0;   
    normal.x = clamp(((packed & 0x7FFF) * 2.0 / 32767.0) - 1, -1, 1);
    packed >>= 15;
    normal.y = clamp(((packed & 0x7FFF) * 2.0 / 32767.0) - 1, -1, 1);
    packed >>= 15;
    normal.z = sqrt(clamp(1 - normal.x * normal.x + normal.y * normal.y, 0, 1));
    if ((packed & 1) != 0) normal.z *= -1;
    return normal;
}



float3 DecodeUnitVector16(min16uint encode)
{
    int n = encode & 0x1FFF;
    int i = (sqrt(1 + 8 * n) - 1) / 2;
    int j = n - (i + 1) * i / 2;
    
    float phi = i * 1.5707963267 / 126;
    float theta = i > 0 ? j * 1.5707963267 / i : 0;

    float3 normal = float3(
        cos(theta) * sin(phi),
        cos(phi),
        sin(theta) * sin(phi));
    
    if ((encode & 0x8000) != 0) normal.x *= -1;
    if ((encode & 0x4000) != 0) normal.y *= -1;
    if ((encode & 0x2000) != 0) normal.z *= -1;
    
    return normal;
}



//convert a pixel coordinate into view space
//viewport in format: [x,y,width,height]
float2 SceenToViewport(float2 screen, float2 viewporsize)
{
    screen /= viewporsize;
    screen *= float2(2, -2);
    screen -= float2(1, -1);
    return screen;
}

//elegant version
float4 PixelToClipSpace2(float4 rect, float2 size)
{
    rect /= size.xyxy;
    rect *= float4(2, -2, 2, 2);
    rect -= float4(1, -1, 0, 0);
    return rect;
}
//basic version
float4 PixelToClipSpace(float4 rect, float2 size)
{
    return float4(
    2.0 * rect.x / size.x - 1.0,
    1.0 - 2.0 * rect.y / size.y,
    2.0 * rect.z / size.x,
    2.0 * rect.w / size.y);
}



/* 
vertex shader without buffer, used to fill a texture to the screen
draw a big triangle with vertex [-1,1] [3,1] [-1,-3]

    v0---v1
    |  /
    | /
    v2
topology = TriangleList
Draw(3)
*/

//output values must match with pixelshader's inputs
void VS_screenquad(in uint VertID : SV_VertexID, out float4 Pos : SV_Position, out float2 Tex : TEXCOORD0)
{
    // Texture coordinates range [0, 2], but only [0, 1] appears on screen.
    Tex = float2((VertID << 1) & 2, VertID & 2);
    Pos = float4(lerp(float2(-1, 1), float2(1, -1), Tex), 0, 1);
    //Pos = float4(Tex * float2(2, -2) + float2(-1, 1), 0, 1);
}

float4 Lambert(float4 diffuse, float3 normal, float3 toLight)
{
    // Calculate diffuse color (Lambert's Cosine Law - dot 
    // product of light and normal). Saturate to clamp the 
    // value within 0 to 1.
    diffuse.rbg  = diffuse.rbg * saturate(dot(normal, toLight));
    return diffuse;
}