﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Common;
using Common.Maths;
using Common.Gui;

using Engine;
using Engine.Direct2D;

namespace Direct2d
{
    class AppTest : DesktopApplication
    {
        D2WindowTarget target;
        Rectangle4i rect;
        Control control;
        GuiButton guibutton;

        public AppTest(Control control) : base()
        {
            this.control = control;
            target = ToDispose(new D2WindowTarget(control));

            control.ClientSizeChanged += Control_ClientSizeChanged;

            rect = new Rectangle4i(100, 100, 400, 300);

            guibutton = new GuiButton(null);
            var guicontrol = guibutton as GuiControl;

        }

        private void Control_ClientSizeChanged(object sender, EventArgs e)
        {
            if (sender is Control ctrl)
            {
                target.Resize(ctrl.ClientSize);
            }
            
        }

        protected override void EachFrame()
        {
            control.Text = $"fps {GameTime.PerformanceInfo.FPS} consuming {GameTime.PerformanceInfo.DrawNanoSeconds}";

            if (target.BeginDraw())
            {
                target.Clear(control.BackColor);

                target.FillRectangle(rect, Color4b.Red);
                target.DrawRectangle(rect, Color4b.Green, 2);
                target.EndDraw();
            }


        }
    }


    internal static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (var form = new Form1())
            {
                form.ClientSize = new Size(800, 600);
                form.BackColor = Color.CornflowerBlue;
                form.Show();

                using (var app = new AppTest(form))
                {
                    app.RunLoop();
                }
            }
        }
    }
}
