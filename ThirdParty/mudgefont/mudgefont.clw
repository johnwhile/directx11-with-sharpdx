; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CAddCharDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "mudgefont.h"
LastPage=0

ClassCount=3
Class1=CAddCharDlg
Class2=CMudgeFontApp
Class3=CMudgeFontDlg

ResourceCount=2
Resource1=IDD_ADDCHAR
Resource2=IDD_MUDGEFONT

[CLS:CAddCharDlg]
Type=0
BaseClass=CDialog
HeaderFile=addchardlg.h
ImplementationFile=addchardlg.cpp
LastObject=CAddCharDlg
Filter=D
VirtualFilter=dWC

[CLS:CMudgeFontApp]
Type=0
BaseClass=CWinApp
HeaderFile=mudgefont.h
ImplementationFile=mudgefont.cpp

[CLS:CMudgeFontDlg]
Type=0
BaseClass=CDialog
HeaderFile=mudgefontdlg.h
ImplementationFile=mudgefontdlg.cpp
LastObject=CMudgeFontDlg
Filter=D
VirtualFilter=dWC

[DLG:IDD_ADDCHAR]
Type=1
Class=CAddCharDlg
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_EDIT2,edit,1350631552

[DLG:IDD_MudgeFont]
Type=1
Class=CMudgeFontDlg

[DLG:IDD_MUDGEFONT]
Type=1
Class=CMudgeFontDlg
ControlCount=52
Control1=IDC_STATIC,button,1342177287
Control2=IDC_UP,scrollbar,1342177281
Control3=IDC_VIEW,static,1342308865
Control4=IDC_RIGHT,scrollbar,1342177280
Control5=IDC_STATIC,static,1342308864
Control6=IDC_ZOOM,msctls_trackbar32,1342242821
Control7=IDC_ZOOM_TEXT,static,1342308865
Control8=IDC_STATIC,button,1342177287
Control9=IDC_W64,button,1342308361
Control10=IDC_W128,button,1342177289
Control11=IDC_W256,button,1342177289
Control12=IDC_W512,button,1342177289
Control13=IDC_STATIC,button,1342177287
Control14=IDC_H64,button,1342308361
Control15=IDC_H128,button,1342177289
Control16=IDC_H256,button,1342177289
Control17=IDC_H512,button,1342177289
Control18=IDC_STATIC,button,1342177287
Control19=IDC_FONT,combobox,1344340227
Control20=IDC_STATIC,static,1342308864
Control21=IDC_STATIC,static,1342308864
Control22=IDC_QUALITY,combobox,1344340227
Control23=IDC_SIZE_EDIT,edit,1350639616
Control24=IDC_SIZE_SPIN,msctls_updown32,1342177314
Control25=IDC_STATIC,static,1342308864
Control26=IDC_STATIC,static,1342308864
Control27=IDC_WEIGHT,combobox,1344339971
Control28=IDC_ITALIC,button,1342242851
Control29=IDC_BT_EDIT,edit,1350639616
Control30=IDC_BT_SPIN,msctls_updown32,1342177314
Control31=IDC_BL_EDIT,edit,1350639616
Control32=IDC_BL_SPIN,msctls_updown32,1342177314
Control33=IDC_BR_EDIT,edit,1350639616
Control34=IDC_BR_SPIN,msctls_updown32,1342177314
Control35=IDC_BB_EDIT,edit,1350639616
Control36=IDC_BB_SPIN,msctls_updown32,1342177314
Control37=IDC_STATIC,button,1342177287
Control38=IDC_CHAR,combobox,1344339971
Control39=IDC_RECT,button,1342242819
Control40=IDC_STATIC,static,1342308864
Control41=IDC_AA,combobox,1478557699
Control42=IDC_FORCE_MONOSPACED,combobox,1342242819
Control43=IDC_CBT_EDIT,edit,1350639616
Control44=IDC_CBT_SPIN,msctls_updown32,1342177314
Control45=IDC_CBL_EDIT,edit,1350639616
Control46=IDC_CBL_SPIN,msctls_updown32,1342177314
Control47=IDC_CBR_EDIT,edit,1350639616
Control48=IDC_CBR_SPIN,msctls_updown32,1342177314
Control49=IDC_CBB_EDIT,edit,1350639616
Control50=IDC_CBB_SPIN,msctls_updown32,1342177314
Control51=IDC_STATIC,button,1342177287
Control52=IDC_STATIC,button,1342177287

[MNU:IDD_MUDGEFONT]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_EXPORT
Command5=ID_FILE_EXIT
Command6=ID_TOOLS_ADDCHAR
Command7=IDC_ABOUT
Command8=IDC_MUDGEFONT_HOME_PAGE
CommandCount=8

