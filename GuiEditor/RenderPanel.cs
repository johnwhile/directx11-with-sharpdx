﻿using Common.Gui;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuiEditor
{
    public class RenderPanel : Panel
    {
        int zoom;
        Bitmap texture;
        Rectangle destination;

        public ImageAtlasLayout layout;

        public RenderPanel()
        {
            destination = default(Rectangle);
            zoom = 100;

            InitializeComponent();
            
            MouseWheel += RenderPanel_MouseWheel;
        }
        public Bitmap Texture
        {
            get => texture;
            set
            {
                texture = value;
                Zoom = 100;
            }
        }
        public int Zoom
        {
            get => zoom;
            set
            {
                zoom = value;
                Debug.Print(zoom.ToString());
                if (texture != null)
                    destination = new Rectangle(10, 10, (int)(texture.Width * zoom / 100.0), (int)(texture.Height * zoom / 100.0));
                Invalidate();
            }
        }


        public Rectangle Selection { get; set; }

        private void RenderPanel_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0) Zoom += 10;
            if (e.Delta < 0) Zoom -= 10;
            if (Zoom < 10) Zoom = 10;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);
            base.OnPaint(e);
            e.Graphics.Clear(BackColor);
            
            if (texture != null)
            {
                e.Graphics.DrawImage(Texture, destination, 0, 0, texture.Width, texture.Height, GraphicsUnit.Pixel);
            }
            Rectangle rect = Selection;
            rect.X = (int)(rect.X * zoom /100.0) + 10;
            rect.Y = (int)(rect.Y * zoom / 100.0) + 10;
            rect.Width = (int)(rect.Width * zoom / 100.0);
            rect.Height = (int)(rect.Height * zoom / 100.0);

            e.Graphics.DrawRectangle(Pens.Green, rect);
        }




        /// <summary>
        /// Codes in InitializeComponent will not execute at design-time, 
        /// but those codes will be deserialized and will be used to create designer of the form.
        /// </summary>
        private void InitializeComponent() { }
    }
}
