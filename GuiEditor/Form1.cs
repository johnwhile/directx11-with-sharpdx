﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Drawing;


using Common.Gui;
using Common.Maths;

using System.Diagnostics;

namespace GuiEditor
{
    public partial class GuiEditorForm : Form
    {
        char[] whitespace = new char[] { ' ' };

        public GuiEditorForm()
        {
            InitializeComponent();

            ListBox.SelectedIndexChanged += ListBox_SelectedIndexChanged;
        }

        private void ListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Debug.Print("Select " + ListBox.SelectedIndex);

            string text = ListBox.SelectedItem as string;

            var split = text.Split(whitespace, StringSplitOptions.RemoveEmptyEntries);

            byte.TryParse(split[0], out byte typecode);

            RenderPanel.layout.TryGetSource(typecode, out var source, out _, out _);

            RenderPanel.Selection = source;

            RenderPanel.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            RenderPanel.Invalidate();
        }


        private void LoadMenu_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openDialog = new OpenFileDialog())
            {
                //openDialog.InitialDirectory = @"";
                openDialog.Filter = "texture (*.png)|*.png|texture (*.bmp)|*.bmp|texture (*.jpg)|*.jpg";
                openDialog.RestoreDirectory = true;

                if (openDialog.ShowDialog() == DialogResult.OK)
                {
                    string filename = Path.GetFullPath(openDialog.FileName);
                    RenderPanel.Texture = new Bitmap(filename);
                    RenderPanel.Invalidate();

                    filename = Path.ChangeExtension(filename, ".xml");

                    if (File.Exists(filename))
                    {
                        RenderPanel.layout = ImageAtlasLayout.Open(filename);
                        Update(RenderPanel.layout);
                    }

                }
            }
        }

        private void Update(ImageAtlasLayout layout)
        {
            ListBox.Items.Clear();

            foreach (byte typecode in layout.TypeCodes)
            {
                layout.TryGetSource(typecode, out var source, out int index, out string name);
                ListBox.Items.Add($"{typecode} {source} {name}");
            }

            ListBox.Invalidate();
        }


    }
}
