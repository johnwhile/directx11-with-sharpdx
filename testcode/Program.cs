﻿using System;

using Common;
using Common.Maths;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var View = Matrix4x4f.MakeViewLH(new Vector3f(1, 2, 3), new Vector3f(4, 5, 6));

            var iView = View;  
            Matrix4x4f.InvertRotationTraslation(ref iView);
            var t = Vector3f.Zero;
            t.TransformCoordinate(in iView);


            Rectangle4i screen = new Rectangle4i(200, 100);
            Rectangle4i pixels = new Rectangle4i(0, 0, 300, 100);

            //var v = Mathelp.ScreenToClipSpace(pixels, screen.size);
            //var p = Mathelp.ClipSpaceToScreen(v, screen.size);

            var vclip2 = Mathelp.ScreenToClipSpace(10, 10, screen.size);
            var vclip3 = new Vector3f(vclip2.x, vclip2.y, 1);

            ViewportClip clip = new ViewportClip(screen, 0, 10);

            vclip3.TransformCoordinate(clip.ClipSpaceMatrix);
            vclip3.TransformCoordinate(clip.InverseClipSpaceMatrix);


            //var v = new Vector3f(-.8, .8, 0);
            //var s = Mathelp.ClipSpaceToScreen(v.x,v.y, screen.size);
        }
    }
}
